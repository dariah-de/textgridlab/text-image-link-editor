License
===========================
This package is provided to you under the terms and conditions of 
the Common Public License Version 1.0 ("CPL").
A copy of the CPL is provided with this Content and is also available at 
http://www.eclipse.org/legal/cpl-v10.html. 

SWT image component 1.0.0.
===========================
This package includes a useful canvas for image showing.

You can use SWTImageCanvas in the same way as you use canvas.
In addition, you have a lot of funtions to zoom and scroll image.
You are given the opportunity to switch between different sessions.
You can implement your own session.
You can implement your own image loader.

How to use:
============
You have two choice using this package:
1) you can simply include the MIPComponent.jar into your class path.
 or
2) you can import the plugin "edu.uky.mip.component" into your workspace.

API:
====
Under the dirctory of "doc".

Demo:
=====
The ImageSample plugin is a very simple plugin using component from
this package. You can use it to draw a cardinal spline.

The demo is a plugin, you simply import into your Eclipse workspace,
and rebuild and run.

Note:
You need either put the MIPComponent in your plugin library path as 1)
or you need to import the edu.uky.mip.component plugin into your workspace as 2).
Both are simple.

Contact:
========
If you have any question or suggestion about this package, please
let me know.

-Chengdong ( cli4@uky.edu )


===================================================================
<!-- license -->
<TR>
<TD>
<b><FONT SIZE="+3" COLOR="#ff0000">License</FONT><p/>
<FONT SIZE="+2" COLOR="#000000">
This package is provided to you under the terms and conditions of the Common Public License Version 1.0 ("CPL").
A copy of the CPL is provided with this Content and is also available at 
<A HREF="http://www.eclipse.org/legal/cpl-v10.html">http://www.eclipse.org/legal/cpl-v10.html</A>. 
<FONT/></b>
<p/>
</TD>
</TR>
insert this into :
overview-summary.html