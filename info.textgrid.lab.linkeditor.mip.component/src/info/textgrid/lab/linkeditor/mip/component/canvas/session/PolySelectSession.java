package info.textgrid.lab.linkeditor.mip.component.canvas.session;

import info.textgrid.lab.linkeditor.mip.component.canvas.SessionedImageCanvas;
import info.textgrid.lab.linkeditor.model.graphics.TGPolygon;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;

/**
 * Poly Select session. Extends BasicSelectSession
 * <p>
 * This class is for drawing Polygons.
 * <p>
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * @author leuk
 * 
 */
public class PolySelectSession extends BasicSelectSession {

	private Point begin = new Point(0, 0);

	private TGPolygon workPoly = null;
	private List<Line> lines = Collections
			.synchronizedList(new ArrayList<Line>());

	public PolySelectSession() {
		super();
		currentShape = new TGPolygon("");
		moveShape = new TGPolygon("");
	}

	/**
	 * Constructs a session.
	 * 
	 * @param id
	 *            session id.
	 */
	public PolySelectSession(String id) {
		super(id);
		currentShape = new TGPolygon("");
		moveShape = new TGPolygon("");
	}

	@Override
	public void endSession() {
		super.endSession();
		clearWorkingPoly();
	}

	public void clearWorkingPoly() {
		workPoly = null;
		lines.clear();
		sessionCanvas.redraw();
	}

	@Override
	public void mouseDoubleClick(MouseEvent evt) {
		if (evt.getSource() instanceof SessionedImageCanvas) {
			// if no image, we return;
			if (sessionCanvas.getSourceImage() == null)
				return;

			if (evt.button == 1) { // left button only
				// To reset the drawing polygon on loading an image

				if (sessionCanvas.imageReloaded) {
					workPoly = null;
					lines.clear();
					sessionCanvas.imageReloaded = false;
				}

				if (selectShape(evt)) {
					workPoly = null;
					lines.clear();
					// *** this.triggerPaint();
					sessionCanvas.redraw();
				} else if (/* cursor == SWT.CURSOR_CROSS && */workPoly != null) {
					Point[] points = workPoly.getPoints();
					workPoly.reset();
					for (int i = 0; i < (points.length - 1); i++) {
						workPoly.addPoint(points[i]);
					}

					if (workPoly.size() < 2) {
						workPoly = null;
						lines.clear();
						return;
					}

					workPoly.setWritingMode(sessionCanvas
							.getDefaultWritingMode());

					workPoly.setLayerValues(
							sessionCanvas.getCurrentActiveLayerNumber(),
							sessionCanvas.getCurrentActiveLayerName(),
							sessionCanvas.getCurrentActiveLayerRGB());

					sessionCanvas.setSelectedShape(sessionCanvas
							.screenToImage(workPoly));

					workPoly = sessionCanvas.screenToImage(workPoly);

					sessionCanvas.addShapeToArrayListShapes(workPoly);

					drawSelectionFrame(null, workPoly);
					// *** this.triggerPaint();
					sessionCanvas.redraw();

					workPoly = null;
					lines.clear();
				}
			}
		}
	};

	@Override
	public void mouseDown(MouseEvent evt) {
		// start the interaction
		if (evt.getSource() instanceof SessionedImageCanvas) {
			// if no image, we return;
			if (sessionCanvas.getSourceImage() == null)
				return;

			if (evt.button == 1) { // left button only

				// To reset the drawing polygon on loading an image
				if (sessionCanvas.imageReloaded) {
					workPoly = null;
					lines.clear();
					sessionCanvas.imageReloaded = false;
				}

				if (cursorStyle != SWT.CURSOR_CROSS) {
					resizeMode = true;
					// used for computing moving increment.
					lastPoint = new Point(evt.x, evt.y);

					moveShape = sessionCanvas.getSelectedShape();

					currentShape = sessionCanvas.imageToScreen(sessionCanvas
							.getSelectedShape());
				} else {
					// draw a polygon at the current point
					if (workPoly == null)
						workPoly = new TGPolygon(
								this.sessionCanvas.getImageUri());
					begin.x = evt.x;
					begin.y = evt.y;
					workPoly.addPoint(begin);
				}

				if (roiGC != null) {
					roiGC.dispose();
				}
				roiGC = new GC(sessionCanvas);
				roiGC.setForeground(sessionCanvas.getDisplay().getSystemColor(
						SWT.COLOR_CYAN));
				roiGC.setBackground(sessionCanvas.getDisplay().getSystemColor(
						SWT.COLOR_CYAN));
				roiGC.setLineStyle(SWT.LINE_DASH);
				roiGC.setLineWidth(2);
				roiGC.setXORMode(true);
				// *** this.abortTrigger();
				// drawSelectionFrame(null, currentPoly);

				sessionCanvas.redraw();
				mousePressed = true;
			} else if (evt.button == 3) {
				clearWorkingPoly();
			}
		}
	}

	@Override
	public void mouseMove(MouseEvent evt) {
		super.mouseMoveOfBase(evt);

		int x = evt.x;
		int y = evt.y;
		TGShape selectShape = sessionCanvas.getSelectedShape();

		if (mousePressed) {
			// *** abortTrigger();
			if (resizeMode) {
				processMoveOrResize(x, y);
			} else { // draw new polygon
				if (currentShape instanceof TGPolygon) {
					roiGC.drawLine(begin.x, begin.y, x, y);
					sessionCanvas.redraw();
					drawLines();
				}
			}
		} else {
			Point p = sessionCanvas.screenToImage(new Point(x, y));
			for (TGShape s : sessionCanvas.getArrayListShapes()) {
				if (!s.equals(selectShape) || cursorStyle == SWT.CURSOR_CROSS) {
					if (s.contains(p.x, p.y)) {
						currentShape = s;
						sessionCanvas.redraw();
						break;
					}
				}
			}
			setCurrentCursor(p.x, p.y);
		}
	}

	@Override
	public void mouseUp(MouseEvent evt) {
		// super.mouseUp(evt);
		if (evt.button == 1) { // left button only
			if (evt.getSource() instanceof SessionedImageCanvas) {

				if (sessionCanvas.getSourceImage() == null)
					return;

				// to avoid creating a shape on double-click on the frame
				// of the lab (maximize/minimize)
				if (!mousePressed)
					return;

				if (resizeMode) {
					sessionCanvas.redraw();
					sessionCanvas.setResize(true);
				}

				if (workPoly != null && cursorStyle == SWT.CURSOR_CROSS) {
					// System.err.println("WorkPoly: " + workPoly);
					lines.add(new Line(begin.x, begin.y, evt.x, evt.y));
					drawLines();
				} else if (/* (cursor == SWT.CURSOR_SIZEALL && resizeMode) || */resizeMode) {
					handleMouseUpResizeMove(evt);
				}
			}
			mousePressed = false;
			resizeMode = false;
			sessionCanvas.setResize(false);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.keyCode == 100 && cursorStyle == SWT.CURSOR_HAND) { // 100 = d(D)
																	// for
																	// d(elete)
			TGPolygon temp = (TGPolygon) sessionCanvas.getSelectedShape();
			Point p = sessionCanvas.screenToImage(new Point(lastPoint.x,
					lastPoint.y));
			((TGPolygon) sessionCanvas.getSelectedShape()).remPoint(p.x, p.y);
			if (temp.equals(sessionCanvas.getSelectedShape())) {
				sessionCanvas.setResize(true);
				sessionCanvas
						.setSelectedShape(sessionCanvas.getSelectedShape());
				sessionCanvas.setResize(false);
				sessionCanvas.redraw();
			}
		} else if (e.keyCode == 97 && cursorStyle == SWT.CURSOR_HAND) { // 97 =
																		// a(A)
																		// for
																		// a(dd)
			final int dist = 0;
			TGPolygon temp = (TGPolygon) sessionCanvas.getSelectedShape();
			Point p = sessionCanvas.screenToImage(new Point(lastPoint.x,
					lastPoint.y));
			((TGPolygon) sessionCanvas.getSelectedShape()).addPointVertex(p.x
					+ dist, p.y + dist);
			if (temp.equals(sessionCanvas.getSelectedShape())) {
				sessionCanvas.setResize(true);
				sessionCanvas
						.setSelectedShape(sessionCanvas.getSelectedShape());
				sessionCanvas.setResize(false);
				sessionCanvas.redraw();
			}
		}
	}

	private void drawLines() {
		for (Line l : lines) {
			roiGC.drawLine(l.begin.x, l.begin.y, l.end.x, l.end.y);

			roiGC.setLineStyle(SWT.LINE_SOLID);
			roiGC.fillRectangle(l.begin.x - 3, l.begin.y - 3, 2 * 3, 2 * 3);
			roiGC.fillRectangle(l.end.x - 3, l.end.y - 3, 2 * 3, 2 * 3);
			roiGC.setLineStyle(SWT.LINE_DASH);
		}
	}

	class Line {
		public Point begin = null;
		public Point end = null;

		public Line(int beginX, int beginY, int endX, int endY) {
			begin = new Point(beginX, beginY);
			end = new Point(endX, endY);
		}
	}
}