package info.textgrid.lab.linkeditor.mip.component.layer_ui;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.linkeditor.mip.component.layer_ui.messages"; //$NON-NLS-1$
	public static String ChangeTGLayer_UI_Active;
	public static String ChangeTGLayer_UI_ActiveNo;
	public static String ChangeTGLayer_UI_ActiveYes;
	public static String ChangeTGLayer_UI_Assume;
	public static String ChangeTGLayer_UI_ChangeLayerTo;
	public static String ChangeTGLayer_UI_Color;
	public static String ChangeTGLayer_UI_EditTheLayerPreferencesForAllSelectedShapes;
	public static String ChangeTGLayer_UI_Exit;
	public static String ChangeTGLayer_UI_LayerEditorForSelectedShapesOnly;
	public static String ChangeTGLayer_UI_Name;
	public static String ChangeTGLayer_UI_No;
	public static String ChangeTGLayer_UI_NoLayer;
	public static String ChangeTGLayer_UI_NoLayer2;
	public static String ChangeTGLayer_UI_Nr;
	public static String ChangeTGLayer_UI_Nr2;
	public static String ChangeTGLayer_UI_SelectANewLayerFromTheTable;
	public static String ChangeTGLayer_UI_VisabilityNo;
	public static String ChangeTGLayer_UI_VisabilityYes;
	public static String ChangeTGLayer_UI_Visibility;
	public static String EditTGLayer_UI_Active;
	public static String EditTGLayer_UI_ActiveNo;
	public static String EditTGLayer_UI_ActiveYes;
	public static String EditTGLayer_UI_AtFirstSelectTheLastLayer;
	public static String EditTGLayer_UI_Color;
	public static String EditTGLayer_UI_EditTheLayerPreferences;
	public static String EditTGLayer_UI_Exit;
	public static String EditTGLayer_UI_Info;
	public static String EditTGLayer_UI_LayerEditor;
	public static String EditTGLayer_UI_Name;
	public static String EditTGLayer_UI_NewOrAssume;
	public static String EditTGLayer_UI_Nr;
	public static String EditTGLayer_UI_Remove;
	public static String EditTGLayer_UI_Visibility;
	public static String EditTGLayer_UI_VisibilityNo;
	public static String EditTGLayer_UI_VisibilityYes;
	public static String EditTGLayer_UI_YouCanOnlyRemoveTheLastLayer;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
