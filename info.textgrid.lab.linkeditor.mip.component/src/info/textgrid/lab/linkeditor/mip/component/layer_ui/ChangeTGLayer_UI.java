/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.mip.component.layer_ui;

import info.textgrid.lab.linkeditor.controller.LinkEditorController;
import info.textgrid.lab.linkeditor.mip.component.canvas.SessionedImageCanvas;
import info.textgrid.lab.linkeditor.model.graphics.TGLayer;
import info.textgrid.lab.linkeditor.model.graphics.TGLayerManager;

import java.util.Iterator;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.swtdesigner.SWTResourceManager;

public class ChangeTGLayer_UI extends TitleAreaDialog {
	private static final String MESSAGE = Messages.ChangeTGLayer_UI_EditTheLayerPreferencesForAllSelectedShapes;
	private static final String TITLE = Messages.ChangeTGLayer_UI_LayerEditorForSelectedShapesOnly;
	private static final int NEW_ID = IDialogConstants.CLIENT_ID + 1;
	private LinkEditorController controller = LinkEditorController
			.getInstance();
	protected SessionedImageCanvas instance;
	private Table table;
	private TGLayerManager sessionLayerManager;
	private Text text_Name;
	private Label label_Nr;
	private Label label_Color;
	private Button button_visibility;
	private Button button_active;
	private int activeLayerNumber = 0;
	private String activeLayerName = ""; //$NON-NLS-1$
	private RGB activeLayerRGB = new RGB(255, 255, 255);
	private int selectedLayerNumber = 0;

	// private boolean Editmode = false;
	private Text txtChangeLayerTo;
	private Text txtChangedLayerName;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 * @param instance
	 * @param layerManager
	 */
	public ChangeTGLayer_UI(Shell parentShell) {
		super(parentShell);
	}

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 * @wbp.parser.constructor
	 */
	public ChangeTGLayer_UI(Shell parentShell,
			TGLayerManager sessionLayerManager, SessionedImageCanvas instance) {
		super(parentShell);
		this.instance = instance;
		this.sessionLayerManager = sessionLayerManager;
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		setTitle(TITLE);
		setMessage(MESSAGE);
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayout(new GridLayout(1, false));
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		Composite composite_table = new Composite(container, SWT.NONE);
		composite_table.setLayout(new GridLayout(1, false));
		GridData gd_composite_table = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_composite_table.heightHint = 148;
		gd_composite_table.widthHint = 434;
		composite_table.setLayoutData(gd_composite_table);

		table = new Table(composite_table, SWT.BORDER | SWT.FULL_SELECTION);
		table.addMouseTrackListener(new MouseTrackAdapter() {
			@Override
			public void mouseHover(MouseEvent e) {
				table.setFocus();
			}
		});
		table.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseScrolled(MouseEvent e) {
				table.setSelection(table.getItemCount());
			}
		});
		table.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int[] selections = table.getSelectionIndices();
				new Integer(table.getItem(selections[0]).getText());

				selectedLayerNumber = new Integer(table.getItem(selections[0])
						.getText());

				Object[] obj = fillEditorLayerChangedArea(selectedLayerNumber);
				txtChangedLayerName.setForeground((Color) obj[1]);
				txtChangedLayerName.setBackground(PlatformUI.getWorkbench()
						.getDisplay().getSystemColor(SWT.COLOR_WHITE));
				if (!(table.getItem(selections[0]).getText()).equals("0")) { //$NON-NLS-1$
					txtChangedLayerName.setText(Messages.ChangeTGLayer_UI_Nr
							+ table.getItem(selections[0]).getText() + " --- " //$NON-NLS-1$
							+ obj[0]);
				} else {
					Color col = new Color(PlatformUI.getWorkbench()
							.getDisplay(), 255, 0, 0);
					txtChangedLayerName.setForeground(col);
					txtChangedLayerName.setText(Messages.ChangeTGLayer_UI_Nr
							+ table.getItem(selections[0]).getText()
							+ Messages.ChangeTGLayer_UI_NoLayer);
				}
			}
		});
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableColumn tblclmnNr = new TableColumn(table, SWT.CENTER);
		tblclmnNr.setWidth(30);
		tblclmnNr.setText(Messages.ChangeTGLayer_UI_Nr2);

		TableColumn tblclmnName = new TableColumn(table, SWT.LEFT);
		tblclmnName.setWidth(145);
		tblclmnName.setText(Messages.ChangeTGLayer_UI_Name);

		TableColumn tblclmnColor = new TableColumn(table, SWT.CENTER);
		tblclmnColor.setWidth(45);
		tblclmnColor.setText(Messages.ChangeTGLayer_UI_Color);

		TableColumn tblclmnVisibility = new TableColumn(table, SWT.CENTER);
		tblclmnVisibility.setWidth(100);
		tblclmnVisibility.setText(Messages.ChangeTGLayer_UI_Visibility);

		TableColumn tblclmnActive = new TableColumn(table, SWT.CENTER);
		tblclmnActive.setWidth(100);
		tblclmnActive.setText(Messages.ChangeTGLayer_UI_Active);

		// Create all Layer Lines
		createTableLines();

		Composite composite = new Composite(container, SWT.NONE);
		GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, false, false,
				1, 1);
		gd_composite.widthHint = 412;
		composite.setLayoutData(gd_composite);
		composite.setLayout(new GridLayout(1, false));

		Composite composite_ChangeLayer = new Composite(composite, SWT.NONE);
		composite_ChangeLayer.setLayout(new GridLayout(2, false));
		GridData gd_composite_1 = new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1);
		gd_composite_1.widthHint = 425;
		composite_ChangeLayer.setLayoutData(gd_composite_1);

		txtChangeLayerTo = new Text(composite_ChangeLayer, SWT.NONE);
		GridData gd_txtChangeLayerTo = new GridData(SWT.LEFT, SWT.CENTER,
				false, false, 1, 1);
		gd_txtChangeLayerTo.widthHint = 98;
		txtChangeLayerTo.setLayoutData(gd_txtChangeLayerTo);
		txtChangeLayerTo.setText(Messages.ChangeTGLayer_UI_ChangeLayerTo);
		txtChangeLayerTo.setEditable(false);

		txtChangedLayerName = new Text(composite_ChangeLayer, SWT.NONE);
		txtChangedLayerName.setEditable(false);
		GridData gd_txtChangedLayerName = new GridData(SWT.FILL, SWT.CENTER,
				false, false, 1, 1);
		gd_txtChangedLayerName.widthHint = 301;
		txtChangedLayerName.setLayoutData(gd_txtChangedLayerName);
		txtChangedLayerName.setForeground(SWTResourceManager
				.getColor(SWT.COLOR_RED));
		txtChangedLayerName
				.setText(Messages.ChangeTGLayer_UI_SelectANewLayerFromTheTable);

		txtChangedLayerName.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
			}
		});
		return area;
	}

	protected void fillEditorArea(int selectionLine) {
		int labelNr = selectionLine;
		this.label_Nr.setText(new Integer(labelNr).toString());

		for (Iterator iterator = sessionLayerManager.getIterator(); iterator
				.hasNext();) {
			TGLayer layer = (TGLayer) iterator.next();
			if (layer.getNumber() == labelNr) {
				this.text_Name.setText(layer.getName());
				this.button_visibility.setSelection(layer.isVisible());
				this.button_active.setSelection(layer.isActive());
				this.label_Color.setBackground(layer.getLayercolor());
			}
		}
	}

	protected Object[] fillEditorLayerChangedArea(int selectionLine) {
		Object[] obj = new Object[2];
		obj[0] = null;
		obj[1] = null;
		int labelNr = selectionLine;
		for (Iterator iterator = sessionLayerManager.getIterator(); iterator
				.hasNext();) {
			TGLayer layer = (TGLayer) iterator.next();
			if (layer.getNumber() == labelNr) {
				layer.getName();
				obj[0] = layer.getName();
				obj[1] = layer.getLayercolor();
			}
		}
		return obj;
	}

	private void createFirstTableLine4EditMode() {
		TableItem tableItem = new TableItem(table, SWT.NONE);
		tableItem.setForeground(new Color(PlatformUI.getWorkbench()
				.getDisplay(), 255, 0, 0));
		tableItem.setBackground(new Color(PlatformUI.getWorkbench()
				.getDisplay(), 192, 192, 192));
		TGLayer noLayer = new TGLayer(Messages.ChangeTGLayer_UI_NoLayer2, 0, false, false);
		tableItem.setText(0, new Integer(noLayer.getNumber()).toString());
		// tableItem.setBackground(2, noLayer.getLayercolor());
		tableItem.setText(1, noLayer.getName());
		tableItem.setText(3, Messages.ChangeTGLayer_UI_No);
		tableItem.setText(4, Messages.ChangeTGLayer_UI_No);

	}

	private void createTableLines() {
		boolean activeLayerChangedFlag = false;
		String visibility = ""; //$NON-NLS-1$
		String active = ""; //$NON-NLS-1$

		createFirstTableLine4EditMode();

		for (Iterator iterator = sessionLayerManager.getIterator(); iterator
				.hasNext();) {
			TGLayer layer = (TGLayer) iterator.next();
			TableItem tableItem = new TableItem(table, SWT.NONE);

			if (layer.isVisible()) {
				visibility = Messages.ChangeTGLayer_UI_VisabilityYes;
				tableItem
						.setForeground(
								3,
								SWTResourceManager.getColor(SWT.COLOR_GREEN
										| SWT.BOLD));
			} else {
				visibility = Messages.ChangeTGLayer_UI_VisabilityNo;
				tableItem.setForeground(3,
						SWTResourceManager.getColor(SWT.COLOR_RED | SWT.BOLD));
			}
			if (layer.isActive()) {
				active = Messages.ChangeTGLayer_UI_ActiveYes;
				tableItem
						.setForeground(
								4,
								SWTResourceManager.getColor(SWT.COLOR_GREEN
										| SWT.BOLD));
				this.activeLayerNumber = layer.getNumber();
				this.activeLayerName = layer.getName();
				this.activeLayerRGB = layer.getLayercolor().getRGB();
				activeLayerChangedFlag = true;
			} else {
				active = Messages.ChangeTGLayer_UI_ActiveNo;
				tableItem.setForeground(4,
						SWTResourceManager.getColor(SWT.COLOR_RED | SWT.BOLD));
			}

			tableItem.setText(0, new Integer(layer.getNumber()).toString());
			tableItem.setBackground(2, layer.getLayercolor());
			tableItem.setText(1, layer.getName());
			tableItem.setText(3, visibility);
			tableItem.setText(4, active);
		}

		if (!activeLayerChangedFlag) { // There is NO active Layer (selected)!!!
			activeLayerNumber = 0;
			controller.setLayerLabel(false, "0"); //$NON-NLS-1$
		}

		layerInfoToImagePanel();

	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {

		Button button_New = createButton(parent, NEW_ID, "New button", false); //$NON-NLS-1$
		button_New.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (table.getSelectionIndex() != -1) { // line selected
					instance.updateLayerValues4SelectedSapes(selectedLayerNumber);
				}
			}

		});
		button_New.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
			}
		});

		button_New.setText(Messages.ChangeTGLayer_UI_Assume);

		Button button_Exit = createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
		button_Exit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				controller.addLayerManagerToMap(sessionLayerManager);
			}
		});

		button_Exit.setText(Messages.ChangeTGLayer_UI_Exit);
	}

	/**
	 * update Table Input
	 */
	protected void updateTable() {
		try {
			table.removeAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// Activator.handleError(e);
		}
		createTableLines();

		label_Nr.setText(new Integer(sessionLayerManager.getSize() + 1)
				.toString());
		text_Name.setText(""); //$NON-NLS-1$
		label_Color.setBackground(null);
		button_visibility.setSelection(true);
		button_active.setSelection(false);

		layerInfoToImagePanel();

		// Info about current active Layer to SessionImageCanvas
		setLayerInfoToSessionedImageCanvas();

		if (table.getSelectionIndex() == -1) { // No line is selected
			fillEditorArea(sessionLayerManager.getNextNumber());
		}

	}

	/**
	 * write active layer info to SessionImageCanvas
	 */
	private void setLayerInfoToSessionedImageCanvas() {
		instance.setCurrentActiveLayerNumber(activeLayerNumber);
		instance.setCurrentActiveLayerName(activeLayerName);
		instance.setCurrentActiveLayerRGB(activeLayerRGB);
		instance.updateLayerInfoOfShapes();
	}

	/**
	 * write active layer info to ImagePanel
	 */
	public void layerInfoToImagePanel() {
		if (activeLayerNumber != 0) {
			controller.setLayerLabel(true,
					new Integer(activeLayerNumber).toString());
		} else {
			controller.setLayerLabel(false,
					new Integer(activeLayerNumber).toString());
		}
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(450, 382);
	}
}