/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.mip.component.memento;

import info.textgrid.lab.linkeditor.controller.LinkEditorController;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class Memento {

	private List<TGShape> allShapes = null;
	private List<TGShape> allShapesClone = Collections
			.synchronizedList(new ArrayList<TGShape>());

	public Memento(List<TGShape> allShapes) {
		this.allShapes = allShapes;
		for (TGShape s : allShapes) {
			this.allShapesClone.add(s.clone());
		}
	}

	public void restore() {
		allShapes.clear();

		for (TGShape s : allShapesClone) {
			TGShape cloneShape = s.clone();
			allShapes.add(cloneShape);
			LinkEditorController.getInstance().updateLink(cloneShape);
		}
	}
}
