/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.loader;

/**
 * An abstract class for all ImageLoaders. It implemented the IImageLoader
 * interface.
 * <p>
 * Also include several common variables and functions.
 * 
 * @author Chengdong Li
 * @see IImageLoader
 * 
 */
public abstract class AbstractLoader implements IImageLoader {

	/** Supported image formats for read */
	public String[] READ_FORMATS = {};

	/** Supported image formats for write */
	public String[] WRITE_FORMATS = {};

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uky.mip.loader.IImageLoader#createLoaderFor(java.lang.String)
	 */
	@Override
	public IImageLoader createLoaderFor(String format, int mode)
			throws UnknownImageFormatException {
		if (mode == IImageLoader.READ) {
			if (READ_FORMATS != null) {
				for (int i = 0; i < READ_FORMATS.length; i++) {
					if (this.READ_FORMATS[i].equalsIgnoreCase(format))
						return this;
				}
			}
		} else if (mode == IImageLoader.WRITE) {
			if (WRITE_FORMATS != null) {
				for (int i = 0; i < WRITE_FORMATS.length; i++) {
					if (this.WRITE_FORMATS[i].equalsIgnoreCase(format))
						return this;
				}
			}
		}
		throw new UnknownImageFormatException("do not support " + format);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uky.mip.loader.IImageLoader#getReadFormats()
	 */
	@Override
	public String[] getReadFormats() {
		return READ_FORMATS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uky.mip.loader.IImageLoader#getWriteFormats()
	 */
	@Override
	public String[] getWriteFormats() {
		return WRITE_FORMATS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uky.mip.loader.IImageLoader#supportRead()
	 */
	@Override
	public boolean canRead(String format) {
		if (READ_FORMATS == null)
			return false;
		for (int i = 0; i < READ_FORMATS.length; i++) {
			if (READ_FORMATS[i].equalsIgnoreCase(format))
				return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uky.mip.loader.IImageLoader#supportWrite()
	 */
	@Override
	public boolean canWrite(String format) {
		if (WRITE_FORMATS == null)
			return false;
		for (int i = 0; i < WRITE_FORMATS.length; i++) {
			if (WRITE_FORMATS[i].equalsIgnoreCase(format))
				return true;
		}
		return false;
	}

}
