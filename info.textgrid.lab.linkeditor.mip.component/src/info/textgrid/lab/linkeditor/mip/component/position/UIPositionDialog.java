/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.mip.component.position;

import info.textgrid.lab.linkeditor.controller.LinkEditorController;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Text;

import com.swtdesigner.SWTResourceManager;

public class UIPositionDialog extends TitleAreaDialog {

	private Slider slider1;
	private Text text;
	private double angle = 0;
	private int lastDockingLineAngle;

	public double getAngle() {
		return angle;
	}

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public UIPositionDialog(Shell parentShell, double angle) {
		super(parentShell);
		this.angle = angle;
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		setTitle(Messages.UIPositionDialog_SetRotationAngle);
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayout(new GridLayout(1, false));
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		Composite composite = new Composite(container, SWT.NONE);
		GridData gd_composite = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_composite.heightHint = 138;
		gd_composite.widthHint = 432;
		composite.setLayoutData(gd_composite);

		lastDockingLineAngle = (int) StrictMath.floor(LinkEditorController
				.getInstance().getLastSelectedDockingLineAngle());

		slider1 = new Slider(composite, SWT.BORDER | SWT.READ_ONLY);
		slider1.setThumb(0);
		slider1.setMaximum(730);
		slider1.setSelection(calculateSliderSelection());
		// slider1.setPageIncrement(1);
		slider1.setIncrement(1);
		// spinner1.setMaximum(180);
		slider1.setBounds(8, 47, 414, 21);

		slider1.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				String t = getSliderText();
				if (t.contains(".")) //$NON-NLS-1$
					t = t.substring(0, t.indexOf(".")); //$NON-NLS-1$
				text.setText(t + "\u00b0"); //$NON-NLS-1$
			}
		});

		Label lblAngle = new Label(composite, SWT.NONE);
		lblAngle.setBounds(8, 10, 110, 20);
		lblAngle.setText(Messages.UIPositionDialog_AngleOfRotation);

		text = new Text(composite, SWT.CENTER);
		text.setBackground(SWTResourceManager
				.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		text.setFont(SWTResourceManager.getFont("Tahoma", 8, SWT.BOLD)); //$NON-NLS-1$
		text.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLUE));
		text.setEditable(false);
		text.setBounds(120, 10, 42, 19);
		text.setText((int) this.angle + "\u00b0"); //$NON-NLS-1$

		Composite composite_1 = new Composite(composite, SWT.NONE);
		composite_1.setBounds(8, 74, 414, 19);

		CLabel label_Minus_180 = new CLabel(composite_1, SWT.SHADOW_IN);
		label_Minus_180.setBounds(98, 0, 28, 19);
		label_Minus_180.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				slider1.setSelection(180);
				text.setText(-180 + "\u00b0"); //$NON-NLS-1$
			}
		});
		label_Minus_180.setText("-180"); //$NON-NLS-1$

		final CLabel label_0 = new CLabel(composite_1, SWT.SHADOW_IN
				| SWT.CENTER);
		label_0.setBounds(190, 0, 34, 19);
		label_0.setFont(SWTResourceManager.getFont("Tahoma", 8, SWT.BOLD)); //$NON-NLS-1$
		label_0.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				slider1.setSelection(0 + 360);
				text.setText(0 + "\u00B0;"/*�-Zeichen*/);
				label_0.setForeground(SWTResourceManager
						.getColor(SWT.COLOR_BLUE));
			}
		});
		label_0.setText("0");

		CLabel label_180 = new CLabel(composite_1, SWT.SHADOW_IN);
		label_180.setBounds(279, 0, 32, 19);
		label_180.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				slider1.setSelection(180 + 360);
				text.setText(180 + "\u00b0"); //$NON-NLS-1$
			}
		});
		label_180.setText("+180"); //$NON-NLS-1$

		CLabel label_360 = new CLabel(composite_1, SWT.SHADOW_IN);
		label_360.setBounds(382, 0, 32, 19);
		label_360.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				slider1.setSelection(360 + 360);
				text.setText(+360 + "\u00b0"); //$NON-NLS-1$
			}
		});
		label_360.setText("+360"); //$NON-NLS-1$

		CLabel label_90 = new CLabel(composite_1, SWT.SHADOW_IN);
		label_90.setBounds(240, 0, 32, 19);
		label_90.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				slider1.setSelection(360 + 90);
				text.setText(+90 + "\u00b0"); //$NON-NLS-1$
			}
		});
		label_90.setText("+90"); //$NON-NLS-1$

		CLabel label_270 = new CLabel(composite_1, SWT.SHADOW_IN);
		label_270.setBounds(330, 0, 32, 19);
		label_270.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				slider1.setSelection(360 + 270);
				text.setText(270 + "\u00b0"); //$NON-NLS-1$
			}
		});
		label_270.setText("+270"); //$NON-NLS-1$

		CLabel label_Minus_90 = new CLabel(composite_1, SWT.SHADOW_IN);
		label_Minus_90.setBounds(149, 0, 32, 19);
		label_Minus_90.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				slider1.setSelection(0 + 270);
				text.setText(-90 + "\u00b0"); //$NON-NLS-1$
			}
		});
		label_Minus_90.setText("-90"); //$NON-NLS-1$

		CLabel label_Minus_270 = new CLabel(composite_1, SWT.SHADOW_IN);
		label_Minus_270.setBounds(54, 0, 32, 19);
		label_Minus_270.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				slider1.setSelection(0 + 90);
				text.setText(-270 + "\u00b0"); //$NON-NLS-1$
			}
		});
		label_Minus_270.setText("-270"); //$NON-NLS-1$

		CLabel label_Minus_360 = new CLabel(composite_1, SWT.SHADOW_IN);
		label_Minus_360.setBounds(0, 0, 32, 19);
		label_Minus_360.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				slider1.setSelection(0);
				text.setText(-360 + "\u00b0"); //$NON-NLS-1$
			}
		});
		label_Minus_360.setText("-360"); //$NON-NLS-1$
		return area;
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Button buttonOK = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		buttonOK.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				getSliderText();
				// System.out.println("anglexxx: "+spinner1.getSelection());
			}
		});
		buttonOK.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});

		// Button buttonCancel = createButton(parent,
		// IDialogConstants.CANCEL_ID,
		// IDialogConstants.CANCEL_LABEL, false);
		// buttonCancel.addMouseListener(new MouseAdapter() {
		// @Override
		// public void mouseUp(MouseEvent e) {
		// // System.out.println("angleyyy: "+spinner1.getSelection());
		// }
		// });
	}

	protected String getDefaultAngleAsString() {
		return String.valueOf(lastDockingLineAngle);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(450, 300);
	}

	protected String getSliderText() {

		if (slider1.getSelection() == 360) {
			this.angle = Double.parseDouble("0"); //$NON-NLS-1$
			return 0 + "\u00b0"; //$NON-NLS-1$
		} else if (slider1.getSelection() == 0) {
			int x = -360;
			double y = x;
			this.angle = y;
			return y + "\u00b0"; //$NON-NLS-1$
		} else if (slider1.getSelection() <= 359) {
			int x = -360 + slider1.getSelection();
			double y = x;
			this.angle = y;
			return y + "\u00b0"; //$NON-NLS-1$
		} else if (slider1.getSelection() >= 361) {
			int x = slider1.getSelection() - 360;
			double y = x;
			this.angle = y;
			return y + "\u00b0"; //$NON-NLS-1$
		} else if (slider1.getSelection() == 720) {
			int x = 360;
			double y = x;
			this.angle = y;
			return y + "\u00b0"; //$NON-NLS-1$
		}

		return "error"; //$NON-NLS-1$
	}

	protected int calculateSliderSelection() {

		return (int) this.angle + 360;
	}
}
