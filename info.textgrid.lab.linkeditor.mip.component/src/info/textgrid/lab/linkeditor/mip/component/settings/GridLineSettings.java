/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.mip.component.settings;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;

/**
 * Grid Line Settings.
 * <p>
 * This class is for handling GridLine(DockingLine) Settings.
 * <p>
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * @author leuk
 * 
 */
public class GridLineSettings {

	private static final RGB default_unSelectedLineRGB = new RGB(85, 0, 238); // Blau
	private static final RGB default_selectedLineRGB = new RGB(204, 0, 34); // Rot
	private static final int default_gridLineWidth = 1;
	private static final int default_gridLineSpacing = 40;
	private static final int default_gridLineStyle = SWT.LINE_DOT;

	private RGB unSelectedLineRGB;
	private RGB selectedLineRGB;
	private int gridLineWidth;
	private int gridLineSpacing;
	private int gridLineStyle;

	private ArrayList<IGridLineSettingsListener> listeners = null;

	public enum Event {
		SETTINGS_RESET, // GridLineSettings are changed
	}

	public GridLineSettings() {
		// TODO Auto-generated constructor stub
		initialize();
	}

	public void initialize() {
		this.unSelectedLineRGB = default_unSelectedLineRGB;
		this.selectedLineRGB = default_selectedLineRGB;
		this.gridLineWidth = default_gridLineWidth;
		this.gridLineSpacing = default_gridLineSpacing;
		this.gridLineStyle = default_gridLineStyle;

		listeners = new ArrayList<IGridLineSettingsListener>();
	}

	public void reset() {
		initialize();
		// System.out.println("fire----->");
		notifyListeners(Event.SETTINGS_RESET);
	}

	public Object[] getDefaultValues() {
		return new Object[] { default_unSelectedLineRGB,
				default_selectedLineRGB, default_gridLineWidth,
				default_gridLineSpacing, default_gridLineStyle };
	}

	public RGB getUnSelectedLineRGB() {
		return unSelectedLineRGB;
	}

	public void setUnSelectedLineRGB(RGB unSelectedLineRGB) {
		this.unSelectedLineRGB = unSelectedLineRGB;
	}

	public RGB getSelectedLineRGB() {
		return selectedLineRGB;
	}

	public void setSelectedLineRGB(RGB selectedLineRGB) {
		this.selectedLineRGB = selectedLineRGB;
	}

	public int getGridLineWidth() {
		return gridLineWidth;
	}

	public void setGridLineWidth(int gridLineWidth) {
		this.gridLineWidth = gridLineWidth;
	}

	public int getGridLineSpacing() {
		return gridLineSpacing;
	}

	public void setGridLineSpacing(int gridLineSpacing) {
		this.gridLineSpacing = gridLineSpacing;
	}

	public int getGridLineStyle() {
		return gridLineStyle;
	}

	public void setGridLineStyle(int gridLineStyle) {
		this.gridLineStyle = gridLineStyle;
	}

	public void addListener(IGridLineSettingsListener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(IGridLineSettingsListener listener) {
		return listeners.remove(listener);
	}

	public void notifyListeners(Event event) {
		for (IGridLineSettingsListener l : listeners) {
			l.gridLineSettingsChanged(event);
		}
	}

}
