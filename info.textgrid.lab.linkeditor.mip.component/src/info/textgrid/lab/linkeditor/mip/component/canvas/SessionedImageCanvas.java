/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.canvas;

import info.textgrid.lab.linkeditor.controller.LinkEditorController;
import info.textgrid.lab.linkeditor.mip.component.ComponentPlugin;
import info.textgrid.lab.linkeditor.mip.component.canvas.session.BasicMoveSession;
import info.textgrid.lab.linkeditor.mip.component.canvas.session.DefaultSession;
import info.textgrid.lab.linkeditor.mip.component.canvas.session.IImageSession;
import info.textgrid.lab.linkeditor.mip.component.canvas.session.PolySelectSession;
import info.textgrid.lab.linkeditor.mip.component.layer_ui.ChangeTGLayer_UI;
import info.textgrid.lab.linkeditor.mip.component.layer_ui.EditTGLayer_UI;
import info.textgrid.lab.linkeditor.mip.component.memento.MementoManager;
import info.textgrid.lab.linkeditor.mip.component.position.UIPositionDialog;
import info.textgrid.lab.linkeditor.mip.component.settings.GridLineSettings;
import info.textgrid.lab.linkeditor.mip.component.settings.GridLineSettingsDialog;
import info.textgrid.lab.linkeditor.mip.component.views.ThumbViewMediator;
import info.textgrid.lab.linkeditor.model.graphics.TGLayer;
import info.textgrid.lab.linkeditor.model.graphics.TGLayerManager;
import info.textgrid.lab.linkeditor.model.graphics.TGLine;
import info.textgrid.lab.linkeditor.model.graphics.TGPolygon;
import info.textgrid.lab.linkeditor.model.graphics.TGRectangle;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;
import info.textgrid.lab.linkeditor.model.graphics.TGShape.TYPE;
import info.textgrid.lab.linkeditor.model.graphics.TGShape.WRITING_MODE;

import java.awt.geom.AffineTransform;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.Transform;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PlatformUI;

/**
 * <p>
 * A scrollable and zoomable image canvas enhanced with multi-sessions.
 * <p>
 * You can extend this class and add new sessions.
 * <p>
 * 
 * @author Chengdong Li
 */
public class SessionedImageCanvas extends AffineImageCanvas {
	protected SessionedImageCanvas instance;

	public static String ID = "info.textgrid.lab.linkeditor.mip.component.canvas.SessionedImageCanvas"; //$NON-NLS-1$
	/* session saved, for spaceMove mode only. */
	private IImageSession savedSession;
	private IImageSession session;
	protected TGShape selectedShape = null;
	private List<TGShape> all_Shapes = Collections
			.synchronizedList(new ArrayList<TGShape>());
	private boolean showHelpLines = true;

	public boolean imageReloaded = false;

	private MessageBox msg = new MessageBox(getShell(), SWT.YES | SWT.NO
			| SWT.ICON_QUESTION);

	private GridLineSettings glSettings;

	private boolean showWritingMode = false;

	private boolean showRotation = true;

	private String imageUri = ""; //$NON-NLS-1$

	protected MementoManager mementoManager = null;

	private WRITING_MODE defaultWritingMode = WRITING_MODE.NONE;

	private Object[] layerInfo = new Object[] { 0, "-", new RGB(255, 255, 255) }; //$NON-NLS-1$

	private List<TGShape> selected_shapes = Collections
			.synchronizedList(new ArrayList<TGShape>());

	private boolean allShapesSelected = false;

	/**
	 * Get all created shapes
	 * 
	 * @return arrayList of {@link TGShape}
	 */
	public synchronized List<TGShape> getArrayListShapes() {
		return all_Shapes;
	}

	protected SessionedImageCanvas getInstance() {
		return instance;
	}

	public synchronized void setArrayListShapes(ArrayList<TGShape> all) {
		all_Shapes = all;
		checkUnlinkedShapes();
	}

	public synchronized void addShapeToArrayListShapes(TGShape s) {
		for (TGShape shape : all_Shapes) {
			if (shape.equals(s)) {
				return;
			}
		}

		all_Shapes.add(s);
		checkUnlinkedShapes();
		saveCurrentState();
	}

	public synchronized void addShapeToArrayListShapes(TGShape s, int index) {
		all_Shapes.add(index, s);
		checkUnlinkedShapes();
		saveCurrentState();
	}

	public synchronized boolean editShapeinArrayListShapes(TGShape oldShape,
			TGShape newShape) {
		for (int i = 0; i < all_Shapes.size(); i++) {
			if (all_Shapes.get(i).equals(oldShape)) {
				all_Shapes.set(i, newShape);
				checkUnlinkedShapes();
				saveCurrentState();
				return true;
			}
		}
		return false;
	}

	public synchronized void removeAllShapes(boolean removeTextSegments) {
		if (removeTextSegments)
			LinkEditorController.getInstance().removeAllLinks(this.imageUri);
		all_Shapes.clear();
		redraw();
		checkUnlinkedShapes();
		resetSelectedShape();
		saveCurrentState();
	}

	public synchronized void removeShapefromArrayListShapes(int index,
			boolean del) {
		this.redraw();
		if (del) {
			TGShape delShape = all_Shapes.get(index);
			LinkEditorController.getInstance().removeLinkByShape(delShape);
		}
		all_Shapes.remove(index);
		checkUnlinkedShapes();
		saveCurrentState();
		this.redraw();
	}

	public synchronized void removeShape(TGShape shape) {
		removeShapefromArrayListShapes(getIndexfromArrayListShapes(shape),
				false);
		int size = getArrayListShapes().size();
		TGShape currentShape = null;
		if (size != 0) {
			currentShape = getArrayListShapes().get(size - 1);
		} else {
			currentShape = null;
		}
		setSelectedShape(currentShape);
		saveCurrentState();
		redraw();
	}

	public int getIndexfromArrayListShapes(TGShape s) {
		return (all_Shapes.indexOf(s));
	}

	private boolean resize = false;

	public void setResize(boolean b) {
		resize = b;
	}

	private void checkUnlinkedShapes() {
		if (session != null)
			session.sendUnlinkedShapeInfo();
	}

	@Override
	public boolean setFocus() {
		super.setFocus();
		checkUnlinkedShapes();
		return true;
	}

	/**
	 * Get the current selected shape in Image domain.
	 * 
	 * @return selected shape
	 */
	public TGShape getSelectedShape() {
		return selectedShape;
	}

	/**
	 * Set current selected shape in Image domain.
	 * 
	 * @param s
	 *            {@link TGShape}
	 */
	public synchronized void setSelectedShape(TGShape s) {

		if (isCtrlPressed()) {
			if (selected_shapes.isEmpty()) {
				selected_shapes.add(selectedShape);
				LinkEditorController.getInstance().addSelectedShape(
						selectedShape);
			}

			selectedShape = s;
			selected_shapes.add(selectedShape);
			LinkEditorController.getInstance().setSelectedShape(selectedShape);
			LinkEditorController.getInstance().addSelectedShape(selectedShape);
			return;
		} else {
			if (!selected_shapes.isEmpty()) {
				selected_shapes.clear();
				LinkEditorController.getInstance().clearSelectedShapes();
			}
		}

		if (s instanceof TGRectangle) {
			TGRectangle r = (TGRectangle) s;
			TGRectangle help = new TGRectangle(this.imageUri);
			if (r.equals(help)) {
				selectedShape = r;
				notifySelectionChanged();
				return;
			}
		} else if (s instanceof TGPolygon) {
			TGPolygon p = (TGPolygon) s;
			TGPolygon help = new TGPolygon(this.imageUri);
			if (p.equals(help)) {
				selectedShape = p;
				notifySelectionChanged();
				return;
			}
		} else if (s instanceof TGLine) {
			TGLine p = (TGLine) s;
			TGLine help = new TGLine(this.imageUri);
			if (p.equals(help)) {
				selectedShape = p;
				notifySelectionChanged();
				return;
			}
		}

		if (selectedShape != null && resize) {
			s.setWritingMode(selectedShape.getWritingMode());
			LinkEditorController.getInstance().updateLink(selectedShape, s);
		}

		// deselect all selected shapes
		LinkEditorController.getInstance().deselectAllSelectedShapes();

		selectedShape = s;
		selected_shapes.add(selectedShape); // added the first (default)
											// selected Shape to TGShape List
											// too
		LinkEditorController.getInstance().addSelectedShape(selectedShape);

		notifySelectionChanged();
	}

	private void setAngleLable(boolean visible, double angle) {
		LinkEditorController.getInstance().setAngleLabel(visible, angle);
	}

	public void resetSelectedShape() {
		if (selectedShape != null) {
			if (selectedShape instanceof TGRectangle)
				setSelectedShape(new TGRectangle(this.imageUri));
			else if (selectedShape instanceof TGPolygon)
				setSelectedShape(new TGPolygon(this.imageUri));
			else if (selectedShape instanceof TGLine)
				setSelectedShape(new TGLine(this.imageUri));
		}
	}

	/**
	 * Notify the selection has been changed to all observers.
	 */
	private void notifySelectionChanged() {
		for (int i = 0; i < imageObservers.size(); i++) {
			((IImageObserver) (imageObservers.elementAt(i)))
					.onSelectionChanged(this, getSelectedShape());
		}
		// if a link is in place -> jump to it
		TGShape shape = getSelectedShape();
		LinkEditorController.getInstance().jumpToText(shape, false);

		// provide the controller with the selected shape
		LinkEditorController.getInstance().setSelectedShape(shape);

		checkUnlinkedShapes();
	}

	/**
	 * Constructor for SessionedImageCanvas.
	 * 
	 * @param parent
	 *            the parent of this control
	 */
	public SessionedImageCanvas(Composite parent) {
		super(parent);
		init();
		initGL();
	}

	/**
	 * Constructor for SessionedImageCanvas.
	 * 
	 * @param parent
	 *            the parent of this control
	 * @param style
	 *            the style of this canvas.
	 * @see org.eclipse.swt.widgets.Canvas for style detail
	 */
	public SessionedImageCanvas(Composite parent, int style) {
		super(parent, style);
		init();
		initGL();
	}

	private void initGL() {
		glSettings = new GridLineSettings();
	}

	private void init() {
		instance = this;
		mementoManager = new MementoManager(this);

		// am 07.05.2009 von M. Leuk
		// eleminiert die "Ghost"-Rectangles
		// removeAllShapes(true);
		setSession(new DefaultSession());
		setupCanvasListeners();
	}

	private void setWritingModeForSelectedShapes(WRITING_MODE writingMode) {
		if (selected_shapes.isEmpty())
			setWritingMode(getSelectedShape(), writingMode);
		else {
			for (TGShape s : selected_shapes) {
				setWritingMode(s, writingMode);
			}
		}

	}

	/**
	 * Dispose the garbage here
	 */
	@Override
	public void dispose() {
		selectedShape = null;
		super.dispose();
	}

	private GC gc = null;

	public GC getGc() {
		return gc;
	}

	/**
	 * Paint function using double buffer.
	 * 
	 * @param gc
	 *            graphics context
	 */
	@Override
	public void paint(GC gc) {
		super.paint(gc);
		this.gc = gc;
		if (getSourceImage() != null) {
			// this.setFocus();
			for (TGShape s : all_Shapes) {
				if (s != null) {
					if (s instanceof TGRectangle) {
						if (s.equals(this.selectedShape)
								|| (!selected_shapes.isEmpty() && selected_shapes
										.contains(s))) {
							drawFrame2(gc, imageToScreen((TGRectangle) s),
									false);
						} else {
							drawFrame(gc, imageToScreen((TGRectangle) s),
									false, true);
						}
					} else if (s instanceof TGPolygon) {
						if (s.equals(this.selectedShape)
								|| (!selected_shapes.isEmpty() && selected_shapes
										.contains(s))) {
							drawFrame2(gc, imageToScreen((TGPolygon) s), false);
						} else {
							drawFrame(gc, imageToScreen((TGPolygon) s), false);
						}
					} else if (s instanceof TGLine) {
						if (showHelpLines) {
							if (s.equals(this.selectedShape)
									|| (!selected_shapes.isEmpty() && selected_shapes
											.contains(s))) {
								drawFrame2(gc, imageToScreen((TGLine) s), false);
							} else {
								drawFrame(gc, imageToScreen((TGLine) s), false);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Get the current session.
	 * 
	 * @return current session
	 */
	public IImageSession getSession() {
		return session;
	}

	/**
	 * Set the session.
	 * <p>
	 * If oldSession != newSession calls oldSession.end() and nweSession.begin()
	 * </p>
	 * 
	 * @param session
	 *            the session to activate; null to disable all sessions.
	 */
	public void setSession(IImageSession newSession) {
		if (newSession != null) {
			if (this.session != null) {
				if (this.session == newSession) {
					return;
				}
				this.session.endSession();
			}
			this.session = newSession;
			this.session.beginSession(this);
		}
	}

	/**
	 * Get saved session (ready to return back to old session)
	 * 
	 * @return saved session
	 */
	public IImageSession getSavedSession() {
		return savedSession;
	}

	/**
	 * Save the session (temporarily interrupt by other session)
	 * 
	 * @param session
	 *            session to save
	 */
	public void setSavedSession(IImageSession session) {
		savedSession = session;
	}

	private void setWritingMode(TGShape shape, WRITING_MODE writingMode) {
		shape.setWritingMode(writingMode);
		LinkEditorController.getInstance().setWritingMode(shape, writingMode);
		editShapeinArrayListShapes(shape, shape);
		redraw();
	}

	private boolean ctrlPressed = false;

	public boolean isCtrlPressed() {
		return ctrlPressed;
	}

	private void setCtrlPressed(boolean ctrlPressed) {
		this.ctrlPressed = ctrlPressed;
	}

	/**
	 * Set up the listeners for the canvas.
	 */
	public void setupCanvasListeners() {

		addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
			}
		});
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent event) {
				if (event.button == 2) {
					try {
						LinkEditorController.getInstance().addNewLink();
					} catch (Exception e) {
						ComponentPlugin.handleError(e);
					}
				} else {
					IImageSession session = instance.getSession();
					if (session != null)
						session.mouseDown(event);
				}
			}

			@Override
			public void mouseUp(MouseEvent event) {
				if (session != null)
					session.mouseUp(event);
			}

			@Override
			public void mouseDoubleClick(MouseEvent event) {
				if (session != null)
					session.mouseDoubleClick(event);
			}
		});
		addMouseMoveListener(new MouseMoveListener() {
			@Override
			public void mouseMove(MouseEvent event) {
				checkUnlinkedShapes();
				if (ThumbViewMediator.getImageSurface() == null)
					registerThumbNailListeners();
				if (session != null)
					session.mouseMove(event);
			}
		});

		addKeyListener(new KeyAdapter() {
			boolean keyPressed = false;

			@Override
			public void keyPressed(KeyEvent event) {
				if (event.keyCode == SWT.DEL) {
					removeSelectedShapes();
				}
				// whitespace move operates among all sessions
				else if (event.character == 0x20 && event.stateMask == 0) { // white
					// space
					if (!keyPressed) {
						setSavedSession(session);
						setSession(new BasicMoveSession());
						keyPressed = true;
					}
				} else if (event.keyCode == SWT.CTRL) {
					setCtrlPressed(true);
				} else if ((event.stateMask & SWT.CTRL) != 0) {
					TGLine copyLine = null;
					switch (event.keyCode) {

					// case 'd':
					// TGShape s = getSelectedShape();
					// setDirection(getSelectedShape(), getNextDirection(s
					// .getDirection()));
					// break;

					case 'a': // select/unselect all shapes <Ctrl + a>
						allShapesSelected = !allShapesSelected;
						selected_shapes.clear();
						LinkEditorController.getInstance()
								.clearSelectedShapes();
						if (allShapesSelected) {
							for (TGShape s : all_Shapes) {
								selected_shapes.add(s);
								LinkEditorController.getInstance()
										.addSelectedShape(s);
							}
						}
						break;

					case 'c': // CTRL + c
						copyLine = defineNextLine();
						break;

					case 'v': // CTRL + v
						copyLine = defineNextLine();
						if (copyLine != null) {
							copyLine = screenToImage(copyLine);
							setSelectedShape(copyLine);
							addShapeToArrayListShapes(copyLine);
						}
						break;
					}

				} else if ((event.stateMask & SWT.SHIFT) != 0) {
					switch (event.keyCode) {
					case SWT.TAB:
						List<TGShape> shapes = getAllVisibleShapes();
						if (selectedShape != null
								&& selectedShape.isCompletedShape()
								&& !shapes.isEmpty()) {
							int i = shapes.indexOf(selectedShape);
							if (i > 0) {
								setSelectedShape(shapes.get(i - 1));
							} else {
								setSelectedShape(shapes.get(shapes.size() - 1));
							}
						}
						break;

					case 'h': // SHIFT + h
						alignDockingLineHorizOrVert("HORIZONTAL"); //$NON-NLS-1$
						break;

					case 'v': // SHIFT + v
						alignDockingLineHorizOrVert("VERTICAL"); //$NON-NLS-1$
						break;

					case 's':
						showWritingMode = !showWritingMode;
						break;

					case 'c':
						createCloneRectangleV();
						break;
						
					case 'b':
						createCloneRectangleH_Back();
						break;
						
					case 'u':
						createCloneRectangleV_Up();
						break;

					case 'a':
						createCloneRectangleWithAngle();
						break;

					case 'p':
						showRotation = !showRotation;
						break;

					case 'l':
						showAllLayersOnImage();
						break;

					case 'r':
						setRotatingPointFlag();
						break;

					}
				} else if (event.keyCode == SWT.TAB) { // Tab key
					List<TGShape> shapes = getAllVisibleShapes();
					if (selectedShape != null
							&& selectedShape.isCompletedShape()
							&& !shapes.isEmpty()) {
						int i = shapes.indexOf(selectedShape);
						// System.err.println(i);
						if (i < shapes.size() - 1) {
							setSelectedShape(shapes.get(i + 1));
						} else {
							setSelectedShape(shapes.get(0));
						}
					}
				} else if (event.keyCode == 'c') {
					createCloneRectangleH();
				} else { // other key event
					if (session != null)
						session.keyPressed(event);
				}

				redraw();
				notifySelectionChanged();
			}

			@Override
			public void keyReleased(KeyEvent event) {
				// whitespace+mouse moves image among all sessions
				if (event.character == 0x20) { // white space
					if (keyPressed) {
						keyPressed = false;
						if (getSavedSession() != null) {
							setSession(getSavedSession());
						}
					} else { // other key event
						if (session != null)
							session.keyReleased(event);
					}
				} else if (event.keyCode == SWT.CTRL) {
					setCtrlPressed(false);
				} else {
					if (session != null)
						session.keyReleased(event);
				}
			}
		});
	}

	public void changeCursor(int style) {
		setCursor(new Cursor(getDisplay(), style));
	}
	
	public URL getInstallURL(String pathString) {
		URL url = FileLocator.find(ComponentPlugin
			.getDefault().getBundle(), new Path(pathString),
			null);
		try {
			URL resolved = FileLocator.resolve(url);
			//System.out.println(url + " -> "  + resolved);
			return resolved;
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Initialize the extensible context menu.
	 * 
	 * @param menuManager
	 *            IMenuManager. The menuManager must be
	 *            setRemoveAllWhenShown(true). You can use
	 *            menuManager.setRemoveAllWhenShown(true) when you initialize
	 *            the view or editor.
	 */
	@Override
	public void initContextMenu(IMenuManager menuManager) {
		//FIXME
//		URL url = getInstallURL(".");
		
		// create actions first.
		Action showShapeAction = new Action(
				Messages.SessionedImageCanvas_Action_CenterSelection) {
			@Override
			public void run() {
				instance.showSelectedShape();
			}
		};
//			URL uurl = new URL(url, "icons/Center16.gif"); //$NON-NLS-1$
		URL uurl = getInstallURL("icons/Center16.gif");
		showShapeAction.setImageDescriptor(ImageDescriptor
				.createFromURL(uurl));

		Action addNewLink = new Action(
				Messages.SessionedImageCanvas_Action_AddNewLink) {
			@Override
			public void run() {
				LinkEditorController.getInstance().addNewLink();
			}
		};

		Action unlink = new Action(
				Messages.SessionedImageCanvas_Action_Unlink) {
			@Override
			public void run() {
				LinkEditorController.getInstance().unlinkSelectedLinks();
			}
		};

		Action delAllShapesAction = new Action(
				Messages.SessionedImageCanvas_Action_DelAllShapes) {
			@Override
			public void run() {
				if (!getArrayListShapes().isEmpty()) {
					if (LinkEditorController.getInstance().showWarnings()) {
						msg.setMessage(Messages.SessionedImageCanvas_Mess_DelAllLinks_1);
						msg.setText(Messages.SessionedImageCanvas_Mess_DelAllLinks_2);
						if (msg.open() == SWT.YES) {
							removeAllShapes(true);
							resetSelectedShape();
							redraw();
						}
					}
				} else {
					removeAllShapes(true);
					resetSelectedShape();
					redraw();
				}
			}
		};

		Action delSelShapeAction = new Action(
				Messages.SessionedImageCanvas_Action_DelSelRec) {
			@Override
			public void run() {
				removeSelectedShapes();
			}
		};

		Action jumpToText = new Action(
				Messages.SessionedImageCanvas_Action_JumpToText) {
			@Override
			public void run() {
				LinkEditorController.getInstance().jumpToText(
						getSelectedShape(), false);
			}
		};

//			Action cloneSelectedRectHorizontally = new Action(
//					Messages.SessionedImageCanvas_Action_CloneActiveRectangleHorizontally) {
//				@Override
//				public void run() {
//					createCloneRectangleH();
//				}
//			};
//			cloneSelectedRectHorizontally.setEnabled(selectedShape != null
//					&& selectedShape.getType() == TYPE.RECT
//					&& selectedShape.isCompletedShape());
//
//			Action cloneSelectedRectVertically = new Action(
//					Messages.SessionedImageCanvas_Action_CloneActiveRectangleVertically) {
//				@Override
//				public void run() {
//					createCloneRectangleV();
//				}
//			};
//			cloneSelectedRectVertically.setEnabled(selectedShape != null
//					&& selectedShape.getType() == TYPE.RECT
//					&& selectedShape.isCompletedShape());
		
		//################################################################
		IMenuManager cloneRectangle = new MenuManager(Messages.SessionedImageCanvas_CloneRecztangle);
		
		cloneRectangle.setVisible(selectedShape != null
				&& selectedShape.getType() == TYPE.RECT
				&& selectedShape.isCompletedShape());

		Action cloneSelectedRectHorizontally = new Action(
				Messages.SessionedImageCanvas_Action_CloneActiveRectangleHorizontally) {
			@Override
			public void run() {
				createCloneRectangleH();
			}
		};
		Action cloneSelectedRectVertically = new Action(
				Messages.SessionedImageCanvas_Action_CloneActiveRectangleVertically) {
			@Override
			public void run() {
				createCloneRectangleV();
			}
		};
		Action cloneSelectedRectDownToUp = new Action(
				Messages.SessionedImageCanvas_Action_CloneActiveRectangleDownToUp) {
			@Override
			public void run() {
				createCloneRectangleV_Up();
			}
		};
		Action cloneSelectedRectRightToLeft = new Action(
				Messages.SessionedImageCanvas_Action_CloneActiveRectangleRightToLeft) {
			@Override
			public void run() {
				createCloneRectangleH_Back();
			}
		};
		Action cloneSelectedRectWithAngle = new Action(
				Messages.SessionedImageCanvas_Action_CloneActiveRectangleWithAngle) {
			@Override
			public void run() {
				createCloneRectangleWithAngle();
			}
		};
		cloneRectangle.add(cloneSelectedRectHorizontally);
		cloneRectangle.add(cloneSelectedRectVertically);
		cloneRectangle.add(cloneSelectedRectDownToUp);
		cloneRectangle.add(cloneSelectedRectRightToLeft);
		cloneRectangle.add(cloneSelectedRectWithAngle);
//			cloneRectangle.add();
//			cloneRectangle.add();
//			cloneRectangle.add();

		// now fill the menu.
//			if (selectedShape != null) {
//				menuManager.add(cloneRectangle);
//				menuManager.add(new Separator());
//			}
		//################################################################
		

		IMenuManager textWritingMode = new MenuManager(
				Messages.SessionedImageCanvas_Action_TextWritingMode);

		Action showHideWritingModeArrow = new Action(
				Messages.SessionedImageCanvas_Action_ShowHideWritingMode) {
			@Override
			public void run() {
				showWritingMode = !showWritingMode;
				redraw();
			}
		};

		IMenuManager setWritingModeForSelectedShape = new MenuManager(
				Messages.SessionedImageCanvas_Action_SetTextWritingModeForSelectedShape);

		Action none = new Action(Messages.SessionedImageCanvas_None) {
			@Override
			public void run() {
				setWritingModeForSelectedShapes(WRITING_MODE.NONE);
				this.setChecked(true);
			}
		};

		Action lr = new Action(Messages.SessionedImageCanvas_lr) {
			@Override
			public void run() {
				setWritingModeForSelectedShapes(WRITING_MODE.LR);
				this.setChecked(true);
			}
		};

		Action lr_tb = new Action(Messages.SessionedImageCanvas_lr_tb) {
			@Override
			public void run() {
				setWritingModeForSelectedShapes(WRITING_MODE.LR_TB);
				this.setChecked(true);
			}
		};

		Action rl = new Action(Messages.SessionedImageCanvas_rl) {
			@Override
			public void run() {
				setWritingModeForSelectedShapes(WRITING_MODE.RL);
				this.setChecked(true);
			}
		};

		Action rl_tb = new Action(Messages.SessionedImageCanvas_rl_tb) {
			@Override
			public void run() {
				setWritingModeForSelectedShapes(WRITING_MODE.RL_TB);
				this.setChecked(true);
			}
		};

		Action tb = new Action(Messages.SessionedImageCanvas_tb) {
			@Override
			public void run() {
				setWritingModeForSelectedShapes(WRITING_MODE.TB);
				this.setChecked(true);
			}
		};

		Action tb_rl = new Action(Messages.SessionedImageCanvas_tb_rl) {
			@Override
			public void run() {
				setWritingModeForSelectedShapes(WRITING_MODE.TB_RL);
				this.setChecked(true);
			}
		};

		setWritingModeForSelectedShape.add(none);
		setWritingModeForSelectedShape.add(lr);
		setWritingModeForSelectedShape.add(lr_tb);
		setWritingModeForSelectedShape.add(rl);
		setWritingModeForSelectedShape.add(rl_tb);
		setWritingModeForSelectedShape.add(tb);
		setWritingModeForSelectedShape.add(tb_rl);

		IMenuManager setWritingModeForDocument = new MenuManager(
				Messages.SessionedImageCanvas_Action_SetDefaultTextWritingMode);

		Action none2 = new Action(Messages.SessionedImageCanvas_None) {
			@Override
			public void run() {
				setDefaultWritingMode(WRITING_MODE.NONE);
				this.setChecked(true);
			}
		};

		Action lr2 = new Action(Messages.SessionedImageCanvas_lr2) {
			@Override
			public void run() {
				setDefaultWritingMode(WRITING_MODE.LR);
				this.setChecked(true);
			}
		};

		Action lr_tb2 = new Action(Messages.SessionedImageCanvas_lr_tb) {
			@Override
			public void run() {
				setDefaultWritingMode(WRITING_MODE.LR_TB);
				this.setChecked(true);
			}
		};

		Action rl2 = new Action(Messages.SessionedImageCanvas_rl) {
			@Override
			public void run() {
				setDefaultWritingMode(WRITING_MODE.RL);
				this.setChecked(true);
			}
		};

		Action rl_tb2 = new Action(Messages.SessionedImageCanvas_rl_tb) {
			@Override
			public void run() {
				setDefaultWritingMode(WRITING_MODE.RL_TB);
				this.setChecked(true);
			}
		};

		Action tb2 = new Action(Messages.SessionedImageCanvas_tb) {
			@Override
			public void run() {
				setDefaultWritingMode(WRITING_MODE.TB);
				this.setChecked(true);
			}
		};

		Action tb_rl2 = new Action(Messages.SessionedImageCanvas_tb_rl) {
			@Override
			public void run() {
				setDefaultWritingMode(WRITING_MODE.TB_RL);
				this.setChecked(true);
			}
		};

		setWritingModeForDocument.add(none2);
		setWritingModeForDocument.add(lr2);
		setWritingModeForDocument.add(lr_tb2);
		setWritingModeForDocument.add(rl2);
		setWritingModeForDocument.add(rl_tb2);
		setWritingModeForDocument.add(tb2);
		setWritingModeForDocument.add(tb_rl2);

		textWritingMode.add(showHideWritingModeArrow);
		textWritingMode.add(setWritingModeForSelectedShape);
		textWritingMode.add(setWritingModeForDocument);
		
		//################################################################
		IMenuManager shapeRotation = new MenuManager(Messages.SessionedImageCanvas_ShapeRotation);

		Action activateTextRotationMode = new Action(
				Messages.SessionedImageCanvas_ActivateOrDeactivateShapeRotation) {
			@Override
			public void run() {
				rotatingPointFlag = !rotatingPointFlag;
				redraw();
			}
		};
		shapeRotation.setVisible(selectedShape != null
				&& selectedShape.getType() == TYPE.RECT
				&& selectedShape.isCompletedShape());

		Action showHidePositionSign = new Action(
				Messages.SessionedImageCanvas_ShowOrHideShapeRotation) {
			@Override
			public void run() {
				showRotation = !showRotation;
				redraw();
			}
		};

		Action setPositionForSelectedShapes = new Action(
				Messages.SessionedImageCanvas_Action_SetShapeRotationForSelectedShape) {
			@Override
			public void run() {
				callEditRotationDialog();
			}
		};

		shapeRotation.add(activateTextRotationMode);
		shapeRotation.add(showHidePositionSign);
		shapeRotation.add(setPositionForSelectedShapes);

		// now fill the menu.
//			if (selectedShape != null) {
//				menuManager.add(showShapeAction);
//				menuManager.add(new Separator());
//			}
		//################################################################

		// **************
		IMenuManager textLayer = new MenuManager(Messages.SessionedImageCanvas_TextLayers);

		Action activateTextLayerMode = new Action(Messages.SessionedImageCanvas_EditLayers) {
			@Override
			public void run() {
				callEditTGLayerDialog();
			}
		};

		Action selectedShapesTextLayerMode = new Action(
				Messages.SessionedImageCanvas_EditLayersForSelectedShapesOnly) {
			@Override
			public void run() {
				callEditTGLayerDialog4SelectedShapes();
			}
		};

		Action activateshowAllLayersMode = new Action(
				Messages.SessionedImageCanvas_ShowAllLayers) {
			@Override
			public void run() {
				showAllLayersOnImage();
			}
		};

		/**
		 * set <Enabled true> if there are layers marked as unVisible
		 */
		if (proofIfThereAreLayersMarkedAsUnvisible()) {
			activateshowAllLayersMode.setEnabled(false);
		} else {
			activateshowAllLayersMode.setEnabled(true);
		}
		textLayer.add(activateTextLayerMode);

		if (this.selectedShape != null
				&& this.selectedShape.getImageUri().equals(imageUri)) {
			selectedShapesTextLayerMode.setEnabled(true);
		} else {
			selectedShapesTextLayerMode.setEnabled(false);
		}
		textLayer.add(selectedShapesTextLayerMode);

		textLayer.add(new Separator());
		textLayer.add(activateshowAllLayersMode);
		// **************

		// DockingLines menu.
		IMenuManager dockingLines = new MenuManager(Messages.SessionedImageCanvas_DockingLines);

		IMenuManager setAlignment = new MenuManager(
				Messages.SessionedImageCanvas_Mess_setAlignment);

		Action horizontal = new Action(
				Messages.SessionedImageCanvas_Mess_setHorizontalAlignment) {
			@Override
			public void run() {
				// System.out.println("Horizontal");
				alignDockingLineHorizOrVert("HORIZONTAL"); //$NON-NLS-1$
			}
		};

		Action vertical = new Action(
				Messages.SessionedImageCanvas_Mess_setVerticalAlignment) {
			@Override
			public void run() {
				alignDockingLineHorizOrVert("VERTICAL"); //$NON-NLS-1$
			}
		};

		setAlignment.add(horizontal);
		setAlignment.add(vertical);
		setAlignment.setVisible(true);

		dockingLines.add(setAlignment);
		dockingLines.add(new Separator());

		// Save Action
		Action saveAllLinesAction = new Action(Messages.SessionedImageCanvas_SaveAllLines) {
			@Override
			public void run() {
				LinkEditorController.getInstance().saveAllDockingLines(
						imageUri);
			}
		};

		// saveAllLinesAction.setEnabled(false);
		dockingLines.add(saveAllLinesAction);
		// Delete Action
		Action delAllLinesAction = new Action(Messages.SessionedImageCanvas_DeleteAllLines) {
			@Override
			public void run() {
				deleteAllDockingLines();
			}
		};
		// dockingLines.add(new Separator());
		dockingLines.add(delAllLinesAction);

		// Show Action
		Action showAllLinesAction = new Action(Messages.SessionedImageCanvas_SowOrHideAllLines,
				IAction.AS_CHECK_BOX) {
			@Override
			public void run() {
				showHelpLines = !showHelpLines;
				redraw();
			}
		};
		dockingLines.add(new Separator());
		dockingLines.add(showAllLinesAction);

		// Show Action
		Action settingLinesAction = new Action(Messages.SessionedImageCanvas_Settings) {
			@Override
			public void run() {
				GridLineSettingsDialog settDialog = new GridLineSettingsDialog(
						PlatformUI.getWorkbench()
								.getActiveWorkbenchWindow().getShell(),
						glSettings);
				settDialog.open();
				redraw();
			}
		};
		dockingLines.add(new Separator());
		dockingLines.add(settingLinesAction);

		// dockingLines.setVisible(false);
		settingLinesAction.setEnabled(true);

		// If no image is loaded no menu will be shown
		if (this.getSourceImage() != null) {
			menuManager.add(addNewLink);
			menuManager.add(unlink);
			menuManager.add(new Separator());
			menuManager.add(delSelShapeAction);
			menuManager.add(delAllShapesAction);
			menuManager.add(new Separator());
			menuManager.add(jumpToText);
			menuManager.add(new Separator());
//				menuManager.add(cloneSelectedRectHorizontally);
//				menuManager.add(cloneSelectedRectVertically);
			menuManager.add(cloneRectangle);
			menuManager.add(new Separator());
			menuManager.add(dockingLines);
			menuManager.add(new Separator());
			menuManager.add(textWritingMode);
			menuManager.add(new Separator());
			menuManager.add(shapeRotation);
			menuManager.add(new Separator());
			menuManager.add(textLayer);
		}

		super.initContextMenu(menuManager);

		// Session menu initializer.
		if (session != null)
			session.initContextMenu(menuManager);

		menuManager.add(new GroupMarker(
				IWorkbenchActionConstants.MB_ADDITIONS));
	}

	/**
	 * 
	 * @return
	 */
	public boolean proofIfThereAreLayersMarkedAsUnvisible() {
		TGLayerManager layMgr = LinkEditorController.getInstance()
				.getLayerManagerFromMap(imageUri);
		if (layMgr == null) {
			return true;
		}
		for (Iterator iterator = layMgr.getIterator(); iterator.hasNext();) {
			TGLayer layer = (TGLayer) iterator.next();
			if (!layer.isVisible()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * call/open layer dialog
	 */
	public void callEditTGLayerDialog() {
		String uri = imageUri;
		TGLayerManager layerManager = LinkEditorController.getInstance()
				.getLayerManagerFromMap(uri);

		if (layerManager == null) {
			layerManager = new TGLayerManager(uri);
		}

		EditTGLayer_UI anl = new EditTGLayer_UI(getShell(), layerManager,
				instance/* , false */);
		anl.open();
		redraw();
	}

	/**
	 * call/open layer dialog for selected shapes
	 */
	public void callEditTGLayerDialog4SelectedShapes() {
		String uri = imageUri;
		TGLayerManager layerManager = LinkEditorController.getInstance()
				.getLayerManagerFromMap(uri);

		if (layerManager == null) {
			layerManager = new TGLayerManager(uri);
		}

		ChangeTGLayer_UI anl = new ChangeTGLayer_UI(getShell(), layerManager,
				instance/* , EditMode */);
		anl.open();
		redraw();
	}

	/**
	 * 
	 */
	public void callEditRotationDialog() {
		UIPositionDialog posDialog = new UIPositionDialog(PlatformUI
				.getWorkbench().getDisplay().getActiveShell(),
				selectedShape.getRotationValue());
		posDialog.open();
		try {
			Double angle = posDialog.getAngle();

			if (selected_shapes.isEmpty()) {
				if (selectedShape != null && selectedShape.isCompletedShape()) {
					if (selectedShape.getRotationValue() != angle) {
						selectedShape.setRotationValue(angle);
						showPositionOnImagePanel();
						setPositionForSelectedShape();
					}
				}
			} else {
				for (TGShape s : selected_shapes) {
					if (s.getRotationValue() != angle) {
						s.setRotationValue(angle);
						setPosition(s);
					}
				}
				showPositionOnImagePanel();
			}
		} catch (NumberFormatException e) {
			ComponentPlugin.handleError(e);
		}
		redraw();
	}

	public void deleteAllDockingLines() {
		if (!getArrayListShapes().isEmpty()) {
			if (LinkEditorController.getInstance().showWarnings()) {
				msg.setMessage(Messages.SessionedImageCanvas_Mess_DelDockingLines_1);
				msg.setText(Messages.SessionedImageCanvas_Mess_DelDockingLines_2);
				if (msg.open() == SWT.YES) {
					removeAllDockingLines();
				}
			} else {
				removeAllDockingLines();
			}
		}
	}

	/**
	 * Get All docking lines
	 * 
	 * @return an arrayList<TGLine>
	 */
	public List<TGLine> getAllDockingLines() {
		if (!getArrayListShapes().isEmpty()) {
			List<TGLine> helpLines = Collections
					.synchronizedList(new ArrayList<TGLine>());
			for (Iterator<TGShape> it = all_Shapes.iterator(); it.hasNext();) {
				TGShape type = it.next();
				if ((type instanceof TGLine)) {
					helpLines.add((TGLine) type);
				}
			}
			return helpLines;
		}
		return null;
	}

	/**
	 * Remove all docking lines
	 */
	public void removeAllDockingLines() {
		for (Iterator<TGShape> it = all_Shapes.iterator(); it.hasNext();) {
			TGShape shape = it.next();
			if (shape.getType() == TYPE.DOCKING_LINE) {
				it.remove();
			}
		}
		redraw();
		LinkEditorController.getInstance().removeAllDockingLineTags(imageUri);
	}

	/**
	 * Sets the default direction for all shapes with unspecified direction
	 * (NONE)
	 * 
	 * @param writingMode
	 */
	public void setDefaultWritingMode(WRITING_MODE writingMode) {
		for (TGShape s : all_Shapes) {
			if (s.getWritingMode() == WRITING_MODE.NONE) {
				setWritingMode(s, writingMode);
				editShapeinArrayListShapes(s, s);
			}
		}

		this.defaultWritingMode = writingMode;
		LinkEditorController.getInstance().setChangedAfterLoading(imageUri,
				true);
		redraw();
	}

	public WRITING_MODE getDefaultWritingMode() {
		return defaultWritingMode;
	}

	/**
	 * Remove the active selections
	 */
	public void removeSelectedShapes() {
		if (!getArrayListShapes().isEmpty()) {
			TGShape currentShape = null;

			if (selected_shapes.isEmpty()) {
				if (LinkEditorController.getInstance().showWarnings()) {
					msg.setMessage(Messages.SessionedImageCanvas_Mess_DelSelLinks_1);
					msg.setText(Messages.SessionedImageCanvas_Mess_DelSelLinks_2);
					if (msg.open() == SWT.YES) {
						removeShapefromArrayListShapes(
								getIndexfromArrayListShapes(getSelectedShape()),
								true);
						int size = getArrayListShapes().size();
						if (size != 0) {
							currentShape = getArrayListShapes().get(size - 1);
							setSelectedShape(currentShape);
						} else {
							currentShape = null;
							resetSelectedShape();
						}
						redraw();
					}
				} else {
					removeShapefromArrayListShapes(
							getIndexfromArrayListShapes(getSelectedShape()),
							true);
					int size = getArrayListShapes().size();
					if (size != 0) {
						currentShape = getArrayListShapes().get(size - 1);
						setSelectedShape(currentShape);
					} else {
						currentShape = null;
						resetSelectedShape();
					}
					redraw();
				}
				checkUnlinkedShapes();
			} else {
				if (LinkEditorController.getInstance().showWarnings()) {
					msg.setMessage(Messages.SessionedImageCanvas_Mess_DelSelLinks_1);
					msg.setText(Messages.SessionedImageCanvas_Mess_DelSelLinks_2);
					if (msg.open() == SWT.YES) {
						for (TGShape s : selected_shapes) {
							removeShapefromArrayListShapes(
									getIndexfromArrayListShapes(s), true);
						}

						int size = getArrayListShapes().size();
						if (size != 0) {
							currentShape = getArrayListShapes().get(size - 1);
							setSelectedShape(currentShape);
						} else {
							currentShape = null;
							resetSelectedShape();
						}
						redraw();
					}
				} else {
					for (TGShape s : selected_shapes) {
						removeShapefromArrayListShapes(
								getIndexfromArrayListShapes(s), true);
					}

					int size = getArrayListShapes().size();
					if (size != 0) {
						currentShape = getArrayListShapes().get(size - 1);
						setSelectedShape(currentShape);
					} else {
						currentShape = null;
						resetSelectedShape();
					}
					redraw();
				}
				checkUnlinkedShapes();
			}
		}
	}

	/**
	 * Scroll the view and make the selected shape center of the clientArea
	 * 
	 */
	public void showSelectedShape() {
		if (selectedShape == null)
			return;
		if (selectedShape instanceof TGRectangle)
			showRect((TGRectangle) selectedShape);
		else if (selectedShape instanceof TGPolygon)
			showPoly((TGPolygon) selectedShape);
		else if (selectedShape instanceof TGLine)
			showLine((TGLine) selectedShape);
	}

	/**
	 * Scroll the view and make the TGRectangle center of the clientArea
	 * 
	 * @param rect
	 *            TGRectangle whose coordinate is original, i.e., before
	 *            transformed by the transform of this canvas
	 */
	@Override
	public void showRect(TGRectangle src) {
		if (src != null) {
			Rectangle clientRect = this.getClientArea();
			int w = clientRect.width, h = clientRect.height;
			double dx = (double) w / 2;
			double dy = (double) h / 2;

			TGRectangle dest = SWT2dUtil
					.transformRect(this.getTransform(), src);
			double ddx = dest.getRectangle().x
					+ (double) dest.getRectangle().width / 2;
			double ddy = dest.getRectangle().y
					+ (double) dest.getRectangle().height / 2;
			AffineTransform af = this.getTransform();
			af.preConcatenate(AffineTransform.getTranslateInstance(dx - ddx, dy
					- ddy));
			this.setTransform(af);
			this.syncScrollBars();
		}
	}

	/**
	 * Scroll the view and make the polygon center of the clientArea
	 * 
	 * @param poly
	 *            polygon whose coordinate is original, i.e., before transformed
	 *            by the transform of this canvas
	 */
	public void showPoly(TGPolygon src) {
		if (src != null) {
			Rectangle clientRect = this.getClientArea();
			int w = clientRect.width, h = clientRect.height;
			double dx = (double) w / 2;
			double dy = (double) h / 2;

			Rectangle dest = SWT2dUtil.transformRect(this.getTransform(),
					src.getBoundsRect());
			double ddx = dest.x + (double) dest.width / 2;
			double ddy = dest.y + (double) dest.height / 2;
			AffineTransform af = this.getTransform();
			af.preConcatenate(AffineTransform.getTranslateInstance(dx - ddx, dy
					- ddy));
			this.setTransform(af);
			this.syncScrollBars();
		}
	}

	/**
	 * Scroll the view and make the Line center of the clientArea
	 * 
	 * @param src
	 *            Lines whose coordinate is original, i.e., before transformed
	 *            by the transform of this canvas
	 */
	public void showLine(TGLine src) {
		if (src != null) {
			Rectangle clientRect = this.getClientArea();
			int w = clientRect.width, h = clientRect.height;
			double dx = (double) w / 2;
			double dy = (double) h / 2;

			Rectangle dest = SWT2dUtil.transformRect(this.getTransform(),
					src.getBoundsRect());
			double ddx = dest.x + (double) dest.width / 2;
			double ddy = dest.y + (double) dest.height / 2;
			AffineTransform af = this.getTransform();
			af.preConcatenate(AffineTransform.getTranslateInstance(dx - ddx, dy
					- ddy));
			this.setTransform(af);
			this.syncScrollBars();
		}
	}

	/**
	 * Draw frame for the select TGRectangle.
	 * 
	 * @param gc
	 *            Graphics context
	 * @param rect
	 *            selected TGRectangle
	 * @param clear
	 *            true if this operation is clear previous one
	 */
	private Color selected;
	private Color unSelected;

	public boolean showLayerOnOrOffFlag = true;
	public boolean rotatingPointFlag = false;
	private static final int drawlineWidth_1 = 1;
	private static final int drawlineWidth_2 = 2;

	public void setSelectedColor(Color c) {
		selected = c;
		if (this != null && !this.isDisposed())
			this.redraw();
	}

	public void setUnSelectedColor(Color c) {
		unSelected = c;
		if (this != null && !this.isDisposed())
			this.redraw();
	}

	private void rotateRect(GC gc, TGRectangle rect) {
		gc.setAntialias(SWT.ON);

		createRotateTransformWithTranslate(gc, rect);

		if (normalGC)
			gc.setAdvanced(false);

		gc.drawRectangle(rect.getRectangle());

		// make layer Color visible
		showLayerColorInBackground(gc, rect);
	}

	/**
	 * show layer Color in background if layer-mode switched ON
	 * 
	 * @param gc
	 * @param rect
	 */
	private void showLayerColorInBackground(GC gc, TGRectangle rect) {
		if (showLayerOnOrOffFlag && !rect.getLayer().equals("0")) { //$NON-NLS-1$
			TGLayerManager layMgr = LinkEditorController.getInstance()
					.getLayerManagerFromMap(imageUri);
			if (layMgr != null) {
				gc.setAlpha(75);
				for (Iterator iterator = layMgr.getIterator(); iterator
						.hasNext();) {
					TGLayer layer = (TGLayer) iterator.next();
					int layNr = new Integer(rect.getLayer());
					if (layer.getNumber() == layNr) {
						gc.setBackground(layer.getLayercolor());
					}
				}
				gc.fillRectangle(rect.getRectangle());
				gc.setAlpha(255);
			}
		}
	}

	/**
	 * show layer Color in background if layer-mode switched ON
	 * 
	 * @param gc
	 * @param poly
	 */
	private void showLayerColorInBackground(GC gc, TGPolygon poly) {
		if (showLayerOnOrOffFlag && !poly.getLayer().equals("0")) { //$NON-NLS-1$
			gc.setAlpha(75);
			gc.setBackground(new Color(PlatformUI.getWorkbench().getDisplay(),
					poly.getLayerRGB()));
			gc.fillPolygon(poly.getPointsArray());
			gc.setAlpha(255);
		}
	}

	/**
	 * set or unset a flag for rotation mechanism
	 */
	private void setRotatingPointFlag() {
		this.rotatingPointFlag = !this.rotatingPointFlag;
	}

	/**
	 * draw a handle for the rotation mechanism
	 * 
	 * @param gc
	 * @param rect
	 */
	private void drawRotatingPoint(GC gc, TGRectangle rect) {

		boolean showPathFlag = true;
		int offset = 0;
		int rotationPointWidth = 0;
		final int rotationPointWidth4 = 4;
		final int rotationPointWidth8 = 8;
		final int rotationPointWidth16 = 16;
		final int rotationPointWidth32 = 32;
		final int rotationPointWidth64 = 64;

		Rectangle r = rect.getRectangle();
		float x = r.x + (r.width / 2); // get the centre point of the rectangle
		// (this is where I got it wrong)
		float y = r.y + (r.height / 2);
		try {
			double factor = getScaleFactor();

			if (factor <= 0.2) {
				showPathFlag = false;
				rotationPointWidth = rotationPointWidth4;
			} else if (factor <= 0.3) {
				showPathFlag = false;
				rotationPointWidth = rotationPointWidth8;
			} else if (factor >= 3.0 && factor < 5.0) {
				offset = 8;
				showPathFlag = true;
				rotationPointWidth = rotationPointWidth32;
			} else if (factor >= 5.0) {
				offset = 24;
				showPathFlag = true;
				rotationPointWidth = rotationPointWidth64;
			} else {
				offset = 0;
				showPathFlag = true;
				rotationPointWidth = rotationPointWidth16;
			}

			gc.setBackground(new Color(this.getDisplay(), new RGB(173, 255, 47)));
			gc.setAlpha(175);

			gc.fillOval((int) x - rotationPointWidth / 2, (int) y
					- rotationPointWidth / 2, rotationPointWidth,
					rotationPointWidth);
			gc.drawArc((int) x - rotationPointWidth / 2, (int) y
					- rotationPointWidth / 2, rotationPointWidth,
					rotationPointWidth, 0, 270);
			org.eclipse.swt.graphics.Path path = (new org.eclipse.swt.graphics.Path(
					PlatformUI.getWorkbench().getDisplay()));
			path.addString(">", x, y + offset, PlatformUI.getWorkbench() //$NON-NLS-1$
					.getDisplay().getSystemFont());

			gc.fillPath(path);

			if (showPathFlag) {
				gc.drawPath(path);
			}

		} catch (Exception e) {
			ComponentPlugin.handleError(e);
		}
	}

	private void drawWritingModeOnShape(GC gc, TGShape shape) {
		if (!showWritingMode)
			return;

		Rectangle r = null;
		if (shape.getType() == TYPE.RECT) {
			r = ((TGRectangle) shape).getRectangle();
		} else if (shape.getType() == TYPE.POLY) {
			r = ((TGPolygon) shape).getBoundsRect();
		}

		WRITING_MODE writingMode = shape.getWritingMode();

		String writing_mode_s = writingMode.toString();

		if (r.y >= 30) {
			gc.drawText(writing_mode_s, r.x + 5, r.y - 20, true);
		} else {
			gc.drawText(writing_mode_s, r.x + 5, r.y + r.height + 5, true);
		}
	}

	public void showPositionOnImagePanel() {
		if (selectedShape instanceof TGLine) {
			double angle = ((TGLine) selectedShape).getAngleValue();
			setAngleLable(true, angle);
			setLastDockingLinesAngle(angle);
		} else {
			if (!showRotation) {
				setAngleLable(true, 999);
			} else {
				setAngleLable(true, selectedShape.getRotationValue());
			}
		}
	}

	public void setPositionForSelectedShape() {
		setPosition(selectedShape);
	}

	private void setPosition(TGShape shape) {
		LinkEditorController.getInstance().updateRotationValue(shape);
		editShapeinArrayListShapes(shape, shape);
		redraw();
	}

	/**
	 * select all changed Shapes
	 * 
	 * @param key
	 */
	public void setNewLayerValues(int intKey) {
		String stringKey = new Integer(intKey).toString();
		int numberKey = new Integer(intKey);
		int newNumber = numberKey;
		String newName = ""; //$NON-NLS-1$
		Color newColor = null;
		List<TGShape> uShapesNew = Collections
				.synchronizedList(new ArrayList<TGShape>());
		List<TGShape> uShapesOld = Collections
				.synchronizedList(new ArrayList<TGShape>());
		TGLayerManager layerManag = LinkEditorController.getInstance()
				.getLayerManagerFromMap(imageUri);
		if (layerManag != null) {
			for (Iterator iterator = layerManag.getIterator(); iterator
					.hasNext();) {
				TGLayer layer = (TGLayer) iterator.next();
				if (layer.getNumber() == numberKey) {
					newName = layer.getName();
					newColor = layer.getLayercolor();
					break;
				}
			}
			for (Iterator iterator = all_Shapes.iterator(); iterator.hasNext();) {
				TGShape tgShape = (TGShape) iterator.next();
				if (tgShape.getLayer().equals(stringKey) && newColor != null) {
					uShapesOld.add(tgShape);
					tgShape.setLayerValues(newNumber, newName,
							newColor.getRGB());
					uShapesNew.add(tgShape);

				}
			}
			updateLayer(uShapesNew, uShapesOld);
		}
	}

	/**
	 * 
	 * @param intKey
	 */
	public void updateLayerValues4SelectedSapes(int intKey) {
		String stringKey = new Integer(intKey).toString();
		int numberKey = new Integer(intKey);
		int newNumber = numberKey;
		String newName = ""; //$NON-NLS-1$
		Color newColor = null;
		List<TGShape> uShapesNew = Collections
				.synchronizedList(new ArrayList<TGShape>());
		List<TGShape> uShapesOld = Collections
				.synchronizedList(new ArrayList<TGShape>());
		TGLayerManager layerManag = LinkEditorController.getInstance()
				.getLayerManagerFromMap(imageUri);
		if (intKey == 0) {
			newName = "\u2013"; //$NON-NLS-1$
			newColor = new Color(getDisplay(), new RGB(255, 255, 255));
		}

		if (layerManag != null) {
			for (Iterator iterator = layerManag.getIterator(); iterator
					.hasNext();) {
				TGLayer layer = (TGLayer) iterator.next();
				if (layer.getNumber() == numberKey) {
					newName = layer.getName();
					newColor = layer.getLayercolor();
					break;
				}
			}
			for (Iterator iterator = selected_shapes.iterator(); iterator
					.hasNext();) {
				TGShape tgShape = (TGShape) iterator.next();
				if (!tgShape.getLayer().equals(stringKey) && newColor != null) {
					uShapesOld.add(tgShape);
					tgShape.setLayerValues(newNumber, newName,
							newColor.getRGB());
					uShapesNew.add(tgShape);
				}
			}
			updateLayer(uShapesNew, uShapesOld);
		}
	}

	/**
	 * select all changed Shapes
	 * 
	 * @param key
	 */
	public void setNewLayerValues4Delete(int intKey) {
		String stringKey = new Integer(intKey).toString();
		int newNumber = 0;
		String newName = "\u2013"; //$NON-NLS-1$
		RGB newColorRGB = new RGB(255, 255, 255);
		List<TGShape> uShapesNew = Collections
				.synchronizedList(new ArrayList<TGShape>());
		List<TGShape> uShapesOld = Collections
				.synchronizedList(new ArrayList<TGShape>());
		TGLayerManager layerManag = LinkEditorController.getInstance()
				.getLayerManagerFromMap(imageUri);
		if (layerManag != null) {
			for (Iterator iterator = all_Shapes.iterator(); iterator.hasNext();) {
				TGShape tgShape = (TGShape) iterator.next();
				// System.out.println("xx: "+tgShape.getLayer()+"---"+stringKey);
				if (tgShape.getLayer().equals(stringKey) && newColorRGB != null) {
					uShapesOld.add(tgShape);
					tgShape.setLayerValues(newNumber, newName, newColorRGB);
					uShapesNew.add(tgShape);

				}
			}
			// System.out.println("updateLayer: "+uShapesNew.size()+"--"+uShapesOld.size());
			updateLayerManagerAfterRemovingLayer();
			updateLayer(uShapesNew, uShapesOld);
		}
	}

	/**
	 * 
	 * @param uShapesNew
	 * @param uShapesOld
	 */
	private void updateLayer(List<TGShape> uShapesNew, List<TGShape> uShapesOld) {
		Iterator<TGShape> iterator2 = uShapesOld.iterator();
		for (Iterator iterator = uShapesNew.iterator(); iterator.hasNext();) {
			TGShape tgShapeNew = (TGShape) iterator.next();
			TGShape tgShapeOld = null;
			if (iterator2.hasNext()) {
				tgShapeOld = iterator2.next();
			}
			LinkEditorController.getInstance().updateLayerValues(tgShapeNew);
			editShapeinArrayListShapes(tgShapeNew, tgShapeOld);
		}
		redraw();
	}

	public void setLastDockingLinesAngle(double lastSelectedAngle) {
		LinkEditorController.getInstance().setLastSelectedDockingLineAngle(
				lastSelectedAngle);
	}

	private boolean normalGC = false;

	/**
	 * To avoid the shadow problem while moving the rectangles.
	 * 
	 * @param normalGC
	 * @category Hack
	 */
	public void setNormalGC(boolean normalGC) {
		this.normalGC = normalGC;
	}

	public void drawFrame2(GC gc, TGRectangle rect, boolean clear) {

		if (getDisplay().isDisposed())
			return;

		// System.out.println("drawFrame2: -->LayerNumber: "+rect.getLayer()+"--"+rect.getImageUri());
		if (rect.isVisible()) {
			if (rect == null || !rect.isCompletedShape())
				return;
			if (selected == null) {
				gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_BLACK));
			} else {
				gc.setForeground(selected);
			}
			if (rect.isLinked()) {
				gc.setLineStyle(SWT.LINE_SOLID); // Linienart des Rechtecks
			} else {
				gc.setLineStyle(SWT.LINE_DASH); // Linienart des Rechtecks
			}
			gc.setLineWidth(drawlineWidth_2);
			if (clear
					&& System.getProperty("os.name").toLowerCase() //$NON-NLS-1$
							.contains("mac")) { //$NON-NLS-1$
				Rectangle r = rect.getOuterRectangle();
				redrawRectArea(r);
			}
			// Rechteck zeichnen
			// Rectangle clipRect = gc.getClipping();
			// Save old Transform
			Transform atOld = new Transform(gc.getDevice());
			rotateRect(gc, rect); // Make Rectangle Rotation
			// Set old Transform
			gc.setTransform(atOld);

			// Draw a Frame around the selected TGRectangle
			// if (rect.getRotationValue() == 0.0) {
			drawFrameAroundSelectedRectangle(gc, rect.getOuterRectangle());
			// } else {
			// drawFrameAroundSelectedRectangle(gc, rect.getBoundry());
			// }

			// gc.setTransform(atOld);
			drawWritingModeOnShape(gc, rect);
			showPositionOnImagePanel();
			if (rotatingPointFlag) {
				drawRotatingPoint(gc, rect);
			}
			// gc.setClipping(clipRect);
		} else {
			return;
		}
	}

	private List<TGShape> getAllVisibleShapes() {
		List<TGShape> visibleShapes = Collections
				.synchronizedList(new ArrayList<TGShape>());

		for (TGShape s : all_Shapes) {
			if (s.isVisible())
				visibleShapes.add(s);
		}

		return visibleShapes;
	}

	/**
	 * Update the layer info of all shapes
	 */
	public void updateLayerInfoOfShapes() {
		boolean showLayer = false;
		for (TGShape shape : all_Shapes) {

			showLayer = false;

			if ((new Integer(shape.getLayer()) != 0)) {
				TGLayerManager layManag = LinkEditorController.getInstance()
						.getLayerManagerFromMap(shape.getImageUri());
				if (layManag != null) {
					for (Iterator iterator = layManag.getIterator(); iterator
							.hasNext();) {
						TGLayer layer = (TGLayer) iterator.next();

						if (layer.getNumber() == new Integer(shape.getLayer())) {
							if (layer.isVisible())
								showLayer = true;
							else
								showLayer = false;
							break;
						}
					}
				}
			} else {
				// Layer with number = 0 are always visible!
				showLayer = true;
			}

			shape.setVisible(showLayer);
			LinkEditorController.getInstance().setLinkVisiblityByShape(shape,
					showLayer);
		}
	}

	/**
	 * Draw frame for the select TGRectangle.
	 * 
	 * @param gc
	 *            Graphics context
	 * @param rect
	 *            selected TGRectangle
	 * 
	 * @param rotate
	 *            To avoid the (ghost polygons) side effect while creating a new
	 *            rectangle
	 */
	public void drawFrame(GC gc, TGRectangle rect, boolean clear, boolean rotate) {

		if (getDisplay().isDisposed())
			return;

		if (rect == null || !rect.isCompletedShape())
			return;

		if (rect.isVisible()) {

			if (unSelected == null) {
				gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_RED));
			} else {
				gc.setForeground(unSelected);
			}
			if (rect.isLinked()) {
				gc.setLineStyle(SWT.LINE_SOLID); // Linienart des Rechtecks
			} else {
				gc.setLineStyle(SWT.LINE_DASH); // Linienart des Rechtecks
			}

			gc.setLineWidth(drawlineWidth_2);

			// XOR-Mode's workaround.
			if (clear
					&& System.getProperty("os.name").toLowerCase() //$NON-NLS-1$
							.contains("mac")) { //$NON-NLS-1$
				Rectangle r = rect.getOuterRectangle();
				redrawRectArea(r);
			}

			if (rotate) {
				Transform atOld = new Transform(gc.getDevice());
				rotateRect(gc, rect);
				gc.setTransform(atOld);
			} else {
				// Rechteck zeichnen
				Rectangle clipRect = gc.getClipping();
				gc.drawRectangle(rect.getRectangle());
				gc.setClipping(clipRect);
			}
		}
	}

	private void redrawRectArea(Rectangle r) {
		redraw(r.x - 80, r.y - 80, r.width + 150, r.height + 150, false);
		update();
	}

	/**
	 * Draw frame around the selected TGRectangle.
	 * 
	 * @param gc
	 *            Graphics context
	 * @param rect
	 *            selected TGRectangle
	 */
	public void drawFrameAroundSelectedRectangle(GC gc, Rectangle rect) {
		// TODO There is maybe a problem with lineWidth2 yet
		int numberOfLines = 0; // count of lines
		final int w_h_diff = 2; // don't change this value!
		final int numberOfLines1 = 1;
		final int numberOfLines3 = 3;
		final int numberOfLines4 = 4;
		final int numberOfLines5 = 5;
		final int numberOfLines6 = 6;
		final int numberOfLines8 = 8;
		final int numberOfLines10 = 10;
		final int numberOfLines12 = 12;
		final int numberOfLines14 = 14;
		final int numberOfLines16 = 16;

		double factor = getScaleFactor();

		if (factor <= 0.1) {
			numberOfLines = numberOfLines1; // count of lines
		} else if (factor < 0.3 && factor >= 0.1) {
			numberOfLines = numberOfLines3; // count of lines
		} else if (factor < 0.4 && factor >= 0.3) {
			numberOfLines = numberOfLines4; // count of lines
		} else if (factor < 0.6 && factor >= 0.4) {
			numberOfLines = numberOfLines5; // count of lines
		} else if (factor > 0.6 && factor <= 2.0) {
			numberOfLines = numberOfLines6; // count of lines
		} else if (factor > 2.0 && factor <= 3.0) {
			numberOfLines = numberOfLines8; // count of lines
		} else if (factor > 3.0 && factor <= 5.0) {
			numberOfLines = numberOfLines10; // count of lines
		} else if (factor > 5.0 && factor <= 10.0) {
			numberOfLines = numberOfLines12; // count of lines
		} else if (factor > 10.0 && factor <= 15.0) {
			numberOfLines = numberOfLines14; // count of lines
		} else {
			numberOfLines = numberOfLines16; // count of lines
		}

		final int lineWidth2 = 1;
		final int gap = 1;
		int lineFactor = drawlineWidth_2 / 2;
		int gapAll = gap + lineFactor;

		gc.setLineWidth(lineWidth2);
		gc.setLineStyle(SWT.LINE_DOT);

		gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));

		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));
		gc.fillRectangle(rect.x - (numberOfLines / 2 - gap) - lineFactor
				+ lineWidth2 - gapAll, rect.y - (numberOfLines / 2 - gap)
				- lineFactor + lineWidth2 - gapAll, numberOfLines + gapAll,
				numberOfLines + gapAll);

		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
		gc.fillRectangle(rect.x - (numberOfLines / 2 - gap) - lineFactor,
				rect.y - (numberOfLines / 2 - gap) - lineFactor, numberOfLines,
				numberOfLines);

		// ------------
		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));
		gc.fillRectangle(rect.x - (numberOfLines / 2 - gap) - lineFactor
				+ lineWidth2 - gapAll, rect.y + (rect.height / 2)
				- numberOfLines / 2 - gapAll / 2 - lineWidth2 / 2,
				numberOfLines + gapAll, numberOfLines + gapAll);

		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
		gc.fillRectangle(rect.x - (numberOfLines / 2 - gap) - lineFactor,
				rect.y + (rect.height / 2) - numberOfLines / 2 + lineFactor
						- gapAll / 2 - lineWidth2 / 2, numberOfLines,
				numberOfLines);
		// ---------------
		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));
		gc.fillRectangle(rect.x - (numberOfLines / 2 - gap) - lineFactor
				+ lineWidth2 - gapAll, rect.y + rect.height + lineFactor
				- lineFactor - (numberOfLines / 2 + gap), numberOfLines
				+ gapAll, numberOfLines + gapAll);
		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
		gc.fillRectangle(rect.x - (numberOfLines / 2 - gap) - lineFactor,
				rect.y - (numberOfLines / 2 + gap) + rect.height + lineFactor,
				numberOfLines, numberOfLines);
		// Middle
		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));
		gc.fillRectangle(rect.x - numberOfLines / 2 + (rect.width / 2)
				- lineFactor, rect.y - (numberOfLines / 2 - gap) - lineFactor
				- lineWidth2, numberOfLines + gapAll, numberOfLines + gapAll);
		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
		gc.fillRectangle(rect.x - numberOfLines / 2 + (rect.width / 2)
				+ lineFactor / 2, rect.y - (numberOfLines / 2 - gap)
				- lineFactor, numberOfLines, numberOfLines);
		// ---------------
		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));
		gc.fillRectangle(rect.x - numberOfLines / 2 + (rect.width / 2)
				- lineFactor, rect.y - (numberOfLines / 2 + gap) + rect.height
				- lineFactor + lineWidth2, numberOfLines + gapAll,
				numberOfLines + gapAll);
		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
		gc.fillRectangle(rect.x - numberOfLines / 2 + (rect.width / 2)
				- lineFactor / 2, rect.y - (numberOfLines / 2 + gap)
				+ rect.height - lineFactor / 2 + lineWidth2, numberOfLines,
				numberOfLines);
		// Right Side
		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));
		gc.fillRectangle(rect.x - (numberOfLines / 2 + gap) + rect.width
				- lineFactor / 2 + lineWidth2 / 2, rect.y
				- (numberOfLines / 2 - gap) - lineFactor + lineWidth2 - gapAll,
				numberOfLines + gapAll, numberOfLines + gapAll);
		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
		gc.fillRectangle(rect.x - (numberOfLines / 2 + gap) + rect.width
				+ lineFactor + lineWidth2 / 2, rect.y
				- (numberOfLines / 2 - gap) - lineFactor, numberOfLines,
				numberOfLines);

		// ---------------
		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));
		gc.fillRectangle(rect.x - (numberOfLines / 2 + gap) + rect.width
				- lineFactor / 2 + lineWidth2 / 2, rect.y + (rect.height / 2)
				- numberOfLines / 2 - gapAll / 2 - lineWidth2 / 2,
				numberOfLines + gapAll, numberOfLines + gapAll);

		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
		gc.fillRectangle(rect.x - (numberOfLines / 2 + gap) + rect.width
				+ lineFactor + lineWidth2 / 2, rect.y + (rect.height / 2)
				- numberOfLines / 2 + lineFactor - gapAll / 2 - lineWidth2 / 2,
				numberOfLines, numberOfLines);
		// ---------------
		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));
		gc.fillRectangle(rect.x - (numberOfLines / 2 + gap) + rect.width
				- lineFactor / 2 + lineWidth2 / 2, rect.y
				- (numberOfLines / 2 + gap) + rect.height + lineFactor
				- lineFactor, numberOfLines + gapAll, numberOfLines + gapAll);

		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
		gc.fillRectangle(rect.x - (numberOfLines / 2 + gap) + rect.width
				+ lineFactor + lineWidth2 / 2, rect.y
				- (numberOfLines / 2 + gap) + rect.height + lineFactor,
				numberOfLines, numberOfLines);

		gc.setLineWidth(drawlineWidth_2);

		if (selected == null) {
			gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_BLACK));
		} else {
			gc.setForeground(selected);
		}
	}

	/*
	 * create a new Transform Object for shape rotation
	 */
	private void createRotateTransformWithTranslate(GC gc, TGRectangle rect) {
		Rectangle r = rect.getRectangle();
		float x1 = r.x + (r.width / 2); // get the centre point of the rectangle
		// (this is where I got it wrong)
		float y1 = r.y + (r.height / 2);
		Transform tr = new Transform(gc.getDevice());
		tr.translate(x1, y1);
		try {
			tr.rotate((float) rect.getRotationValue());
		} catch (SWTException e) {
			ComponentPlugin.handleError(e);
		} finally {
			tr.translate(-x1, -y1);
		}

		gc.setTransform(tr);
		tr.dispose();
	}

	private double getScaleFactor() {
		AffineTransform self = this.getTransform();
		double factor = self.getScaleX();
		return factor;
	}

	/**
	 * Draw rectangles on the edges of the selected TGPolygon.
	 * 
	 * @param gc
	 *            Graphics context
	 * @param poly
	 *            selected TGPolygon
	 */
	public void drawRectanglesOnEdgesOfSelectedPolygon(GC gc, TGPolygon poly) {

		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));
		if (selected == null) {
			gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_BLACK));
		} else {
			gc.setForeground(selected);
		}

		int numberOfPixels = 0;
		final int numberOfPixels1 = 1;
		final int numberOfPixels2 = 2;
		final int numberOfPixels3 = 3;
		final int numberOfPixels4 = 4;
		final int numberOfPixels5 = 5;
		final int numberOfPixels6 = 6;
		final int numberOfPixels8 = 8;
		final int numberOfPixels10 = 10;
		final int numberOfPixels12 = 12;

		double factor = getScaleFactor();
		if (factor <= 0.1) {
			numberOfPixels = numberOfPixels1; // count of lines
		} else if (factor < 0.3 && factor >= 0.1) {
			numberOfPixels = numberOfPixels2; // count of lines
		} else if (factor < 0.6 && factor >= 0.3) {
			numberOfPixels = numberOfPixels3; // count of lines
		} else if (factor < 1.3 && factor >= 0.6) {
			numberOfPixels = numberOfPixels4; // count of lines
		} else if (factor > 1.3 && factor <= 2.0) {
			numberOfPixels = numberOfPixels5; // count of lines
		} else if (factor > 2.0 && factor <= 3.0) {
			numberOfPixels = numberOfPixels6; // count of lines
		} else if (factor > 3.0 && factor <= 5.0) {
			numberOfPixels = numberOfPixels8; // count of lines
		} else if (factor > 5.0 && factor <= 10.0) {
			numberOfPixels = numberOfPixels10; // count of lines
		} else if (factor > 10.0 && factor <= 15.0) {
			numberOfPixels = numberOfPixels12; // count of lines
		} else {
			numberOfPixels = numberOfPixels12; // count of lines
		}

		for (Point p : poly.getPoints()) {
			gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));
			gc.fillRectangle(TGPolygon.createRectAroundThePoint(p,
					numberOfPixels));
			gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
			gc.fillRectangle(TGPolygon.createRectAroundThePoint(p,
					numberOfPixels - 1));
		}

		gc.setLineWidth(drawlineWidth_2);

		if (selected == null) {
			gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_BLACK));
		} else {
			gc.setForeground(selected);
		}
	}

	/**
	 * Draw Selected TGPolygon
	 * 
	 * @param gc
	 * @param poly
	 * @param clear
	 */
	public void drawFrame2(GC gc, TGPolygon poly, boolean clear) {

		if (getDisplay().isDisposed())
			return;

		Point[] points = poly.getPoints();

		if (points == null)
			return;

		if (points.length == 0)
			return;

		if (poly.isVisible()) {
			if (selected == null) {
				gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_BLACK));
			} else {
				gc.setForeground(selected);
			}

			if (poly.isLinked()) {
				gc.setLineStyle(SWT.LINE_SOLID); // Linienart des Polygons
			} else {
				gc.setLineStyle(SWT.LINE_DOT); // Linienart des Polygons
			}
			gc.setLineWidth(drawlineWidth_2);

			if (clear
					&& System.getProperty("os.name").toLowerCase() //$NON-NLS-1$
							.contains("mac")) { //$NON-NLS-1$
				Rectangle r = poly.getBoundsRect();
				redrawRectArea(r);
			}

			// Rechteck zeichnen
			Rectangle clipRect = gc.getClipping();

			gc.drawPolygon(poly.getPointsArray());

			// make layer Color visible
			showLayerColorInBackground(gc, poly);

			// Draw a Frame around the selected polygon
			drawRectanglesOnEdgesOfSelectedPolygon(gc, poly);

			drawWritingModeOnShape(gc, poly);
			showPositionOnImagePanel();

			gc.setClipping(clipRect);
		} else {
			return;
		}
	}

	/**
	 * Draw rectangles on the edges of the selected TGLine.
	 * 
	 * @param gc
	 *            Graphics context
	 * @param line
	 *            selected TGLine
	 */
	public void drawRectanglesAOnEdgesOfSelectedLine(GC gc, TGLine line) {

		gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));

		if (selected == null) {
			gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_BLACK));
		} else {
			gc.setForeground(selected);
		}

		int numberOfPixels = 0;
		final int numberOfPixels1 = 1;
		final int numberOfPixels2 = 2;
		final int numberOfPixels3 = 3;
		final int numberOfPixels4 = 4;
		final int numberOfPixels5 = 5;
		final int numberOfPixels6 = 6;
		final int numberOfPixels8 = 8;
		final int numberOfPixels10 = 10;
		final int numberOfPixels12 = 12;

		double factor = getScaleFactor();

		if (factor <= 0.1) {
			numberOfPixels = numberOfPixels1; // count of lines
		} else if (factor < 0.3 && factor >= 0.1) {
			numberOfPixels = numberOfPixels2; // count of lines
		} else if (factor < 0.6 && factor >= 0.3) {
			numberOfPixels = numberOfPixels3; // count of lines
		} else if (factor < 1.3 && factor >= 0.6) {
			numberOfPixels = numberOfPixels4; // count of lines
		} else if (factor > 1.3 && factor <= 2.0) {
			numberOfPixels = numberOfPixels5; // count of lines
		} else if (factor > 2.0 && factor <= 3.0) {
			numberOfPixels = numberOfPixels6; // count of lines
		} else if (factor > 3.0 && factor <= 5.0) {
			numberOfPixels = numberOfPixels8; // count of lines
		} else if (factor > 5.0 && factor <= 10.0) {
			numberOfPixels = numberOfPixels10; // count of lines
		} else if (factor > 10.0 && factor <= 15.0) {
			numberOfPixels = numberOfPixels12; // count of lines
		} else {
			numberOfPixels = numberOfPixels12; // count of lines
		}

		for (Point p : line.getPoints()) {
			gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));
			gc.fillRectangle(TGLine.createRectAroundThePoint(p, numberOfPixels));
			gc.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
			gc.fillRectangle(TGLine.createRectAroundThePoint(p,
					numberOfPixels - 1));
		}

		gc.setLineWidth(drawlineWidth_1);

		if (selected == null) {
			gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_BLACK));
		} else {
			gc.setForeground(selected);
		}
	}

	public void drawFrame2(GC gc, TGLine line, boolean clear) {
		if (getDisplay().isDisposed())
			return;

		Point[] points = line.getPoints();

		if (points == null)
			return;

		if (points.length == 0)
			return;

		gc.setForeground(new Color(PlatformUI.getWorkbench().getDisplay(),
				this.glSettings.getSelectedLineRGB()));

		gc.setLineStyle(this.glSettings.getGridLineStyle());
		gc.setLineWidth(this.glSettings.getGridLineWidth());

		if (clear
				&& System.getProperty("os.name").toLowerCase().contains("mac")) { //$NON-NLS-1$ //$NON-NLS-2$
			Rectangle r = line.getBoundsRect();
			redrawRectArea(r);
		}

		// Rechteck zeichnen
		Rectangle clipRect = gc.getClipping();

		gc.drawLine(line.getPointsArray()[0], line.getPointsArray()[1],
				line.getPointsArray()[2], line.getPointsArray()[3]);

		// Draw a Frame around the selected polygon
		drawRectanglesAOnEdgesOfSelectedLine(gc, line);
		showPositionOnImagePanel();

		gc.setClipping(clipRect);
	}

	/**
	 * Draw frame for the (un)selected polygon.
	 * 
	 * @param gc
	 *            Graphics context
	 * @param points
	 *            the points of selected polygon
	 */
	public void drawFrame(GC gc, TGPolygon poly, boolean clear) {
		if (getDisplay().isDisposed() || poly.size() == 0)
			return;

		Point[] points = poly.getPoints();
		if (points == null)
			return;

		if (poly.isVisible()) {

			if (unSelected == null) {
				gc.setForeground(getDisplay().getSystemColor(SWT.COLOR_RED));
			} else {
				gc.setForeground(unSelected);
			}

			if (poly.isLinked()) {
				gc.setLineStyle(SWT.LINE_SOLID); // Linienart des Polygons
			} else {
				gc.setLineStyle(SWT.LINE_DOT); // Linienart des Polygons
			}

			gc.setLineWidth(drawlineWidth_2);

			if (clear
					&& System.getProperty("os.name").toLowerCase() //$NON-NLS-1$
							.contains("mac")) { //$NON-NLS-1$
				Rectangle r = poly.getBoundsRect();
				redrawRectArea(r);
			} else if (clear
					&& LinkEditorController.getInstance()
							.getLayerManagerFromMap(imageUri) != null) {
				Rectangle r = poly.getBoundsRect();
				redrawRectArea(r);
			}

			// Rechteck zeichnen
			Rectangle clipRect = gc.getClipping();

			gc.drawPolygon(poly.getPointsArray());

			// make layer Color visible
			showLayerColorInBackground(gc, poly);

			gc.setClipping(clipRect);
		}

	}

	/**
	 * Draw frame for the selected Line.
	 * 
	 * @param gc
	 *            Graphics context
	 * @param points
	 *            the points of selected Line
	 */
	public void drawFrame(GC gc, TGLine line, boolean clear) {
		if (getDisplay().isDisposed() || line.size() == 0)
			return;

		Point[] points = line.getPoints();
		if (points == null)
			return;

		gc.setForeground(new Color(PlatformUI.getWorkbench().getDisplay(),
				this.glSettings.getUnSelectedLineRGB()));
		gc.setLineStyle(this.glSettings.getGridLineStyle());
		gc.setLineWidth(this.glSettings.getGridLineWidth());

		if (clear
				&& System.getProperty("os.name").toLowerCase().contains("mac")) { //$NON-NLS-1$ //$NON-NLS-2$
			Rectangle r = line.getBoundsRect();
			redrawRectArea(r);
		}

		// Rechteck zeichnen
		Rectangle clipRect = gc.getClipping();

		gc.drawLine(line.getPointsArray()[0], line.getPointsArray()[1],
				line.getPointsArray()[2], line.getPointsArray()[3]);
		gc.setClipping(clipRect);
	}

	public void clearWorkingPoly() {
		if (session instanceof PolySelectSession) {
			PolySelectSession s = (PolySelectSession) session;
			if (s != null)
				s.clearWorkingPoly();
		}
	}

	public GridLineSettings getGlSettingsObject() {
		return glSettings;
	}

	public void alignDockingLineHorizOrVert(String trans) {
		if (getSelectedShape() instanceof TGLine) {
			Point points[] = null;
			if (trans.equals("HORIZONTAL")) { //$NON-NLS-1$
				points = (this.imageToScreen((TGLine) getSelectedShape())
						.transformLine(trans));
			} else if (trans.equals("VERTICAL")) { //$NON-NLS-1$
				points = (this.imageToScreen((TGLine) getSelectedShape())
						.transformLine(trans));
			}

			TGShape alignedLine = new TGLine(points, this.imageUri);
			alignedLine = this.screenToImage(alignedLine);

			this.editShapeinArrayListShapes(getSelectedShape(), alignedLine);
			this.setSelectedShape(alignedLine);
			this.redraw();
		}
		;
	}

	private static final int minCopiedRectDistance = 2;
	

	/**
	 * Clone the current selected rectangle for angels != 0.
	 */
	public void createCloneRectangleWithAngle() {
		if (selectedShape != null && selectedShape instanceof TGRectangle) {
			TGRectangle helpR = (TGRectangle) selectedShape;
			// ###############################################################
			double gKath_d = Math.sin(Math.toRadians(helpR.getRotationValue())) * (helpR.getRectangle().width /*+ minCopiedRectDistance*/);
			double aKath_d = Math.cos(Math.toRadians(helpR.getRotationValue())) * (helpR.getRectangle().width /*+ minCopiedRectDistance*/);
			int gKath = new Integer((int) gKath_d);
			int aKath = new Integer((int) aKath_d);
			
			int xn = helpR.getRectangle().x + aKath;
			int yn = helpR.getRectangle().y + gKath;
			
			TGRectangle newRec = new TGRectangle(xn,yn,
						helpR.getRectangle().width,
						helpR.getRectangle().height, helpR.getImageUri());
			// ###############################################################
			//FIXME Fix the minXRec and minXImage
			newRec.setWritingMode(helpR.getWritingMode());
			newRec.setRotationValue(helpR.getRotationValue());
			newRec.setLayerValues(new Integer(helpR.getLayer()),
					helpR.getLayerName(), helpR.getLayerRGB());

			long maxXImage = getImageData().x + getImageData().width;
			long maxXRec = newRec.getRectangle().x
					+ newRec.getRectangle().width;

//			if (maxXRec > maxXImage) {
//				System.out.println("print 1: "+maxXRec +"     "+ maxXImage);
//				newRec.getRectangle().width = (int) Math.abs(maxXImage
//						- newRec.getRectangle().x);
//			}
			
			//######################

			//######################
			maxXRec = newRec.getRectangle().x + newRec.getRectangle().width;

//			System.out.println("print: "+getImageData().width+"       "+newRec.getRectangle().width);
			
			if (maxXRec <= maxXImage) {
//				System.out.println("print 2");
				if ((newRec.getRectangle().x - newRec.getRectangle().width) < maxXImage ) {
//					System.out.println("print 3");
					setSelectedShape(newRec);
					addShapeToArrayListShapes(newRec);
					notifySelectionChanged();
					showPositionOnImagePanel();
				}
				redraw();
			}

		}
	}

	/**
	 * Clone the current selected rectangle horizontally.
	 */
	public void createCloneRectangleH() {
		if (selectedShape != null && selectedShape instanceof TGRectangle) {
			TGRectangle helpR = (TGRectangle) selectedShape;
			int xHelpR = helpR.getRectangle().x + helpR.getRectangle().width
					+ minCopiedRectDistance;
			int yHelpR = helpR.getRectangle().y;
			TGRectangle newRec = new TGRectangle(xHelpR, yHelpR,
					helpR.getRectangle().width, helpR.getRectangle().height,
					helpR.getImageUri());
			newRec.setWritingMode(helpR.getWritingMode());
			newRec.setRotationValue(helpR.getRotationValue());
			newRec.setLayerValues(new Integer(helpR.getLayer()),
					helpR.getLayerName(), helpR.getLayerRGB());

			long maxXImage = getImageData().x + getImageData().width;
			long maxXRec = newRec.getRectangle().x
					+ newRec.getRectangle().width;

			if (maxXRec > maxXImage) {
				newRec.getRectangle().width = (int) Math.abs(maxXImage
						- newRec.getRectangle().x);
			}

			maxXRec = newRec.getRectangle().x + newRec.getRectangle().width;

			if (maxXRec <= maxXImage) {
				setSelectedShape(newRec);
				addShapeToArrayListShapes(newRec);
				notifySelectionChanged();
				showPositionOnImagePanel();
				redraw();
			}

		}
	}
	
	/**
	 * Clone the current selected rectangle horizontally right to left.
	 */
	public void createCloneRectangleH_Back() {
		if (selectedShape != null && selectedShape instanceof TGRectangle) {
			TGRectangle helpR = (TGRectangle) selectedShape;
			int xHelpR = helpR.getRectangle().x - helpR.getRectangle().width
					- minCopiedRectDistance;
			int yHelpR = helpR.getRectangle().y;
			TGRectangle newRec = new TGRectangle(xHelpR, yHelpR,
					helpR.getRectangle().width, helpR.getRectangle().height,
					helpR.getImageUri());
			newRec.setWritingMode(helpR.getWritingMode());
			newRec.setRotationValue(helpR.getRotationValue());
			newRec.setLayerValues(new Integer(helpR.getLayer()),
					helpR.getLayerName(), helpR.getLayerRGB());
			
			long minXImage = getImageData().x;
			long minXRec = newRec.getRectangle().x
					- newRec.getRectangle().width;
			
			minXRec = newRec.getRectangle().x + newRec.getRectangle().width;
			
			if ((minXRec - newRec.getRectangle().width < 0) && minXRec >= minXImage) {
				newRec.getRectangle().width = (int) minXRec;
				newRec.getRectangle().x = (int) minXImage;
			}
			
			if (minXRec >= minXImage) {
				setSelectedShape(newRec);
				addShapeToArrayListShapes(newRec);
				notifySelectionChanged();
				showPositionOnImagePanel();
				redraw();
			}
		}
	}

	/**
	 * Clone the current selected rectangle vertically.
	 */
	public void createCloneRectangleV() {
		if (selectedShape != null && selectedShape instanceof TGRectangle) {
			TGRectangle helpR = (TGRectangle) selectedShape;

			int xHelpR = helpR.getRectangle().x;
			int yHelpR = helpR.getRectangle().y + helpR.getRectangle().height
					+ minCopiedRectDistance;
			TGRectangle newRec = new TGRectangle(xHelpR, yHelpR,
					helpR.getRectangle().width, helpR.getRectangle().height,
					helpR.getImageUri());
			newRec.setWritingMode(helpR.getWritingMode());
			newRec.setRotationValue(helpR.getRotationValue());
			newRec.setLayerValues(new Integer(helpR.getLayer()),
					helpR.getLayerName(), helpR.getLayerRGB());

			long maxYImage = getImageData().y + getImageData().height;
			long maxYRec = newRec.getRectangle().y
					+ newRec.getRectangle().height;

			if (maxYRec > maxYImage) {
				newRec.getRectangle().height = (int) Math.abs(maxYImage
						- newRec.getRectangle().y);
			}

			maxYRec = newRec.getRectangle().y + newRec.getRectangle().height;

			if (maxYRec <= maxYImage) {
				setSelectedShape(newRec);
				addShapeToArrayListShapes(newRec);
				notifySelectionChanged();
				showPositionOnImagePanel();
				redraw();
			}
		}
	}
	
	/**
	 * Clone the current selected rectangle vertically Up.
	 */
	public void createCloneRectangleV_Up() {
		if (selectedShape != null && selectedShape instanceof TGRectangle) {
			TGRectangle helpR = (TGRectangle) selectedShape;

			int xHelpR = helpR.getRectangle().x;
			int yHelpR = helpR.getRectangle().y - helpR.getRectangle().height
					- minCopiedRectDistance;
			TGRectangle newRec = new TGRectangle(xHelpR, yHelpR,
					helpR.getRectangle().width, helpR.getRectangle().height,
					helpR.getImageUri());
			newRec.setWritingMode(helpR.getWritingMode());
			newRec.setRotationValue(helpR.getRotationValue());
			newRec.setLayerValues(new Integer(helpR.getLayer()),
					helpR.getLayerName(), helpR.getLayerRGB());

			long minYImage = getImageData().y;
			long minYRec = minYImage;


			minYRec = newRec.getRectangle().y + newRec.getRectangle().height;
			
			if ((minYRec - newRec.getRectangle().height < 0) && minYRec >= minYImage) {
				newRec.getRectangle().height = (int) minYRec;
				newRec.getRectangle().y = (int) minYImage;
			}
			
			if (minYImage <= minYRec) {
				setSelectedShape(newRec);
				addShapeToArrayListShapes(newRec);
				notifySelectionChanged();
				showPositionOnImagePanel();
				redraw();
			}
			
			
		}
	}

	// FIXME --- fix the exception-handling!!
	public TGLine defineNextLine() {
		TGShape s = getSelectedShape();
		if (s == null || !(s instanceof TGLine))
			return null;

		TGLine newLine = (TGLine) s;
		newLine = imageToScreen(newLine);

		Point nStart = null;
		Point nEnd = null;
		int[] px = newLine.getXPoints();
		int[] py = newLine.getYPoints();

		int gridDistance = getGlSettingsObject().getGridLineSpacing();

		try {
			if ((Math.abs(px[0] - px[1]) / Math.abs(py[0] - py[1])) <= 1) {
				nStart = new Point(px[0] + gridDistance, py[0]);
				nEnd = new Point(px[1] + gridDistance, py[1]);
			} else {
				nStart = new Point(px[0], py[0] + gridDistance);
				nEnd = new Point(px[1], py[1] + gridDistance);
			}
		} catch (ArithmeticException e) {
			if (py[0] == py[1]) {
				nStart = new Point(px[0], py[0] + gridDistance);
				nEnd = new Point(px[1], py[1] + gridDistance);
			} else {
				e.printStackTrace();
				// FIXME Is it a warning when it fails or an Error ??
				ComponentPlugin.handleWarning(e);
			}

		} catch (Exception e) {
			e.printStackTrace();
			// FIXME Is it a warning when it fails or an Error ??
			ComponentPlugin.handleWarning(e);
		}

		// delete old Points
		if (newLine != null) {
			newLine.reset();
			newLine.addPoint(nStart);
			newLine.addPoint(nEnd);
		}
		return newLine;
	}

	/**
	 * Set the uri of the current used image
	 * 
	 * @param uri
	 */
	public void setImageUri(String uri) {
		imageUri = uri;
	}

	/**
	 * Get the uri of the current used image
	 * 
	 * @return the uri as {@link String}
	 */
	public String getImageUri() {
		return imageUri;
	}

	public void registerThumbNailListeners() {
	}

	/**
	 * Save the current state
	 */
	public void saveCurrentState() {
		mementoManager.saveCurrentState();
	}

	public void undo() {
		mementoManager.undo();
	}

	public void redo() {
		mementoManager.redo();
	}

	/**
	 * 
	 * @param num
	 */
	public synchronized void setCurrentActiveLayerNumber(int num) {
		this.layerInfo[0] = num;
	}

	/**
	 * 
	 * @return
	 */
	public synchronized int getCurrentActiveLayerNumber() {
		return (Integer) this.layerInfo[0];
	}

	/**
	 * 
	 * @param name
	 */
	public synchronized void setCurrentActiveLayerName(String name) {
		this.layerInfo[1] = name;
	}

	/**
	 * 
	 * @return
	 */
	public synchronized String getCurrentActiveLayerName() {
		return (String) this.layerInfo[1];
	}

	/**
	 * 
	 * @param layRGB
	 */
	public synchronized void setCurrentActiveLayerRGB(RGB layRGB) {
		this.layerInfo[2] = layRGB;
	}

	/**
	 * 
	 * @return
	 */
	public synchronized RGB getCurrentActiveLayerRGB() {
		return (RGB) this.layerInfo[2];
	}

	/**
	 * 
	 * @return
	 */
	public boolean isLayerOnOrOffFlag() {
		return showLayerOnOrOffFlag;
	}

	/**
	 * 
	 * @param layerFlag
	 */
	public void setLayerShowOrHideFlag(boolean layerFlag) {
		this.showLayerOnOrOffFlag = layerFlag;
		redraw();
	}

	/**
	 * set all TGLayers as visible again
	 */
	public void showAllLayersOnImage() {

		TGLayerManager layManag = LinkEditorController.getInstance()
				.getLayerManagerFromMap(this.imageUri);

		if (layManag != null) {
			for (Iterator iterator = layManag.getIterator(); iterator.hasNext();) {
				TGLayer layer = (TGLayer) iterator.next();
				if (!layer.isVisible()) {
					layer.setVisible(true);
				}
			}
			LinkEditorController.getInstance().addLayerManagerToMap(layManag);

			for (TGShape s : all_Shapes)
				s.setVisible(true);

			LinkEditorController.getInstance().setAllLinksVisible();

			redraw();

		}
	}

	/**
	 * 
	 */
	public void updateLayerManagerAfterRemovingLayer() {

		TGLayerManager layManag = LinkEditorController.getInstance()
				.getLayerManagerFromMap(this.imageUri);

		if (layManag != null) {
			LinkEditorController.getInstance().addLayerManagerToMap(layManag);

			for (TGShape s : all_Shapes)
				s.setVisible(true);

			redraw();
		}
	}
	
	public boolean isShowRotation() {
		return showRotation;
	}

	public void setShowRotation(boolean showRotation) {
		this.showRotation = showRotation;
	}

	
}