/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.canvas;

/**
 * Observer for draw event.
 * 
 * @author Chengdong Li
 */
public interface IDrawObserver {
	/**
	 * Function called whenever the canvas is scrolled or repainted.
	 * 
	 * @param canvas
	 *            image canvas.
	 */
	public void onSynchronize(AffineImageCanvas canvas);

	/**
	 * Function called whenever the canvas is zoomed.
	 * 
	 * @param canvas
	 *            image canvas.
	 */
	public void onZoom(AffineImageCanvas canvas);

}
