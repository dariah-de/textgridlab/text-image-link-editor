/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.canvas.session;

import info.textgrid.lab.linkeditor.mip.component.ComponentPlugin;
import info.textgrid.lab.linkeditor.mip.component.canvas.SWT2dUtil;
import info.textgrid.lab.linkeditor.mip.component.canvas.SessionedImageCanvas;
import info.textgrid.lab.linkeditor.model.graphics.TGLine;
import info.textgrid.lab.linkeditor.model.graphics.TGPolygon;
import info.textgrid.lab.linkeditor.model.graphics.TGRectangle;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Menu;

/**
 * Abstract Select session. You can extend this class to draw the customized
 * selection rectangle.
 * <p>
 * Uses a rectangle frame to select the sub image.
 * <p>
 * 
 * @author Chengdong Li
 * 
 */
public class BasicSelectSession extends AbstractImageSession {
	Menu contextMenu = null;
	protected TGShape currentShape = null; // screen domain
	protected TGShape moveShape = null;
	protected GC roiGC = null; // canvas GC.
	protected boolean mousePressed = false; // mouse is pressed
	protected boolean resizeMode = false; // in resize mode
	protected boolean antRow = true; // antRow trigger
	public final int MOVE_N = 0;
	public final int MOVE_S = 1;
	public final int MOVE_E = 2;
	public final int MOVE_W = 3;
	public final int MOVE_NE = 4;
	public final int MOVE_NW = 5;
	public final int MOVE_SE = 6;
	public final int MOVE_SW = 7;
	public final int MOVE_NOMOVE = -1;

	private final int minRectWidth = 5;
	private final int minRectHeight = 5;
	protected boolean doubleClick = false; // doubleClick event

	// last point used for positioning mouse in dragging and moving.
	Point lastPoint = new Point(0, 0);
	int direction = -1; // mouse moving direction
	int mouseStrip = 3; // mouse pad around select rectangle

	/**
	 * Basic constructor
	 */
	public BasicSelectSession() {
		currentShape = new TGRectangle("");
		moveShape = new TGRectangle("");
	}

	/**
	 * Constructs a session.
	 * 
	 * @param id
	 *            session id.
	 * 
	 */
	public BasicSelectSession(String id) {
		super(id);
		currentShape = new TGRectangle("");
		moveShape = new TGRectangle("");
	}

	/**
	 * Begin the current session
	 */
	@Override
	public void beginSession(Object canvas) {
		super.beginSession(canvas);
		setCursor(SWT.CURSOR_CROSS);
	}

	/**
	 * End this session
	 */
	@Override
	public void endSession() {
		super.endSession();
	}

	/**
	 * Get customized session cursor.
	 * 
	 * @return customized cursor for this session.
	 */
	@Override
	public Cursor getSessionCursor() {
		return new Cursor(sessionCanvas.getDisplay(), SWT.CURSOR_CROSS);
	}

	protected void checkCursor(MouseEvent evt) {
		Point p = sessionCanvas.screenToImage(new Point(evt.x, evt.y));
		setCurrentCursor(p.x, p.y);
	}

	@Override
	public void mouseDown(MouseEvent evt) {

		if (evt.getSource() instanceof SessionedImageCanvas) {
			// if no image, we return;
			if (sessionCanvas.getSourceImage() == null)
				return;

			if (evt.button == 1) { // left button only

				if (cursorStyle != SWT.CURSOR_CROSS) {
					resizeMode = true;
					// used for computing moving increment.
					lastPoint = new Point(evt.x, evt.y);

					moveShape = sessionCanvas.getSelectedShape();

					currentShape = sessionCanvas.imageToScreen(sessionCanvas
							.getSelectedShape());
				} else {
					// draw a rectangle at the current point
					currentShape = new TGRectangle(evt.x, evt.y, 0, 0,
							this.sessionCanvas.getImageUri());
				}

				if (roiGC != null) {
					roiGC.dispose();
				}
				roiGC = new GC(sessionCanvas);
				roiGC.setForeground(sessionCanvas.getDisplay().getSystemColor(
						SWT.COLOR_RED));
				roiGC.setLineStyle(SWT.LINE_DASH);
				roiGC.setLineWidth(3);

				// ***
				roiGC.setXORMode(true);

				sessionCanvas.redraw();
				mousePressed = true;
			}
		}
	}

	@Override
	public void mouseUp(MouseEvent evt) {
		int helpwidth = 0;
		int helpheight = 0;
		TGShape helpShape = null;
		super.mouseUp(evt);

		if (evt.button == 1) { // left button only
			if (evt.getSource() instanceof SessionedImageCanvas) {

				if (sessionCanvas.getSourceImage() == null)
					return;

				// to avoid creating a shape on double-click on the frame
				// of the lab (maximize/minimize)
				if (!mousePressed)
					return;

				if (resizeMode) {
					sessionCanvas.redraw();
					sessionCanvas.setResize(true);
				}

				if (cursorStyle == SWT.CURSOR_CROSS && currentShape != null
						&& currentShape.isCompletedShape()) {
					helpShape = currentShape;
					// stop the interaction
					if (currentShape instanceof TGRectangle) {
						Rectangle r = ((TGRectangle) currentShape)
								.getRectangle();
						currentShape = new TGRectangle(r.x, r.y, evt.x - r.x,
								evt.y - r.y, this.sessionCanvas.getImageUri());

						currentShape.setWritingMode(sessionCanvas
								.getDefaultWritingMode());

						currentShape.setLayerValues(
								sessionCanvas.getCurrentActiveLayerNumber(),
								sessionCanvas.getCurrentActiveLayerName(),
								sessionCanvas.getCurrentActiveLayerRGB());
					}

					TGRectangle rect = (TGRectangle) currentShape;
					helpwidth = rect.getRectangle().width;
					helpheight = rect.getRectangle().height;

					if (rect.getRectangle().width < 0) {
						helpwidth = helpwidth * -1;
					}

					if (rect.getRectangle().height < 0) {
						helpheight = helpheight * -1;
					}

					if (helpwidth < minRectWidth || helpheight < minRectHeight) {
						mousePressed = false;
						resizeMode = false;
						// *** this.triggerPaint();
						sessionCanvas.setResize(false);
						currentShape = helpShape;
						sessionCanvas.redraw();
						return;
					}

					// <= (on mac it's always == 0)
					if (evt.count <= 1)
						sessionCanvas.setSelectedShape(sessionCanvas
								.screenToImage(currentShape));

					currentShape = sessionCanvas.screenToImage(currentShape);
					sessionCanvas.addShapeToArrayListShapes(currentShape);

					// *** this.triggerPaint();
					sessionCanvas.redraw();
				} else if (resizeMode) {
					handleMouseUpResizeMove(evt);
				}
			}
			mousePressed = false;
			resizeMode = false;
			sessionCanvas.setResize(false);
		}
	}

	@Override
	public void mouseDoubleClick(MouseEvent evt) {
		super.mouseDoubleClick(evt);
		// mousePresses = false to avoid creating shadow shapes
		// while moving the cursor after doubleclick
		mousePressed = false;
		selectShape(evt);
	}

	protected boolean selectShape(MouseEvent evt) {
		sessionCanvas.setFocus();
		int x = evt.x;
		int y = evt.y;
		TGShape selectShape = sessionCanvas.getSelectedShape();
		Point p = sessionCanvas.screenToImage(new Point(x, y));
		for (TGShape s : sessionCanvas.getArrayListShapes()) {
			if (s.contains(p.x, p.y) && s.isVisible()) {
				if (!s.equals(selectShape)/* || cursor == SWT.CURSOR_CROSS */) {
					currentShape = selectShape;
					sessionCanvas.setSelectedShape(s);
					sessionCanvas.redraw();
					setCurrentCursor(p.x, p.y);
					if (sessionCanvas.rotatingPointFlag) {
						setCursor(SWT.CURSOR_IBEAM);
					}

					// To avoid jumping to the parent shape on double click.
					resizeMode = false;
					
					setCursor(SWT.CURSOR_IBEAM);
					return true;
				} else {
					sessionCanvas.setSelectedShape(s);
					resizeMode = false;
				}
			}
		}
		return false;
	}

	/**
	 * Call the mouseMove method of the base class {@link AbstractImageSession}
	 * 
	 * @param evt
	 */
	public void mouseMoveOfBase(MouseEvent evt) {
		super.mouseMove(evt);
	}

	@Override
	public void mouseMove(MouseEvent evt) {
		super.mouseMove(evt);

		int x = evt.x;
		int y = evt.y;
		TGShape selectShape = sessionCanvas.getSelectedShape();

		if (mousePressed) {
			// *** abortTrigger();
			if (resizeMode) {
				processMoveOrResize(x, y);
			} else { // draw new rectangle
				if (currentShape instanceof TGRectangle) {
					TGRectangle rect = (TGRectangle) currentShape;
					TGRectangle oldRect = rect;
					Rectangle r = rect.getRectangle();
					currentShape = new TGRectangle(r.x, r.y, x - r.x, y - r.y,
							rect.getImageUri());

					drawSelectionFrame(oldRect, currentShape);
				}
			}
		} else {
			Point p = sessionCanvas.screenToImage(new Point(x, y));
			for (TGShape s : sessionCanvas.getArrayListShapes()) {
				if (!s.equals(selectShape) || cursorStyle == SWT.CURSOR_CROSS) {
					if (s.contains(p.x, p.y) && s.isVisible()) {
						// deactivated because mouseDoubleClick should
						// select a TGShape.
						// sessionCanvas.setSelectRect(rectang);
						currentShape = s;
						sessionCanvas.redraw();
						break;
					}
				}
			}
			setCurrentCursor(p.x, p.y);
		}
	}

	/**
	 * Handles the mouseUp event after moving or resizing the shape.
	 * 
	 * @param evt
	 */
	protected void handleMouseUpResizeMove(MouseEvent evt) {
		currentShape = sessionCanvas.screenToImage(currentShape);

		// To solve the problem of precision loss after rounding the double
		// values
		if (!currentShape.equals(moveShape)) {
			if (currentShape instanceof TGRectangle
					&& moveShape instanceof TGRectangle) {
				TGRectangle currentRect = (TGRectangle) currentShape;
				TGRectangle moveRect = (TGRectangle) moveShape;

				if (Math.abs(currentRect.getRectangle().x
						- moveRect.getRectangle().x) == 1) {
					currentRect.getRectangle().x = moveRect.getRectangle().x;
				}

				if (Math.abs(currentRect.getRectangle().y
						- moveRect.getRectangle().y) == 1) {
					currentRect.getRectangle().y = moveRect.getRectangle().y;
				}
			}
		}

		// update the selected shape just if this shape has been
		// moved
		if (!currentShape.equals(moveShape)) {
			// <= (on mac it's always == 0)
			if (evt.count <= 1)
				sessionCanvas.setSelectedShape(currentShape);

			if (!sessionCanvas.editShapeinArrayListShapes(moveShape,
					currentShape)) {
				sessionCanvas.addShapeToArrayListShapes(currentShape);
			}

			// If the moveShape is still selected (abnormal behavior), then
			// select
			// the currentShape
			if (sessionCanvas.getSelectedShape().equals(moveShape)) {
				sessionCanvas.setSelectedShape(currentShape);
			}

			setCursor(SWT.CURSOR_SIZEALL);
			// *** this.triggerPaint();
			sessionCanvas.redraw();
		}
	}

	/**
	 * Check the moving direction base on the old position and the current
	 * position.
	 * 
	 * @param curPoint
	 *            current mouse position
	 */
	protected void checkDirection(Point curPoint) {
		int dx = curPoint.x - lastPoint.x;
		int dy = curPoint.y - lastPoint.y;
		if (dx > 0 && dy > 0) {
			direction = MOVE_SE;
		} else if (dx > 0 && dy == 0) {
			direction = MOVE_E;
		} else if (dx > 0 && dy < 0) {
			direction = MOVE_NE;
		} else if (dx == 0 && dy > 0) {
			direction = MOVE_S;
		} else if (dx == 0 && dy == 0) {
			direction = MOVE_NOMOVE;
		} else if (dx == 0 && dy < 0) {
			direction = MOVE_N;
		} else if (dx < 0 && dy > 0) {
			direction = MOVE_SW;
		} else if (dx < 0 && dy == 0) {
			direction = MOVE_W;
		} else if (dx < 0 && dy < 0) {
			direction = MOVE_NW;
		}
	}

	/**
	 * Process the move/resize event
	 * 
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 */
	protected void processMoveOrResize(int x, int y) {
		checkDirection(new Point(x, y));
		if (currentShape != null) {
			switch (cursorStyle) {
			case SWT.CURSOR_SIZEALL: // moving
				moveCurrentShape(x - lastPoint.x, y - lastPoint.y);
				lastPoint = new Point(x, y);
				return;
			case SWT.CURSOR_IBEAM:
				double angle = sessionCanvas.getSelectedShape()
						.getRotationValue();
				int diffAngle = (int) angle;

				if (x > lastPoint.x) {
					diffAngle = (int) angle + 1;
				} else {
					diffAngle = (int) angle - 1;
				}
				if (sessionCanvas.rotatingPointFlag) { // change Rectangle angle
					try {
						sessionCanvas.getSelectedShape().setRotationValue(
								(diffAngle) % 360);
						sessionCanvas.showPositionOnImagePanel();
						sessionCanvas.setPositionForSelectedShape();
					} catch (NumberFormatException e) {
						ComponentPlugin.handleWarning(e);
					}
					// sessionCanvas.redraw();
					return;
				}
			case SWT.CURSOR_HAND:
			case SWT.CURSOR_SIZEE:
			case SWT.CURSOR_SIZEW:
			case SWT.CURSOR_SIZEN:
			case SWT.CURSOR_SIZES:
			case SWT.CURSOR_SIZENE:
			case SWT.CURSOR_SIZESE:
			case SWT.CURSOR_SIZENW:
			case SWT.CURSOR_SIZESW:
				resizeCurrentShape(x - lastPoint.x, y - lastPoint.y,
						cursorStyle);
				lastPoint = new Point(x, y);
				return;
			}
		}
	}

	/**
	 * Move current shape with given offset
	 * 
	 * @param dx
	 *            offset in x direction
	 * @param dy
	 *            offset in y direction
	 */
	protected synchronized void moveCurrentShape(int dx, int dy) {
		TGShape oldShape = null;
		boolean isLinked = currentShape.isLinked();

		if (currentShape instanceof TGRectangle) {
			TGRectangle rect = (TGRectangle) currentShape;
			oldShape = new TGRectangle(rect.getRectangle(), rect.getImageUri());
			oldShape.setLinked(isLinked);
			rect.move(dx, dy);
		} else if (currentShape instanceof TGPolygon) {
			TGPolygon poly = (TGPolygon) currentShape;
			oldShape = new TGPolygon(poly.getPoints(), poly.getImageUri());
			oldShape.setLinked(isLinked);
			poly.move(dx, dy);
		} else if (currentShape instanceof TGLine) {
			TGLine line = (TGLine) currentShape;
			oldShape = new TGLine(line.getPoints(), line.getImageUri());
			oldShape.setLinked(isLinked);
			line.move(dx, dy);
		}

		drawSelectionFrame(oldShape, currentShape);
	}

	/**
	 * Resize current Shape.
	 * 
	 * @param dx
	 *            delta x
	 * @param dy
	 *            delta y
	 * @param cursor
	 *            cursor shape
	 */
	protected void resizeCurrentShape(int dx, int dy, int cursor) {
		TGShape oldShape = null;
		boolean isLinked = currentShape.isLinked();

		if (this.currentShape instanceof TGRectangle) {
			TGRectangle rect = (TGRectangle) this.currentShape;
			if (rect == null)
				return;
			oldShape = new TGRectangle(rect.getRectangle(), rect.getImageUri());
			oldShape.setLinked(isLinked);
			rect.resize(dx, dy, cursor);
		} else if (this.currentShape instanceof TGPolygon) {
			TGPolygon poly = (TGPolygon) this.currentShape;
			if (poly == null)
				return;
			oldShape = new TGPolygon(poly.getPoints(), poly.getImageUri());
			oldShape.setLinked(isLinked);
			poly.isPointAlmostOnSelectedRegion(lastPoint.x, lastPoint.y);
			poly.resize(dx, dy, cursor);
		} else if (this.currentShape instanceof TGLine) {
			TGLine line = (TGLine) this.currentShape;
			if (line == null)
				return;
			oldShape = new TGLine(line.getPoints(), line.getImageUri());
			oldShape.setLinked(isLinked);
			line.isPointAlmostOnSelectedRegion(lastPoint.x, lastPoint.y);
			line.resize(dx, dy, cursor);
		}

		int index = 0;
		for (TGShape s : sessionCanvas.getArrayListShapes()) {
			if (s.equals(oldShape)) {
				index = sessionCanvas.getIndexfromArrayListShapes(s);
				sessionCanvas.removeShapefromArrayListShapes(index, false);
			}
		}
		drawSelectionFrame(oldShape, currentShape);
	}

	/**
	 * Set the cursor based on the position of the mouse.
	 * 
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 */
	protected void setCurrentCursor(int x, int y) {
		if (currentShape == null)
			return;

		if (!currentShape.isCompletedShape()) {
			setCursor(SWT.CURSOR_CROSS);
			return;
		}

		TGShape selectShape = sessionCanvas.getSelectedShape();
		if (selectShape == null || !selectShape.isVisible())
			return;

		if (selectShape instanceof TGRectangle) {
			TGRectangle rect = (TGRectangle) selectShape;
			if (rect.getRectangle().width == 0
					|| rect.getRectangle().height == 0) {
				setCursor(SWT.CURSOR_CROSS);
				return;
			}
			// Rechtecke um die Ecken zeichnen
			// Wenn Mauszeiger in einem dieser Rechtecke oder
			// im Hauptrechteck ist - Cursor anpassen
			int h = mouseStrip;
			TGRectangle east, west, north, south, nw, ne, sw, se, center;
			TGRectangle r = rect;
			center = SWT2dUtil.absRect(new TGRectangle(r.getOuterRectangle().x
					+ h, r.getOuterRectangle().y + h,
					r.getOuterRectangle().width - 2 * h,
					r.getOuterRectangle().height - 2 * h, r.getImageUri()));
			west = SWT2dUtil.absRect(new TGRectangle(r.getOuterRectangle().x
					- h, r.getOuterRectangle().y + h, 2 * h + 1, r
					.getOuterRectangle().height - 2 * h, r.getImageUri()));
			east = SWT2dUtil.absRect(new TGRectangle(r.getOuterRectangle().x
					+ r.getOuterRectangle().width - h, r.getOuterRectangle().y
					+ h, 2 * h + 1, r.getOuterRectangle().height - 2 * h, r
					.getImageUri()));
			north = SWT2dUtil.absRect(new TGRectangle(r.getOuterRectangle().x
					+ h, r.getOuterRectangle().y - h,
					r.getOuterRectangle().width - 2 * h, 2 * h + 1, r
							.getImageUri()));
			south = SWT2dUtil.absRect(new TGRectangle(r.getOuterRectangle().x
					+ h, r.getOuterRectangle().y + r.getOuterRectangle().height
					- h, r.getOuterRectangle().width - 2 * h, 2 * h + 1, r
					.getImageUri()));
			nw = SWT2dUtil.absRect(new TGRectangle(r.getOuterRectangle().x - h,
					r.getOuterRectangle().y - h, 2 * h + 1, 2 * h + 1, r
							.getImageUri()));
			ne = SWT2dUtil.absRect(new TGRectangle(r.getOuterRectangle().x
					+ r.getOuterRectangle().width - h, r.getOuterRectangle().y
					- h, 2 * h + 1, 2 * h + 1, r.getImageUri()));
			sw = SWT2dUtil.absRect(new TGRectangle(r.getOuterRectangle().x - h,
					r.getOuterRectangle().y + r.getOuterRectangle().height - h,
					2 * h + 1, 2 * h + 1, r.getImageUri()));
			se = SWT2dUtil.absRect(new TGRectangle(r.getOuterRectangle().x
					+ r.getOuterRectangle().width - h, r.getOuterRectangle().y
					+ r.getOuterRectangle().height - h, 2 * h + 1, 2 * h + 1, r
					.getImageUri()));

			if (center.contains(x, y)) {
				if (sessionCanvas.rotatingPointFlag) {
					setCursor(SWT.CURSOR_IBEAM);
				} else
					setCursor(SWT.CURSOR_SIZEALL);
			} else {
				if (east.contains(x, y))
					setCursor(SWT.CURSOR_SIZEE);
				else if (west.contains(x, y))
					setCursor(SWT.CURSOR_SIZEW);
				else if (north.contains(x, y))
					setCursor(SWT.CURSOR_SIZEN);
				else if (south.contains(x, y))
					setCursor(SWT.CURSOR_SIZES);
				else if (ne.contains(x, y))
					setCursor(SWT.CURSOR_SIZENE);
				else if (nw.contains(x, y))
					setCursor(SWT.CURSOR_SIZENW);
				else if (se.contains(x, y))
					setCursor(SWT.CURSOR_SIZESE);
				else if (sw.contains(x, y))
					setCursor(SWT.CURSOR_SIZESW);
				else
					setCursor(SWT.CURSOR_CROSS);
			}
		} else if (selectShape instanceof TGPolygon) {
			TGPolygon poly = (TGPolygon) selectShape;

			if (poly == null) {
				setCursor(SWT.CURSOR_CROSS);
				return;
			}

			if (poly.size() == 0) {
				setCursor(SWT.CURSOR_CROSS);
				return;
			}

			if (poly.isPointAlmostOnSelectedRegion(x, y)) {
				setCursor(SWT.CURSOR_HAND);
			}
			// else if (poly.isPointOnBoundary(x, y)) {
			// setCursor(SWT.CURSOR_HAND);
			// }
			else if (poly.contains(x, y)) {
				setCursor(SWT.CURSOR_SIZEALL);
			} else {
				setCursor(SWT.CURSOR_CROSS);
			}
		} else if (selectShape instanceof TGLine) {
			TGLine line = (TGLine) selectShape;

			if (line == null) {
				setCursor(SWT.CURSOR_CROSS);
				return;
			}

			if (line.size() == 0) {
				setCursor(SWT.CURSOR_CROSS);
				return;
			}

			if (line.isPointAlmostOnSelectedRegion(x, y)) {
				setCursor(SWT.CURSOR_HAND);
			} else if (line.contains(x, y)) {
				setCursor(SWT.CURSOR_SIZEALL);
			} else {
				setCursor(SWT.CURSOR_CROSS);
			}
		}
	}

	/**
	 * Draw the selection frame.
	 * 
	 * @param oldRect
	 *            previous shape needed to be erase.
	 * @param newRect
	 *            new shape needed to be drawn.
	 */
	synchronized protected void drawSelectionFrame(TGShape oldShape,
			TGShape newShape) {

		if (roiGC == null || newShape == null)
			return;

		if (newShape instanceof TGRectangle) {

			TGRectangle oldRect = null;
			if (oldShape != null)
				oldRect = (TGRectangle) oldShape;

			sessionCanvas.setNormalGC(true);

			// a potential bug. Hope it's not because of me.
			if (oldRect != null && oldRect.isCompletedShape()) {
				oldRect = SWT2dUtil.absRect(oldRect);
				sessionCanvas.drawFrame(roiGC, oldRect, true, false);
			}

			TGRectangle newRect = (TGRectangle) newShape;

			if (newRect != null && newRect.isCompletedShape()) {
				newRect = SWT2dUtil.absRect(newRect);
				sessionCanvas.drawFrame(roiGC, newRect, false, false);
			}

			sessionCanvas.setNormalGC(false);

		} else if (newShape instanceof TGPolygon) {

			TGPolygon oldPoly = null;
			if (oldShape != null)
				oldPoly = (TGPolygon) oldShape;

			if (oldPoly != null && oldPoly.isCompletedShape()) {
				sessionCanvas.drawFrame(roiGC, oldPoly, true);
			}

			TGPolygon newPoly = (TGPolygon) newShape;

			if (newPoly != null && newPoly.size() > 0) {
				sessionCanvas.drawFrame(roiGC, newPoly, false);
			}
		} else if (newShape instanceof TGLine) {

			TGLine oldLine = null;
			if (oldShape != null)
				oldLine = (TGLine) oldShape;

			if (oldLine != null && oldLine.isCompletedShape()) {
				sessionCanvas.drawFrame(roiGC, oldLine, true);
			}

			TGLine newLine = (TGLine) newShape;

			if (newLine != null && newLine.size() > 0) {
				sessionCanvas.drawFrame(roiGC, newLine, false);
			}
		}
	}
}