/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.mip.component.settings;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.ui.PlatformUI;

import com.swtdesigner.ResourceManager;

/**
 * Instances of this class are GUIParts; they will show a Settings- Dialog for
 * DockingLines
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * @author leuk
 * 
 */
public class GridLineSettingsDialog extends TitleAreaDialog {

	private GridLineSettings gLSettings;

	private Label lineColorLabel;
	private Label selectedLineColorLabel;

	private RGB unSelectedLineRGB;
	private RGB selectedLineRGB;
	private int lineWidth;
	private int lineSpacing;
	private int lineStyle;
	private String lineStyleString;

	private Spinner spinnerLineWidth;
	private Spinner spinnerLineSpacing;

	private Combo lineStylecombo;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public GridLineSettingsDialog(Shell parentShell, GridLineSettings gLSett) {
		super(parentShell);
		this.gLSettings = gLSett;
		inititialize();
		setHelpAvailable(false);
	}

	/**
	 * initialize Grid Line Settings
	 * 
	 * @param parentShell
	 */
	private void inititialize() {
		this.selectedLineRGB = this.gLSettings.getSelectedLineRGB();
		this.unSelectedLineRGB = this.gLSettings.getUnSelectedLineRGB();
		this.lineWidth = this.gLSettings.getGridLineWidth();
		this.lineSpacing = this.gLSettings.getGridLineSpacing();
		this.lineStyle = this.gLSettings.getGridLineStyle();
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		inititialize();

		setTitle(Messages.GridLineSettingsDialog_DockingLineSettings);
		setMessage(Messages.GridLineSettingsDialog_ConfiguereSettingsForDisplayingDockingLines);
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayout(new GridLayout(7, false));
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		lineStylecombo = new Combo(container, SWT.NONE);
		lineStylecombo
				.setToolTipText(Messages.GridLineSettingsDialog_ChooseTheLineStyle);
		lineStylecombo.setVisibleItemCount(5);
		lineStylecombo.setItems(new String[] { "Dot", "Solid", "Dash", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				"DashDot", "DashDotDot" }); //$NON-NLS-1$ //$NON-NLS-2$
		{
			GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false,
					false, 1, 1);
			gridData.widthHint = 89;
			lineStylecombo.setLayoutData(gridData);
		}
		lineStylecombo.setText(getLineStyle2String());

		Label lblLineStyle = new Label(container, SWT.NONE);
		lblLineStyle.setText(Messages.GridLineSettingsDialog_LineStyle);
		new Label(container, SWT.NONE);

		Composite composite = new Composite(container, SWT.NONE);
		{
			GridData gridData = new GridData(SWT.RIGHT, SWT.CENTER, true,
					false, 4, 3);
			gridData.widthHint = 138;
			gridData.heightHint = 78;
			composite.setLayoutData(gridData);
		}
		composite.setLayout(null);
		{
			selectedLineColorLabel = new Label(composite, SWT.NONE);
			selectedLineColorLabel
					.setToolTipText(Messages.GridLineSettingsDialog_ChooseTheSelectedLineColor);
			selectedLineColorLabel.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseUp(MouseEvent e) {
					// System.out.println("Choose Background Color");
					getColorDialog(2);
				}
			});
			selectedLineColorLabel.setBounds(28, 19, 49, 51);
			selectedLineColorLabel.setBackground(new Color(PlatformUI
					.getWorkbench().getDisplay(), selectedLineRGB));
		}
		{
			lineColorLabel = new Label(composite, SWT.NONE);
			lineColorLabel.setToolTipText(Messages.GridLineSettingsDialog_ChooseTheLineColor);
			lineColorLabel.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseUp(MouseEvent e) {
					// System.out.println("Choose Foreground Color");
					getColorDialog(1);
				}
			});
			lineColorLabel.setBackground(new Color(PlatformUI.getWorkbench()
					.getDisplay(), unSelectedLineRGB));
			lineColorLabel.setBounds(0, 0, 49, 51);
		}
		{
			Label labelSwitchColor = new Label(composite, SWT.NONE);
			labelSwitchColor.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseUp(MouseEvent e) {
					switchColor();
				}
			});
			labelSwitchColor.setImage(ResourceManager.getPluginImage(
					"info.textgrid.lab.linkeditor.mip.component", //$NON-NLS-1$
					"icons/Switch12.gif")); //$NON-NLS-1$
			labelSwitchColor.setBounds(55, 0, 22, 20);
		}

		spinnerLineWidth = new Spinner(container, SWT.BORDER);
		spinnerLineWidth
				.setToolTipText(Messages.GridLineSettingsDialog_ChooseTheLineWidth);
		spinnerLineWidth.setMaximum(5);
		spinnerLineWidth.setMinimum(1);
		spinnerLineWidth.setSelection(lineWidth);

		Label lblLineWidth = new Label(container, SWT.NONE);
		lblLineWidth.setText(Messages.GridLineSettingsDialog_LineWidth);
		new Label(container, SWT.NONE);

		spinnerLineSpacing = new Spinner(container, SWT.BORDER);
		spinnerLineSpacing
				.setToolTipText(Messages.GridLineSettingsDialog_ChooseTheSpacing);
		spinnerLineSpacing.setIncrement(2);
		spinnerLineSpacing.setMaximum(150);
		spinnerLineSpacing.setMinimum(10);
		spinnerLineSpacing.setSelection(this.lineSpacing);
		spinnerLineSpacing.setEnabled(true);

		Label lblLineSpacing = new Label(container, SWT.NONE);
		lblLineSpacing.setText(Messages.GridLineSettingsDialog_LineSpacing);
		new Label(container, SWT.NONE);

		// This is for the case that you want edit selected/unselected Lines
		// separate
		// Group grpToApplyFor = new Group(container, SWT.NONE);
		// grpToApplyFor.setText("to apply to");
		// {
		// GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false,
		// 3, 1);
		// gridData.heightHint = 50;
		// gridData.widthHint = 185;
		// grpToApplyFor.setLayoutData(gridData);
		// }
		// {
		// Button btnSelectedLines = new Button(grpToApplyFor, SWT.CHECK);
		// btnSelectedLines.setBounds(43, 25, 113, 16);
		// btnSelectedLines.setSelection(true);
		// btnSelectedLines.setText("selected Line(s)");
		// }
		// {
		// Button btnUnselectedLines = new Button(grpToApplyFor, SWT.CHECK);
		// btnUnselectedLines.setBounds(43, 47, 113, 16);
		// btnUnselectedLines.setSelection(true);
		// btnUnselectedLines.setText("unSelected Line(s)");
		// }
		// new Label(container, SWT.NONE);
		// new Label(container, SWT.NONE);
		// new Label(container, SWT.NONE);

		Composite composite_1 = new Composite(container, SWT.NONE);
		composite_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		composite_1.setLayout(null);

		Button btnRestoredefaults = new Button(composite_1, SWT.NONE);
		btnRestoredefaults.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				resetGridLineSettings();
			}
		});
		btnRestoredefaults.setBounds(10, 41, 156, 23);
		btnRestoredefaults.setText(Messages.GridLineSettingsDialog_RestoreDefaults);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		return area;
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Button button_OK = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		button_OK.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		button_OK.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				updateGLSettingsInstance();
			}
		});
		Button button = createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
		button.setGrayed(true);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(450, 300);
	}

	/**
	 * Create Color-Dialog.
	 */
	private void getColorDialog(int art) {
		ColorDialog colorDialog = new ColorDialog(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getShell());
		if (art == 1) {
			// Change the title bar text
			colorDialog.setText(Messages.GridLineSettingsDialog_ChooseLineColor);
		} else {
			// Change the title bar text
			colorDialog.setText(Messages.GridLineSettingsDialog_ChooseSelectedLineColor);
		}
		RGB selectedColor = colorDialog.open();
		if (selectedColor != null) {
			if (art == 1) {
				// Change the title bar text
				colorDialog.setText(Messages.GridLineSettingsDialog_ChooseLineColor);
				this.unSelectedLineRGB = colorDialog.getRGB();
				this.lineColorLabel.setBackground(new Color(PlatformUI
						.getWorkbench().getDisplay(), unSelectedLineRGB));
			} else {
				// Change the title bar text
				colorDialog.setText(Messages.GridLineSettingsDialog_ChooseSelectedLineColor);
				this.selectedLineRGB = colorDialog.getRGB();
				this.selectedLineColorLabel.setBackground(new Color(PlatformUI
						.getWorkbench().getDisplay(), selectedLineRGB));
			}
		}

	}

	/**
	 * Return the int value of the linestyle.
	 */
	private int getLineStyle(String style) {
		if (style.equals("Dot")) { //$NON-NLS-1$
			lineStyle = SWT.LINE_DOT;
		} else if (style.equals("Solid")) { //$NON-NLS-1$
			lineStyle = SWT.LINE_SOLID;
		} else if (style.equals("Dash")) { //$NON-NLS-1$
			lineStyle = SWT.LINE_DASH;
		} else if (style.equals("DashDot")) { //$NON-NLS-1$
			lineStyle = SWT.LINE_DASHDOT;
		} else if (style.equals("DashDotDot")) { //$NON-NLS-1$
			lineStyle = SWT.LINE_DASHDOTDOT;
		}
		return lineStyle;
	}

	/**
	 * Return the String for the lineStyle comboBox
	 */
	private String getLineStyle2String() {
		if (lineStyle == SWT.LINE_DOT) {
			lineStyleString = "DOT"; //$NON-NLS-1$
		} else if (lineStyle == SWT.LINE_SOLID) {
			lineStyleString = "Solid"; //$NON-NLS-1$
		} else if (lineStyle == SWT.LINE_DASH) {
			lineStyleString = "Dash"; //$NON-NLS-1$
		} else if (lineStyle == SWT.LINE_DASHDOT) {
			lineStyleString = "DashDot"; //$NON-NLS-1$
		} else if (lineStyle == SWT.LINE_DASHDOTDOT) {
			lineStyleString = "DashDotDot"; //$NON-NLS-1$
		}
		return lineStyleString;
	}

	/**
	 * Set the default Values.
	 */
	private void setDefaultValues() {
		Object[] ergArr;
		try {
			ergArr = this.gLSettings.getDefaultValues();
			this.unSelectedLineRGB = (RGB) ergArr[0];
			this.selectedLineRGB = (RGB) ergArr[1];
			this.lineWidth = (Integer) ergArr[2];
			this.lineSpacing = (Integer) ergArr[3];
			this.lineStyle = (Integer) ergArr[4];
			updateDialogGUI();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Update Dialog GUI.
	 */
	private void updateDialogGUI() {
		this.selectedLineColorLabel.setBackground(new Color(PlatformUI
				.getWorkbench().getDisplay(), this.selectedLineRGB));
		this.lineColorLabel.setBackground(new Color(PlatformUI.getWorkbench()
				.getDisplay(), this.unSelectedLineRGB));
		this.lineStylecombo.setText(this.lineStyleString);
		this.spinnerLineWidth.setSelection(this.lineWidth);
		this.spinnerLineSpacing.setSelection(this.lineSpacing);

	}

	/**
	 * Set the default Values.
	 */
	private void resetGridLineSettings() {
		setDefaultValues();
		this.gLSettings.reset();
	}

	/**
	 * Update glSettings instance
	 */
	private void updateGLSettingsInstance() {
		gLSettings.setGridLineWidth(spinnerLineWidth.getSelection());
		gLSettings.setGridLineSpacing(spinnerLineSpacing.getSelection());
		gLSettings.setGridLineStyle(getLineStyle(lineStylecombo.getText()));
		gLSettings.setSelectedLineRGB(selectedLineRGB);
		gLSettings.setUnSelectedLineRGB(unSelectedLineRGB);
	}

	/**
	 * Switch between selected- and unselectedColor
	 */
	private void switchColor() {
		Color help = lineColorLabel.getBackground();
		lineColorLabel.setBackground(selectedLineColorLabel.getBackground());
		selectedLineColorLabel.setBackground(help);
		selectedLineRGB = selectedLineColorLabel.getBackground().getRGB();
		unSelectedLineRGB = lineColorLabel.getBackground().getRGB();
	}
}
