/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.views;

import java.util.Vector;

import org.eclipse.swt.graphics.Image;

/**
 * Mediator for IImageSurface and IThumbView. It is used to loose the coupling
 * between IImageSurface and IThumbView.
 * <p/>
 * By using mediator, the IImageSurface need not know the IThumbView and its
 * state, it just register to mediator. The IThumbView need not to know the
 * IImageSurfaces and their state. It just show the image of the IImageSurface.
 * <p/>
 * 
 * @author Chengdong Li
 */
public class ThumbViewMediator {
	static IThumbView thumbView;
	static IImageSurface imageSurf;
	static Vector surfs = new Vector();

	/**
	 * Register IThumbView or IImageSurface.
	 * 
	 * @param obj
	 *            object (either IThumbView or IImageSurface).
	 */
	static public void register(Object obj) {
		if (obj instanceof ThumbView) {
			thumbView = (ThumbView) obj;
		} else if (obj instanceof IImageSurface) {
			addSurf((IImageSurface) obj);
		}
	}

	/**
	 * Get the current imageSurface.
	 * 
	 * @return {@link IImageSurface}
	 */
	static public IImageSurface getImageSurface() {
		return imageSurf;
	}

	/**
	 * Unregister IThumbView or IImageSurface.
	 * 
	 * @param obj
	 *            object (either IThumbView or IImageSurface).
	 */
	static public void unRegister(Object obj) {
		if (obj instanceof ThumbView) {
			thumbView = null;
		} else if (obj instanceof IImageSurface) {
			imageSurf = null;
			if (thumbView != null) {
				if (thumbView.isDisposed())
					return;
				thumbView.setImage(null, 0, 0, 1, 1, 1, 1);
				thumbView.repaint();
			}
			removeSurf((IImageSurface) obj);
		}
	}

	/**
	 * Get IImageSurface source Image.
	 * 
	 * @return source image of IImageSurface.
	 */
	static public Image getISImage() {
		if (imageSurf == null)
			return null;
		return imageSurf.getImage();
	}

	/**
	 * Called when IImageSurface scrolls image.
	 */
	static public void onISScroll() {
		imageSurf = getActiveSurf();
		if (thumbView == null)
			return;
		if (imageSurf == null)
			return;
		if (imageSurf.getImage() == null || imageSurf.isDisposed())
			return;

		double tx = imageSurf.getTX();
		double ty = imageSurf.getTY();
		double sx = imageSurf.getSX();
		double sy = imageSurf.getSY();
		double scaleX = imageSurf.getScaleX();
		double scaleY = imageSurf.getScaleY();
		thumbView.onISScroll(tx, ty, sx, sy);
		thumbView.synchronizeZoom(scaleX, scaleY);
	}

	/**
	 * Called when IImageSurface zooms image.
	 */
	static public void onISZoom() {
		imageSurf = getActiveSurf();
		if (thumbView == null)
			return;
		if (imageSurf == null)
			return;
		if (imageSurf.getImage() == null || imageSurf.isDisposed())
			return;

		double sx = imageSurf.getScaleX();
		double sy = imageSurf.getScaleY();
		thumbView.onISZoom(sx, sy);
	}

	/**
	 * Called when ThumbView scrolls image.
	 * 
	 * @param x
	 *            the relative ratio of slider position to image in X.
	 * @param y
	 *            teh relative ratio of slider position to image in Y.
	 */
	static public void onTVScroll(double x, double y) {
		if (imageSurf == null || thumbView == null)
			return;
		imageSurf.onTVScroll(x, y);
	}

	/**
	 * Called when ThumbView zooms image.
	 * 
	 * @param sx
	 *            the relative ratio of slider position to image in X.
	 * @param sy
	 *            teh relative ratio of slider position to image in Y.
	 */
	static public void onTVZoom(double sx, double sy) {
		if (imageSurf == null || thumbView == null)
			return;
		imageSurf.onTVZoom(sx, sy);
	}

	/*
	 * Add IImageSurface into surfs.
	 */
	static private void addSurf(IImageSurface surf) {
		for (int i = 0; i < surfs.size(); i++) {
			if (surf.isDisposed())
				surfs.remove(i);
			else if (surf == surfs.elementAt(i)) {
				surfs.remove(surf);
				break;
			}
		}
		surfs.insertElementAt(surf, 0);
		imageSurf = surf;
		if (thumbView != null) {
			if (thumbView.isDisposed())
				return;
			thumbView.setImage(surf.getImage(), surf.getTX(), surf.getTY(),
					surf.getSX(), surf.getSY(), surf.getScaleX(),
					surf.getScaleY());
			thumbView.repaint();
		}
	}

	/*
	 * Remove IImageSurface from surfs.
	 */
	static private void removeSurf(IImageSurface surf) {
		for (int i = 0; i < surfs.size(); i++) {
			IImageSurface s = (IImageSurface) surfs.elementAt(i);
			if (s.isDisposed() || surf == s)
				surfs.remove(i);
		}
	}

	/*
	 * Get active IImageSurface from surfs.<p/> Here we also maintain a dynamic
	 * array, which always keeps the focused or activated surface on the
	 * first.<p/> If we do not do this, it won't recognize the focusControl,
	 * since SWT may focus serveral surface simultaneously, and just put the
	 * most important one on the first. <p/> You can use the isFocusControl()
	 * and print all the return true control, there are serverals at the same
	 * time.
	 */
	static private IImageSurface getActiveSurf() {
		IImageSurface vSurf = null;
		IImageSurface fSurf = null;
		for (int i = 0; i < surfs.size(); i++) {
			IImageSurface surf = (IImageSurface) surfs.elementAt(i);
			if (!surf.isDisposed()) {
				if (surf.isFocused()) {
					fSurf = surf;
					surfs.remove(surf);
					surfs.insertElementAt(surf, 0);
				} else if (surf.isVisible() && vSurf == null) {
					vSurf = surf;
				}
			}
		}
		imageSurf = null;
		if (fSurf != null)
			imageSurf = fSurf;
		else if (vSurf != null)
			imageSurf = vSurf;
		return imageSurf;
	}
}
