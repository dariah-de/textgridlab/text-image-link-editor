/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.mip.component.canvas;

import org.eclipse.osgi.util.NLS;

/**
 * The Messages class for the Sessioned Image Canvas.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * @author leuk
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.linkeditor.mip.component.canvas.messages"; //$NON-NLS-1$
	public static String AffineImageCanvas_41;
	public static String AffineImageCanvas_ContextMenue;
	public static String AffineImageCanvas_FitHorizontally;
	public static String AffineImageCanvas_FitVertically;
	public static String AffineImageCanvas_FitWindow;
	public static String AffineImageCanvas_FlipHorizontally;
	public static String AffineImageCanvas_FlipVertically;
	public static String AffineImageCanvas_ImageSize;
	public static String AffineImageCanvas_LoadImage;
	public static String AffineImageCanvas_OpenImageFile;
	public static String AffineImageCanvas_RotateAgainstClockWise;
	public static String AffineImageCanvas_RotateClockWise;
	public static String AffineImageCanvas_SaveImage;
	public static String AffineImageCanvas_Scrolling;
	public static String AffineImageCanvas_ShowOriginally;
	public static String SessionedImageCanvas_Action_CloneActiveRectangleWithAngle;
	public static String SessionedImageCanvas_Action_CloneActiveRectangleRightToLeft;
	public static String SessionedImageCanvas_Action_CloneActiveRectangleDownToUp;
	public static String SessionedImageCanvas_Action_CenterSelection;
	public static String SessionedImageCanvas_Action_AddNewLink;
	public static String SessionedImageCanvas_Action_Unlink;
	public static String SessionedImageCanvas_Action_DelAllPoly;
	public static String SessionedImageCanvas_Action_DelAllRect;
	public static String SessionedImageCanvas_Action_DelAllShapes;
	public static String SessionedImageCanvas_Action_DelSelPoly;
	public static String SessionedImageCanvas_Action_DelSelRec;
	public static String SessionedImageCanvas_Action_JumpToText;
	public static String SessionedImageCanvas_Action_CloneActiveRectangleHorizontally;
	public static String SessionedImageCanvas_Action_CloneActiveRectangleVertically;
	public static String SessionedImageCanvas_Action_TextWritingMode;
	public static String SessionedImageCanvas_Action_ShowHideWritingMode;
	public static String SessionedImageCanvas_Action_SetDefaultTextWritingMode;
	public static String SessionedImageCanvas_Action_SetTextWritingModeForSelectedShape;
	public static String SessionedImageCanvas_Action_SetShapeRotationForSelectedShape;
	public static String SessionedImageCanvas_ActivateOrDeactivateShapeRotation;
	public static String SessionedImageCanvas_CloneRecztangle;
	public static String SessionedImageCanvas_DeleteAllLines;
	public static String SessionedImageCanvas_DockingLines;
	public static String SessionedImageCanvas_EditLayers;
	public static String SessionedImageCanvas_EditLayersForSelectedShapesOnly;
	public static String SessionedImageCanvas_lr;
	public static String SessionedImageCanvas_lr_tb;
	public static String SessionedImageCanvas_lr2;
	public static String SessionedImageCanvas_Mess_DelAllLinks_1;
	public static String SessionedImageCanvas_Mess_DelAllLinks_2;
	public static String SessionedImageCanvas_Mess_DelAllPoly_1;
	public static String SessionedImageCanvas_Mess_DelAllPoly_2;
	public static String SessionedImageCanvas_Mess_DelAllRecLinks_1;
	public static String SessionedImageCanvas_Mess_DelAllRecLinks_2;
	public static String SessionedImageCanvas_Mess_DelSelLinks_1;
	public static String SessionedImageCanvas_Mess_DelSelLinks_2;
	public static String SessionedImageCanvas_Mess_DelSelPoly_1;
	public static String SessionedImageCanvas_Mess_DelSelPoly_2;
	public static String SessionedImageCanvas_Mess_DelDockingLines_1;
	public static String SessionedImageCanvas_Mess_DelDockingLines_2;
	public static String SessionedImageCanvas_Mess_setAlignment;
	public static String SessionedImageCanvas_Mess_setHorizontalAlignment;
	public static String SessionedImageCanvas_Mess_setVerticalAlignment;
	public static String SessionedImageCanvas_None;
	public static String SessionedImageCanvas_rl;
	public static String SessionedImageCanvas_rl_tb;
	public static String SessionedImageCanvas_SaveAllLines;
	public static String SessionedImageCanvas_Settings;
	public static String SessionedImageCanvas_ShapeRotation;
	public static String SessionedImageCanvas_ShowAllLayers;
	public static String SessionedImageCanvas_ShowOrHideShapeRotation;
	public static String SessionedImageCanvas_SowOrHideAllLines;
	public static String SessionedImageCanvas_tb;
	public static String SessionedImageCanvas_tb_rl;
	public static String SessionedImageCanvas_TextLayers;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
