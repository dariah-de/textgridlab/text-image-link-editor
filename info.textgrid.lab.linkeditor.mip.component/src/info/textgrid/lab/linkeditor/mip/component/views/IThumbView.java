/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.views;

import org.eclipse.swt.graphics.Image;

/**
 * A thumb view: the thumb view of the M(Mediator)S(Surface)T(ThumbView) model.
 * 
 * @author Chengdong Li
 */
public interface IThumbView {
	/**
	 * Called when Image Surface is zoomed.
	 * 
	 * @param sx
	 *            x direction zoom ratio (absolute value, not increment).
	 * @param sy
	 *            y direction zoom ratio (absolute value, not increment).
	 */
	public void onISZoom(double sx, double sy);

	/**
	 * Called when Image Surface is scrolled.
	 * 
	 * @param tx
	 *            x translation ratio (ralative to thumbnail) of red rectangle.
	 * @param ty
	 *            y translation ratio (ralative to thumbnail) of red rectangle.
	 * @param sx
	 *            x scale (relative to thumbnail image) of red rectangle.
	 * @param sy
	 *            y scale (relative to thumbnail image) of red rectangle.
	 */
	public void onISScroll(double tx, double ty, double sx, double sy);

	/**
	 * Called for synchronize zoom slider.
	 * 
	 * @param scaleX
	 *            x zoom scale in X direction.
	 * @param scaleY
	 *            y zoom scale in Y direction.
	 */
	public void synchronizeZoom(double scaleX, double scaleY);

	/**
	 * Called to repaint the Thumb View. For example, when the size of Thumb
	 * View has changed.
	 */
	public void repaint();

	/**
	 * Called to set a new image for the Thumb View.
	 * 
	 * @param image
	 *            a new Image to be set for Thumb View.
	 */
	public void setImage(Image image, double tx, double ty, double sx,
			double sy, double scaleX, double scaleY);

	/**
	 * Check up the validity of the Thumb View.
	 * 
	 * @return true: if Thumb View is disposed; false: TV not disposed.
	 */
	public boolean isDisposed();
}
