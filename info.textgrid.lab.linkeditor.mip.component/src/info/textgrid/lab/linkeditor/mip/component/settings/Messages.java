package info.textgrid.lab.linkeditor.mip.component.settings;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.linkeditor.mip.component.settings.messages"; //$NON-NLS-1$
	public static String GridLineSettingsDialog_ChooseLineColor;
	public static String GridLineSettingsDialog_ChooseSelectedLineColor;
	public static String GridLineSettingsDialog_ChooseTheLineColor;
	public static String GridLineSettingsDialog_ChooseTheLineStyle;
	public static String GridLineSettingsDialog_ChooseTheLineWidth;
	public static String GridLineSettingsDialog_ChooseTheSelectedLineColor;
	public static String GridLineSettingsDialog_ChooseTheSpacing;
	public static String GridLineSettingsDialog_ConfiguereSettingsForDisplayingDockingLines;
	public static String GridLineSettingsDialog_DockingLineSettings;
	public static String GridLineSettingsDialog_LineSpacing;
	public static String GridLineSettingsDialog_LineStyle;
	public static String GridLineSettingsDialog_LineWidth;
	public static String GridLineSettingsDialog_RestoreDefaults;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
