/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.mip.component.layer_ui;

import info.textgrid.lab.linkeditor.controller.Activator;
import info.textgrid.lab.linkeditor.controller.LinkEditorController;
import info.textgrid.lab.linkeditor.mip.component.canvas.SessionedImageCanvas;
import info.textgrid.lab.linkeditor.model.graphics.TGLayer;
import info.textgrid.lab.linkeditor.model.graphics.TGLayerManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.swtdesigner.SWTResourceManager;

public class EditTGLayer_UI extends TitleAreaDialog {
	private static final int NEW_ID = IDialogConstants.CLIENT_ID + 1;
	private LinkEditorController controller = LinkEditorController
			.getInstance();
	protected SessionedImageCanvas instance;
	private Table table;
	private TGLayerManager sessionLayerManager;
	private Text text_Name;
	private Label label_Nr;
	private Label label_Color;
	private Button button_visibility;
	private Button button_active;
	private int activeLayerNumber = 0;
	private String activeLayerName = ""; //$NON-NLS-1$
	private RGB activeLayerRGB = new RGB(255, 255, 255);
	private int selectedLayerNumber = 0;

	private boolean activeFlag = false;
	private boolean visibilityFlag = false;
	private boolean nameFlag = false;
	private boolean colorFlag = false;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 * @param instance
	 * @param layerManager
	 */
	public EditTGLayer_UI(Shell parentShell) {
		super(parentShell);
	}

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 * @wbp.parser.constructor
	 */
	public EditTGLayer_UI(Shell parentShell,
			TGLayerManager sessionLayerManager, SessionedImageCanvas instance) {
		super(parentShell);
		this.instance = instance;
		this.sessionLayerManager = sessionLayerManager;
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		setTitle(Messages.EditTGLayer_UI_LayerEditor);
		setMessage(Messages.EditTGLayer_UI_EditTheLayerPreferences);

		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayout(new GridLayout(1, false));
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		Composite composite_table = new Composite(container, SWT.NONE);
		composite_table.setLayout(new GridLayout(1, false));
		GridData gd_composite_table = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_composite_table.heightHint = 148;
		gd_composite_table.widthHint = 434;
		composite_table.setLayoutData(gd_composite_table);

		table = new Table(composite_table, SWT.BORDER | SWT.FULL_SELECTION);
		table.addMouseTrackListener(new MouseTrackAdapter() {
			@Override
			public void mouseHover(MouseEvent e) {
				table.setFocus();
			}
		});
		table.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseScrolled(MouseEvent e) {
				table.setSelection(table.getItemCount());
			}
		});
		table.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int[] selections = table.getSelectionIndices();
				int selectionedNr = new Integer(table.getItem(selections[0])
						.getText());

				selectedLayerNumber = new Integer(table.getItem(selections[0])
						.getText());
				fillEditorArea(selectionedNr);
			}
		});
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableColumn tblclmnNr = new TableColumn(table, SWT.CENTER);
		tblclmnNr.setWidth(30);
		tblclmnNr.setText(Messages.EditTGLayer_UI_Nr);

		TableColumn tblclmnName = new TableColumn(table, SWT.LEFT);
		tblclmnName.setWidth(145);
		tblclmnName.setText(Messages.EditTGLayer_UI_Name);

		TableColumn tblclmnColor = new TableColumn(table, SWT.CENTER);
		tblclmnColor.setWidth(45);
		tblclmnColor.setText(Messages.EditTGLayer_UI_Color);

		TableColumn tblclmnVisibility = new TableColumn(table, SWT.CENTER);
		tblclmnVisibility.setWidth(100);
		tblclmnVisibility.setText(Messages.EditTGLayer_UI_Visibility);

		TableColumn tblclmnActive = new TableColumn(table, SWT.CENTER);
		tblclmnActive.setWidth(100);
		tblclmnActive.setText(Messages.EditTGLayer_UI_Active);

		// Create all Layer Lines
		createTableLines();

		Composite composite = new Composite(container, SWT.NONE);
		GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, false, false,
				1, 1);
		gd_composite.widthHint = 412;
		composite.setLayoutData(gd_composite);
		composite.setLayout(new GridLayout(1, false));

		Composite composite_layer = new Composite(composite, SWT.BORDER);
		GridData gd_composite_layer = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_composite_layer.widthHint = 420;
		composite_layer.setLayoutData(gd_composite_layer);
		composite_layer.setLayout(new GridLayout(5, false));
		Label lblNr = new Label(composite_layer, SWT.NONE);
		lblNr.setAlignment(SWT.CENTER);
		GridData gd_lblNr = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1,
				1);
		gd_lblNr.widthHint = 19;
		lblNr.setLayoutData(gd_lblNr);
		lblNr.setText(Messages.EditTGLayer_UI_Nr);
		Label lblName = new Label(composite_layer, SWT.NONE);
		GridData gd_lblName = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		gd_lblName.widthHint = 139;
		lblName.setLayoutData(gd_lblName);
		lblName.setText(Messages.EditTGLayer_UI_Name);
		Label lblColor = new Label(composite_layer, SWT.NONE);
		lblColor.setAlignment(SWT.CENTER);
		GridData gd_lblColor = new GridData(SWT.LEFT, SWT.CENTER, false, false,
				1, 1);
		gd_lblColor.widthHint = 35;
		lblColor.setLayoutData(gd_lblColor);
		lblColor.setText(Messages.EditTGLayer_UI_Color);
		Label lblVisibility = new Label(composite_layer, SWT.NONE);
		lblVisibility.setAlignment(SWT.CENTER);
		GridData gd_lblVisibility = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_lblVisibility.widthHint = 96;
		lblVisibility.setLayoutData(gd_lblVisibility);
		lblVisibility.setText(Messages.EditTGLayer_UI_Visibility);
		Label lblActive = new Label(composite_layer, SWT.NONE);
		GridData gd_lblActive = new GridData(SWT.LEFT, SWT.CENTER, false,
				false, 1, 1);
		gd_lblActive.widthHint = 93;
		lblActive.setLayoutData(gd_lblActive);
		lblActive.setAlignment(SWT.CENTER);
		lblActive.setText(Messages.EditTGLayer_UI_Active);
		label_Nr = new Label(composite_layer, SWT.HORIZONTAL | SWT.CENTER);
		label_Nr.setAlignment(SWT.CENTER);
		GridData gd_label_Nr = new GridData(SWT.CENTER, SWT.CENTER, false,
				false, 1, 1);
		gd_label_Nr.widthHint = 19;
		label_Nr.setLayoutData(gd_label_Nr);
		label_Nr.setText(new Integer(sessionLayerManager.getNextNumber())
				.toString());
		text_Name = new Text(composite_layer, SWT.BORDER);
		text_Name.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				nameFlag = !nameFlag;
			}
		});
		text_Name.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));
		label_Color = new Label(composite_layer, SWT.BORDER);
		label_Color.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		label_Color.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				ColorDialog cd = new ColorDialog(PlatformUI.getWorkbench()
						.getDisplay().getActiveShell());
				cd.setText("ColorDialog Demo"); //$NON-NLS-1$
				cd.setRGB(new RGB(255, 255, 255));
				RGB newColor = cd.open();
				if (newColor == null) {
					return;
				}
				label_Color.setBackground(new Color(PlatformUI.getWorkbench()
						.getDisplay(), newColor));
				colorFlag = !colorFlag;
			}
		});
		button_visibility = new Button(composite_layer, SWT.CHECK);
		button_visibility.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				visibilityFlag = !activeFlag;
			}
		});
		button_visibility.setSelection(true);
		button_visibility.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER,
				false, false, 1, 1));
		button_visibility.setAlignment(SWT.CENTER);
		button_active = new Button(composite_layer, SWT.CHECK);
		button_active.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				activeFlag = !activeFlag;
			}
		});
		button_active.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false,
				false, 1, 1));

		return area;
	}

	protected void fillEditorArea(int selectionLine) {
		int labelNr = selectionLine;
		this.label_Nr.setText(new Integer(labelNr).toString());

		for (Iterator iterator = sessionLayerManager.getIterator(); iterator
				.hasNext();) {
			TGLayer layer = (TGLayer) iterator.next();
			if (layer.getNumber() == labelNr) {
				this.text_Name.setText(layer.getName());
				this.button_visibility.setSelection(layer.isVisible());
				this.button_active.setSelection(layer.isActive());
				this.label_Color.setBackground(layer.getLayercolor());
			}
		}
	}

	protected Object[] fillEditorLayerChangedArea(int selectionLine) {
		Object[] obj = new Object[2];
		obj[0] = null;
		obj[1] = null;
		int labelNr = selectionLine;
		for (Iterator iterator = sessionLayerManager.getIterator(); iterator
				.hasNext();) {
			TGLayer layer = (TGLayer) iterator.next();
			if (layer.getNumber() == labelNr) {
				layer.getName();
				obj[0] = layer.getName();
				obj[1] = layer.getLayercolor();
			}
		}
		return obj;
	}

	private void createTableLines() {
		boolean activeLayerChangedFlag = false;
		String visibility = ""; //$NON-NLS-1$
		String active = ""; //$NON-NLS-1$

		@SuppressWarnings("rawtypes")
		List sortedList = sortSessionLayerManager();

		for (@SuppressWarnings("rawtypes")
		Iterator iterator = sortedList.iterator(); iterator.hasNext();) {
			TGLayer layer = (TGLayer) iterator.next();
			TableItem tableItem = new TableItem(table, SWT.NONE);

			if (layer.isVisible()) {
				visibility = Messages.EditTGLayer_UI_VisibilityYes;
				tableItem
						.setForeground(
								3,
								SWTResourceManager.getColor(SWT.COLOR_GREEN
										| SWT.BOLD));
			} else {
				visibility = Messages.EditTGLayer_UI_VisibilityNo;
				tableItem.setForeground(3,
						SWTResourceManager.getColor(SWT.COLOR_RED | SWT.BOLD));
			}
			if (layer.isActive()) {
				active = Messages.EditTGLayer_UI_ActiveYes;
				tableItem
						.setForeground(
								4,
								SWTResourceManager.getColor(SWT.COLOR_GREEN
										| SWT.BOLD));
				this.activeLayerNumber = layer.getNumber();
				this.activeLayerName = layer.getName();
				this.activeLayerRGB = layer.getLayercolor().getRGB();
				activeLayerChangedFlag = true;
			} else {
				active = Messages.EditTGLayer_UI_ActiveNo;
				tableItem.setForeground(4,
						SWTResourceManager.getColor(SWT.COLOR_RED | SWT.BOLD));
			}

			tableItem.setText(0, new Integer(layer.getNumber()).toString());
			tableItem.setBackground(2, layer.getLayercolor());
			tableItem.setText(1, layer.getName());
			tableItem.setText(3, visibility);
			tableItem.setText(4, active);
		}

		if (!activeLayerChangedFlag) { // There is NO active Layer (selected)!!!
			activeLayerNumber = 0;
			controller.setLayerLabel(false, "0"); //$NON-NLS-1$
		}

		layerInfoToImagePanel();
	}

	/**
	 * sort Layer List
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private List sortSessionLayerManager() {
		@SuppressWarnings({ "unchecked" })
		List yourMapValues = new ArrayList(sessionLayerManager.values());
		@SuppressWarnings({ "unchecked" })
		TreeSet sortedSet = new TreeSet(yourMapValues);
		Object[] sortedArray = sortedSet.toArray();
		List sortedList = Arrays.asList(sortedArray);

		return sortedList;
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {

		Button button_New = createButton(parent, NEW_ID, "New button", false); //$NON-NLS-1$
		button_New.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// if (!Editmode) {
				TGLayer newLayer = new TGLayer(text_Name.getText(),
						new Integer(label_Nr.getText()), button_visibility
								.getSelection(), button_active.getSelection());
				newLayer.setLayercolor(label_Color.getBackground());
				// If a Layer marked as active --> isVisible must be true!!!
				if (newLayer.isActive()) {
					newLayer.setVisible(true);
				}
				// set the last active layer to false!
				proofIfOneLayerIsAlreadyActive(newLayer);

				sessionLayerManager.add(newLayer.getNumber(), newLayer);
				updateTable();
				// TODO find a better place to call this method
				if (table.getSelectionIndex() == -1) { // line selected
					if (activeFlag && !(nameFlag | visibilityFlag | colorFlag)) {
						// Do nothing
					} else {
						instance.setNewLayerValues(selectedLayerNumber);
					}

				}
			}

		});
		button_New.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
			}
		});

		button_New.setText(Messages.EditTGLayer_UI_NewOrAssume);

		Button button_Remove = createButton(parent, NEW_ID,
				IDialogConstants.OK_LABEL, false);
		button_Remove.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (selectedLayerNumber == sessionLayerManager.getSize()) {
					sessionLayerManager.remove(selectedLayerNumber);
					updateTable();

					// TODO find a better place to call this method
					if (selectedLayerNumber != 0) {
						activeLayerNumber = 0;
						layerInfoToImagePanel();
						instance.setNewLayerValues4Delete(selectedLayerNumber);
					}
				} else {
					MessageBox msgBox = new MessageBox(getParentShell());
					msgBox.setText(Messages.EditTGLayer_UI_Info);
					if (table.getSelectionCount() == 0) {
						msgBox.setMessage(Messages.EditTGLayer_UI_AtFirstSelectTheLastLayer);
					} else {
						msgBox.setMessage(Messages.EditTGLayer_UI_YouCanOnlyRemoveTheLastLayer);
					}
					msgBox.open();
				}
			}
		});
		button_Remove.setText(Messages.EditTGLayer_UI_Remove); // Delete

		Button button_Exit = createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
		button_Exit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				controller.addLayerManagerToMap(sessionLayerManager);
			}
		});

		button_Exit.setText(Messages.EditTGLayer_UI_Exit);
	}

	/**
	 * set the last active layer as inactive, if there is already one
	 * 
	 * @param newLayer
	 */
	private void proofIfOneLayerIsAlreadyActive(TGLayer newLayer) {
		if (newLayer.isActive()) {
			for (Iterator iterator = sessionLayerManager.getIterator(); iterator
					.hasNext();) {
				TGLayer layer = (TGLayer) iterator.next();

				if (layer.isActive()) {
					layer.setActive(false);
					sessionLayerManager.add(layer.getNumber(), layer);
				}
			}
		}
	}

	/**
	 * update Table Input
	 */
	protected void updateTable() {

		if (table.isDisposed()) {
			return;
		}

		try {
			table.removeAll();

			createTableLines();

			label_Nr.setText(new Integer(sessionLayerManager.getSize() + 1)
					.toString());
			text_Name.setText(""); //$NON-NLS-1$
			label_Color.setBackground(null);
			button_visibility.setSelection(true);
			button_active.setSelection(false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			Activator.handleError(e);
		}

		layerInfoToImagePanel();

		// Info about current active Layer to SessionImageCanvas
		setLayerInfoToSessionedImageCanvas();

		if (table.getSelectionIndex() == -1) { // No line is selected
			fillEditorArea(sessionLayerManager.getNextNumber());
		}

	}

	/**
	 * write active layer info to SessionImageCanvas
	 */
	private void setLayerInfoToSessionedImageCanvas() {
		instance.setCurrentActiveLayerNumber(activeLayerNumber);
		instance.setCurrentActiveLayerName(activeLayerName);
		instance.setCurrentActiveLayerRGB(activeLayerRGB);
		instance.updateLayerInfoOfShapes();
	}

	/**
	 * write active layer info to ImagePanel
	 */
	public void layerInfoToImagePanel() {
		// Layer info to SessionCanvas

		if (activeLayerNumber != 0) {
			controller.setLayerLabel(true,
					new Integer(activeLayerNumber).toString());
		} else {
			controller.setLayerLabel(false,
					new Integer(activeLayerNumber).toString());
		}
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(450, 382);
	}
}