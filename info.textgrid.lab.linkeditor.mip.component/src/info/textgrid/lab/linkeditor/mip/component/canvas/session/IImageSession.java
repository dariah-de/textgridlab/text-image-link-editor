/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.canvas.session;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.graphics.Cursor;

/**
 * An interface for the image canvas session.
 * 
 * @author Chengdong Li
 */
public interface IImageSession extends MouseListener, MouseMoveListener,
		KeyListener {

	/**
	 * Activates the session.
	 * 
	 * Note: When overriding this method, call super.beginSession() at method
	 * start.
	 */
	public abstract void beginSession(Object obj);

	/**
	 * Deactivates the session.
	 * 
	 * Note: When overriding this method, call super.endSession() at method
	 * exit.
	 */
	public abstract void endSession();

	/**
	 * A function can represent an execution of this session.
	 */
	public abstract void run();

	/**
	 * Get session id.
	 * 
	 * @return Session id
	 */
	public String getSessionID();

	/**
	 * Set session id.
	 * 
	 * @param id
	 *            Session id
	 */
	public void setSessionID(String id);

	/**
	 * Get session prompt message.
	 * 
	 * @return prompt message
	 */
	public String getSessionPrompt();

	/**
	 * Set session prompt message.
	 * 
	 * @param prompt
	 *            prompt message
	 */
	public void setSessionPrompt(String prompt);

	/**
	 * Get session ToolTip message.
	 * 
	 * @return prompt message
	 */
	public String getSessionToolTip();

	/**
	 * Set session ToolTip message.
	 * 
	 * @param tip
	 *            tooltip message
	 */
	public void setSessionToolTip(String tip);

	/**
	 * Get session cursor.
	 * 
	 * @return Session Cursor
	 */
	public Cursor getSessionCursor();

	/**
	 * Set session cursor.
	 * 
	 * @param cursor
	 *            Session Cursor
	 */
	public void setSessionCursor(Cursor cusor);

	/**
	 * Get session icon image.
	 * 
	 * @return Session icon image.
	 */
	public ImageDescriptor getSessionIcon();

	/**
	 * Set session icon image.
	 * 
	 * @param icon
	 *            icon image.
	 */
	public void setSessionIcon(ImageDescriptor icon);

	/**
	 * Set session selected.
	 * 
	 * @param selected
	 *            if session is selected.
	 */
	public void setSessionSelected(boolean selected);

	/**
	 * Get session selected state.
	 * 
	 * @return true: if selected; false: if not selected.
	 */
	public boolean isSessionSelected();

	/**
	 * Initialize the context menu inside session.
	 * 
	 * @param menuManager
	 *            the context menu Manager.
	 */
	public void initContextMenu(IMenuManager menuManager);

	public void sendUnlinkedShapeInfo();
}