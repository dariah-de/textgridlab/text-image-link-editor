/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.canvas;

import info.textgrid.lab.linkeditor.mip.component.views.IImageSurface;
import info.textgrid.lab.linkeditor.mip.component.views.ThumbViewMediator;

import java.awt.geom.AffineTransform;

import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;

/**
 * An image canvas which can operate with ThumbView to show thumb-nail image
 * using the MST (Mediator + image-Surface + Thumb-view) model.
 * 
 * @author Chengdong Li
 */
public class ThumbedImageCanvas extends SessionedImageCanvas implements
		IImageSurface {
	/**
	 * Constructor.
	 * 
	 * @param parent
	 *            the parent of this control
	 * @param style
	 *            the style of this canvas.
	 * @see org.eclipse.swt.widgets.Canvas for style detail
	 */
	public ThumbedImageCanvas(Composite parent, int style) {
		super(parent, style);
		registerThumbNailListeners();
	}

	/**
	 * Constructor.
	 * 
	 * @param parent
	 *            the parent of this control
	 */
	public ThumbedImageCanvas(Composite parent) {
		super(parent);
		registerThumbNailListeners();
	}

	@Override
	public void dispose() {
		ThumbViewMediator.unRegister(this);
		super.dispose();
	}

	@Override
	public void registerThumbNailListeners() {
		// notify thumbView drawing event.
		IDrawObserver drawListener = new IDrawObserver() {
			@Override
			public void onSynchronize(AffineImageCanvas canvas) {
				ThumbViewMediator.onISScroll();
			}

			@Override
			public void onZoom(AffineImageCanvas canvas) {
				ThumbViewMediator.onISZoom();
			}
		};
		registerDrawObserver(drawListener);
		ThumbViewMediator.register(instance);

		this.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				ThumbViewMediator.register(instance);
			}

			@Override
			public void focusLost(FocusEvent e) {
			}
		});

	}

	@Override
	public Image getImage() {
		return getSourceImage();
	}

	@Override
	public double getTX() {
		Image image = getSourceImage();
		if (image == null || image.isDisposed())
			return 0;
		Rectangle iRect = image.getBounds();
		AffineTransform af = getTransform();
		double tx = af.getTranslateX();
		if (tx > 0)
			return 0;
		return -tx / af.getScaleX() / (iRect.width);
	}

	@Override
	public double getTY() {
		Image image = getSourceImage();
		if (image == null || image.isDisposed())
			return 0;
		Rectangle iRect = image.getBounds();
		AffineTransform af = getTransform();
		double ty = af.getTranslateY();
		if (ty > 0)
			return 0;
		return -ty / af.getScaleY() / (iRect.height);
	}

	@Override
	public double getSX() {
		Image image = getSourceImage();
		if (image == null || image.isDisposed())
			return 0;
		Rectangle iRect = image.getBounds();
		Rectangle cRect = getClientArea();
		AffineTransform af = getTransform();
		double sx = af.getScaleX();
		sx = ((double) cRect.width) / ((double) iRect.width) / sx;
		if (sx > 1)
			sx = 1.0;
		return sx;
	}

	@Override
	public double getSY() {
		Image image = getSourceImage();
		if (image == null || image.isDisposed())
			return 0;
		Rectangle iRect = image.getBounds();
		Rectangle cRect = getClientArea();
		AffineTransform af = getTransform();
		double sy = af.getScaleY();
		sy = ((double) cRect.height) / ((double) iRect.height) / sy;
		if (sy > 1)
			sy = 1.0;
		return sy;
	}

	@Override
	public double getScaleX() {
		Image image = getSourceImage();
		if (image == null || image.isDisposed())
			return 1.0;
		double sx = getTransform().getScaleX();
		return sx;
	}

	@Override
	public double getScaleY() {
		Image image = getSourceImage();
		if (image == null || image.isDisposed())
			return 1.0;
		double sy = getTransform().getScaleY();
		return sy;
	}

	@Override
	public void onTVScroll(double tx, double ty) {
		Image image = getSourceImage();
		if (image == null || image.isDisposed())
			return;
		Rectangle iRect = image.getBounds();
		AffineTransform af = getTransform();
		double sx = af.getScaleX();
		double sy = af.getScaleY();
		AffineTransform t = AffineTransform.getTranslateInstance(-tx * sx
				* iRect.width, -ty * sy * iRect.height);
		af.preConcatenate(t);
		setTransform(af);
		syncScrollBars();
	}

	@Override
	public void onTVZoom(double x, double y) {
		Image image = getSourceImage();
		if (image == null || image.isDisposed())
			return;
		AffineTransform af = getTransform();
		double sx = af.getScaleX();
		zoom(x / sx);
	}

	@Override
	public boolean isFocused() {
		return isFocusControl();
	}

}
