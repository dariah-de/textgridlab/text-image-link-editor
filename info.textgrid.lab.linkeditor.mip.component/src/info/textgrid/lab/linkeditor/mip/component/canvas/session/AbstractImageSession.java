/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.canvas.session;

import info.textgrid.lab.linkeditor.controller.LinkEditorController;
import info.textgrid.lab.linkeditor.mip.component.canvas.SessionedImageCanvas;
import info.textgrid.lab.linkeditor.model.graphics.TGLine;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;

import java.util.Iterator;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;

/**
 * An abstract class for the image canvas session.
 * 
 * @author Chengdong Li
 */
public abstract class AbstractImageSession implements IImageSession {
	protected String id;
	protected ImageDescriptor icon;
	protected boolean selected;
	protected String prompt = "";
	protected String tooltip;
	protected SessionedImageCanvas sessionCanvas;
	private Cursor cursor = null;
	private boolean focusImageCanvasAfterInit = true;
	protected int cursorStyle;
	protected boolean keyPressed = false;

	public AbstractImageSession() {
	}

	public AbstractImageSession(String id) {
		this.id = id;
	}

	@Override
	public String getSessionID() {
		if (id == null)
			return "id";
		return id;
	}

	@Override
	public void setSessionID(String id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uky.mip.views.IImageSession#beginSession()
	 */
	@Override
	public void beginSession(Object obj) {
		if (obj instanceof SessionedImageCanvas) {
			sessionCanvas = (SessionedImageCanvas) obj;
		}
		cursor = getSessionCursor();
		if (cursor != null)
			sessionCanvas.setCursor(cursor);

		focusImageCanvasAfterInit = true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uky.mip.gui.sessiondelegate.IImageSession#endSession()
	 */
	@Override
	public void endSession() {
		sessionCanvas.changeCursor(SWT.CURSOR_ARROW);
		if (cursor != null)
			cursor.dispose();
		focusImageCanvasAfterInit = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.events.MouseListener#mouseDoubleClick(org.eclipse.swt
	 * .events.MouseEvent)
	 */
	@Override
	public void mouseDoubleClick(MouseEvent e) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.events.MouseListener#mouseDown(org.eclipse.swt.events
	 * .MouseEvent)
	 */
	@Override
	public void mouseDown(MouseEvent e) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.swt.events.MouseListener#mouseUp(org.eclipse.swt.events.
	 * MouseEvent)
	 */
	@Override
	public void mouseUp(MouseEvent e) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.events.MouseMoveListener#mouseMove(org.eclipse.swt.events
	 * .MouseEvent)
	 */
	@Override
	public void mouseMove(MouseEvent evt) {
		if (evt.getSource() instanceof SessionedImageCanvas) {
			SessionedImageCanvas canvas = (SessionedImageCanvas) evt
					.getSource();

			if (canvas.getSourceImage() == null)
				return;

			if (focusImageCanvasAfterInit && !canvas.isFocusControl())
				canvas.setFocus();
			focusImageCanvasAfterInit = false;
		} else {
			return;
		}
	}

	/*
	 * @see KeyListener#keyReleased
	 */
	@Override
	public void keyReleased(KeyEvent e) {
	}

	/*
	 * @see KeyListener#keyPressed
	 */
	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void initContextMenu(IMenuManager menuManager) {
	}

	@Override
	public void run() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uky.mip.template.session.IImageSession#getSessionPrompt()
	 */
	@Override
	public String getSessionPrompt() {
		return prompt;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.uky.mip.template.session.IImageSession#setSessionPrompt(java.lang
	 * .String)
	 */
	@Override
	public void setSessionPrompt(String prompt) {
		this.prompt = prompt;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uky.mip.template.session.IImageSession#getSessionToolTip()
	 */
	@Override
	public String getSessionToolTip() {
		return tooltip;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.uky.mip.template.session.IImageSession#setSessionToolTip(java.lang
	 * .String)
	 */
	@Override
	public void setSessionToolTip(String tip) {
		this.tooltip = tip;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uky.mip.template.session.IImageSession#getSessionCursor()
	 */
	@Override
	public Cursor getSessionCursor() {
		return this.cursor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.uky.mip.template.session.IImageSession#setSessionCursor(org.eclipse
	 * .swt.graphics.Cursor)
	 */
	@Override
	public void setSessionCursor(Cursor cursor) {
		this.cursor = cursor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uky.mip.template.session.IImageSession#getSessionIcon()
	 */
	@Override
	public ImageDescriptor getSessionIcon() {
		return this.icon;
	}

	public SessionedImageCanvas getSessionCanvas() {
		return sessionCanvas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.uky.mip.template.session.IImageSession#setSessionIcon(org.eclipse
	 * .swt.graphics.Image)
	 */
	@Override
	public void setSessionIcon(ImageDescriptor icon) {
		this.icon = icon;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.uky.mip.template.session.IImageSession#setSessionSelected(boolean)
	 */
	@Override
	public void setSessionSelected(boolean sel) {
		this.selected = sel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uky.mip.template.session.IImageSession#isSessionSelected()
	 */
	@Override
	public boolean isSessionSelected() {
		return this.selected;
	}

	@Override
	public void sendUnlinkedShapeInfo() {
		int count = 0;
		for (Iterator<TGShape> it = sessionCanvas.getArrayListShapes()
				.iterator(); it.hasNext();) {
			TGShape shape = it.next();
			if (!(shape instanceof TGLine) && !shape.isLinked()) {
				count++;
			}
		}
		LinkEditorController.getInstance().setCountDiffLabel(count);
	}

	/**
	 * Set the cursor of canvas.
	 * 
	 * @param style
	 *            cursor style, see SWT.CURSOR_xxx
	 */
	public void setCursor(int style) {
		sessionCanvas.changeCursor(style);
		this.cursorStyle = style;
	}

	// cursor related variables.
	static RGB[] rgbs = { new RGB(0, 0, 0), new RGB(255, 255, 255) };
	protected static PaletteData paletteData = new PaletteData(rgbs);
	protected static int[] cursorMask = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0 };

}
