/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.canvas;

import info.textgrid.lab.linkeditor.model.graphics.TGLine;
import info.textgrid.lab.linkeditor.model.graphics.TGPolygon;
import info.textgrid.lab.linkeditor.model.graphics.TGRectangle;

import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.io.File;
import java.util.Vector;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;

/**
 * Utility for Java2d transforms.
 * 
 * @author Chengdong Li
 */
public class SWT2dUtil {
	public static PaletteData palette = new PaletteData(0x0000ff00, 0x00ff0000,
			0xff000000);

	/**
	 * Get the intersection of two rectangle: inner line algorithm. Sort the x
	 * and y directly, and then get the inner part.
	 * 
	 * @param r1
	 *            source rectangle 1
	 * @param r2
	 *            source rectangle 2
	 * @return rectangle after intesection
	 */
	public static Rectangle intersectRects(Rectangle r1, Rectangle r2) {
		Rectangle dest = new Rectangle(0, 0, 0, 0);
		int x1 = r1.x;
		int x2 = r2.x;
		int y1 = r1.y;
		int y2 = r2.y;
		int w1 = r1.width;
		int h1 = r1.height;
		int w2 = r2.width;
		int h2 = r2.height;

		int[] x = { x1, x1 + w1, x2, x2 + w2 };
		int[] y = { y1, y1 + h1, y2, y2 + h2 };

		sortInts(x);
		sortInts(y);

		if ((x[0] == x1 && x[1] == x1 + w1) || (x[0] == x2 && x[1] == x2 + w2)
				|| (y[0] == y1 && y[1] == y1 + h1)
				|| (y[0] == y2 && y[1] == y2 + h2)) {
			return dest;
		} else {
			dest.x = x[1];
			dest.y = y[1];
			dest.width = x[2] - x[1];
			dest.height = y[2] - y[1];
			return dest;
		}
	}

	/**
	 * test if a point is inside the given Rect.
	 * 
	 * @param pt
	 *            point
	 * @param rect
	 *            rectangle
	 * @return true if point inside rectangle; false if not.
	 */
	public static boolean isInside(Point pt, Rectangle rect) {
		if ((pt.x <= rect.x + rect.width) && (pt.x >= rect.x)
				&& (pt.y <= rect.y + rect.height) && (pt.y >= rect.y))
			return true;
		else
			return false;
	}

	/**
	 * Given a point and affine transform, we get the transformed point.
	 * 
	 * @param af
	 *            affine transform
	 * @param pt
	 *            point to be transformed
	 * @return point after transform
	 */
	public static Point transformPoint(AffineTransform af, Point pt) {
		Point2D src = new Point2D.Float(pt.x, pt.y);
		Point2D dest = af.transform(src, null);
		Point point = new Point((int) Math.round(dest.getX()),
				(int) Math.round(dest.getY()));
		return point;
	}

	/**
	 * Given an arbitrary point, get the point with the inverse given transform.
	 * 
	 * @param af
	 *            AffineTransform
	 * @param pt
	 *            source point
	 * @return point after transform
	 */
	public static Point inverseTransformPoint(AffineTransform af, Point pt) {
		Point2D src = new Point2D.Float(pt.x, pt.y);
		try {
			Point2D dest = af.inverseTransform(src, null);
			return new Point((int) Math.round(dest.getX()),
					(int) Math.round(dest.getY()));
		} catch (Exception e) {
			return new Point(0, 0);
		}
	}

	/**
	 * Given an arbitrary point, get the point with the given transform.
	 * 
	 * @param af
	 *            AffineTransform
	 * @param pt
	 *            source point
	 * @return point after transform
	 */
	public static Point transformPoint1(AffineTransform af, Point pt) {
		Point2D src = new Point2D.Float(pt.x, pt.y);
		Point2D dest = af.transform(src, null);
		double x0, x1, y0, y1;
		x0 = dest.getX();
		y0 = dest.getY();
		dest = af.transform(new Point2D.Float(pt.x + 1, pt.y + 1), null);
		x1 = dest.getX();
		y1 = dest.getY();
		Point point = new Point((int) ((x0 + x1) / 2), (int) ((y0 + y1) / 2));
		return point;
	}

	/**
	 * Given a point point and affine transform, we get the transformed point.
	 * This is much more accurate than others and should be used in future
	 * development.
	 * 
	 * @param af
	 *            affine transform
	 * @param pt
	 *            point to be transformed
	 * @return point after tranform
	 */
	public static Point transformPoint2(AffineTransform af, Point pt) {
		Point2D src = new Point2D.Float(pt.x, pt.y);
		Point2D dest = af.transform(src, null);
		Point point = new Point((int) Math.floor(dest.getX()),
				(int) Math.floor(dest.getY()));
		return point;
	}

	/**
	 * Given an arbitrary point, get the point with the inverse given transform.
	 * This is much more accurate than others and should be used in future
	 * development.
	 * 
	 * @param af
	 *            AffineTransform
	 * @param pt
	 *            source point
	 * @return point after transform
	 */
	public static Point inverseTransformPoint2(AffineTransform af, Point pt) {
		Point2D src = new Point2D.Float(pt.x, pt.y);
		try {
			Point2D dest = af.inverseTransform(src, null);
			return new Point((int) Math.floor(dest.getX()),
					(int) Math.floor(dest.getY()));
		} catch (Exception e) {
			// e.printStackTrace();
			return new Point(0, 0);
		}
	}

	/**
	 * Given an arbitrary rectangle, get the rectangle with the inverse given
	 * transform. The result rectangle is nonNegative width and nonNegative
	 * height.
	 * 
	 * @param af
	 *            an AffineTransform
	 * @param src
	 *            source rectangle
	 * @return rectangle after transform with positive width and height
	 */
	public static TGRectangle inverseTransformRect(AffineTransform af,
			TGRectangle src) {
		TGRectangle dest = new TGRectangle(0, 0, 0, 0, src.getImageUri());
		src = absRect(src);
		Point p1 = new Point(src.getRectangle().x, src.getRectangle().y);
		p1 = inverseTransformPoint(af, p1);
		double w = src.getRectangle().width / af.getScaleX();
		double h = src.getRectangle().height / af.getScaleY();
		dest.getRectangle().x = p1.x;
		dest.getRectangle().y = p1.y;
		dest.getRectangle().width = (int) Math.round(w);
		dest.getRectangle().height = (int) Math.round(h);
		return dest;
	}

	public static Rectangle inverseTransformRect(AffineTransform af,
			Rectangle src) {
		Rectangle dest = new Rectangle(0, 0, 0, 0);
		src = absRect(src);
		Point p1 = new Point(src.x, src.y);
		p1 = inverseTransformPoint(af, p1);
		double w = src.width / af.getScaleX();
		double h = src.height / af.getScaleY();
		dest.x = p1.x;
		dest.y = p1.y;
		dest.width = (int) Math.round(w);
		dest.height = (int) Math.round(h);
		return dest;
	}

	public static TGPolygon inverseTransformPoly(AffineTransform af,
			TGPolygon src) {

		TGPolygon dest = new TGPolygon(src.getImageUri());
		for (Point p : src.getPoints()) {
			Point p1 = new Point(p.x, p.y);
			p1 = inverseTransformPoint(af, p1);
			dest.addPoint(p1.x, p1.y);
		}
		return dest;
	}

	public static TGLine inverseTransformLine(AffineTransform af, TGLine src) {

		TGLine dest = new TGLine(src.getImageUri());
		for (Point p : src.getPoints()) {
			Point p1 = new Point(p.x, p.y);
			p1 = inverseTransformPoint(af, p1);
			dest.addPoint(p1.x, p1.y);
		}
		return dest;
	}

	/**
	 * Given an arbitrary rectangle, get the rectangle with the given transform.
	 * The result rectangle is positive width and positive height.
	 * 
	 * @param af
	 *            AffineTransform
	 * @param src
	 *            source rectangle
	 * @return rectangle after transform with positive width and height
	 */
	public static TGRectangle transformRect(AffineTransform af, TGRectangle src) {
		TGRectangle dest = new TGRectangle(0, 0, 0, 0, src.getImageUri());
		if (src == null)
			return dest;
		src = absRect(src);
		Point p1 = new Point(src.getRectangle().x, src.getRectangle().y);
		p1 = transformPoint(af, p1);
		double w = src.getRectangle().width * af.getScaleX();
		double h = src.getRectangle().height * af.getScaleY();
		dest.getRectangle().x = p1.x;
		dest.getRectangle().y = p1.y;
		dest.getRectangle().width = (int) Math.round(w);
		dest.getRectangle().height = (int) Math.round(h);
		dest.setLinked(src.isLinked());
		dest.setVisible(src.isVisible());
		dest.setWritingMode(src.getWritingMode());
		dest.setRotationValue(src.getRotationValue());

		return dest;
	}

	public static Rectangle transformRect(AffineTransform af, Rectangle src) {
		Rectangle dest = new Rectangle(0, 0, 0, 0);
		if (src == null)
			return dest;
		src = absRect(src);
		Point p1 = new Point(src.x, src.y);
		p1 = transformPoint(af, p1);
		double w = src.width * af.getScaleX();
		double h = src.height * af.getScaleY();
		dest.x = p1.x;
		dest.y = p1.y;
		dest.width = (int) Math.round(w);
		dest.height = (int) Math.round(h);
		return dest;
	}

	/**
	 * Given arbitrary rectangle, get a rectangle with upper-left start and
	 * positive width and height.
	 * 
	 * @param src
	 *            source rectangle
	 * @return result rectangle with positive width and height
	 */
	public static TGRectangle absRect(TGRectangle src) {
		TGRectangle dest = new TGRectangle(0, 0, 0, 0, src.getImageUri());
		if (src.getRectangle().width < 0) {
			dest.getRectangle().x = src.getRectangle().x
					+ src.getRectangle().width + 1;
			dest.getRectangle().width = -src.getRectangle().width;
		} else {
			dest.getRectangle().x = src.getRectangle().x;
			dest.getRectangle().width = src.getRectangle().width;
		}
		if (src.getRectangle().height < 0) {
			dest.getRectangle().y = src.getRectangle().y
					+ src.getRectangle().height + 1;
			dest.getRectangle().height = -src.getRectangle().height;
		} else {
			dest.getRectangle().y = src.getRectangle().y;
			dest.getRectangle().height = src.getRectangle().height;
		}

		dest.setLinked(src.isLinked());
		dest.setVisible(src.isVisible());
		dest.setWritingMode(src.getWritingMode());
		dest.setRotationValue(src.getRotationValue());

		return dest;
	}

	public static Rectangle absRect(Rectangle src) {
		Rectangle dest = new Rectangle(0, 0, 0, 0);
		if (src.width < 0) {
			dest.x = src.x + src.width + 1;
			dest.width = -src.width;
		} else {
			dest.x = src.x;
			dest.width = src.width;
		}
		if (src.height < 0) {
			dest.y = src.y + src.height + 1;
			dest.height = -src.height;
		} else {
			dest.y = src.y;
			dest.height = src.height;
		}
		return dest;
	}

	/**
	 * Fill the point as a rectangle based on the af on gc.
	 * 
	 * @param gc
	 *            graphics context for drawing.
	 * @param af
	 *            current AffineTransform.
	 * @param p
	 *            point at current GC domain.
	 */
	public static void fillPoint(GC gc, AffineTransform af, Point pt) {
		Point p = inverseTransformPoint2(af, pt);
		Point p1 = transformPoint2(af, new Point(p.x + 1, p.y + 1));
		Point p2 = transformPoint2(af, new Point(p.x, p.y));
		gc.fillRectangle(p2.x, p2.y, p1.x - p2.x, p1.y - p2.y);
	}

	/**
	 * Load SWT image given an absolute path.
	 * 
	 * @param path
	 *            absolute path of image file
	 * @return SWT image
	 */
	public static Image loadSWTImage(String path) {
		File file = new File(path);
		Image image = null;
		try {
			image = ImageDescriptor.createFromURL(file.toURL()).createImage();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return image;
	}

	/**
	 * Save SWT image with the given an absolute path and format. Currently only
	 * support bmp file, since this is a stable format for current eclipse
	 * version. Suppose eclipse can write other formats without any error, we
	 * can release this constrain.
	 * 
	 * @param data
	 *            iamge data to be saved
	 * @param path
	 *            absolute path of image file
	 * @param format
	 *            image format
	 */
	public static void saveSWTImage(ImageData data, String path, String format) {
		ImageLoader loader = new ImageLoader();
		loader.data = new ImageData[] { data };
		int saveFormat;
		if (format == null) {
			path += ".bmp";
		} else {
			saveFormat = mapSuffixToType(format);
			if (saveFormat != SWT.IMAGE_BMP) {
				System.out.println(format
						+ " is not supported, default saved as bmp file");
				path += ".bmp";
			}
		}
		saveFormat = SWT.IMAGE_BMP;
		try {
			loader.save(path, saveFormat);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Map the suffix of the image file to the swt image type.
	 * 
	 * @param suffix
	 *            suffix of image file
	 * @return SWT image type
	 */
	public static int mapSuffixToType(String suffix) {
		int result = SWT.IMAGE_JPEG;
		if (suffix == null)
			return result;
		if (suffix.equalsIgnoreCase("gif")) {
			result = SWT.IMAGE_GIF;
		} else if (suffix.equalsIgnoreCase("jpeg")
				|| suffix.equalsIgnoreCase("jpg")) {
			result = SWT.IMAGE_JPEG;
		} else if (suffix.equalsIgnoreCase("bmp")) {
			result = SWT.IMAGE_BMP;
		} else if (suffix.equalsIgnoreCase("png")) {
			result = SWT.IMAGE_PNG;
		} else if (suffix.equalsIgnoreCase("ico")) {
			result = SWT.IMAGE_ICO;
		}
		return result;
	}

	/**
	 * Rescale the SWT image, and make it fit the given size while maintain it's
	 * scale.
	 * 
	 * @param device
	 *            device hosted the image
	 * @param src
	 *            src image
	 * @param w
	 *            width to fit
	 * @param h
	 *            height to fit
	 * @return zoomed image
	 */
	public static Image fitSWTImage(Device device, Image src, int w, int h) {
		Rectangle rect = src.getBounds();
		Point size = computeFitSize(new Point(rect.width, rect.height),
				new Point(w, h));
		ImageData imageData = src.getImageData();
		Image dest = new Image(device, imageData.scaledTo(size.x, size.y));
		return dest;
	}

	/**
	 * Rescale the SWT image, and make it fit the given width while maintain
	 * it's scale.
	 * 
	 * @param device
	 *            device hosted the image
	 * @param src
	 *            src image
	 * @param wHint
	 *            width to fit
	 * @return zoomed image
	 */
	public static Image fitSWTImageH(Device device, Image src, int wHint) {
		Rectangle rect = src.getBounds();
		double s = (double) wHint / (double) rect.width;
		double w = rect.width * s;
		double h = rect.height * s;

		ImageData imageData = src.getImageData();
		Image dest = new Image(device, imageData.scaledTo((int) w, (int) h));
		return dest;
	}

	/**
	 * Rescale the SWT image, and make it fit the given height while maintain
	 * it's scale.
	 * 
	 * @param device
	 *            device hosted the image
	 * @param src
	 *            src image
	 * @param hHint
	 *            height to fit
	 * @return zoomed image
	 */
	public static Image fitSWTImageV(Device device, Image src, int hHint) {
		Rectangle rect = src.getBounds();
		double s = (double) hHint / (double) rect.height;
		double w = rect.width * s;
		double h = rect.height * s;

		ImageData imageData = src.getImageData();
		Image dest = new Image(device, imageData.scaledTo((int) w, (int) h));
		return dest;
	}

	/**
	 * Compute the actual size of fit the src to dest while maintain the
	 * original scale.
	 * 
	 * @param src
	 *            size of source
	 * @param dest
	 *            size to be fitted
	 * @return actual size after fit
	 */
	public static Point computeFitSize(Point src, Point dest) {
		double sx = (double) dest.x / (double) src.x;
		double sy = (double) dest.y / (double) src.y;
		double s = Math.min(sx, sy);

		double dx = src.x * s;
		double dy = src.y * s;

		return new Point((int) dx, (int) dy);

	}

	/**
	 * Set the control's font.
	 * 
	 * @param c
	 *            control
	 * @param style
	 *            font style
	 * @param color
	 *            font color
	 */
	public static void setCorlorFont(Control c, int style, int color) {
		FontData[] fontdata = c.getFont().getFontData();
		fontdata[0].setStyle(style);
		Font font = new Font(c.getDisplay(), fontdata);
		c.setFont(font);
		c.setForeground(c.getDisplay().getSystemColor(color));
	}

	/**
	 * Set the control's font.
	 * 
	 * @param c
	 *            control
	 * @param style
	 *            font style
	 * @param color
	 *            font color
	 * @param height
	 *            font height
	 */
	public static void setCorlorFont(Control c, int style, int color, int height) {
		FontData[] fontdata = c.getFont().getFontData();
		fontdata[0].setStyle(style);
		fontdata[0].setHeight(height);
		fontdata[0].setName("SansSerif");
		Font font = new Font(c.getDisplay(), fontdata);
		c.setFont(font);
		c.setForeground(c.getDisplay().getSystemColor(color));
	}

	/**
	 * Sort integer array. The integer array will be changed after sorting.
	 * 
	 * @param src
	 *            integer array
	 */
	public static void sortInts(int[] src) {
		for (int i = 0; i < src.length; i++) {
			for (int j = i; j < src.length; j++) {
				if (src[i] > src[j]) {
					int temp = src[i];
					src[i] = src[j];
					src[j] = temp;
				}
			}
		}
	}

	/**
	 * Given the image data and pixel position (x,y), return the RGB balue.
	 * 
	 * @param data
	 *            Image Data.
	 * @param x
	 *            x coordinate.
	 * @param y
	 *            y coordianate.
	 * @return RGB value of data at (x,y)
	 */
	public static RGB getRGB(ImageData data, int x, int y) {
		if (data == null)
			return null;
		if (x < data.width && x >= 0 && y < data.height && y >= 0) {
			int pixel = data.getPixel(x, y);
			RGB rgb = data.palette.getRGB(pixel);
			return rgb;
		}
		return null;
	}

	/**
	 * Copy a sub-rectangle part of ImageData.
	 * 
	 * @param src
	 *            source ImageData
	 * @param rect
	 *            rectangle of src to be copied
	 * @return R the copy of origial image inside the rectangle, 32 depth
	 */
	public static Image getSubImage(Device dev, Image src, Rectangle rect) {
		Image dest;
		if (src == null || src.isDisposed())
			return null;
		if (rect == null || rect.width == 0 || rect.height == 0)
			return null;

		dest = new Image(dev, rect);
		GC srcGC = new GC(src);
		srcGC.copyArea(dest, rect.x, rect.y);
		srcGC.dispose();
		return dest;
	}

	public static Image getSubImage(Device dev, Image src, TGRectangle rect) {
		return getSubImage(dev, src,
				new Rectangle(rect.getRectangle().x, rect.getRectangle().y,
						rect.getRectangle().width, rect.getRectangle().height));
	}

	/**
	 * Copy a sub-rectangle part of ImageData.
	 * 
	 * @param src
	 *            source ImageData
	 * @param rect
	 *            rectangle of src to be copied
	 * @return R the copy of origial image inside the rectangle, 32 depth
	 */
	public static ImageData getSubImage(ImageData src, Rectangle rect) {
		ImageData dest;
		if (src == null)
			return null;
		if (rect == null || rect.width == 0 || rect.height == 0)
			return (ImageData) src.clone();

		Rectangle bound = new Rectangle(0, 0, src.width - 1, src.height - 1);
		Rectangle rrect = rect.intersection(bound);

		if (rrect == null || rrect.width == 0 || rrect.height == 0) {
			return null;
		}

		// Note: Get rid of dark edge
		dest = new ImageData(rrect.width + 1, rrect.height + 1, 32,
				new PaletteData(0x0000ff00, 0x00ff0000, 0xff000000));

		for (int i = rrect.x; i < rrect.width + rrect.x + 1; i++) {
			for (int j = rrect.y; j < rrect.height + rrect.y + 1; j++) {
				int pixel = src.getPixel(i, j);
				dest.setPixel(i - rrect.x, j - rrect.y, pixel);
			}
		}
		return dest;
	}

	public static ImageData getSubImage(ImageData src, TGRectangle rect) {
		return getSubImage(src,
				new Rectangle(rect.getRectangle().x, rect.getRectangle().y,
						rect.getRectangle().width, rect.getRectangle().height));
	}

	/**
	 * Copy the src data to the sub-rectangle part of the dest image (must be
	 * the 32 depth).
	 * 
	 * @param sub
	 *            source from which the data will be copyed, ImageData, 32 depth
	 * @param dest
	 *            destination, ImageData
	 * @param rect
	 *            reectangle part of dest
	 */
	public static void setSubImage(ImageData dest, ImageData sub, Rectangle rect) {
		if (sub == null || dest == null)
			return;
		if (rect == null || rect.width == 0 || rect.height == 0) {
			dest = (ImageData) sub.clone();
			return;
		}

		Rectangle bound = new Rectangle(rect.x < 0 ? 0 : rect.x, rect.y < 0 ? 0
				: rect.y, sub.width, sub.height);
		Rectangle rrect = rect.intersection(bound);

		if (rrect == null || rrect.width == 0 || rrect.height == 0) {
			return;
		}

		for (int i = rrect.x; i < rrect.width + rrect.x + 1; i++) {
			for (int j = rrect.y; j < rrect.height + rrect.y + 1; j++) {
				int pixel = sub.getPixel(i - rrect.x, j - rrect.y);
				dest.setPixel(i, j, pixel);
			}
		}
		return;
	}

	/**
	 * Copy the src image to the sub-rectangle part of the dest image.
	 * 
	 * @param sub
	 *            source from which the data will be copyed
	 * @param dest
	 *            destination, Image
	 * @param rect
	 *            reectangle part of dest
	 */
	public static void setSubImage(Image dest, Image sub, Rectangle rect) {
		if (sub == null || dest == null)
			return;
		if (rect == null || rect.width == 0 || rect.height == 0) {
			return;
		}
		GC gc = new GC(dest);
		gc.drawImage(sub, 0, 0, rect.width, rect.height, rect.x, rect.y,
				rect.width, rect.height);
		gc.dispose();
		return;
	}

	/**
	 * Convert any format image into the 32 depth imagedata
	 * 
	 * @param src
	 *            image data source
	 * @return image data of 32-bit depth
	 */
	public static ImageData createDepth32Data(ImageData src) {
		int w = src.width;
		int h = src.height;
		ImageData dest = new ImageData(w, h, 32, palette);

		for (int i = 0; i < w; i++) {
			for (int j = 0; j < h; j++) {
				int old = src.getPixel(i, j);
				RGB rgb = src.palette.getRGB(old);
				int red = rgb.red;
				int green = rgb.green;
				int blue = rgb.blue;
				int pixel = (red << 8) | (green << 16) | (blue << 24);
				dest.setPixel(i, j, pixel);
			}
		}
		return dest;
	}

	/**
	 * Convert any format image into the 32 depth imagedata
	 * 
	 * @param src
	 *            image file name
	 * @return image data of 32-bit depth
	 */
	public static ImageData createDepth32Data(String file) {
		ImageData src = new ImageData(file);
		return createDepth32Data(src);
	}

	/**
	 * Given the full path of a filename, it will return the suffix.
	 * 
	 * @param path
	 *            full path, ex: c:\temp\test.gif
	 * @return suffix of the path, ex: gif
	 */
	public static String findSuffix(String path) {
		if (path == null)
			return null;
		if (path.charAt(path.length() - 1) == '.')
			return null;
		for (int i = path.length() - 1; i > 0; i--) {
			if (path.charAt(i) == '.') {
				return path.substring(i + 1);
			}
		}
		return null;
	}

	/**
	 * Create a inverse color for the given point.
	 * 
	 * @param data
	 *            imageData
	 * @param pt
	 *            pixel index
	 * @param dev
	 *            device
	 * @return inverse Color
	 */
	public static Color getInverseColor(ImageData data, Point pt, Device dev) {
		Color color = null;
		RGB rgb = data.palette.getRGB(data.getPixel(pt.x, pt.y));
		color = new Color(dev, 255 - rgb.red, 255 - rgb.green, 255 - rgb.blue);
		return color;
	}

	/**
	 * Compute affineTransform given point list a and b. Currently support two
	 * pairs only, i.e., only support rotate, translation and scaling. NO shear.
	 * The transform will transform points in list b to list a
	 * 
	 * @param a
	 *            point list
	 * @param b
	 *            point list
	 * @return AffineTransform a transform which will map pointlist b to a
	 */
	public static AffineTransform computeTransform(Vector a, Vector b) {
		// Before call this function you should make sure a and b
		// both size == 2;
		AffineTransform attemp = new AffineTransform();

		Point m1, m2, s1, s2;
		m1 = (Point) a.get(0);
		m2 = (Point) a.get(1);
		int mx1 = m1.x;
		int my1 = m1.y;
		int mx2 = m2.x;
		int my2 = m2.y;

		s1 = (Point) b.get(0);
		s2 = (Point) b.get(1);
		int sx1 = ((Point) b.get(0)).x;
		int sy1 = ((Point) b.get(0)).y;
		int sx2 = ((Point) b.get(1)).x;
		int sy2 = ((Point) b.get(1)).y;

		// We suppose we can transform the images through 3 basic
		// trosformation: translate, rotate and scale.
		// 1) traslate.
		// computer the norm of each vector:
		double rm, rs;
		rm = Math.sqrt((mx2 - mx1) * (mx2 - mx1) + (my2 - my1) * (my2 - my1));
		rs = Math.sqrt((sx2 - sx1) * (sx2 - sx1) + (sy2 - sy1) * (sy2 - sy1));
		// Note: here we need reverse the order.
		attemp.translate(mx1, my1);
		attemp.scale(rm / rs, rm / rs);
		attemp.rotate(computeAngle(m1, m2) - computeAngle(s1, s2));
		attemp.translate(-sx1, -sy1);
		return attemp;
	}

	// Compute the angle of vector (p2-p1).
	private static double computeAngle(Point p1, Point p2) {
		int x1 = p1.x;
		int y1 = p1.y;
		int x2 = p2.x;
		int y2 = p2.y;

		double ret = 0;

		double detx = x2 - x1;
		double dety = y2 - y1;

		if (detx > 0 && dety > 0)
			ret = Math.atan(dety / detx);
		if (detx > 0 && dety < 0)
			ret = Math.atan(dety / detx);
		if (detx < 0 && dety > 0)
			ret = Math.PI + Math.atan(dety / detx);
		if (detx < 0 && dety < 0)
			ret = Math.PI + Math.atan(dety / detx);
		if (detx > 0 && dety == 0)
			ret = 0;
		if (detx < 0 && dety == 0)
			ret = Math.PI;
		if (detx == 0 && dety > 0)
			ret = Math.PI / 2;
		if (detx == 0 && dety < 0)
			ret = -Math.PI / 2;
		if (detx == 0 && dety == 0)
			ret = 0;

		return ret;

	}

	/**
	 * Transform a polygon with the given affine transform.
	 * 
	 * @param ply
	 *            source polygon
	 * @param af
	 *            affine transform
	 * @return polygon after transformation
	 */
	public static Point[] transformPolygon(Point[] ply, AffineTransform af) {
		Point[] ret = new Point[ply.length];
		for (int i = 0; i < ply.length; i++) {
			Point pt = ply[i];
			Point2D dest = af.transform(new Point2D.Float(pt.x, pt.y), null);
			ret[i] = new Point((int) dest.getX(), (int) dest.getY());
		}
		return ret;
	}

	/**
	 * Use the 2D scan-converting-polygon algorithm to get the intersection
	 * pairs.
	 * 
	 * @see <code>http://www-scf.usc.edu/~akotaobi/gptut19.html</code>
	 * @see <code>http://www.cc.gatech.edu/gvu/multimedia/nsfmmedia/graphics/elabor/polyscan/polyscan1.html<code>
	 * @param vertex
	 *            vertexes of the polygon
	 * @param scanline
	 *            Vector will hold the return value of scanline
	 * @return pair which represents intersection (x1,x2) for each scanline
	 */
	public static Vector scanline2DPolygon(Point[] vertex, Rectangle bound,
			Vector scanline) {
		class Edge {
			int top, bottom;
		}

		Vector ret = new Vector();
		if (scanline == null)
			return ret;
		scanline.clear();

		int numpoints = vertex.length;
		Vector edges = new Vector();
		// fill the edge first.
		// int numedges = 0; // The number of edges
		for (int i = 0; i < numpoints; i++) {
			int start = i, next = (i == numpoints - 1 ? 0 : i + 1);
			// Horizontal edges may be ignored completely.
			// edges.elementAt()=new Edge();
			Edge edge = new Edge();
			if (vertex[start].y == vertex[next].y)
				continue;

			// Make sure uppermost vertex is top.
			if (vertex[start].y < vertex[next].y) {
				edge.top = start;
				edge.bottom = next;
			} else {
				edge.top = next;
				edge.bottom = start;
			}
			edges.add(edge);
		}

		// find the vertex increments
		float[] currentx = new float[numpoints];
		float[] xincr = new float[numpoints];

		for (int i = 0; i < edges.size(); i++) {

			// We will start at the top of the edge (when we reach it.)
			Edge edge = (Edge) edges.elementAt(i);
			currentx[i] = vertex[edge.top].x;

			// Calculate y increments.
			// In other words: what do we need to add to x to
			// correspond to a change of 1.0 in y?)

			float height = vertex[edge.bottom].y - vertex[edge.top].y + 1;
			xincr[i] = (vertex[edge.bottom].x - vertex[edge.top].x) / height;
		}

		// prepare for scan coversion
		float miny, maxy;
		miny = maxy = vertex[0].y;

		for (int i = 1; i < edges.size(); i++) {
			Edge edge = (Edge) edges.elementAt(i);
			if (vertex[edge.bottom].y > maxy)
				maxy = vertex[edge.bottom].y;
			if (vertex[edge.top].y < miny)
				miny = vertex[edge.top].y;
		}

		int[] active_edges = new int[numpoints];

		// Adding 0.5 eliminates ambiguities when two edges meet--
		// one edge will be "passed" and the other won't be.
		float y = (float) ((int) miny + 0.5);

		while (y <= maxy) {

			// Search for active edges to add to the table.
			// An edge is considered active if y falls between the edge's y
			// coordinates.

			int active = 0;
			for (int i = 0; i < edges.size(); i++) {
				Edge edge = (Edge) edges.elementAt(i);
				if (vertex[edge.top].y <= y && vertex[edge.bottom].y >= y) {

					// Add to the active edge table.
					active_edges[active++] = i;

				}
			}

			// Each active edge has a "current" coordinate that represents the
			// intersection of the scanline with the edge.
			// Sort these edges from lowest to highest x.
			// The algorithm here is selection sort, the simplest and most
			// inefficient sorting algorithm I know.

			for (int i = 0; i < active - 1; i++)
				for (int j = i; j < active; j++)
					if (currentx[active_edges[i]] > currentx[active_edges[j]]) {
						int temp = active_edges[i];
						active_edges[i] = active_edges[j];
						active_edges[j] = temp;
					}

			// Note how the sorting screwed up the order of the active edge
			// table.
			// That's okay, because we're going to be refilling it next time
			// anyway.

			// We have (essentially) a list of sorted x coordinates for all
			// active edges.
			// Now we draw lines between every other pair.

			for (int i = 0; i < active; i += 2) {

				int leftx = (int) currentx[active_edges[i]];
				int rightx = (int) currentx[active_edges[i + 1]];

				if (y >= bound.y && y <= bound.y + bound.height) {
					// THE INNER LOOP GOES HERE.
					if (leftx < bound.x)
						leftx = bound.x;
					if (rightx >= bound.x + bound.width)
						rightx = bound.x + bound.width - 1;
					Point pt = new Point(leftx, rightx);
					ret.add(pt);
					scanline.add(new Integer((int) y));
				}

			}

			// Now we increment the "current points" along each active edge.

			for (int i = 0; i < active; i++) {
				int edge = active_edges[i];
				currentx[edge] += xincr[edge];
			}

			// Next scanline.
			y++;

		} // End of scanconversion.

		return ret;
	}

	/**
	 * Compute the composition of two imageData First the src will be
	 * transformed by af, then we use the 2D polygon scanconverting algorithm to
	 * fill the transformed area.
	 * 
	 * @param dest
	 *            destination imageData
	 * @param destRect
	 *            destination rectangle (not necessary [0,0,w,h] it may be
	 *            [offsetx, offsety, w, h])
	 * @param src
	 *            imageData will be blended with dest
	 * @param af
	 *            affineTransform for src
	 * @param alfa
	 *            blending coefficient
	 * @return dest
	 */
	public static ImageData computeComposite(ImageData dest,
			Rectangle destRect, ImageData src, AffineTransform af, double alfa) {
		// transform the rectangle to the dest coordinate
		Point[] poly = new Point[4];
		int w = src.width;
		int h = src.height;
		poly[0] = new Point(0, 0);
		poly[1] = new Point(w, 0);
		poly[2] = new Point(w, h);
		poly[3] = new Point(0, h);
		poly = SWT2dUtil.transformPolygon(poly, af);
		// compute the scanline intersections
		Vector scanline = new Vector();
		Vector pairs = SWT2dUtil.scanline2DPolygon(poly, destRect, scanline);

		AffineTransform invAF = new AffineTransform();

		try {
			invAF = af.createInverse();
		} catch (Exception e) {
			e.printStackTrace();
		}

		for (int i = 0; i < pairs.size(); i++) {
			Point p = (Point) pairs.elementAt(i);
			int y = ((Integer) scanline.elementAt(i)).intValue();
			for (int x = p.x; x <= p.y; x++) {
				Point2D pp = invAF.transform(new Point2D.Float(x, y), null);
				RGB drgb = dest.palette.getRGB(dest.getPixel(x - destRect.x, y
						- destRect.y));
				int xx = (int) pp.getX();
				int yy = (int) pp.getY();
				if (xx >= src.width)
					xx = src.width - 1;
				if (xx < 0)
					xx = 0;
				if (yy >= src.height)
					yy = src.height - 1;
				if (yy < 0)
					yy = 0;

				RGB srgb = src.palette.getRGB(src.getPixel(xx, yy));
				int red = (int) (drgb.red * alfa + srgb.red * (1 - alfa));
				int green = (int) (drgb.green * alfa + srgb.green * (1 - alfa));
				int blue = (int) (drgb.blue * alfa + srgb.blue * (1 - alfa));
				int val = ((red & 0xff) << 8) | ((green & 0xff) << 16)
						| ((blue & 0xff) << 24);
				dest.setPixel(x - destRect.x, y - destRect.y, val);
			}
		}

		return dest;
	}

	/**
	 * Compute the composition of two imageData First the src will be
	 * transformed by af, then we use the 2D polygon scanconverting algorithm to
	 * fill the transformed area.
	 * <p>
	 * It will ignore the white pixel in the src data.
	 * 
	 * @param dest
	 *            destination imageData
	 * @param destRect
	 *            destination rectangle.
	 * @param src
	 *            imageData will be blended with dest
	 * @param af
	 *            affineTransform for src
	 * @param alfa
	 *            blending coefficient
	 * @return dest
	 */
	public static ImageData computeCompositeBG(ImageData dest,
			Rectangle destRect, ImageData src, AffineTransform af, float alfa) {
		// transform the rectangle to the dest coordinate
		Point[] poly = new Point[4];
		int w = src.width;
		int h = src.height;
		poly[0] = new Point(0, 0);
		poly[1] = new Point(w, 0);
		poly[2] = new Point(w, h);
		poly[3] = new Point(0, h);
		poly = SWT2dUtil.transformPolygon(poly, af);
		// compute the scanline intersections
		Vector scanline = new Vector();
		Vector pairs = SWT2dUtil.scanline2DPolygon(poly, destRect, scanline);

		AffineTransform invAF = new AffineTransform();

		try {
			invAF = af.createInverse();
		} catch (Exception e) {
			e.printStackTrace();
		}

		for (int i = 0; i < pairs.size(); i++) {
			Point p = (Point) pairs.elementAt(i);
			int y = ((Integer) scanline.elementAt(i)).intValue();
			for (int x = p.x; x < p.y; x++) {
				Point2D pp = invAF.transform(new Point2D.Float(x, y), null);
				RGB drgb = dest.palette.getRGB(dest.getPixel(x - destRect.x, y
						- destRect.y));
				int xx = (int) pp.getX();
				int yy = (int) pp.getY();
				if (xx >= src.width)
					xx = src.width - 1;
				if (xx < 0)
					xx = 0;
				if (yy >= src.height)
					yy = src.height - 1;
				if (yy < 0)
					yy = 0;

				RGB srgb = src.palette.getRGB(src.getPixel(xx, yy));
				if (srgb.red < 230 || srgb.blue < 230 || srgb.green < 230) {
					int red = (int) (drgb.red * alfa + srgb.red * (1 - alfa));
					int green = (int) (drgb.green * alfa + srgb.green
							* (1 - alfa));
					int blue = (int) (drgb.blue * alfa + srgb.blue * (1 - alfa));
					int val = ((red & 0xff) << 8) | ((green & 0xff) << 16)
							| ((blue & 0xff) << 24);
					dest.setPixel(x - destRect.x, y - destRect.y, val);
				}
			}
		}

		return dest;
	}

	/**
	 * Given RGB value, return an int pixel vale.
	 * 
	 * @param rgb
	 *            rgb color
	 * @return int pixel value.
	 */
	static public int createPixel(RGB rgb) {
		int red = rgb.red;
		int green = rgb.green;
		int blue = rgb.blue;
		int pixel = (red << 8) | (green << 16) | (blue << 24);
		return pixel;
	}

	/**
	 * Given a center and the width of shape, compute the leftmost start drawing
	 * point based on the bounding box of GC.
	 * <p>
	 * Used in filling pixels
	 * 
	 * @param center
	 *            center (x) of shape.
	 * @param width
	 *            width (x) of shape
	 * @param bound
	 *            bounding box of GC (image)
	 * @return leftmost start drawing point.
	 */
	public static int getLeft(int center, int width, Rectangle bound) {
		if (width == 1)
			return center;
		int x = center - width / 2;
		if (x < bound.x)
			return bound.x;
		else
			return x;
	}

	/**
	 * Given a center and the height of shape, compute the topmost start drawing
	 * point based on the bounding box of GC.
	 * <p>
	 * Used in filling pixels
	 * 
	 * @param center
	 *            center (y) of shape.
	 * @param height
	 *            height(y) of shape
	 * @param bound
	 *            bounding box of GC (image)
	 * @return topmost start drawing point.
	 */
	public static int getTop(int center, int height, Rectangle bound) {
		if (height == 1)
			return center;
		int y = center - height / 2;
		if (y < bound.y)
			return bound.y;
		else
			return y;
	}

	/**
	 * Return the extension of the file name. For exampel: abc.xml will return
	 * "xml".
	 * 
	 * @param filename
	 *            filename
	 * @return The extension of the file.
	 */
	public static String getFileExtension(String filename) {
		if (filename == null)
			return filename;

		int index = filename.lastIndexOf(".");
		if (index != -1) {
			return filename.substring(filename.lastIndexOf(".") + 1);
		}
		return filename;
	}

}
