/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.loader;

import java.util.Vector;

/**
 * An class provides image loaders for loading/saving image.
 * <p>
 * 
 * @author Chengdong Li
 * @see IImageLoader
 * 
 */
public class ImageLoaderFactory {
	// All image loaders for this platform
	private final static Vector loaders = new Vector();
	static {
		registerDefaultLoader();
	}

	public ImageLoaderFactory() {
		registerDefaultLoader();
	}

	/**
	 * Register the image loader for image loading ans saving. By default, it
	 * will register the SWTLoader. You can add loader later with addLoader().
	 */
	public static void registerDefaultLoader() {
		loaders.add(new SWTLoader());
	}

	/**
	 * register image loader.
	 * 
	 * @param loader
	 *            image loader to be registered.
	 */
	public static void registerLoader(IImageLoader loader) {
		loaders.add(loader);
	}

	/**
	 * unregister image loader.
	 * 
	 * @param loader
	 *            image loader to be unregistered.
	 */
	public static void unRegisterLoader(IImageLoader loader) {
		for (int i = 0; i < loaders.size(); i++) {
			if (loaders.elementAt(i) == loader) {
				loaders.removeElementAt(i);
				break;
			}
		}
	}

	/**
	 * Create a image reader for the specifiled image file.
	 * 
	 * @param format
	 *            image format
	 * @return image loader which can load this file
	 * @throws UnknownImageFormatException
	 */
	public static IImageLoader createReaderFor(String format)
			throws UnknownImageFormatException {
		for (int i = 0; i < loaders.size(); i++) {

			// System.out.println("i: " + i);
			// System.out.println("Format: " + format);
			IImageLoader loader = (IImageLoader) loaders.elementAt(i);
			if (loader.canRead(format))
				return loader;
		}
		throw new UnknownImageFormatException(format);
	}

}
