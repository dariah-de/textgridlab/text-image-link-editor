/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.canvas;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.swt.widgets.Composite;

/**
 * <p>
 * A scrollable and zoomable image canvas, with different sessions including
 * selection function.
 * <p>
 * You can extend this class to get the image canvas with selection and lense
 * function.
 * <p>
 * 
 * @author Chengdong Li
 */
public class SWTImageCanvas extends ThumbedImageCanvas {
	protected SWTImageCanvas instance;

	public SWTImageCanvas(Composite parent) {
		super(parent);
		instance = this;
	}

	/**
	 * Constructor for ScrollableCanvas.
	 * 
	 * @param parent
	 *            the parent of this control
	 */
	public SWTImageCanvas(Composite parent, int style) {
		super(parent, style);
		instance = this;
	}

	@Override
	public void initContextMenu(IMenuManager menuManager) {
		super.initContextMenu(menuManager);

	}

}
