/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.canvas;

import org.eclipse.swt.graphics.GC;

/**
 * Paint Contributor contributes to the canvas painting process. Interface will
 * either be called before or after the normal rendering method and all
 * rendering will be appeared on screen.
 * <p>
 * You can implement this interface and register it to the image canvas, so that
 * your implementation will be called each time the canvas redraw itself.
 * 
 * @author Chengdong Li
 */
public interface IPaintContributor {
	/**
	 * Prepaint processing which will be called before canvas rendering. Usually
	 * for pre-processing before rendering.
	 * 
	 * @param gc
	 *            graphics context.
	 */
	public void prePaint(GC gc);

	/**
	 * Paint processing which will be called when canvas rendering right after
	 * it paint itself. This is an buffered painting.
	 * 
	 * @param gc
	 *            graphics context.
	 */
	public void paint(GC gc);

	/**
	 * Postpaint processing which will be called after canvas rendering.
	 * 
	 * @param gc
	 *            graphics context.
	 */
	public void postPaint(GC gc);
}
