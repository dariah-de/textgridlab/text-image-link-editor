/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.canvas.session;

import info.textgrid.lab.linkeditor.mip.component.canvas.SessionedImageCanvas;

import java.awt.geom.AffineTransform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Point;

/**
 * Freely move session.
 * <p>
 * Move the image around the current canvas. It also update the scrollbars.
 * <p>
 * This session will also appear whenever the whitespace+mouse is pressed.
 * <p>
 * 
 * @author Chengdong Li
 */
public class BasicMoveSession extends AbstractImageSession {
	private Point hotSpot = new Point(0, 0);
	private Point oldHotSpot = new Point(0, 0);
	private boolean inProgress = false;

	public BasicMoveSession() {
	}

	/**
	 * Constructs a session.
	 * 
	 * @param id
	 *            session id.
	 * 
	 */
	public BasicMoveSession(String id) {
		super(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeinfo.textgrid.lab.linkeditor.mip.gui.sessiondelegate.IImageSession#
	 * beginSession()
	 */
	@Override
	public void beginSession(Object canvas) {
		super.beginSession(canvas);
		inProgress = false;
		setCursor(SWT.CURSOR_SIZEALL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * info.textgrid.lab.linkeditor.mip.gui.sessiondelegate.IImageSession#endSession
	 * ()
	 */
	@Override
	public void endSession() {
		super.endSession();
		inProgress = false;
	}

	/**
	 * Customize session cursor
	 */
	@Override
	public Cursor getSessionCursor() {
		return new Cursor(sessionCanvas.getDisplay(), SWT.CURSOR_SIZEALL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.events.MouseListener#mouseDown(org.eclipse.swt.events
	 * .MouseEvent)
	 */
	@Override
	public void mouseDown(MouseEvent evt) {

		if (evt.getSource() instanceof SessionedImageCanvas) {
			SessionedImageCanvas canvas = (SessionedImageCanvas) (evt
					.getSource());
			if (canvas.getSourceImage() == null)
				return;

			setCursor(SWT.CURSOR_SIZEALL);

			// start the interaction
			if (evt.button == 1) {
				hotSpot.x = oldHotSpot.x = evt.x;
				hotSpot.y = oldHotSpot.y = evt.y;
				inProgress = true;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.swt.events.MouseListener#mouseUp(org.eclipse.swt.events.
	 * MouseEvent)
	 */
	@Override
	public void mouseUp(MouseEvent evt) {
		// stop the interaction
		if (evt.getSource() instanceof SessionedImageCanvas) {
			SessionedImageCanvas canvas = (SessionedImageCanvas) (evt
					.getSource());

			if (canvas.getSourceImage() == null)
				return;

			if (inProgress) {
				inProgress = false;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.events.MouseMoveListener#mouseMove(org.eclipse.swt.events
	 * .MouseEvent)
	 */
	@Override
	public void mouseMove(MouseEvent evt) {
		super.mouseMove(evt);

		SessionedImageCanvas canvas = (SessionedImageCanvas) evt.getSource();

		if (canvas == null)
			return;

		if (inProgress) {
			hotSpot.x = Math.max(evt.x, 0);
			hotSpot.x = Math.min(hotSpot.x, canvas.getSize().x);
			hotSpot.y = Math.max(evt.y, 0);
			hotSpot.y = Math.min(hotSpot.y, canvas.getSize().y);
			// notify the observer
			int deltax = hotSpot.x - oldHotSpot.x;
			int deltay = hotSpot.y - oldHotSpot.y;
			AffineTransform trans = AffineTransform.getTranslateInstance(
					deltax, deltay);
			AffineTransform af = canvas.getTransform();
			af.preConcatenate(trans);
			canvas.setTransform(af);
			canvas.syncScrollBars();
			canvas.redraw();

			oldHotSpot.x = hotSpot.x;
			oldHotSpot.y = hotSpot.y;
		}

	}

	@Override
	public void mouseDoubleClick(MouseEvent evt) {
		super.mouseDoubleClick(evt);
	}

}