/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.loader;

import org.eclipse.swt.graphics.ImageData;

/**
 * Interface for image loader.
 * <p>
 * You can add your own image loaders to overcome the shortcoming of SWT image
 * loaders. For example, you may add your own image loader to load .psd file
 * from photoshop.
 * 
 * @author Chengdong Li
 */
public interface IImageLoader {
	/** Read mode (0) */
	public static int READ = 0;
	/** Write mode (1) */
	public static int WRITE = 1;

	/**
	 * Create loader for specific format, with the specified mode
	 * 
	 * @param format
	 *            image format
	 * @param mode
	 *            access mode: READ or WRITE
	 * @return IImageLoader
	 * @throws UnknownImageFormatException
	 */
	public IImageLoader createLoaderFor(String format, int mode)
			throws UnknownImageFormatException;

	/**
	 * Read image data from file
	 * 
	 * @param filename
	 *            given image file name
	 * @return ImageData loaded from image file
	 */
	public ImageData readImageData(String filename);

	/**
	 * Write image data into file
	 * 
	 * @param filename
	 *            image file to be written
	 * @param format
	 *            write format
	 * @param data
	 *            ImageData to be written
	 */
	public void writeImageData(String filename, String format, ImageData data);

	/**
	 * Get all supported read formats
	 * 
	 * @return A String array contains all supported reading formats
	 */
	public String[] getReadFormats();

	/**
	 * Get all supported write formats
	 * 
	 * @return A String array contains all supported writing formats
	 */
	public String[] getWriteFormats();

	/**
	 * Check if given image format is supported for read
	 * 
	 * @param format
	 *            the given image format
	 * @return <ul>
	 *         <li><b>true</b> if image format is supported for read</li>
	 *         <li><b>false</b> if image format is not supported for read</li>
	 *         </ul>
	 */
	public boolean canRead(String format);

	/**
	 * Check if given image format is supported for write
	 * 
	 * @param format
	 *            the given image format
	 * @return <ul>
	 *         <li><b>true</b> if image format is supported for write</li>
	 *         <li><b>false</b> if image format is not supported for write</li>
	 *         </ul>
	 */
	public boolean canWrite(String format);

}
