/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.canvas.session;

import info.textgrid.lab.linkeditor.mip.component.canvas.SessionedImageCanvas;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;

/**
 * Zoom lense session.
 * <p>
 * Using keyboard operation which can change the zoom rate and using mouse
 * operation can change shape of lense.
 * <p>
 * The Lenseparam is just for maintain lense parameters.
 * <p>
 * The basic mechanism is as following:
 * <ol>
 * <li>at the begining of session, save the screen image</li>
 * <li>use the ctrl+mouse to select the lense shape (select mode)</li>
 * <li>use the 2,3,4 keyboard to change zoom rate</li>
 * <li>use mouse move to show the zoomed image (zoom mode)</li>
 * <li>we save the whole off screen image, and try to recover it</li>
 * <li>the lense moves by computing the difference of the movement.</li>
 * </ol>
 * 
 * @author Chengdong Li
 * 
 */
public class BasicLenseSession extends AbstractImageSession {
	LenseParam lenseParam;

	public BasicLenseSession() {
	}

	/**
	 * Constructor.
	 * 
	 * @param id
	 *            session id.
	 */
	public BasicLenseSession(String id) {
		super(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeinfo.textgrid.lab.linkeditor.mip.gui.sessiondelegate.IImageSession#
	 * beginSession()
	 */
	@Override
	public void beginSession(Object canvas) {
		super.beginSession(canvas);
		lenseParam = new LenseParam();
		lenseParam.canvas = (SessionedImageCanvas) canvas;
		refreshSWTImage(lenseParam);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * info.textgrid.lab.linkeditor.mip.gui.sessiondelegate.IImageSession#endSession
	 * ()
	 */
	@Override
	public void endSession() {
		super.endSession();
	}

	/**
	 * Customize session cursor
	 */
	@Override
	public Cursor getSessionCursor() {
		ImageData sourceData = new ImageData(32, 32, 1, paletteData);
		ImageData maskData = new ImageData(32, 32, 1, paletteData);
		sourceData.setPixels(0, 0, 1024, cursorLense, 0);
		maskData.setPixels(0, 0, 1024, cursorMask, 0);
		return new Cursor(sessionCanvas.getDisplay(), sourceData, maskData, 6,
				5);
	}

	/**
	 * Update the image buffer.
	 * 
	 * @param param
	 */
	public void refreshSWTImage(LenseParam param) {
		if (param.canvasGC != null) {
			param.canvasGC.dispose();
			param.canvasGC = null;
		}
		if (param.all != null) {
			param.all.dispose();
			param.all = null;
		}

		param.canvasGC = new GC(param.canvas);
		param.canvas.setForeground(param.canvas.getDisplay().getSystemColor(
				SWT.COLOR_WHITE));

		param.all = new Image(param.canvas.getDisplay(),
				param.canvas.getClientArea().width,
				param.canvas.getClientArea().height);
		param.canvasGC.copyArea(param.all, 0, 0);

	}

	/*
	 * @see KeyListener#keyPressed
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		LenseParam param;
		param = lenseParam;
		if (e.character == 0x32 && e.stateMask == 0) { // 2x
			param.zoomFactor = 2;
		}
		if (e.character == 0x33 && e.stateMask == 0) { // 3x
			param.zoomFactor = 3;
		}
		if (e.character == 0x34 && e.stateMask == 0) { // 4x
			param.zoomFactor = 4;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.events.MouseListener#mouseDown(org.eclipse.swt.events
	 * .MouseEvent)
	 */
	@Override
	public void mouseDown(MouseEvent evt) {
		super.mouseDown(evt);

		LenseParam param;

		if (evt.getSource() instanceof SessionedImageCanvas) {
			SessionedImageCanvas canvas = (SessionedImageCanvas) evt
					.getSource();
			if (canvas.getSourceImage() == null)
				return;

			if (!canvas.isFocusControl())
				canvas.setFocus();
			param = lenseParam;

			if (evt.button == 1) {
				if ((evt.stateMask & SWT.CTRL) != 0) {
					// select the param.lenseBound
					param.startPoint = new Point(evt.x, evt.y);
					if (param.canvasGC != null) {
						param.canvasGC.dispose();
						param.canvasGC = null;
					}
					param.canvasGC = new GC(param.canvas);
					param.canvasGC.setForeground(param.canvas.getDisplay()
							.getSystemColor(SWT.COLOR_WHITE));
					param.canvasGC.setXORMode(true);
					param.selectMode = true;
				} else { // draw zoom image.
					if (!param.zoomMode) {
						param.zoomMode = true;
						param.hotSpot.x = param.oldHotSpot.x = evt.x;
						param.hotSpot.y = param.oldHotSpot.y = evt.y;
						refreshSWTImage(param);
						draw(param);
					} else {
					}
				}

			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.swt.events.MouseListener#mouseUp(org.eclipse.swt.events.
	 * MouseEvent)
	 */
	@Override
	public void mouseUp(MouseEvent evt) {
		if (evt.getSource() instanceof SessionedImageCanvas) {
			SessionedImageCanvas canvas = (SessionedImageCanvas) evt
					.getSource();
			if (canvas.getSourceImage() == null)
				return;

			if (!canvas.isFocusControl())
				canvas.setFocus();
			LenseParam param;
			param = lenseParam;
			if (evt.button == 1) {
				if (param.selectMode) {
					// select param.lenseBound
					param.selectMode = false;
					param.endPoint = new Point(evt.x, evt.y);
					Rectangle rect = createRectangle(param.startPoint,
							param.endPoint);
					param.canvasGC.drawRectangle(rect);
					param.canvasGC.setXORMode(false);

					// initialize the non-param.selectMode variables
					param.lenseBound = rect;
					param.oldHotSpot.x = param.endPoint.x;
					param.oldHotSpot.y = param.endPoint.y;
					param.hotSpot.x = param.endPoint.x;
					param.hotSpot.y = param.endPoint.y;

					param.startPoint = null;
					param.endPoint = null;

					if (param.lenseBound.width == 0
							|| param.lenseBound.height == 0) {
						param.lenseBound.width = 50;
						param.lenseBound.height = 50;
						return;
					}
				} else {
					// zoom mode
					restore(param); // restore the origial image
					param.zoomMode = false;
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.events.MouseMoveListener#mouseMove(org.eclipse.swt.events
	 * .MouseEvent)
	 */
	@Override
	public void mouseMove(MouseEvent evt) {
		super.mouseMove(evt);

		LenseParam param;
		param = lenseParam;
		// if((evt.stateMask & SWT.CTRL) != 0 && param.startPoint!=null &&
		// param.endPoint!=null){
		if ((evt.stateMask & SWT.CTRL) != 0 && param.startPoint != null) {

			if (param.endPoint != null) {
				Rectangle rect = createRectangle(param.startPoint,
						param.endPoint);
				param.canvasGC.drawRectangle(rect);
				param.endPoint = new Point(evt.x, evt.y);
				rect = createRectangle(param.startPoint, param.endPoint);
				param.canvasGC.drawRectangle(rect);
			} else {
				param.endPoint = new Point(evt.x, evt.y);
				Rectangle rect = createRectangle(param.startPoint,
						param.endPoint);
				param.canvasGC.drawRectangle(rect);
			}

		} else if (param.zoomMode) {
			param.hotSpot.x = evt.x;
			param.hotSpot.y = evt.y;
			updateImage(param);
			param.oldHotSpot.x = param.hotSpot.x;
			param.oldHotSpot.y = param.hotSpot.y;
		} else {
		}
	}

	/**
	 * Given any two point, return a rectangle with positive width and height.
	 * 
	 * @param p1
	 *            point 1
	 * @param p2
	 *            point 2
	 * @return rectangle with poisitive width and height
	 */
	private Rectangle createRectangle(Point p1, Point p2) {
		int x0, y0, w, h;
		if (p1.x > p2.x) {
			x0 = p2.x;
			w = p1.x - p2.x + 1;
		} else {
			x0 = p1.x;
			w = p2.x - p1.x + 1;
		}
		if (p1.y > p2.y) {
			y0 = p2.y;
			h = p1.y - p2.y + 1;
		} else {
			y0 = p1.y;
			h = p2.y - p1.y + 1;
		}
		return new Rectangle(x0, y0, w, h);
	}

	// make sure the param.lenseBound reasonable
	private void checkLenseBound(LenseParam param) {
		Rectangle canvasBound = param.canvas.getClientArea();
		if (param.lenseBound.width > canvasBound.width) {
			param.lenseBound.width = canvasBound.width;
		}
		if (param.lenseBound.height > canvasBound.height) {
			param.lenseBound.height = canvasBound.height;
		}
	}

	// make sure the hotPoints is leagal
	private void checkHotPoints(LenseParam param) {
		Rectangle canvasBound = param.canvas.getClientArea();
		if (param.hotSpot.x <= param.lenseBound.width) {
			param.hotSpot.x = param.lenseBound.width - 1;
		}
		if (param.hotSpot.y <= param.lenseBound.height) {
			param.hotSpot.y = param.lenseBound.height - 1;
		}
		if (param.hotSpot.x >= canvasBound.width) {
			param.hotSpot.x = canvasBound.width - 1;
		}
		if (param.hotSpot.y >= canvasBound.height) {
			param.hotSpot.y = canvasBound.height - 1;
		}

		if (param.oldHotSpot.x <= param.lenseBound.width) {
			param.oldHotSpot.x = param.lenseBound.width - 1;
		}
		if (param.oldHotSpot.y <= param.lenseBound.height) {
			param.oldHotSpot.y = param.lenseBound.height - 1;
		}
		if (param.oldHotSpot.x >= canvasBound.width) {
			param.oldHotSpot.x = canvasBound.width - 1;
		}
		if (param.oldHotSpot.y >= canvasBound.height) {
			param.oldHotSpot.y = canvasBound.height - 1;
		}

		param.lenseBound.x = param.hotSpot.x - param.lenseBound.width + 1;
		param.lenseBound.y = param.hotSpot.y - param.lenseBound.height + 1;
	}

	private void draw(LenseParam param) {
		// 1) find the new param.hotSpot based on the current leftCanvas size,
		// the
		// lense bound and the given hotspot.
		doubleCheck(param);
		// 2) save the param.original area zoomed into lense ==> param.original
		Rectangle orig = compOriginArea(param);
		if (orig.width == 0 || orig.height == 0)
			return;
		if (param.original != null) {
			param.original.dispose();
		}
		param.original = new Image(param.canvas.getDisplay(), orig.width,
				orig.height);
		param.canvasGC.copyArea(param.original, orig.x, orig.y);
		// 3) draw the magnified image of orig into param.lenseBound
		param.canvasGC.drawImage(param.original, 0, 0, orig.width, orig.height,
				param.lenseBound.x, param.lenseBound.y, param.lenseBound.width,
				param.lenseBound.height);
		param.lenseBound.width--;
		param.lenseBound.height--;
		param.canvasGC.drawRectangle(param.lenseBound);
		param.lenseBound.width++;
		param.lenseBound.height++;

	}

	/**
	 * Compute the param.original image area which will be zoomed into the lense
	 * area.
	 */
	private Rectangle compOriginArea(LenseParam param) {
		Point center = getLenseCenter(param);
		int width = param.lenseBound.width / param.zoomFactor;
		int height = param.lenseBound.height / param.zoomFactor;
		Rectangle rect = new Rectangle(center.x - width / 2, center.y - height
				/ 2, width, height);
		return rect;
	}

	private Point getLenseCenter(LenseParam param) {
		Point center = new Point(0, 0);
		center.x = param.hotSpot.x - param.lenseBound.width / 2;
		center.y = param.hotSpot.y - param.lenseBound.height / 2;
		return center;
	}

	/**
	 * find the new param.hotSpot based on the current leftCanvas size, the
	 * lense bound and the given hotspot.
	 */
	private void doubleCheck(LenseParam p) {
		checkLenseBound(p);
		checkHotPoints(p);
	}

	// Still need to deal with the edge..
	private void updateImage(LenseParam param) {
		checkHotPoints(param);
		Rectangle orig = compOriginArea(param);
		// Here the hotspot is the bottom-right point of rectangle.
		Rectangle oldMag = new Rectangle(param.oldHotSpot.x
				- param.lenseBound.width + 1, param.oldHotSpot.y
				- param.lenseBound.height + 1, param.lenseBound.width,
				param.lenseBound.height);
		Rectangle newMag = new Rectangle(param.hotSpot.x
				- param.lenseBound.width + 1, param.hotSpot.y
				- param.lenseBound.height + 1, param.lenseBound.width,
				param.lenseBound.height);
		// draw new Image.
		try {
			param.canvasGC.drawImage(param.all, orig.x, orig.y, orig.width,
					orig.height, newMag.x, newMag.y, newMag.width,
					newMag.height);
		} catch (Exception e) {
			e.printStackTrace();
		}

		int x0 = oldMag.x, y0 = oldMag.y, w = oldMag.width, h = oldMag.height;
		int dx = param.hotSpot.x - param.oldHotSpot.x, dy = param.hotSpot.y
				- param.oldHotSpot.y;
		int absDx = Math.abs(dx), absDy = Math.abs(dy);

		// resume param.original.
		int x1 = 0, y1 = 0, w1 = 0, h1 = 0;
		int x2 = 0, y2 = 0, w2 = 0, h2 = 0;

		// SE
		if (dx > 0 && dy > 0) {
			x1 = x0;
			y1 = y0;
			w1 = absDx + 1;
			h1 = h + 1;
			x2 = x0 + dx;
			y2 = y0;
			w2 = w - absDx + 1;
			h2 = absDy + 1;
		}

		// NW
		if (dx < 0 && dy < 0) {
			x1 = x0 + w + dx;
			y1 = y0;
			w1 = absDx + 1;
			h1 = h + 1;
			x2 = x0;
			y2 = y0 + h + dy;
			w2 = w - absDx + 1;
			h2 = absDy + 1;

		}

		// NE
		if (dx > 0 && dy < 0) {
			x1 = x0;
			y1 = y0;
			w1 = absDx + 1;
			h1 = h + 1;
			x2 = x0 + dx;
			y2 = y0 + dy + h;
			w2 = w - absDx + 1;
			h2 = absDy + 1;
		}

		// SW
		if (dx < 0 && dy > 0) {
			x1 = x0 + w + dx;
			y1 = y0;
			w1 = absDx + 1;
			h1 = h + 1;
			x2 = x0;
			y2 = y0;
			w2 = w - absDx + 1;
			h2 = absDy + 1;
		}

		// N
		if (dx == 0 && dy < 0) {
			x1 = x0;
			y1 = y0 + dy + h;
			w1 = w + 1;
			h1 = absDy + 1;
			x2 = y2 = w2 = h2 = 0;
		}

		// S
		if (dx == 0 && dy > 0) {
			x1 = x0;
			y1 = y0;
			w1 = w + 1;
			h1 = absDy;
			x2 = y2 = w2 = h2 = 0;
		}

		// E
		if (dx > 0 && dy == 0) {
			x1 = x0;
			y1 = y0;
			w1 = absDx + 1;
			h1 = h + 1;
			x2 = y2 = w2 = h2 = 0;
		}

		// W
		if (dx < 0 && dy == 0) {
			x1 = x0 + dx + w;
			y1 = y0;
			w1 = absDx + 1;
			h1 = h + 1;
			x2 = y2 = w2 = h2 = 0;
		}

		try {
			if (w1 < 0 || w2 < 0) { // too fast, so redraw param.all old part.
				System.out.println("width<0, in BasicLenseSession");
				param.canvasGC.drawImage(param.all, x0, y0, w, h, x0, y0, w, h);
			} else {
				if (x1 < 0) {
					w1 = w1 + x1;
					x1 = 0;
				}
				if (y1 < 0) {
					h1 = h1 + y1;
					y1 = 0;
				}
				if (x2 < 0) {
					w2 = w2 + x2;
					x2 = 0;
				}
				if (y2 < 0) {
					h2 = h2 + y2;
					y2 = 0;
				}
				if (w1 < 0) {
					w1 = 0;
				}
				if (w2 < 0) {
					w2 = 0;
				}
				if (h1 < 0) {
					h1 = 0;
				}
				if (h2 < 0) {
					h2 = 0;
				}
				Rectangle bound = param.all.getBounds();
				if (x1 > bound.width) {
					x1 = bound.width;
					w1 = 0;
				}
				if (y1 > bound.height) {
					y1 = bound.height;
					h1 = 0;
				}
				if (x2 > bound.width) {
					x2 = bound.width;
					w2 = 0;
				}
				if (y2 > bound.height) {
					y2 = bound.height;
					h2 = 0;
				}
				if (w1 + x1 > bound.width) {
					w1 = bound.width - x1;
				}
				if (h1 + y1 > bound.height) {
					h1 = bound.height - y1;
				}
				if (w2 + x2 > bound.width) {
					w2 = bound.width - x2;
				}
				if (h2 + y2 > bound.height) {
					h2 = bound.height - y2;
				}

				param.canvasGC.drawImage(param.all, x1, y1, w1, h1, x1, y1, w1,
						h1);
				param.canvasGC.drawImage(param.all, x2, y2, w2, h2, x2, y2, w2,
						h2);
			}
			// rectangle is extent, not points, so with=1 means has two points.
			newMag.width--;
			newMag.height--;
			param.canvasGC.drawRectangle(newMag);
		} catch (Exception ie) {
			ie.printStackTrace();
		}
	}

	// used for restore param.original image (screenImage)
	private void restore(LenseParam param) {
		param.canvasGC.drawImage(param.all, 0, 0);
	}

	class LenseParam {
		public int zoomFactor = 2;

		public GC canvasGC = null;
		public Image all; // screen image
		public Image original; // original image under lense

		public Rectangle lenseBound = new Rectangle(0, 0, 200, 200);

		// select mode variables.
		public boolean selectMode = false;
		public Point startPoint;
		public Point endPoint;

		// non-param.selectMode varaiable
		public Point hotSpot = new Point(0, 0);
		public Point oldHotSpot = new Point(0, 0);
		public boolean zoomMode = false;

		public SessionedImageCanvas canvas;

		public LenseParam() {
		}
	}

	static int[] cursorLense = new int[] { 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1,
			1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1,
			0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0,
			0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
			0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
			0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

}
