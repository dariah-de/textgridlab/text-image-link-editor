package info.textgrid.lab.linkeditor.mip.component.position;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.linkeditor.mip.component.position.messages"; //$NON-NLS-1$
	public static String UIPositionDialog_AngleOfRotation;
	public static String UIPositionDialog_SetRotationAngle;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
