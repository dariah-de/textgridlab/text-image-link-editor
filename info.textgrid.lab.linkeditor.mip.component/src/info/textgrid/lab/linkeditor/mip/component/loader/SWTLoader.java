/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.loader;

import info.textgrid.lab.linkeditor.mip.component.canvas.SWT2dUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.eclipse.swt.graphics.ImageData;

/**
 * SWT Image loader wrapper.
 * <p>
 * This Image loader has wrapped up the default ImageLoader from SWT.
 * 
 * @author Chengdong Li
 */
public class SWTLoader extends AbstractLoader {
	// ImageData imageData=null;

	public SWTLoader() {
		READ_FORMATS = new String[] { "bmp", "ico", "jpg", "jpeg", "gif",
				"png", "tiff" };
		WRITE_FORMATS = new String[] { "bmp", "ico", "jpg", "jpeg", "gif",
				"png", "tiff" };
	}

	@Override
	public ImageData readImageData(String filename) {
		try {
			// ImageData imageData = new ImageData(new FileInputStream(new
			// File(filename)));
			// return imageData;
			InputStream in = new FileInputStream(new File(filename));
			return readImageData(in);
		} catch (Exception e) {
			System.out.println("failed to load " + filename + " in SWTLoader");
			return null;
		}
	}

	public ImageData readImageData(InputStream in) {
		try {
			ImageData imageData = new ImageData(in);
			return imageData;
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public void writeImageData(String filename, String format, ImageData data) {
		SWT2dUtil.saveSWTImage(data, filename, format);
	}
}
