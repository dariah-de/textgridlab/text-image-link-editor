/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.views;

import org.eclipse.swt.graphics.Image;

/**
 * An image surface: The surface of the M(Mediator)S(Surface)T(ThumbView) model.
 * <p/>
 * Any component (ViewPart or Canvas or other SWT widget) who implement this
 * interface can reigister itself to the ThumbViewMediator and collaborate with
 * ThumbView to manipulate the image inside the surface.
 * <p/>
 * 
 * @author Chengdong Li
 */
public interface IImageSurface {
	/**
	 * Get the image of the Image Surface.
	 * 
	 * @return image of the Image Surface.
	 */
	public Image getImage();

	/**
	 * How much percentage of the Left shift, i.e., how much percentage of image
	 * is beyond the left bound of the current Image Surface (canvas)?
	 * 
	 * @return percentage of left invisible image.
	 */
	public double getTX();

	/**
	 * How much percentage of the up shift, i.e., how much percentage of image
	 * is beyond the upper bound of the current Image Surface (canvas)?
	 * 
	 * @return percentage of upper invisible image.
	 */
	public double getTY();

	/**
	 * How much percentage of the X-visible image, i.e., how much percentage of
	 * image horizontally is inside the current Image Surface (canvas)?
	 * 
	 * @return percentage of horizontally visible image.
	 */
	public double getSX();

	/**
	 * How much percentage of the Y-visible image, i.e., how much percentage of
	 * image vertically is inside the current Image Surface (canvas)?
	 * 
	 * @return percentage of vertically visible image.
	 */
	public double getSY();

	/**
	 * Get the current scale ratio in X direction (horizontal).
	 * 
	 * @return current horizontal scale ratio.
	 */
	public double getScaleX();

	/**
	 * Get the current scale ratio in Y direction (vertical).
	 * 
	 * @return current vertical scale ratio.
	 */
	public double getScaleY();

	/**
	 * Called when Thumb View scrolls.
	 * 
	 * @param tx
	 *            percentage of left-shift.
	 * @param ty
	 *            percentage of upper-shift.
	 */
	public void onTVScroll(double tx, double ty);

	/**
	 * Called when Thumb View zooms, in our case, sx=sy usually since the
	 * zoomimg is equal in X and Y direction.
	 * 
	 * @param sx
	 *            percentage of horizontally-visible image.
	 * @param sy
	 *            percentage of vertically-visible image.
	 */
	public void onTVZoom(double sx, double sy);

	/**
	 * Check if the Image Surface has been disposed or not.
	 * 
	 * @return true: disposed; false: not disposed.
	 */
	public boolean isDisposed();

	/**
	 * Check if the Image Surface is focused or not.
	 * 
	 * @return true: focused; false: not focused.
	 */
	public boolean isFocused();

	/**
	 * Check if the Image Surface is visible or not.
	 * 
	 * @return true: visible; false: not visible.
	 */
	public boolean isVisible();
}
