package info.textgrid.lab.linkeditor.mip.component.canvas.session;

import info.textgrid.lab.linkeditor.mip.component.canvas.SessionedImageCanvas;
import info.textgrid.lab.linkeditor.model.graphics.TGLine;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;

/**
 * DockingLine Select session. Extends BasicSelectSession
 * <p>
 * This class is for drawing DockingLines.
 * <p>
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * @author leuk
 * 
 */
public class DockingLineSelectSession extends BasicSelectSession {

	private Point begin = new Point(0, 0);

	private TGLine workLine = null;
	private List<Line> lines = Collections
			.synchronizedList(new ArrayList<Line>());

	public DockingLineSelectSession() {
		super();
		currentShape = new TGLine("");
		moveShape = new TGLine("");
	}

	/**
	 * Constructs a session.
	 * 
	 * @param id
	 *            session id.
	 */
	public DockingLineSelectSession(String id) {
		super(id);
		currentShape = new TGLine("");
		moveShape = new TGLine("");
	}

	/**
	 * Handles mouse Double Clicks Events
	 * 
	 * @param evt
	 * 
	 */
	@Override
	public void mouseDoubleClick(MouseEvent evt) {
		if (evt.getSource() instanceof SessionedImageCanvas) {
			// if no image, we return;
			if (sessionCanvas.getSourceImage() == null)
				return;

			if (evt.button == 1) { // left button only
				// To reset the drawing polygon on loading an image

				if (sessionCanvas.imageReloaded) {
					workLine = null;
					lines.clear();
					sessionCanvas.imageReloaded = false;
				}

				if (selectShape(evt)) {
					workLine = null;
					lines.clear();
					// *** this.triggerPaint();
					sessionCanvas.redraw();
				}
			}
		}
	};

	/**
	 * Handles mouse Down Events
	 * 
	 * @param evt
	 * 
	 */
	@Override
	public void mouseDown(MouseEvent evt) {
		// start the interaction
		if (evt.getSource() instanceof SessionedImageCanvas) {
			// if no image, we return;
			if (sessionCanvas.getSourceImage() == null)
				return;

			if (evt.button == 1) { // left button only

				// checkCursor(evt);

				// To reset the drawing line on loading an image
				if (sessionCanvas.imageReloaded) {
					workLine = null;
					lines.clear();
					sessionCanvas.imageReloaded = false;
				}

				if (cursorStyle != SWT.CURSOR_CROSS) {
					resizeMode = true;
					// used for computing moving increment.
					lastPoint = new Point(evt.x, evt.y);

					moveShape = sessionCanvas.getSelectedShape();

					currentShape = sessionCanvas.imageToScreen(sessionCanvas
							.getSelectedShape());
				} else {
					// draw a Line at the current point
					if (workLine == null)
						workLine = new TGLine(this.sessionCanvas.getImageUri());
					begin.x = evt.x;
					begin.y = evt.y;
					workLine.addPoint(begin);
				}

				if (roiGC != null) {
					roiGC.dispose();
				}
				roiGC = new GC(sessionCanvas);
				roiGC.setForeground(sessionCanvas.getDisplay().getSystemColor(
						SWT.COLOR_CYAN));
				roiGC.setLineStyle(SWT.LINE_DASH);
				roiGC.setLineWidth(3);
				roiGC.setXORMode(true);
				// *** this.abortTrigger();
				// drawSelectionFrame(null, currentPoly);

				sessionCanvas.redraw();
				mousePressed = true;
			}
		}
	}

	/**
	 * Handles mouse Move Events
	 * 
	 * @param evt
	 * 
	 */
	@Override
	public void mouseMove(MouseEvent evt) {
		super.mouseMoveOfBase(evt);

		int x = evt.x;
		int y = evt.y;
		TGShape selectShape = sessionCanvas.getSelectedShape();

		if (mousePressed) {
			// *** abortTrigger();
			if (resizeMode) {
				processMoveOrResize(x, y);
			} else { // draw new polygon
				if (currentShape instanceof TGLine) {
					roiGC.drawLine(begin.x, begin.y, x, y);
					sessionCanvas.redraw();
					for (Line l : lines) {
						roiGC.drawLine(l.begin.x, l.begin.y, l.end.x, l.end.y);
					}
				}
			}
		} else {
			Point p = sessionCanvas.screenToImage(new Point(x, y));
			for (TGShape s : sessionCanvas.getArrayListShapes()) {
				if (!s.equals(selectShape) || cursorStyle == SWT.CURSOR_CROSS) {
					if (s.contains(p.x, p.y)) {
						currentShape = s;
						sessionCanvas.redraw();
						break;
					}
				}
			}
			setCurrentCursor(p.x, p.y);
		}
	}

	/**
	 * Handles mouse Up Events
	 * 
	 * @param evt
	 * 
	 */
	@Override
	public void mouseUp(MouseEvent evt) {
		if (evt.button == 1) { // left button only
			if (evt.getSource() instanceof SessionedImageCanvas) {

				if (sessionCanvas.getSourceImage() == null)
					return;

				// to avoid creating a shape on double-click on the frame
				// of the lab (maximize/minimize)
				if (!mousePressed)
					return;

				if (resizeMode) {
					sessionCanvas.redraw();
					sessionCanvas.setResize(true);
				}

				if (workLine != null) {
					Point end = new Point(evt.x, evt.y);
					workLine.addPoint(end);

					if (!workLine.isDeltaXOK()) {
						workLine = null;
						lines.clear();
						mousePressed = false;
						resizeMode = false;
						// *** this.triggerPaint();
						sessionCanvas.setResize(false);
						sessionCanvas.redraw();
						return;
					}

					// <= (on mac it's always == 0)
					if (evt.count <= 1)
						sessionCanvas.setSelectedShape(sessionCanvas
								.screenToImage(workLine));
					workLine = sessionCanvas.screenToImage(workLine);

					sessionCanvas.addShapeToArrayListShapes(workLine);

					drawSelectionFrame(null, workLine);
					// *** this.triggerPaint();
					sessionCanvas.redraw();

					workLine = null;
					lines.clear();

				} else if (/* (cursor == SWT.CURSOR_SIZEALL && resizeMode) || */resizeMode) {
					handleMouseUpResizeMove(evt);
				}
			}
			mousePressed = false;
			resizeMode = false;
			sessionCanvas.setResize(false);
		}
	}

	/**
	 * @see KeyListener#keyReleased
	 */
	@Override
	public void keyReleased(KeyEvent e) {
	}

	/**
	 * Calculate the Image Size
	 * 
	 * @return arrImgSize as int Array
	 */
	private int[] getImageSize() {
		int[] arrImgSize = new int[2];
		Image img = sessionCanvas.getSourceImage();
		arrImgSize[0] = img.getImageData().height;
		arrImgSize[1] = img.getImageData().width;
		return arrImgSize;
	}

	/**
	 * inner class for Line handling
	 * 
	 */
	class Line {
		public Point begin = null;
		public Point end = null;

		public Line(int beginX, int beginY, int endX, int endY) {
			begin = new Point(beginX, beginY);
			end = new Point(endX, endY);
		}
	}

}