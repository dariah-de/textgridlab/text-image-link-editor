/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.canvas;

import info.textgrid.lab.linkeditor.mip.component.ComponentPlugin;
import info.textgrid.lab.linkeditor.mip.component.io.Utils;
import info.textgrid.lab.linkeditor.mip.component.loader.IImageLoader;
import info.textgrid.lab.linkeditor.mip.component.loader.ImageLoaderFactory;
import info.textgrid.lab.linkeditor.mip.component.loader.UnknownImageFormatException;
import info.textgrid.lab.linkeditor.model.graphics.TGLine;
import info.textgrid.lab.linkeditor.model.graphics.TGPolygon;
import info.textgrid.lab.linkeditor.model.graphics.TGRectangle;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;

import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Vector;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.progress.UIJob;

/**
 * <p>
 * A scrollable image canvas which is a descendant of Canvas. This
 * implementation use the double buffer to reduce the flicker.
 * <p>
 * This implementation uses the pure SWT, no awt UI package is used. For
 * convenience, I put everything into one class (so it more than 1000 lines :().
 * <p>
 * The tranformation function:
 * <ul>
 * <li>Zooming in/out</li>
 * <li>Fit to currrent window</li>
 * <li>Fit horizontally</li>
 * <li>Fit vertically</li>
 * <li>Scrolling vertically and horizontally</li>
 * </ul>
 * 
 * The file I/O function:
 * <ul>
 * <li>Open image file dialog</li>
 * <li>Save image file dialog</li>
 * </ul>
 * You can also extend the image I/O by adding new imageLoader which must
 * implement the info.textgrid.lab.linkeditor.mip.loader.IImageLoader interface,
 * currently,only the SWTLoader has been implemented.
 * <p>
 * 
 * The shortcut key processed:
 * <ul>
 * <li>"o": open file</li>
 * <li>"s": save file</li>
 * <li>"+": zoom in</li>
 * <li>"-": zoom out</li>
 * <li>"left arrow": move left</li>
 * <li>"right arrow": move right</li>
 * <li>"up arrow": move up</li>
 * <li>"down arrow": move down</li>
 * <li>"page up": page up</li>
 * <li>"page down": page down</li>
 * <li>"home": move to line leftmost</li>
 * <li>"end": move to line rightmost</li>
 * <li>"ctrl+home": move to top-left</li>
 * <li>"ctrl+end": move to bottom-right</li>
 * <li>"TAB": show tool bar panel</li>
 * </ul>
 * 
 * <p>
 * 
 * @see IImageLoader
 * @see IImageObserver
 * @author Chengdong Li
 */
public class AffineImageCanvas extends Canvas {
	public final static String[] EXTENSIONS_SUPPORTED = { "jpg", "gif", "bmp", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"ovl" }; //$NON-NLS-1$
	public final static double ZOOM_UPPER_BOUND = 200.0F;// maximum zoomin.
	public final static double ZOOM_LOWER_BOUND = 1.0 / 10.0F;// minimum
	// zoomout.

	protected float zoom_rate = 1.2f; // zoom rate in x and y is equal
	protected Image sourceImage; // original image
	private Image screenImage; // screen image
	private AffineImageCanvas instance;
	private AffineTransform transform = new AffineTransform();

	/** the current image file name. */
	private String imageFile;

	// observers and listeners.
	protected Vector imageObservers = new Vector();
	private KeyAdapter keyListener; // optional key listener

	protected Vector drawObservers = new Vector(); // draw observer.
	protected Vector paintContributors = new Vector(); // paint contributor.

	private String noImagePrompt = ""; //$NON-NLS-1$

	private MenuManager contextMenuManager;

	private Color backColor;
	private boolean fitToWindow = false;

	/**
	 * Constructor.
	 * 
	 * @param parent
	 *            parent Composite
	 */
	public AffineImageCanvas(Composite parent) {
		this(parent, SWT.DOUBLE_BUFFERED | SWT.V_SCROLL | SWT.H_SCROLL);
	}

	/**
	 * Constructor for ScrollableCanvas.
	 * 
	 * @param parent
	 *            the parent of this control
	 */
	public AffineImageCanvas(Composite parent, int style) {
		super(parent, style | SWT.DOUBLE_BUFFERED | SWT.V_SCROLL | SWT.H_SCROLL);
		instance = this;
		// Hook resize and dispose listeners.
		this.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent event) {
				resizeCanvas();
			}
		});
		this.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(final PaintEvent event) {
				paint(event.gc);
			}
		});

		initScrollBars();
		enableHotKey();
		createContextMenu();
	}

	/**
	 * Dispose the garbage here.
	 */
	@Override
	public void dispose() {
		if (sourceImage != null && !sourceImage.isDisposed()) {
			sourceImage.dispose();
			sourceImage = null;
		}
		if (screenImage != null) {
			screenImage.dispose();
			screenImage = null;
		}
		if (backColor != null) {
			backColor.dispose();
		}
		super.dispose();
	}

	/**
	 * Paint function using double buffer.
	 * 
	 * @param gc
	 *            graphics context
	 */
	public void paint(GC gc) {
		Rectangle clientRect = getClientArea(); // client area
		if (clientRect == null || clientRect.width == 0
				|| clientRect.height == 0)
			return;

		if (sourceImage != null && !sourceImage.isDisposed()) {
			if (screenImage != null)
				screenImage.dispose();
			screenImage = new Image(getDisplay(), clientRect.width,
					clientRect.height);
			GC newGC = new GC(screenImage);
			callPrePaintContributors(newGC);

			Rectangle imageBound = sourceImage.getBounds();
			Rectangle imageRect = SWT2dUtil.inverseTransformRect(transform,
					clientRect);

			// outset for smoothing
			int gap = 2;
			imageRect.x -= gap;
			imageRect.y -= gap;
			imageRect.width += 2 * gap;
			imageRect.height += 2 * gap;

			imageRect = imageRect.intersection(imageBound);
			Rectangle destRect = SWT2dUtil.transformRect(transform, imageRect);

			newGC.setClipping(clientRect);
			newGC.drawImage(sourceImage, imageRect.x, imageRect.y,
					imageRect.width, imageRect.height, destRect.x, destRect.y,
					destRect.width, destRect.height);
			callPaintContributors(newGC);
			newGC.dispose();

			gc.drawImage(screenImage, 0, 0);
		} else {
			gc.setClipping(clientRect);
			gc.fillRectangle(clientRect);
			gc.setForeground(new Color(getDisplay(), 255, 0, 0));
			Font font = new Font(getDisplay(), "Courier New", 10, SWT.BOLD); //$NON-NLS-1$
			gc.drawText(noImagePrompt, 0, 0);
			font.dispose();
			initScrollBars();
		}

		callPostPaintContributors(gc);
	}

	public void setBackColor(Color c) {
		backColor = c;
		setBackground(c);
	}

	public void clear() {
		Point size = getSize();
		GC gc = new GC(this);

		if (backColor != null) {
			gc.setBackground(backColor);
		}

		gc.fillRectangle(0, 0, size.x, size.y);
		if (sourceImage != null) {
			sourceImage.dispose();
		}
	}

	/*
	 * Create an extensible context menu.
	 */
	private void createContextMenu() {
		contextMenuManager = new MenuManager(Messages.AffineImageCanvas_ContextMenue);
		contextMenuManager.setRemoveAllWhenShown(true);
		contextMenuManager.addMenuListener(new IMenuListener() {
			@Override
			public void menuAboutToShow(IMenuManager menuManager) {
				initContextMenu(menuManager);
			}
		});

		Menu menu = contextMenuManager.createContextMenu(this);
		this.setMenu(menu);
	}
	
	public URL getInstallURL(String pathString) {
		URL url = FileLocator.find(ComponentPlugin
			.getDefault().getBundle(), new Path(pathString),
			null);
		try {
			URL resolved = FileLocator.resolve(url);
			//System.out.println(url + " -> "  + resolved);
			return resolved;
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Initialize the extensible context menu.
	 * 
	 * @param menuManager
	 *            IMenuManager
	 */
	public void initContextMenu(IMenuManager menuManager) {
		try {
//			URL url = // Platform.getPlugin("info.textgrid.lab.linkeditor.mip.component").getDescriptor().getInstallURL();
//			Platform.asLocalURL(Platform.find(Platform
//					.getBundle("info.textgrid.lab.linkeditor.mip.component"), //$NON-NLS-1$
//					new Path("."))); //$NON-NLS-1$

			// load image menuitem.
			Action openAction = new Action(Messages.AffineImageCanvas_LoadImage) {
				@Override
				public void run() {
					onFileOpen();
				}
			};
//			URL uurl = new URL(url, "icons/Open16.gif"); //$NON-NLS-1$
			URL uurl = getInstallURL("icons/Open16.gif");
			openAction.setImageDescriptor(ImageDescriptor.createFromURL(uurl));

			// transform menu.
			IMenuManager transform = new MenuManager(Messages.AffineImageCanvas_ImageSize);

			Action vFlipAction = new Action(Messages.AffineImageCanvas_FlipVertically) {
				@Override
				public void run() {
					vFlip();
				}
			};
//			uurl = new URL(url, "icons/Flipv16.gif"); //$NON-NLS-1$
			uurl = getInstallURL("icons/Flipv16.gif");
			vFlipAction.setImageDescriptor(ImageDescriptor.createFromURL(uurl));

			Action hFlipAction = new Action(Messages.AffineImageCanvas_FlipHorizontally) {
				@Override
				public void run() {
					hFlip();
				}
			};
//			uurl = new URL(url, "icons/Fliph16.gif"); //$NON-NLS-1$
			
			hFlipAction.setImageDescriptor(ImageDescriptor.createFromURL(uurl));

			Action rotateAction = new Action(Messages.AffineImageCanvas_RotateClockWise) {
				@Override
				public void run() {
					rotate(SWT.RIGHT);
				}
			};
//			uurl = new URL(url, "icons/Rotate16.gif"); //$NON-NLS-1$
			uurl = getInstallURL("icons/Rotate16.gif");
			rotateAction
					.setImageDescriptor(ImageDescriptor.createFromURL(uurl));

			Action rotate2Action = new Action(Messages.AffineImageCanvas_RotateAgainstClockWise) {
				@Override
				public void run() {
					rotate(SWT.LEFT);
				}
			};
//			uurl = new URL(url, "icons/Rotater16.gif"); //$NON-NLS-1$
			uurl = getInstallURL("icons/Rotater16.gif");
			rotate2Action.setImageDescriptor(ImageDescriptor
					.createFromURL(uurl));

			Action orgAction = new Action(Messages.AffineImageCanvas_ShowOriginally) {
				@Override
				public void run() {
					showOriginal();
				}
			};
//			uurl = new URL(url, "icons/Normal16.gif"); //$NON-NLS-1$
			uurl = getInstallURL("icons/Normal16.gif");
			orgAction.setImageDescriptor(ImageDescriptor.createFromURL(uurl));

			Action fitAction = new Action(Messages.AffineImageCanvas_FitWindow) {
				@Override
				public void run() {
					fitCanvas();
				}
			};
//			uurl = new URL(url, "icons/Content16.gif"); //$NON-NLS-1$
			uurl = getInstallURL("icons/Content16.gif");
			fitAction.setImageDescriptor(ImageDescriptor.createFromURL(uurl));

			Action fitVAction = new Action(Messages.AffineImageCanvas_FitVertically) {
				@Override
				public void run() {
					fitVertically();
				}
			};
//			uurl = new URL(url, "icons/Fitv16.gif"); //$NON-NLS-1$
			uurl = getInstallURL("icons/Fitv16.gif");
			fitVAction.setImageDescriptor(ImageDescriptor.createFromURL(uurl));

			Action fitHAction = new Action(Messages.AffineImageCanvas_FitHorizontally) {
				@Override
				public void run() {
					fitHorizontally();
				}
			};
//			uurl = new URL(url, "icons/Fith16.gif"); //$NON-NLS-1$
			uurl = getInstallURL("icons/Fith16.gif");
			fitHAction.setImageDescriptor(ImageDescriptor.createFromURL(uurl));

			transform.add(orgAction);
			transform.add(fitVAction);
			transform.add(fitHAction);
			transform.add(fitAction);
			// transform.add(new Separator());
			// transform.add(rotateAction);
			// transform.add(rotate2Action);
			// transform.add(vFlipAction);
			// transform.add(hFlipAction);

			if (this.getSourceImage() != null) {
				// menuManager.add(openAction);
				menuManager.add(new Separator());
				menuManager.add(transform);
				menuManager.add(new Separator());
			} else {
				// menuManager.add(openAction);
			}
		} catch (Exception e) {
			ComponentPlugin.handleWarning(e);
		}
		menuManager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	/**
	 * initalize the scrollbar and register listeners.
	 */
	private void initScrollBars() {
		ScrollBar horizontal = getHorizontalBar();
		if (horizontal != null) {
			horizontal.setEnabled(false);
			horizontal.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent event) {
					scrollHorizontally((ScrollBar) event.widget);
				}
			});
		}
		ScrollBar vertical = getVerticalBar();
		if (vertical != null) {
			vertical.setEnabled(false);
			vertical.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent event) {
					scrollVertically((ScrollBar) event.widget);
				}
			});
		}
	}

	/**
	 * Scroll horizontally
	 * 
	 * @param ScrollBar
	 *            horizontal scrollbar
	 */
	private void scrollHorizontally(final ScrollBar scrollBar) {
		if (this.sourceImage == null)
			return;

		new UIJob(Messages.AffineImageCanvas_Scrolling) {

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				AffineTransform af = getTransform();
				/* previous horizontal translation. */
				double tx = af.getTranslateX();

				if (scrollBar == null || scrollBar.isDisposed()) {
					cancel();
					return Status.CANCEL_STATUS;
				}

				/* current horizontal translation. */
				double select = -scrollBar.getSelection();
				af.preConcatenate(AffineTransform.getTranslateInstance(select
						- tx, 0));
				setTransform(af);
				syncScrollBars();

				return Status.OK_STATUS;
			}
		}.schedule();

	}

	/**
	 * Scroll vertically
	 * 
	 * @param ScrollBar
	 *            vertical scrollbar
	 */
	private void scrollVertically(final ScrollBar scrollBar) {
		if (this.sourceImage == null)
			return;

		new UIJob(Messages.AffineImageCanvas_Scrolling) {

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {

				AffineTransform af = getTransform();
				/* previous vertical translation. */
				double ty = af.getTranslateY();

				if (scrollBar == null || scrollBar.isDisposed()) {
					cancel();
					return Status.CANCEL_STATUS;
				}

				/* current vertical translation. */
				double select = -scrollBar.getSelection();
				af.preConcatenate(AffineTransform.getTranslateInstance(0,
						select - ty));
				setTransform(af);
				syncScrollBars();

				return Status.OK_STATUS;
			}
		}.schedule();
	}

	/**
	 * Translate image of canvas
	 * 
	 * @param dx
	 *            x translation
	 * @param dy
	 *            y translation
	 */
	public void translate(double dx, double dy) {
		AffineTransform af = getTransform();
		af.preConcatenate(AffineTransform.getTranslateInstance(dx, dy));
		setTransform(af);
		syncScrollBars();
	}

	/**
	 * synchronize the scrollbar with the image. If the transform is out of
	 * range, it will correct it. This function will only consider :
	 * <b>transform, image size, client area</b>.
	 */
	public void syncScrollBars() {
		if (isDisposed() || (sourceImage != null && sourceImage.isDisposed()))
			return;
		int cw = getClientArea().width, ch = getClientArea().height;
		if (cw == 0 || ch == 0)
			return;

		if (this.sourceImage == null) {
			redraw(0, 0, cw, ch, true);
			return;
		}

		AffineTransform af = getTransform();
		double sx = af.getScaleX(), sy = af.getScaleY();
		double tx = af.getTranslateX(), ty = af.getTranslateY();

		Rectangle imageBound = sourceImage.getBounds();

		if (tx > 0) {
			tx = 0;
		}
		if (ty > 0) {
			ty = 0;
		}

		ScrollBar horizontal = getHorizontalBar();
		if (horizontal != null) {
			horizontal.setIncrement((getClientArea().width / 100));
			horizontal.setPageIncrement(getClientArea().width);
			if (imageBound.width * sx > cw) {
				horizontal.setMaximum((int) (imageBound.width * sx));
				horizontal.setEnabled(true);
				if (((int) -tx) > horizontal.getMaximum() - cw) {
					tx = -horizontal.getMaximum() + cw;
				}
			} else {
				horizontal.setMaximum(0);
				horizontal.setEnabled(false);
				tx = (cw - imageBound.width * sx) / 2; // center if too small.
			}
			horizontal.setSelection((int) (-tx));
			horizontal.setThumb((getClientArea().width));
			if(!fitToWindow)
				horizontal.setVisible(true);
		}

		ScrollBar vertical = getVerticalBar();
		if (vertical != null) {
			vertical.setIncrement((getClientArea().height / 100));
			vertical.setPageIncrement((getClientArea().height));
			if (imageBound.height * sy > ch) {
				vertical.setMaximum((int) (imageBound.height * sy));
				vertical.setEnabled(true);
				if (((int) -ty) > vertical.getMaximum() - ch) {
					ty = -vertical.getMaximum() + ch;
				}
			} else {
				vertical.setMaximum(0);
				vertical.setEnabled(false);
				ty = (ch - imageBound.height * sy) / 2; // center if too small.
			}
			vertical.setSelection((int) (-ty));
			vertical.setThumb((getClientArea().height));
			if(!fitToWindow)
				vertical.setVisible(true);
		}
		/* set new transform. */
		af = AffineTransform.getScaleInstance(sx, sy);
		af.preConcatenate(AffineTransform.getTranslateInstance(tx, ty));
		setTransform(af);
		notifySynchronizeEvent();
		redraw();
	}

	/**
	 * Get the affine transform of canvas
	 * 
	 * @return affine transform associated with this canvas
	 */
	public AffineTransform getTransform() {
		return transform;
	}

	/**
	 * Set the transform for canvas, this does not repaint image.
	 * 
	 * @param transform
	 *            affine transform to be set
	 */
	public void setTransform(AffineTransform transform) {
		this.transform = transform;
	}

	/**
	 * when canvas resize, synchronize the scrollbars.
	 */
	public void resizeCanvas() {
		if (this.sourceImage == null)
			return;
		syncScrollBars();
	}

	/**
	 * Fit image vertically.
	 */
	public void fitVertically() {
		if (this.sourceImage == null)
			return;
		// old transform.
		AffineTransform self = this.getTransform();
		double sy = self.getScaleY();

		// First find the zoom center.
		Rectangle rect = this.getClientArea();
		int w = rect.width, h = rect.height;
		double dx = (double) w / 2;
		double dy = (double) h / 2;

		// compute scale cofficeint
		Rectangle imageBound = sourceImage.getBounds();
		double s = ((double) h) / ((double) imageBound.height);
		double rate = s / sy;

		// Transfrom: T(-Tx,-Ty)*S*T(Tx,Ty)*old
		centerZoom(dx, dy, rate, self);
	}

	/**
	 * Fit image horizontally.
	 */
	public void fitHorizontally() {
		if (this.sourceImage == null)
			return;
		// old transform.
		AffineTransform self = this.getTransform();
		double sx = self.getScaleX(); // First find the zoom center.
		Rectangle rect = this.getClientArea();
		int w = rect.width, h = rect.height;
		double dx = (double) w / 2;
		double dy = (double) h / 2;

		// compute scale cofficeint.
		Rectangle imageBound = sourceImage.getBounds();
		double s = ((double) w) / ((double) imageBound.width);
		double rate = s / sx;

		// Transfrom: T(-Tx,-Ty)*S*T(Tx,Ty)*old
		centerZoom(dx, dy, rate, self);
	}

	/**
	 * Fit the image onto the canvas
	 */
	public void fitCanvas() {
		if (sourceImage == null || sourceImage.isDisposed())
			return;
		Rectangle imageBound = sourceImage.getBounds();
		Rectangle dest = this.getClientArea();

		double sx = (double) dest.width / (double) imageBound.width;
		double sy = (double) dest.height / (double) imageBound.height;
		double s = Math.min(sx, sy);
		double dx = 0.5 * (dest.width - s * imageBound.width);
		double dy = 0.5 * (dest.height - s * imageBound.height);
		AffineTransform af = AffineTransform.getTranslateInstance(dx, dy);
		af.scale(s, s);
		this.setTransform(af);
		this.fitToWindow = true;
		this.syncScrollBars();
		
		if(fitToWindow){
			getHorizontalBar().setVisible(false);
			getVerticalBar().setVisible(false);
			fitToWindow = false;
		}
		
	}

	/**
	 * Show the image with the original size
	 */
	public void showOriginal() {
		if (this.sourceImage == null)
			return;
		this.setTransform(new AffineTransform());
		this.syncScrollBars();
	}

	/**
	 * Zoom around the center of (dx,dy) with scale, af is the original
	 * transform If the zoomLocked is set, then it will ignore dx, dy and only
	 * zoom around the center of selectRect. It equals following transform:
	 * T(-Tx,-Ty)*S*T(Tx,Ty)*old_transform
	 * 
	 * @param dx
	 *            center x
	 * @param dy
	 *            center y
	 * @param scale
	 *            zoom rate
	 * @param af
	 *            original affinetransform
	 */
	public void centerZoom(final double dx, final double dy,
			final double scale, final AffineTransform tf) {

		// new UIJob("Zooming...") {
		//
		// @Override
		// public IStatus runInUIThread(IProgressMonitor monitor) {
		tf.preConcatenate(AffineTransform.getTranslateInstance(-dx, -dy));
		tf.preConcatenate(AffineTransform.getScaleInstance(scale, scale));
		tf.preConcatenate(AffineTransform.getTranslateInstance(dx, dy));

		setTransform(tf);
		syncScrollBars();
		notifyZoomEvent();
		// return Status.OK_STATUS;
		// }
		// }.schedule();

	}

	/**
	 * Zoom in around the center of client Area.
	 */
	public void zoomIn() {
		if (sourceImage == null)
			return;
		Rectangle rect = getClientArea();
		int w = rect.width, h = rect.height;
		double dx = ((w / 2));
		double dy = ((h / 2));
		if (transform.getScaleX() > ZOOM_UPPER_BOUND
				|| transform.getScaleY() > ZOOM_UPPER_BOUND)
			return;
		centerZoom(dx, dy, zoom_rate, transform);
	}

	/**
	 * Zoom in/out around the center of client Area with rate.
	 * 
	 * @param rate
	 *            zoom rate.
	 */
	public void zoom(double rate) {
		if (sourceImage == null)
			return;
		Rectangle rect = getClientArea();
		int w = rect.width, h = rect.height;
		double dx = ((w / 2));
		double dy = ((h / 2));
		centerZoom(dx, dy, rate, transform);
	}

	/**
	 * Zoom out around the center of client Area.
	 */
	public void zoomOut() {
		if (sourceImage == null)
			return;
		Rectangle rect = getClientArea();
		int w = rect.width, h = rect.height;
		double dx = ((w / 2));
		double dy = ((h / 2));
		if (transform.getScaleX() < ZOOM_LOWER_BOUND
				|| transform.getScaleY() < ZOOM_LOWER_BOUND)
			return;
		centerZoom(dx, dy, 1.0 / zoom_rate, transform);
	}

	/**
	 * Move origin of image to origin of client Area.
	 */
	public void toHome() {
		if (sourceImage == null)
			return;
		double sx = transform.getScaleX();
		double sy = transform.getScaleY();
		AffineTransform af = AffineTransform.getTranslateInstance(0, 0);
		af.scale(sx, sy);
		this.setTransform(af);
		this.syncScrollBars();
	}

	/**
	 * Move end of image to end of client Area.
	 */
	public void toEnd() {
		if (sourceImage == null)
			return;
		double sx = transform.getScaleX();
		double sy = transform.getScaleY();
		AffineTransform af = AffineTransform.getScaleInstance(sx, sy);
		Rectangle bound = sourceImage.getBounds();
		double dx = bound.width * sx;
		double dy = bound.height * sy;
		af.translate(-dx, -dy);
		this.setTransform(af);
		this.syncScrollBars();
	}

	/**
	 * Move to head of line.
	 */
	public void toLineHome() {
		if (sourceImage == null)
			return;
		AffineTransform af = this.getTransform();
		af.preConcatenate(AffineTransform.getTranslateInstance(
				-af.getTranslateX(), 0));
		this.setTransform(af);
		this.syncScrollBars();
	}

	/**
	 * Move to end of line.
	 */
	public void toLineEnd() {
		if (sourceImage == null)
			return;
		AffineTransform af = this.getTransform();
		Rectangle bound = sourceImage.getBounds();
		double sx = transform.getScaleX();
		double dx = bound.width * sx;
		af.preConcatenate(AffineTransform.getTranslateInstance(-dx, 0));
		this.setTransform(af);
		this.syncScrollBars();
	}

	/**
	 * Scroll the view and make the rectangle center of the clientArea
	 * 
	 * @param rect
	 *            rectangle whose coordinate is original, i.e., before
	 *            transformed by the transform of this canvas
	 */
	public void showRect(TGRectangle src) {
		if (src != null) {
			Rectangle clientRect = this.getClientArea();
			int w = clientRect.width, h = clientRect.height;
			double dx = (double) w / 2;
			double dy = (double) h / 2;

			TGRectangle dest = SWT2dUtil
					.transformRect(this.getTransform(), src);
			double ddx = dest.getRectangle().x
					+ (double) dest.getRectangle().width / 2;
			double ddy = dest.getRectangle().y
					+ (double) dest.getRectangle().height / 2;
			AffineTransform af = this.getTransform();
			af.preConcatenate(AffineTransform.getTranslateInstance(dx - ddx, dy
					- ddy));
			this.setTransform(af);
			this.syncScrollBars();
		}
	}

	/**
	 * Reload source image from an image
	 * 
	 * @param image
	 *            an SWT Image.
	 * @return swt image
	 */
	public Image reloadImage(Image image) {
		if (image == null) {
			clear();
			return null;
		}
		if (sourceImage != null && !sourceImage.isDisposed()) {
			sourceImage.dispose();
			sourceImage = null;
		}
		// FIXME
		if (image.isDisposed()) {
			Rectangle clientRect = getClientArea(); // client area
			image = new Image(getDisplay(), clientRect.width, clientRect.height);
		}
		sourceImage = image;

		ImageData data = image.getImageData();
		this.setTransform(new AffineTransform());
		setImageData(data);

		syncScrollBars();
		notifyImageChanged();
		return image;
	}

	/**
	 * @deprecated Reload source image from a file
	 * @param filename
	 *            image file
	 * @return swt image created from image file
	 */
	@Deprecated
	public Image reloadImage(String filename) {
		if (filename == null || !Utils.exists(filename)) {
			clear();
			return null;
		}
		this.setImageFile(filename);
		if (sourceImage != null && !sourceImage.isDisposed()) {
			sourceImage.dispose();
			sourceImage = null;
		}
		try {
			try {
				IImageLoader loader = ImageLoaderFactory
						.createReaderFor(SWT2dUtil.findSuffix(filename));
				if (loader != null) {
					ImageData data = loader.readImageData(filename);
					this.setTransform(new AffineTransform());
					setImageData(data);
					data = null;
					return sourceImage;
				}
				return null;
			} catch (UnknownImageFormatException e) {
				ComponentPlugin.handleError(e,
						"No image loader for this image format!"); //$NON-NLS-1$
			}
			return null;
		} catch (Exception e) {
			ComponentPlugin.handleError(e,
					"Image format is not correct, please check up!"); //$NON-NLS-1$
			return null;
		}
	}

	public Image reloadImage(IFile file) {
		String filepath = Utils.resolvePath(file);
		if (filepath == null || !Utils.exists(filepath)) {
			clear();
			return null;
		}
		this.setImageFile(filepath);
		if (sourceImage != null && !sourceImage.isDisposed()) {
			sourceImage.dispose();
			sourceImage = null;
		}

		ImageData data = new ImageData(filepath);
		setTransform(new AffineTransform());
		setImageData(data);
		data = null;
		return sourceImage;
	}

	/**
	 * Test if the image format with the given extension is supported for open.
	 * 
	 * @return true: is supported; false: not supported.
	 */
	public boolean isFormatSupported(String extension) {
		if (extension == null || extension.length() == 0) {
			return false;
		}

		for (int i = 0; i < EXTENSIONS_SUPPORTED.length; i++) {
			if (extension.equalsIgnoreCase(EXTENSIONS_SUPPORTED[i])) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Call back funtion of button "open". Will open a file dialog, and choose
	 * the image file.
	 */
	public void onFileOpen() {
		String filename; // current seleceted file name

		// Get the user to choose an image file.
		FileDialog fileChooser = new FileDialog(this.getShell(), SWT.OPEN);
		fileChooser.setText(Messages.AffineImageCanvas_OpenImageFile);
		if (imageFile != null) {
			String currentDir = getDirFromFile(imageFile);
			if (currentDir != null) {
				fileChooser.setFilterPath(currentDir);
			}
		}

		fileChooser.setFilterExtensions(new String[] {
				"*.gif; *.jpeg; *.jpg; *.png; *.ico; *.bmp; *.tiff", //$NON-NLS-1$
				"*.psd; *.tiff; *.tif" }); //$NON-NLS-1$
		fileChooser.setFilterNames(new String[] {
				"SWT image" + " (gif, jpeg, jpg, png, ico, bmp, tiff)", //$NON-NLS-1$ //$NON-NLS-2$
				"other (psd, tif, tiff)" }); //$NON-NLS-1$
		filename = fileChooser.open();

		if (filename == null)
			return;
		imageFile = filename;
		this.reloadImage(filename);
	}

	/**
	 * Call back funtion of button "save". Will open a file dialog, and choose
	 * the image file for save.
	 */
	public void onFileSave() {
		if (sourceImage == null)
			return;
		String filename; // current seleceted file name
		// Get the user to choose an image file.
		FileDialog fileChooser = new FileDialog(this.getShell(), SWT.SAVE);
		fileChooser.setText(Messages.AffineImageCanvas_SaveImage);
		if (getDirFromFile(imageFile) != null)
			fileChooser.setFilterPath(getDirFromFile(imageFile));
		fileChooser.setFilterExtensions(new String[] { "*.bmp" }); //$NON-NLS-1$
		fileChooser.setFilterNames(new String[] { "Bitmap file (*.bmp)" }); //$NON-NLS-1$
		String fullPath = fileChooser.open();
		if (fullPath == null)
			return;

		Cursor waitCursor = new Cursor(this.getDisplay(), SWT.CURSOR_WAIT);
		this.setCursor(waitCursor);

		filename = fileChooser.getFileName();
		imageFile = filename;

		int dotPos = filename.lastIndexOf(Messages.AffineImageCanvas_41);
		if (dotPos < filename.length() - 1 && dotPos > 0) {
			String suffix = filename.substring(dotPos + 1, filename.length());
			SWT2dUtil
					.saveSWTImage(sourceImage.getImageData(), fullPath, suffix);
		} else {
			SWT2dUtil.saveSWTImage(sourceImage.getImageData(), fullPath, null);
		}

		this.setCursor(null);
		waitCursor.dispose();
	}

	/**
	 * Draw the bounding box of the current selection.
	 * 
	 * @param gc
	 *            GC
	 * @param rect
	 *            bounding rectangle
	 */
	public void drawFrame(GC gc, TGRectangle rect) {
		int w = rect.getRectangle().width;
		int h = rect.getRectangle().height;
		// points are in clockwise direction
		int xpoints[] = { rect.getRectangle().x, rect.getRectangle().x + w,
				rect.getRectangle().x + w, rect.getRectangle().x };
		int ypoints[] = { rect.getRectangle().y, rect.getRectangle().y,
				rect.getRectangle().y + h, rect.getRectangle().y + h };

		int npoints = xpoints.length;
		int xxpoints[] = new int[xpoints.length * 2];
		int yypoints[] = new int[ypoints.length * 2];
		for (int i = 0; i < npoints - 1; i++) {
			xxpoints[2 * i] = xpoints[i];
			yypoints[2 * i] = ypoints[i];
			xxpoints[2 * i + 1] = (xpoints[i] + xpoints[i + 1]) / 2;
			yypoints[2 * i + 1] = (ypoints[i] + ypoints[i + 1]) / 2;
		}
		xxpoints[2 * (npoints - 1)] = xpoints[npoints - 1];
		yypoints[2 * (npoints - 1)] = ypoints[npoints - 1];
		xxpoints[2 * (npoints - 1) + 1] = (xpoints[npoints - 1] + xpoints[0]) / 2;
		yypoints[2 * (npoints - 1) + 1] = (ypoints[npoints - 1] + ypoints[0]) / 2;

		for (int i = 0; i < 2 * npoints; i++) {
			gc.fillRectangle(xxpoints[i] - 2, yypoints[i] - 2, 5, 5);
		}

		gc.drawRectangle(rect.getRectangle().x, rect.getRectangle().y,
				rect.getRectangle().width, rect.getRectangle().height);
	}

	/**
	 * Draw selection frame.
	 * 
	 * @param gc
	 *            graphics context
	 * @param rect
	 *            rectangle of the frame
	 */
	public void drawSelectionFrame(GC gc, TGRectangle rect) {
		if (rect == null)
			return;
		if (gc == null)
			return;
		if (rect.getRectangle().width == 0 || rect.getRectangle().height == 0)
			return;
		drawFrame(gc, SWT2dUtil.absRect(rect));
	}

	/**
	 * Get the screen image of this canvas.
	 * 
	 * @return off-screen image
	 */
	public Image getScreenImage() {
		return screenImage;
	}

	/**
	 * Get the image data of image.
	 * 
	 * @return image data of canvas
	 */
	public ImageData getImageData() {
		if (sourceImage == null)
			return null;
		return sourceImage.getImageData();
	}

	/**
	 * Reset the image data and update the image
	 * 
	 * @param data
	 *            image data to be set
	 */
	public void setImageData(ImageData data) {
		if (sourceImage != null) {
			sourceImage.dispose();
			sourceImage = null;
		}
		if (data != null) {
			sourceImage = new Image(getDisplay(), data);
		}
		syncScrollBars();
		notifyImageChanged();
	}

	/**
	 * Get the device dependent image.
	 * 
	 * @return Image to be drawn by this canvas
	 */
	public Image getSourceImage() {
		return sourceImage;
	}

	/**
	 * Register image observer.
	 * 
	 * @param observer
	 *            image observer to be registered
	 */
	public void registerImageObserver(IImageObserver observer) {
		imageObservers.add(observer);
	}

	/**
	 * Remove image observer.
	 * 
	 * @param observer
	 *            image observer to be removed
	 */
	public void removeImageObserver(IImageObserver observer) {
		for (int i = 0; i < imageObservers.size(); i++) {
			if (imageObservers.elementAt(i) == observer) {
				imageObservers.removeElement(observer);
				return;
			}
		}
	}

	/**
	 * Notify the image data has been changed to all obeservers.
	 */
	public void notifyImageChanged() {
		for (int i = 0; i < imageObservers.size(); i++) {
			((IImageObserver) (imageObservers.elementAt(i))).onImageChanged(
					this, getImageData());
		}
	}

	/**
	 * Given an absolute file path, return the directory.
	 * 
	 * @param filename
	 *            absolute path name
	 * @return directory name
	 */
	private String getDirFromFile(String filename) {
		if (filename == null)
			return null;
		int sepPos = filename.lastIndexOf(File.separator);
		if (sepPos < filename.length() - 1 && sepPos > 0) {
			String curDir = filename.substring(0, sepPos);
			return curDir;
		} else {
			return null;
		}
	}

	/**
	 * Enable hot key listener.
	 */
	public void enableHotKey() {
		keyListener = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent event) {
				ScrollBar horizontal = instance.getHorizontalBar();
				ScrollBar vertical = instance.getVerticalBar();
				int vPage = vertical.getPageIncrement();
				int vInc = vertical.getIncrement();
				int hInc = horizontal.getIncrement();

				if (event.character == '+') { // zoomin
					zoomIn();
				} else if (event.character == '-') { // zoomout
					zoomOut();
				} else if (event.character == '0') { // original
					showOriginal();
				} else if (event.keyCode == SWT.HOME) { // home key
					if ((event.stateMask & SWT.CTRL) != 0) {
						toHome();
					} else {
						toLineHome();
					}
				} else if (event.keyCode == SWT.END) { // end key
					if ((event.stateMask & SWT.CTRL) != 0) {
						toEnd();
					} else {
						toLineEnd();
					}
				} else {
					AffineTransform af = getTransform();
					if (event.keyCode == SWT.PAGE_UP) { // page up
						af.preConcatenate(AffineTransform.getTranslateInstance(
								0, vPage));
					} else if (event.keyCode == SWT.PAGE_DOWN) { // page down
						af.preConcatenate(AffineTransform.getTranslateInstance(
								0, -vPage));
					} else if (event.keyCode == SWT.ARROW_UP) { // up
						af.preConcatenate(AffineTransform.getTranslateInstance(
								0, vInc));
					} else if (event.keyCode == SWT.ARROW_DOWN) { // down
						af.preConcatenate(AffineTransform.getTranslateInstance(
								0, -vInc));
					} else if (event.keyCode == SWT.ARROW_LEFT) { // left
						af.preConcatenate(AffineTransform.getTranslateInstance(
								hInc, 0));
					} else if (event.keyCode == SWT.ARROW_RIGHT) { // right
						af.preConcatenate(AffineTransform.getTranslateInstance(
								-hInc, 0));
					}
					setTransform(af);
					syncScrollBars();
				}
			}
		};
		addKeyListener(keyListener);
	}

	/**
	 * Disable the hot key listener by removing it from keylistener list.
	 */
	public void disableHotKey() {
		if (keyListener != null) {
			removeKeyListener(keyListener);
			this.keyListener = null;
		}
	}

	/**
	 * Return an ascii code of the key if possible.
	 * 
	 * @param event
	 *            key event
	 * @return ascii key
	 */
	public int convertEventKey(KeyEvent event) {
		int key = event.character;
		if (key == 0) {
			key = event.keyCode;
		} else {
			if (0 <= key && key <= 0x1F) {
				if ((event.stateMask & SWT.CTRL) != 0) {
					key += 0x40;
				}
			} else {
				if ('a' <= key && key <= 'z') {
					key -= 'a' - 'A';
				}
			}
		}
		return key;
	}

	/**
	 * Get RGB info for a point (x,y) relative to sourceImage.
	 * 
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 * @return RGB value of pixel at (x,y)
	 */
	public RGB getRGB(int x, int y) {
		if (sourceImage == null)
			return null;
		Rectangle bound = sourceImage.getBounds();
		if (x < bound.width && x >= 0 && y < bound.height && y >= 0) {
			Image tempImage = new Image(getDisplay(), 1, 1);
			GC tempGC = new GC(tempImage);
			tempGC.drawImage(sourceImage, x, y, 1, 1, 0, 0, 1, 1);
			tempGC.dispose();
			ImageData data = tempImage.getImageData();
			int pixel = data.getPixel(0, 0);
			RGB rgb = data.palette.getRGB(pixel);
			tempImage.dispose();
			return rgb;
		}
		return null;
	}

	/**
	 * Register image observer.
	 * 
	 * @param observer
	 *            image observer to be registered
	 */
	public void registerDrawObserver(IDrawObserver observer) {
		drawObservers.add(observer);
	}

	/**
	 * Remove image observer.
	 * 
	 * @param observer
	 *            image observer to be removed
	 */
	public void removeDrawObserver(IDrawObserver observer) {
		for (int i = 0; i < drawObservers.size(); i++) {
			if (drawObservers.elementAt(i) == observer) {
				drawObservers.removeElement(observer);
				return;
			}
		}
	}

	/**
	 * Notify the canvas has been synchronized.
	 */
	public void notifySynchronizeEvent() {
		for (int i = 0; i < drawObservers.size(); i++) {
			((IDrawObserver) (drawObservers.elementAt(i))).onSynchronize(this);
		}
	}

	/**
	 * Notify the image has been zoomed.
	 */
	public void notifyZoomEvent() {
		for (int i = 0; i < drawObservers.size(); i++) {
			((IDrawObserver) (drawObservers.elementAt(i))).onZoom(this);
		}
	}

	/**
	 * Register paint contributor.
	 * 
	 * @param contributor
	 *            paint contributor to be registered
	 */
	public void registerPaintContributor(IPaintContributor contributor) {
		paintContributors.add(contributor);
	}

	/**
	 * Remove paint contributor.
	 * 
	 * @param contributor
	 *            image observer to be removed
	 */
	public void removePaintContributor(IPaintContributor contributor) {
		for (int i = 0; i < paintContributors.size(); i++) {
			if (paintContributors.elementAt(i) == contributor) {
				paintContributors.removeElement(contributor);
				return;
			}
		}
	}

	/**
	 * Pre-painting from the paint contributors.
	 * 
	 * @param gc
	 *            prepaint GC.
	 */
	public void callPrePaintContributors(GC gc) {
		for (int i = 0; i < paintContributors.size(); i++) {
			IPaintContributor painter = (IPaintContributor) paintContributors
					.elementAt(i);
			painter.prePaint(gc);
		}
	}

	/**
	 * Painting from the paint contributors.
	 * 
	 * @param gc
	 *            prepaint GC.
	 */
	public void callPaintContributors(GC gc) {
		for (int i = 0; i < paintContributors.size(); i++) {
			IPaintContributor painter = (IPaintContributor) paintContributors
					.elementAt(i);
			painter.paint(gc);
		}
	}

	/**
	 * Post-painting from the paint contributors.
	 * 
	 * @param gc
	 *            postpaint GC.
	 */
	public void callPostPaintContributors(GC gc) {
		for (int i = 0; i < paintContributors.size(); i++) {
			IPaintContributor painter = (IPaintContributor) paintContributors
					.elementAt(i);
			painter.postPaint(gc);
		}
	}

	public TGShape screenToImage(TGShape src) {
		if (src instanceof TGRectangle) {
			return screenToImage((TGRectangle) src);
		} else if (src instanceof TGPolygon) {
			return screenToImage((TGPolygon) src);
		} else if (src instanceof TGLine) {
			return screenToImage((TGLine) src);
		}
		return null;
	}

	/**
	 * Returns a point which is the result of converting the argument, which is
	 * specified in display relative coordinates, to coordinates relative to the
	 * source image.
	 * 
	 * @param devP
	 *            The point to be translated (must not be null).
	 * @return the translated coordinate.
	 */
	public Point screenToImage(Point src) {
		return SWT2dUtil.inverseTransformPoint2(transform, src);
	}

	/**
	 * Returns a Rectangle which is the result of converting the argument, which
	 * is specified in display relative coordinates, to coordinates relative to
	 * the source image.
	 * 
	 * @param devRect
	 *            The Rectangle to be translated (must not be null).
	 * @return the translated rectangle.
	 */
	public TGRectangle screenToImage(TGRectangle src) {
		TGRectangle help = src;
		src = SWT2dUtil.inverseTransformRect(transform, src);
		if (help.isLinked()) {
			src.setLinked(true);
		}
		src.setWritingMode(help.getWritingMode());
		src.setRotationValue(help.getRotationValue());
		src.setLayerValues(new Integer(help.getLayer()), help.getLayerName(),
				help.getLayerRGB());

		return src;
	}

	public TGPolygon screenToImage(TGPolygon src) {
		TGPolygon help = src;
		src = SWT2dUtil.inverseTransformPoly(transform, src);
		if (help.isLinked()) {
			src.setLinked(true);
		}
		src.setWritingMode(help.getWritingMode());
		src.setLayerValues(new Integer(help.getLayer()), help.getLayerName(),
				help.getLayerRGB());

		return src;
	}

	public TGLine screenToImage(TGLine src) {
		TGLine help = src;
		src = SWT2dUtil.inverseTransformLine(transform, src);
		if (help.isLinked()) {
			src.setLinked(true);
		}
		// src.setDirection(help.getDirection());

		return src;
	}

	/**
	 * Returns a point which is the result of converting the argument, which is
	 * specified in image relative coordinates, to coordinates relative to the
	 * display.
	 * 
	 * @param imgP
	 *            The point to be translated (must not be null).
	 * @return the translated coordinate.
	 */
	public Point imageToScreen(Point src) {
		return SWT2dUtil.transformPoint2(transform, src);
	}

	public TGShape imageToScreen(TGShape src) {
		if (src instanceof TGRectangle)
			return imageToScreen((TGRectangle) src);
		else if (src instanceof TGPolygon)
			return imageToScreen((TGPolygon) src);
		else if (src instanceof TGLine)
			return imageToScreen((TGLine) src);

		return null;
	}

	/**
	 * Returns a Rectangle which is the result of converting the argument, which
	 * is specified in image relative coordinates, to coordinate relative to the
	 * display.
	 * 
	 * @param src
	 *            The rectangle to be translated (must not be null).
	 * @return the translated Rectangle.
	 */
	public TGRectangle imageToScreen(TGRectangle src) {
		TGRectangle help = src;
		src = SWT2dUtil.transformRect(transform, src);
		if (help.isLinked()) {
			src.setLinked(true);
		}
		src.setWritingMode(help.getWritingMode());
		src.setRotationValue(help.getRotationValue());
		src.setLayerValues(new Integer(help.getLayer()), help.getLayerName(),
				help.getLayerRGB());
		return src;
	}

	public TGPolygon imageToScreen(TGPolygon src) {
		Point[] p = new Point[src.size()];
		for (int i = 0; i < src.size(); i++) {
			p[i] = new Point(src.getXPoints()[i], src.getYPoints()[i]);
		}
		TGPolygon srcNew = new TGPolygon(SWT2dUtil.transformPolygon(p,
				transform), src.getImageUri());

		srcNew.setLinked(src.isLinked());
		srcNew.setVisible(src.isVisible());
		srcNew.setWritingMode(src.getWritingMode());
		srcNew.setRotationValue(src.getRotationValue());

		srcNew.setLayerValues(new Integer(src.getLayer()), src.getLayerName(),
				src.getLayerRGB());

		return srcNew;
	}

	public TGLine imageToScreen(TGLine src) {
		Point[] p = new Point[src.size()];
		for (int i = 0; i < src.size(); i++) {
			p[i] = new Point(src.getXPoints()[i], src.getYPoints()[i]);
		}
		TGLine srcNew = new TGLine(SWT2dUtil.transformPolygon(p, transform),
				src.getImageUri());

		// if (src.isLinked()) {
		// srcNew.setLinked(true);
		// }
		// srcNew.setDirection(src.getDirection());

		return srcNew;
	}

	/**
	 * Return the file name opened.
	 * 
	 * @return current image file name.
	 */
	public String getImageFile() {
		return imageFile;
	}

	/**
	 * Set the file path.
	 * 
	 * @param string
	 *            the image file name.
	 */
	public void setImageFile(String string) {
		imageFile = string;
	}

	/**
	 * Prompt when no image inside the canvas.
	 * 
	 * @param string
	 *            prompt text.
	 */
	public void setNoImagePrompt(String string) {
		noImagePrompt = string;
	}

	/**
	 * Get context menu manager.
	 * 
	 * @return context menu manager.
	 */
	public MenuManager getContextMenuManager() {
		return contextMenuManager;
	}

	/**
	 * Set context menu manager.
	 * 
	 * @param manager
	 *            MenuManager to be set.
	 */
	public void setContextMenuManager(MenuManager manager) {
		contextMenuManager = manager;
	}

	/**
	 * Flip vertically.
	 */
	public void vFlip() {
		ImageData src = getImageData();
		if (src == null)
			return;
		PaletteData srcPal = src.palette;
		PaletteData destPal;
		ImageData dest;
		/* construct a new ImageData */
		if (srcPal.isDirect) {
			destPal = new PaletteData(srcPal.redMask, srcPal.greenMask,
					srcPal.blueMask);
		} else {
			destPal = new PaletteData(srcPal.getRGBs());
		}
		dest = new ImageData(src.width, src.height, src.depth, destPal);
		/* flip by rearranging the pixels */
		for (int i = 0; i < src.width; i++) {
			for (int j = 0; j < src.height; j++) {
				dest.setPixel(i, j, src.getPixel(i, src.height - 1 - j));
			}
		}
		setImageData(dest);
		return;
	}

	/**
	 * Flip horizontal.
	 */
	public void hFlip() {
		ImageData src = getImageData();
		if (src == null)
			return;
		PaletteData srcPal = src.palette;
		PaletteData destPal;
		ImageData dest;
		/* construct a new ImageData */
		if (srcPal.isDirect) {
			destPal = new PaletteData(srcPal.redMask, srcPal.greenMask,
					srcPal.blueMask);
		} else {
			destPal = new PaletteData(srcPal.getRGBs());
		}
		dest = new ImageData(src.width, src.height, src.depth, destPal);
		/* flip by rearranging the pixels */
		for (int i = 0; i < src.width; i++) {
			for (int j = 0; j < src.height; j++) {
				dest.setPixel(i, j, src.getPixel(src.width - 1 - i, j));
			}
		}
		setImageData(dest);
		return;
	}

	/**
	 * Rotate image
	 */
	public void rotate(int direction) {
		ImageData srcData = getImageData();
		int bytesPerPixel = srcData.bytesPerLine / srcData.width;
		int destBytesPerLine = (direction == SWT.DOWN) ? srcData.width
				* bytesPerPixel : srcData.height * bytesPerPixel;
		byte[] newData = new byte[srcData.data.length];
		int width = 0, height = 0;
		for (int srcY = 0; srcY < srcData.height; srcY++) {
			for (int srcX = 0; srcX < srcData.width; srcX++) {
				int destX = 0, destY = 0, destIndex = 0, srcIndex = 0;
				switch (direction) {
				case SWT.LEFT: // left 90 degrees
					destX = srcY;
					destY = srcData.width - srcX - 1;
					width = srcData.height;
					height = srcData.width;
					break;
				case SWT.RIGHT: // right 90 degrees
					destX = srcData.height - srcY - 1;
					destY = srcX;
					width = srcData.height;
					height = srcData.width;
					break;
				case SWT.DOWN: // 180 degrees
					destX = srcData.width - srcX - 1;
					destY = srcData.height - srcY - 1;
					width = srcData.width;
					height = srcData.height;
					break;
				}
				destIndex = (destY * destBytesPerLine)
						+ (destX * bytesPerPixel);
				srcIndex = (srcY * srcData.bytesPerLine)
						+ (srcX * bytesPerPixel);
				System.arraycopy(srcData.data, srcIndex, newData, destIndex,
						bytesPerPixel);
			}
		}
		// destBytesPerLine is used as scanlinePad to ensure that no padding is
		// required
		ImageData dest = new ImageData(width, height, srcData.depth,
				srcData.palette, destBytesPerLine, newData);

		setImageData(dest);
	}
}