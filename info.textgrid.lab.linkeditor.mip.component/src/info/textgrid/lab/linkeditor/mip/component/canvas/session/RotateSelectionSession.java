package info.textgrid.lab.linkeditor.mip.component.canvas.session;

import info.textgrid.lab.linkeditor.model.graphics.TGShape.TYPE;

public class RotateSelectionSession extends BasicSelectSession{

	public RotateSelectionSession() {
	}
	
	public RotateSelectionSession(String id) {
		super(id);
	}
	
	@Override
	public void beginSession(Object canvas) {
		super.beginSession(canvas);
		sessionCanvas.showPositionOnImagePanel();
		
		if(sessionCanvas.getSelectedShape().getType() == TYPE.RECT){
			sessionCanvas.rotatingPointFlag = true;
			sessionCanvas.setShowRotation(true);
			sessionCanvas.redraw();
		}
		
	}
	
	@Override
	public void endSession() {
		super.endSession();
		sessionCanvas.showPositionOnImagePanel();
		
		if(sessionCanvas.getSelectedShape().getType() == TYPE.RECT){
			sessionCanvas.rotatingPointFlag = false;
			sessionCanvas.setShowRotation(false);
			sessionCanvas.redraw();
		}
	}
	
}
