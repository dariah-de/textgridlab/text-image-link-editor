/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.canvas;

import info.textgrid.lab.linkeditor.model.graphics.TGShape;

import org.eclipse.swt.graphics.ImageData;

/**
 * Image observer interface specified the basic behaviors of an image observer.
 * 
 * @author Chengdong Li
 */
public interface IImageObserver {
	/**
	 * Function called when the image-changed (reload/load) event is fired.
	 * 
	 * @param imageData
	 *            the changed image data
	 */
	public void onImageChanged(AffineImageCanvas canvas, ImageData imageData);

	/**
	 * Function called when the selection area is changed.
	 * 
	 * @param rect
	 *            the changed selection area
	 */
	public void onSelectionChanged(AffineImageCanvas canvas, TGShape rect);
}
