package info.textgrid.lab.linkeditor.mip.component.views;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.linkeditor.mip.component.views.messages"; //$NON-NLS-1$
	public static String ThumbView_ZoomIn;
	public static String ThumbView_ZoomOut;
	public static String ThumbView_ZoomOutOrZoomIn;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
