/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component.views;

import info.textgrid.lab.linkeditor.controller.ILinkEditorListener;
import info.textgrid.lab.linkeditor.controller.LinkEditorController;
import info.textgrid.lab.linkeditor.controller.LinkEditorController.Event;
import info.textgrid.lab.linkeditor.controller.utils.Pair;
import info.textgrid.lab.linkeditor.mip.component.ComponentPlugin;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import com.swtdesigner.ResourceManager;
import com.swtdesigner.SWTResourceManager;

/**
 * <code>ThumbView</code> is the thumb view of image.
 * 
 * @author Chengdong Li
 */
public class ThumbView extends ViewPart implements IThumbView,
		ILinkEditorListener {
	public ThumbCanvas canvas = null;
	public Slider slider;
	public final static int RANGE = 800;
	public final static double ZOOMINBOUND = 10.0;
	public final static double ZOOMOUTBOUND = 0.1;
	public static String ID = "edu.uky.mip.component.views.ThumbView"; //$NON-NLS-1$
	public final static double ZOOMINDEX = 2.5;

	/**
	 * The constructor.
	 */
	public ThumbView() {
	}

	/**
	 * This is a callback function that will allow us to create the viewer and
	 * initialize it.
	 * 
	 * @param parent
	 *            parent composite
	 */
	@Override
	public void createPartControl(Composite parent) {
		GridLayout gridLayout = new GridLayout();
		parent.setLayout(gridLayout);

		canvas = new ThumbCanvas(parent);
		GridData gridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.FILL_VERTICAL);
		canvas.setLayoutData(gridData);

		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true,
				false, 1, 1));
		composite.setLayout(new GridLayout(3, false));

		Label lblMin = new Label(composite, SWT.NONE);
		lblMin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				double pos = slider.getSelection() - ZOOMINDEX;
				double zoomRate = 1.0;
				if (pos >= RANGE / 2) {
					zoomRate = (pos - RANGE / 2)
							/ ((RANGE / 2) / (ZOOMINBOUND - 1.0)) + 1.0;
				} else {
					zoomRate = pos / ((RANGE / 2) / (1.0 - ZOOMOUTBOUND))
							+ ZOOMOUTBOUND;
				}
				ThumbViewMediator.onTVZoom(zoomRate, zoomRate);
			}
		});
		lblMin.setImage(ResourceManager.getPluginImage(
				"info.textgrid.lab.linkeditor.mip.component", //$NON-NLS-1$
				"icons/ZoomOut16.gif")); //$NON-NLS-1$
		lblMin.setToolTipText(Messages.ThumbView_ZoomOut);
		lblMin.setFont(SWTResourceManager.getFont("Tahoma", 8, SWT.NORMAL)); //$NON-NLS-1$
		lblMin.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		lblMin.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false,
				1, 1));
		lblMin.setBounds(0, 0, 49, 13);

		slider = new Slider(composite, SWT.BORDER);
		slider.setToolTipText(Messages.ThumbView_ZoomOutOrZoomIn);
		GridData gridData_1 = new GridData(SWT.CENTER, SWT.CENTER, true, false,
				1, 1);
		gridData_1.widthHint = 125;
		slider.setLayoutData(gridData_1);
		slider.setMaximum(RANGE);
		slider.setIncrement(1);
		slider.setPageIncrement(2);
		slider.setThumb(1);
		slider.setSelection(RANGE / 2);
		slider.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// if(e.detail==SWT.DRAG) {return;}
				double pos = slider.getSelection();
				double zoomRate = 1.0;
				if (pos >= RANGE / 2) {
					zoomRate = (pos - RANGE / 2)
							/ ((RANGE / 2) / (ZOOMINBOUND - 1.0)) + 1.0;
				} else {
					zoomRate = pos / ((RANGE / 2) / (1.0 - ZOOMOUTBOUND))
							+ ZOOMOUTBOUND;
				}
				ThumbViewMediator.onTVZoom(zoomRate, zoomRate);
			}
		});

		Label lblMax = new Label(composite, SWT.NONE);
		lblMax.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				double pos = slider.getSelection() + ZOOMINDEX;
				double zoomRate = 1.0;
				if (pos >= RANGE / 2) {
					zoomRate = (pos - RANGE / 2)
							/ ((RANGE / 2) / (ZOOMINBOUND - 1.0)) + 1.0;
				} else {
					zoomRate = pos / ((RANGE / 2) / (1.0 - ZOOMOUTBOUND))
							+ ZOOMOUTBOUND;
				}
				ThumbViewMediator.onTVZoom(zoomRate, zoomRate);
			}
		});
		lblMax.setImage(ResourceManager.getPluginImage(
				"info.textgrid.lab.linkeditor.mip.component", //$NON-NLS-1$
				"icons/ZoomIn16.gif")); //$NON-NLS-1$
		lblMax.setToolTipText(Messages.ThumbView_ZoomIn);
		lblMax.setFont(SWTResourceManager.getFont("Tahoma", 8, SWT.NORMAL)); //$NON-NLS-1$
		lblMax.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		lblMax.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false,
				1, 1));
		lblMax.setSize(49, 13);

		LinkEditorController.getInstance().addListener(this);
		ThumbViewMediator.register(this);

		PlatformUI
				.getWorkbench()
				.getHelpSystem()
				.setHelp(canvas,
						"info.textgrid.lab.linkeditor.mip.component.ThumbView"); //$NON-NLS-1$
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		if (canvas != null)
			canvas.setFocus();
	}

	/**
	 * unregister the view from mediator before disposed.
	 */
	@Override
	public void dispose() {
		LinkEditorController.getInstance().removeListener(this);
		ThumbViewMediator.unRegister(this);
		super.dispose();
	}

	private Rectangle getImageRect(Image src, Canvas c) {
		if (src == null || src.isDisposed())
			return null;
		Rectangle cRect = c.getClientArea();
		Rectangle bound = src.getBounds();
		int cw = cRect.width;
		int ch = cRect.height;
		int bw = bound.width;
		int bh = bound.height;
		double sw = ((double) bw) / ((double) cw);
		double sh = ((double) bh) / ((double) ch);
		double s = sw > sh ? sw : sh;
		int iw = (int) ((bound.width) / s);
		int ih = (int) ((bound.height) / s);

		int x0 = (cRect.width - iw) / 2;
		int y0 = (cRect.height - ih) / 2;
		Rectangle bRect = new Rectangle(0, 0, 0, 0);
		bRect.x = x0;
		bRect.y = y0;
		bRect.width = iw;
		bRect.height = ih;
		return bRect;
	}

	/**
	 * Synchronize the slider bar.
	 * 
	 * @param sx
	 *            ratio equals to: (red rect width)/(width of thumb image)
	 */
	void synchronizeSlider(double sx) {
		double x = 0;
		if (sx >= 1.0) {
			if (sx <= ZOOMINBOUND) {
				x = (sx - 1.0) / (2.0 * (ZOOMINBOUND - 1.0) / RANGE) + (RANGE)
						/ 2;
			} else {
				x = RANGE;
			}
		} else {
			if (sx > ZOOMOUTBOUND) {
				x = (sx - ZOOMOUTBOUND) / (2.0 * (1 - ZOOMOUTBOUND) / RANGE);
			} else {
				x = 0;
			}
		}
		slider.setSelection((int) Math.round(x)); // (int) does not work.
	}

	/**
	 * Given one source image and a canvas, return an image which can exactly
	 * fit canvas.
	 * 
	 * @param src
	 *            source image.
	 * @param canvas
	 *            destination canvas
	 * @return iconImage which will fit canvas
	 */
	static public Image createImage(Image src, ThumbCanvas c) {
		if (c == null || c.isDisposed())
			return null;
		if (src == null || src.isDisposed())
			return null;
		Rectangle cRect = c.getClientArea();
		Rectangle bound = src.getBounds();
		int cw = cRect.width;
		int ch = cRect.height;
		int bw = bound.width;
		int bh = bound.height;
		if (bw <= 0 || bh <= 0)
			return null;
		if (cw <= 0 || ch <= 0)
			return null;
		double sw = ((double) bw) / ((double) cw);
		double sh = ((double) bh) / ((double) ch);
		double s = sw > sh ? sw : sh;
		int iw = (int) ((bound.width) / s);
		int ih = (int) ((bound.height) / s);
		Image dest = new Image(c.getDisplay(), iw, ih);
		GC gc = new GC(dest);
		gc.drawImage(src, 0, 0, bw, bh, 0, 0, iw, ih);
		gc.dispose();
		return dest;
	}

	/**
	 * A basic thumbCanvas which implement the fitCanvas funtion.
	 * 
	 * @author Chengdong Li
	 */
	class ThumbCanvas extends Canvas {
		private ThumbCanvas instance;
		protected Image sourceImage; // original image
		protected boolean mousePressed = false;
		protected Point hotPot = null;
		protected Rectangle block = new Rectangle(0, 0, 0, 0);

		/**
		 * Constructor for ThumbCanvas.
		 * 
		 * @param parent
		 *            A SWT Composite
		 */
		public ThumbCanvas(Composite parent) {
			super(parent, SWT.NO_REDRAW_RESIZE | SWT.NO_BACKGROUND);
			this.instance = this;
			this.addControlListener(new ControlAdapter() {
				@Override
				public void controlResized(ControlEvent event) {
					ThumbViewMediator.onISScroll();
				}
			});

			this.addPaintListener(new PaintListener() {
				@Override
				public void paintControl(final PaintEvent event) {
					paint(event.gc);
				}
			});

			this.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseDown(MouseEvent e) {
					if (block == null)
						return;
					if (canvas.getSourceImage() == null)
						return;
					mousePressed = true;
					hotPot = new Point(e.x, e.y);
					if (!block.contains(e.x, e.y)) {
						Point center = getBlockCenter();
						moveBlock(e.x - center.x, e.y - center.y);
					}
				}

				@Override
				public void mouseUp(MouseEvent e) {
					mousePressed = false;
				}
			});
			this.addMouseMoveListener(new MouseMoveListener() {
				@Override
				public void mouseMove(MouseEvent e) {
					if (block == null)
						return;
					if (canvas.getSourceImage() == null)
						return;
					if (block.contains(e.x, e.y))
						instance.setCursor(new Cursor(instance.getDisplay(),
								SWT.CURSOR_SIZEALL));
					else
						instance.setCursor(new Cursor(instance.getDisplay(),
								SWT.CURSOR_HAND));
					if (mousePressed) {
						moveBlock(e.x - hotPot.x, e.y - hotPot.y);
						hotPot.x = e.x;
						hotPot.y = e.y;
					}
				}
			});
		}

		/**
		 * Paint function
		 * 
		 * @param gc
		 *            graphics context
		 */
		public void paint(GC gc) {
			if (sourceImage != null) {
				Rectangle bound = sourceImage.getBounds();
				int bw = bound.width;
				int bh = bound.height;
				Rectangle cRect = getClientArea(); // client area
				Image screenImage = new Image(getDisplay(), cRect.width,
						cRect.height);
				int cw = cRect.width;
				int ch = cRect.height;
				GC newGC = new GC(screenImage);
				newGC.drawImage(sourceImage, 0, 0, bw, bh, (cw - bw) / 2,
						(ch - bh) / 2, bw, bh);
				newGC.setForeground(new Color(this.getDisplay(), 255, 0, 0));
				newGC.setLineWidth(2);
				newGC.drawRectangle(block);
				newGC.dispose();
				gc.drawImage(screenImage, 0, 0);
				screenImage.dispose();
			} else {
				gc.fillRectangle(getClientArea());
			}
		}

		/**
		 * reload image who fits current window
		 * 
		 * @param image
		 *            image with size exactly fit canvas.
		 */
		public void reloadImage(Image image, double tx, double ty, double sx,
				double sy) {
			if (this.isDisposed())
				return;

			if (!canvas.isVisible())
				canvas.setVisible(true);

			if (sourceImage != null) {
				sourceImage.dispose();
				sourceImage = null;
			}
			if (image == null)
				return;

			if (image.isDisposed())
				return;

			sourceImage = createImage(image, canvas);
			if (sourceImage == null) {
				return;
			}

			Rectangle bound = sourceImage.getBounds();
			Rectangle cRect = canvas.getClientArea();
			int x0 = (cRect.width - bound.width) / 2;
			int y0 = (cRect.height - bound.height) / 2;
			Rectangle bRect = new Rectangle(0, 0, 0, 0);
			bRect.x = x0 + (int) (bound.width * tx);
			bRect.y = y0 + (int) (bound.height * ty);
			bRect.width = (int) (bound.width * sx);
			bRect.height = (int) (bound.height * sy);
			setBlock(bRect);
			redraw();
		}

		/**
		 * Get slider rectangle.
		 * 
		 * @return Returns the block.
		 */
		public Rectangle getBlock() {
			return block;
		}

		/**
		 * Set slider rectangle.
		 * 
		 * @param block
		 *            The block to set.
		 */
		public void setBlock(Rectangle bk) {
			this.block = bk;
			redraw();
		}

		/**
		 * Return source image
		 * 
		 * @return Returns the sourceImage.
		 */
		public Image getSourceImage() {
			return sourceImage;
		}

		/**
		 * Set source image.
		 * 
		 * @param image
		 *            The sourceImage to set.
		 */
		public void setSourceImage(Image image) {
			this.sourceImage = image;
		}

		/**
		 * Move the slider.
		 * 
		 * @param detX
		 *            increasement in x.
		 * @param detY
		 *            increasement in y.
		 */
		public void moveBlock(int detX, int detY) {
			Rectangle iRect = getImageRect(canvas.getSourceImage(), canvas);
			ThumbViewMediator.onTVScroll(((double) detX)
					/ ((double) iRect.width), ((double) detY)
					/ ((double) iRect.height));
		}

		/* get slider center */
		private Point getBlockCenter() {
			return new Point((block.x * 2 + block.width) / 2,
					(block.y * 2 + block.height) / 2);
		}
	}

	// ////////////////// IThumbView Interface ///////////////////////////
	@Override
	public void onISScroll(double tx, double ty, double sx, double sy) {
		Image image = ThumbViewMediator.getISImage();
		if (image == null || image.isDisposed()) {
			this.canvas.setSourceImage(null);
			canvas.redraw();
			return;
		}

		if (canvas == null || canvas.isDisposed() || !canvas.isEnabled()
				|| canvas.getDisplay() == null
				|| canvas.getDisplay().isDisposed())
			return;

		try { // This is a bug I could not fix now..:(
			canvas.reloadImage(image, tx, ty, sx, sy);
		} catch (Error e) {
			ComponentPlugin.handleWarning(e);
		} catch (Throwable e) {
			ComponentPlugin.handleWarning(e);
		}
	}

	@Override
	public void onISZoom(double sx, double sy) {
		synchronizeSlider(sx);
	}

	@Override
	public void repaint() {
		canvas.redraw();
	}

	@Override
	public void setImage(Image image, double tx, double ty, double sx,
			double sy, double scaleX, double scaleY) {
		canvas.reloadImage(image, tx, ty, sx, sy);
		synchronizeSlider(scaleX);
	}

	@Override
	public boolean isDisposed() {
		return canvas.isDisposed();
	}

	@Override
	public void synchronizeZoom(double scaleX, double scaleY) {
		synchronizeSlider(scaleX);
	}

	@Override
	public void update(Event event, Pair<String, ?> pair) {
		if (event == Event.SET_IMAGEVIEW_INVISIBLE) {
			canvas.setVisible(false);
		}
	}
}
