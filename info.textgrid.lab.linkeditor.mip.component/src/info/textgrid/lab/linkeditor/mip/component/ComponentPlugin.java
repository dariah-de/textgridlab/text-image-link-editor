/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 * Author: Chengdong Li <cdli@ccs.uky.edu>
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.component;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * The main plugin class to be used in the desktop.
 */
public class ComponentPlugin extends AbstractUIPlugin {

	private static ComponentPlugin plugin = null;

	/**
	 * The constructor.
	 */
	public ComponentPlugin() {
		super();
		plugin = this;
	}

	/**
	 * Returns the shared instance.
	 */
	public static ComponentPlugin getDefault() {
		return plugin;
	}

	public static IStatus handleError(Throwable cause, String message,
			Object... args) {
		return ComponentPlugin.getDefault().handleProblem(IStatus.ERROR,
				NLS.bind(message, args), cause);
	}

	public static IStatus handleError(Throwable cause) {
		return ComponentPlugin.getDefault().handleProblem(IStatus.ERROR, cause);
	}

	public static IStatus handleWarning(Throwable cause, String message,
			Object... args) {
		return ComponentPlugin.getDefault().handleProblem(IStatus.WARNING,
				NLS.bind(message, args), cause);
	}

	public static IStatus handleWarning(Throwable cause) {
		return ComponentPlugin.getDefault().handleProblem(IStatus.WARNING,
				cause);
	}

	public IStatus handleProblem(int severity, Throwable e) {
		return handleProblem(severity, null, e);
	}

	public IStatus handleProblem(int severity, String message, Throwable cause) {
		if (message == null || message.equals("")) {
			if (cause != null)
				message = cause.getLocalizedMessage();
		}
		IStatus status = new Status(severity, "MIP ImagePlugin", message, cause);
		StatusManager.getManager().handle(
				status,
				status.getSeverity() == IStatus.ERROR ? StatusManager.SHOW
						| StatusManager.LOG : StatusManager.LOG);
		return status;
	}

}
