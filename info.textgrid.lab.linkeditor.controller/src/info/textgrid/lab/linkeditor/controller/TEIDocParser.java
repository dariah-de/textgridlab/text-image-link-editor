/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.controller;

import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.linkeditor.controller.LinkEditorController.Event;
import info.textgrid.lab.linkeditor.controller.TeiFactory.OMUtil;
import info.textgrid.lab.linkeditor.controller.TeiFactory.SVGGroup;
import info.textgrid.lab.linkeditor.controller.TeiFactory.SVGImage;
import info.textgrid.lab.linkeditor.controller.TeiFactory.SVGShape;
import info.textgrid.lab.linkeditor.controller.TeiFactory.SVGText;
import info.textgrid.lab.linkeditor.controller.TeiFactory.TEIDocFactory;
import info.textgrid.lab.linkeditor.controller.TeiFactory.TEILink;
import info.textgrid.lab.linkeditor.controller.TeiFactory.TEILinkGroup;
import info.textgrid.lab.linkeditor.controller.utils.Pair;
import info.textgrid.lab.linkeditor.model.graphics.TGLayer;
import info.textgrid.lab.linkeditor.model.graphics.TGLayerManager;
import info.textgrid.lab.linkeditor.model.graphics.TGLine;
import info.textgrid.lab.linkeditor.model.graphics.TGPolygon;
import info.textgrid.lab.linkeditor.model.graphics.TGRectangle;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;
import info.textgrid.lab.linkeditor.model.graphics.TGShape.WRITING_MODE;
import info.textgrid.lab.linkeditor.model.links.ILink;
import info.textgrid.lab.linkeditor.model.links.PolyLink;
import info.textgrid.lab.linkeditor.model.links.RectLink;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;
import org.jaxen.JaxenException;

/**
 * This class parses the linkeditor's tei-doc to open all used resources and to
 * create the links.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class TEIDocParser {

	private Map<String, SVGGroup> SVGGroupsMap = Collections
			.synchronizedMap(new HashMap<String, SVGGroup>());

	private List<TEILinkGroup> teiLinkGroupsList = Collections
			.synchronizedList(new ArrayList<TEILinkGroup>());

	private LinkEditorController controller = LinkEditorController
			.getInstance();

	private static String COMMA = ",";

	private Job fetchingResJob = null;

	private String parentPath = null;

	public void parse(InputStream input, IProgressMonitor monitor)
			throws XMLStreamException, JaxenException {

		if (monitor == null)
			monitor = new NullProgressMonitor();

		final OMElement root = new StAXOMBuilder(input).getDocumentElement();

		if (!root.getLocalName().equals("TEI")) {

			new UIJob("Starting error dialog...") {
				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					Status status = new Status(IStatus.ERROR,
							Activator.PLUGIN_ID, "Old style Text-Image-Link Object");
					ErrorDialog.openError(PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getShell(),
							"Old style Text-Image-Link Object",
							"The selected Text-Image-Link Object is not supported!",
							status);
					return Status.OK_STATUS;
				}
			}.schedule();

			return;
		}

		fetchingResJob = new Job("Fetching resources...") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					openAllUsedTGResources(root, monitor);

					return Status.OK_STATUS;

				} catch (JaxenException e) {
					Activator.handleError(e);
				} catch (CrudServiceException e) {
					Activator.handleError(e);
				} catch (IOException e) {
					Activator.handleError(e);
				} catch (URISyntaxException e) {
					Activator.handleError(e);
				}

				return Status.CANCEL_STATUS;
			}
		};

		fetchingResJob.setProgressGroup(monitor, 60);
		fetchingResJob.setUser(true);
		fetchingResJob.schedule();

		// wait for the resources fetching job
		try {
			fetchingResJob.join();
		} catch (InterruptedException e) {
			Activator.handleWarning(e);
		}

		if (fetchingResJob.getResult() != Status.OK_STATUS)
			return;

		createLinks(root, monitor);
	}

	private void openAllUsedTGResources(OMElement rootElem,
			IProgressMonitor monitor) throws JaxenException, IOException,
			URISyntaxException, CrudServiceException {

		List<OMElement> imageElements = OMUtil
				.getElementListWithName("image", rootElem,
						TEIDocFactory.SVG_NS_URI, TEIDocFactory.SVG_NS_Prefix);

		TreeSet<String> xmlImageResources = new TreeSet<String>();

		for (OMElement e : imageElements) {
			String imageUri = e.getAttributeValue(new QName(
					TEIDocFactory.XLINK_NS_URI, "href",
					TEIDocFactory.XLINK_NS_Prefix));

			xmlImageResources.add(imageUri);
		}

		List<OMElement> linkElems = OMUtil
				.getElementListWithName("link", rootElem,
						TEIDocFactory.TEI_NS_URI, TEIDocFactory.TEI_NS_Prefix);

		// controller.resetXmlUris();

		for (OMElement e : linkElems) {
			String textUri = getTextUriFromElement(e, monitor);

			xmlImageResources.add(textUri);
			// controller.addXmlUri(textUri);
		}

		for (String s : xmlImageResources) {
			if (!s.startsWith("textgrid") && !s.startsWith("file")) {
				if (parentPath != null) {
					controller.addResourceToLinkEditor(
							(new File(parentPath, s)).toURI().toString(),
							monitor);
				} else {
					throw new IOException("No valid TextGrid/File-uri found!");
				}
			} else {
				controller.addResourceToLinkEditor(s, monitor);
			}
		}
	}

	private int counter = 0;

	private void createLinks(OMElement rootElem, IProgressMonitor monitor)
			throws JaxenException {

		List<OMElement> SVGGroupElements = OMUtil
				.getElementListWithName("g", rootElem,
						TEIDocFactory.SVG_NS_URI, TEIDocFactory.SVG_NS_Prefix);

		// collect all svg:g elements
		for (OMElement g : SVGGroupElements) {
			SVGGroup group = getSVGGroupFromOM(g, monitor);
			SVGGroupsMap.put(group.getId(), group);
		}

		List<OMElement> teiLinkGroupElements = OMUtil.getElementListWithName(
				"linkGrp", rootElem, TEIDocFactory.TEI_NS_URI,
				TEIDocFactory.TEI_NS_Prefix);

		// tei:linkGrp
		for (OMElement linkGrp : teiLinkGroupElements) {
			TEILinkGroup group = getTeiLinkGroupFromOM(linkGrp, monitor);
			teiLinkGroupsList.add(group);
		}

		// create the links
		for (TEILinkGroup g : teiLinkGroupsList) {
			List<TEILink> teilinks = g.getLinks();
			final int size = teilinks.size();

			for (TEILink l : teilinks) {
				final ILink link = l.getLink();

				controller.addLink(link);

				new UIJob("Updating the image views...") {
					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						if (++counter < size) {
							controller.notifyListeners(
									Event.ADD_LINK_LOADING,
									new Pair<String, TGShape>(link
											.getImageUri(), link.getShape()));
						} else {
							controller.notifyListeners(
									Event.ADD_LINK,
									new Pair<String, TGShape>(link
											.getImageUri(), link.getShape()));
							counter = 0;
						}
						return Status.OK_STATUS;
					}
				}.schedule();
			}
		}
	}

	private SVGImage getSVGImageFromOM(OMElement imageElem,
			IProgressMonitor monitor) {

		String uri = imageElem.getAttributeValue(new QName(
				TEIDocFactory.XLINK_NS_URI, "href",
				TEIDocFactory.XLINK_NS_Prefix));

		Point p = getImageWidthHeight(uri);

		int width = p.x;
		int height = p.y;

		return new SVGImage(uri, width, height);
	}

	@SuppressWarnings("static-access")
	private void setTGShapeLayer(String layer_, TGShape s) {
		if (layer_ == null) {
			layer_ = controller.getDefaultLayer();
		}
		try {
			String[] strLayer_Split;
			String[] strRGB_Split;
			strLayer_Split = layer_.split(","); //$NON-NLS-1$

			if (strLayer_Split.length < 1)
				return;

			s.setLayer(java.lang.Integer.valueOf(strLayer_Split[0].trim()));

			if (strLayer_Split.length > 1) {// layerName
				s.setLayerName(strLayer_Split[1].trim());

				if (strLayer_Split.length > 2) {
					// layerRGB
					String rgbStr = strLayer_Split[2].trim();
					strRGB_Split = rgbStr.split("#"); //$NON-NLS-1$

					if (strRGB_Split.length < 1)
						return;

					s.setLayerRGB(new RGB(new Integer(strRGB_Split[0]),
							new Integer(strRGB_Split[1]), new Integer(
									strRGB_Split[2])));
				}
			}
		} catch (NumberFormatException e) {
			Activator.handleError(e);
		} catch (ArrayIndexOutOfBoundsException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private TGRectangle createTGRect(String rect_, String imageUri_,
			String rotate_, String layer_, int imageWidth, int imageHeight) {

		String[] strA_Split;
		int x, y, width, height;

		rect_ = controller.percentToRect(rect_, imageWidth, imageHeight);

		if (rect_ == null)
			return null;

		strA_Split = rect_.split(","); //$NON-NLS-1$

		x = Math.round(Float.valueOf(strA_Split[0].trim()));
		y = Math.round(Float.valueOf(strA_Split[1].trim()));
		width = Math.round(Float.valueOf(strA_Split[2].trim()));
		height = Math.round(Float.valueOf(strA_Split[3].trim()));

		TGRectangle r = new TGRectangle(x, y, width, height, imageUri_);

		// TODO
		// r.setDirection(controller.getDirectionFromString(direction_));

		r.setRotationValue(Double.valueOf(rotate_));
		setTGShapeLayer(layer_, r);

		return r;
	}

	private TGPolygon createTGPolygon(String poly_, String imageUri_,
			String layer_, int imageWidth, int imageHeight) {

		String[] strA_Split;

		poly_ = controller.percentToPoly(poly_, imageWidth, imageHeight);

		if (poly_ == null)
			return null;

		strA_Split = poly_.split(","); //$NON-NLS-1$

		int x = 0;
		int y = 0;

		TGPolygon p = new TGPolygon(imageUri_);
		setTGShapeLayer(layer_, p);

		int i = 0;
		for (String s : strA_Split) {
			if (i % 2 == 0) {
				// x-coordinate
				x = Math.round(Float.valueOf(s.trim()));
			} else {
				// y-coordinate
				y = Math.round(Float.valueOf(s.trim()));
				p.addPoint(x, y);
			}
			i++;
		}

		// TODO
		// p.setDirection(controller.getDirectionFromString(direction_));

		return p;
	}

	private TGLine createTGLine(String dockingLine_, String imageUri_,
			String layer_, int imageWidth, int imageHeight) {

		String[] strA_Split;

		dockingLine_ = controller.percentToDockingLine(dockingLine_,
				imageWidth, imageHeight);

		if (dockingLine_ == null)
			return null;

		strA_Split = dockingLine_.split(","); //$NON-NLS-1$
		int x = 0;
		int y = 0;

		TGLine gline = new TGLine(imageUri_);
		setTGShapeLayer(layer_, gline);

		int i = 0;
		for (String s : strA_Split) {
			if (i % 2 == 0) {
				// x-coordinate
				x = Math.round(Float.valueOf(s.trim()));
			} else {
				// y-coordinate
				y = Math.round(Float.valueOf(s.trim()));
				gline.addPoint(x, y);
			}
			i++;
		}

		return gline;
	}

	private SVGGroup getSVGGroupFromOM(OMElement gElem, IProgressMonitor monitor)
			throws JaxenException {

		String groupId = gElem.getAttributeValue(new QName("id"));

		final String default_writing_mode = gElem.getAttributeValue(new QName(
				"writing-mode"));

		SVGGroup group = new SVGGroup(groupId,
				controller.getWritingModeFromString(default_writing_mode));

		// image
		OMElement imageElem = OMUtil.getElementWithName("image", gElem,
				TEIDocFactory.SVG_NS_URI, TEIDocFactory.SVG_NS_Prefix);

		SVGImage svgImage = getSVGImageFromOM(imageElem, monitor);
		final String uri = getValidUri(svgImage.getUri(), true);

		group.setImage(svgImage);

		new UIJob("") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				controller.setDefaultWritingMode(uri, controller
						.getWritingModeFromString(default_writing_mode));
				return Status.OK_STATUS;
			}
		}.schedule();

		// ******************************************
		TGLayerManager tgLM = new TGLayerManager(uri); // create new
		// TGLayerManager for
		// this Image

		TreeSet<TGLayer> tgLayerTreeSet = new TreeSet<TGLayer>();
		// ******************************************
		// rects
		List<OMElement> rectElems = OMUtil.getElementListWithName("rect",
				gElem, TEIDocFactory.SVG_NS_URI, TEIDocFactory.SVG_NS_Prefix);

		for (OMElement rectElem : rectElems) {
			SVGShape shape = getSVGRectFromOM(rectElem, uri, monitor);
			group.addShape(shape);
			TGLayer tgLayer = new TGLayer(shape.getShape().getLayerName(),
					Integer.valueOf(shape.getShape().getLayer()));
			tgLayer.setLayercolor(new Color(PlatformUI.getWorkbench()
					.getDisplay(), shape.getShape().getLayerRGB()));
			tgLayerTreeSet.add(tgLayer);
		}

		// polygons
		@SuppressWarnings("unchecked")
		List<OMElement> polyElems = OMUtil.getElementListWithName("polygon",
				gElem, TEIDocFactory.SVG_NS_URI, TEIDocFactory.SVG_NS_Prefix);

		for (OMElement polyElem : polyElems) {
			SVGShape shape = getSVGPolygonFromOM(polyElem, uri, monitor);
			group.addShape(shape);
			TGLayer tgLayer = new TGLayer(shape.getShape().getLayerName(),
					Integer.valueOf(shape.getShape().getLayer()));
			tgLayer.setLayercolor(new Color(PlatformUI.getWorkbench()
					.getDisplay(), shape.getShape().getLayerRGB()));
			tgLayerTreeSet.add(tgLayer);
		}

		for (Iterator<TGLayer> iterator = tgLayerTreeSet.iterator(); iterator
				.hasNext();) {
			TGLayer obj = iterator.next();
			if (obj.getNumber() != 0) {
				tgLM.add(obj.getNumber(), obj);
			}
		}

		// lines
		List<OMElement> lineElems = OMUtil.getElementListWithName("line",
				gElem, TEIDocFactory.SVG_NS_URI, TEIDocFactory.SVG_NS_Prefix);

		for (OMElement lineElem : lineElems) {
			SVGShape shape = getSVGLineFromOM(lineElem, uri, monitor);
			group.addShape(shape);
		}

		// texts
		List<OMElement> textElems = OMUtil.getElementListWithName("text",
				gElem, TEIDocFactory.SVG_NS_URI, TEIDocFactory.SVG_NS_Prefix);

		for (OMElement textElem : textElems) {
			SVGText text = getSVGTextFromOM(textElem, uri, monitor);
			group.addText(text);
		}

		// Add the new TGLayerManager to the Map
		controller.addLayerManagerToMap(tgLM);

		return group;
	}

	private SVGShape getSVGRectFromOM(OMElement rectElem, String uri,
			IProgressMonitor monitor) throws JaxenException {

		String id = rectElem.getAttributeValue(new QName("id"));

		String x = rectElem.getAttributeValue(new QName("x")).replace("%", "");

		String y = rectElem.getAttributeValue(new QName("y")).replace("%", "");

		String width = rectElem.getAttributeValue(new QName("width")).replace(
				"%", "");

		String height = rectElem.getAttributeValue(new QName("height"))
				.replace("%", "");

		String rect_s = x + COMMA + y + COMMA + width + COMMA + height;

		String rotate = rectElem.getAttributeValue(new QName("transform"))
				.replaceFirst(".*\\(", "").replaceFirst("\\,.*", "");

		String layer = rectElem.getAttributeValue(new QName("class"));

		Point p = getImageWidthHeight(uri);

		TGRectangle rect = createTGRect(rect_s, uri, rotate, layer, p.x, p.y);

		return new SVGShape(id, rect, p.x, p.y);
	}

	private String getValidUri(final String uri, final boolean isImageUri) {
		String absoluteUri = uri;
		if (!uri.startsWith("textgrid") && !uri.startsWith("file")) {
			if (parentPath != null) {
				absoluteUri = new File(parentPath, uri).toURI().toString();
				return absoluteUri;
			}
		}

		// if the uri has no revision number
		if (absoluteUri.startsWith("textgrid")
				&& !absoluteUri.matches("textgrid:.+\\.\\d+")) {
			absoluteUri = controller.getRevisionedUri(uri, isImageUri);
		}

		return absoluteUri;
	}

	private Point getImageWidthHeight(String imageUri) {
		return controller.getImageWidthHeight(getValidUri(imageUri, true));
	}

	private SVGShape getSVGPolygonFromOM(OMElement polyElem, String uri,
			IProgressMonitor monitor) throws JaxenException {

		String id = polyElem.getAttributeValue(new QName("id"));

		String points = polyElem.getAttributeValue(new QName("points"))
				.replaceAll("%", "").replaceAll("\\s", COMMA);

		String layer = polyElem.getAttributeValue(new QName("class"));

		Point p = getImageWidthHeight(uri);

		TGPolygon poly = createTGPolygon(points, uri, layer, p.x, p.y);

		return new SVGShape(id, poly, p.x, p.y);
	}

	private SVGShape getSVGLineFromOM(OMElement lineElem, final String uri,
			IProgressMonitor monitor) throws JaxenException {

		String id = lineElem.getAttributeValue(new QName("id"));

		String x1 = lineElem.getAttributeValue(new QName("x1"))
				.replace("%", "");

		String y1 = lineElem.getAttributeValue(new QName("y1"))
				.replace("%", "");

		String x2 = lineElem.getAttributeValue(new QName("x2"))
				.replace("%", "");

		String y2 = lineElem.getAttributeValue(new QName("y2"))
				.replace("%", "");

		String layer = lineElem.getAttributeValue(new QName("class"));

		String line_s = x1 + COMMA + y1 + COMMA + x2 + COMMA + y2;

		Point p = getImageWidthHeight(uri);

		final TGLine line = createTGLine(line_s, uri, layer, p.x, p.y);

		// create the line
		controller.addTosaveDockingLinesUriList(uri);
		new UIJob("Updating the image views...") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				controller.notifyListeners(Event.ADD_DOCKING_LINE,
						new Pair<String, TGShape>(uri, line));
				return Status.OK_STATUS;
			}
		}.schedule();

		return new SVGShape(id, line, p.x, p.y);
	}

	private SVGText getSVGTextFromOM(OMElement textElem, String uri,
			IProgressMonitor monitor) throws JaxenException {

		String id = textElem.getAttributeValue(new QName("id"));

		String writing_mode = textElem.getAttributeValue(new QName(
				"writing-mode"));

		return new SVGText(id,
				controller.getWritingModeFromString(writing_mode));
	}

	private TEILinkGroup getTeiLinkGroupFromOM(OMElement linkGrpElem,
			IProgressMonitor monitor) throws JaxenException {

		String id = linkGrpElem.getAttributeValue(new QName(
				TEIDocFactory.XML_NS_URI, "id", TEIDocFactory.XML_NS_Prefix));
		String type = linkGrpElem.getAttributeValue(new QName("type"));

		String facs = linkGrpElem.getAttributeValue(new QName("facs"));

		SVGGroup g = SVGGroupsMap.get(facs.substring(1));

		TEILinkGroup teiLinkgroup = new TEILinkGroup(id, type, g);

		List<OMElement> teiLinkElems = OMUtil.getElementListWithName("link",
				linkGrpElem, TEIDocFactory.TEI_NS_URI,
				TEIDocFactory.TEI_NS_Prefix);

		for (OMElement teiLinkElem : teiLinkElems) {
			TEILink link = getTEILinkFromOM(teiLinkElem, g, teiLinkgroup,
					monitor);
			if (link != null)
				teiLinkgroup.addLink(link);
		}

		return teiLinkgroup;
	}

	private String getTextUriFromElement(OMElement linkElem,
			IProgressMonitor monitor) {

		String targets = linkElem.getAttributeValue(new QName("targets"));

		String[] values = targets.split("\\s");

		if (values.length < 2)
			throw new RuntimeException(NLS.bind(
					"The target attribute in tei:link have {0} values.",
					values.length));

		return values[1].split("#")[0];
	}

	private TEILink getTEILinkFromOM(OMElement linkElem, SVGGroup g,
			TEILinkGroup teiLinkGroup, IProgressMonitor monitor) {

		OMAttribute targets = linkElem.getAttribute(new QName("targets"));

		String[] values = targets.getAttributeValue().split("\\s");

		if (values.length < 2)
			throw new RuntimeException(NLS.bind(
					"The target attribute in tei:link have {0} values.",
					values.length));

		String refId = values[0].substring(1);

		String textUri = values[1].split("#")[0];

		textUri = getValidUri(textUri, false);

		ILink link = null;

		if (values.length > 2) {
			// two anchor elements
			String anchorId1 = values[1].split("#")[1];
			String anchorId2 = values[2].split("#")[1];

			if (refId.startsWith("shape")) {

				TGShape shape = g.getSVGShapeById(refId).getShape();

				if (shape.getType() == TGShape.TYPE.RECT) {
					link = new RectLink((TGRectangle) shape, anchorId1,
							anchorId2, textUri, true);
				} else if (shape.getType() == TGShape.TYPE.POLY) {
					link = new PolyLink((TGPolygon) shape, anchorId1,
							anchorId2, textUri, true);
				}

				return new TEILink(refId, link);

			} else if (refId.startsWith("text")) {
				// search the created links
				for (TEILink tl : teiLinkGroup.getLinks()) {
					ILink _l = tl.getLink();
					if (_l.getTextUri().equals(textUri)
							&& _l.getStartAnchorId().equals(anchorId1)
							&& _l.getEndAnchorId().equals(anchorId2)) {

						WRITING_MODE writing_mode = g.getSVGTextById(refId)
								.getWriting_mode();

						_l.getShape().setWritingMode(writing_mode);
					}
				}
			}

		} else if (values.length == 2) { // if the TBLE works without anchors
			// and just with an element id
			String id = values[1].split("#")[1];

			if (refId.startsWith("shape")) {

				TGShape shape = g.getSVGShapeById(refId).getShape();

				if (shape.getType() == TGShape.TYPE.RECT) {
					link = new RectLink((TGRectangle) shape, id, "", textUri,
							true);
				} else if (shape.getType() == TGShape.TYPE.POLY) {
					link = new PolyLink((TGPolygon) shape, id, "", textUri,
							true);
				}

				return new TEILink(refId, link);

			} else if (refId.startsWith("text")) {
				// search the created links
				for (TEILink tl : teiLinkGroup.getLinks()) {
					ILink _l = tl.getLink();
					if (_l.getTextUri().equals(textUri)
							&& _l.getStartAnchorId().equals(id)
							&& _l.getEndAnchorId().equals("")) {

						WRITING_MODE writing_mode = g.getSVGTextById(refId)
								.getWriting_mode();

						_l.getShape().setWritingMode(writing_mode);
					}
				}
			}

		} else {
			throw new RuntimeException(NLS.bind(
					"The target attribute in tei:link have {0} values.",
					values.length));
		}

		// if text-element, return null
		return null;
	}

	public void setParentPath(String parentPath) {
		this.parentPath = parentPath;
	}

	public String getParentPath() {
		return parentPath;
	}

}