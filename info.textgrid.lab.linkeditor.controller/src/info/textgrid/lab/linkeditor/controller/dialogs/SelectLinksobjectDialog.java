/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.controller.dialogs;

import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.linkeditor.controller.Activator;
import info.textgrid.lab.linkeditor.controller.LinkEditorController;
import info.textgrid.lab.linkeditor.controller.utils.RefersToClient;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;

public class SelectLinksobjectDialog extends TrayDialog {

	private LinkEditorController controller = null;
	private String selectedObjectTitle = null;
	private TextGridObject tgObject = null;
	private Map<String, String> uriMap = Collections
			.synchronizedMap(new HashMap<String, String>());

	protected SelectLinksobjectDialog(Shell shell) {
		super(shell);
	}

	public static boolean openDialog(final LinkEditorController controller,
			final TextGridObject tgObject) {

		UIJob dialogJ = new UIJob("Starting the dialog...") {

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {

				SelectLinksobjectDialog dialog = new SelectLinksobjectDialog(
						PlatformUI.getWorkbench().getActiveWorkbenchWindow()
								.getShell());
				dialog.controller = controller;
				dialog.tgObject = tgObject;

				if (!dialog.fetchRelations())
					return Status.CANCEL_STATUS;

				dialog.open();

				return Status.OK_STATUS;
			}
		};

		dialogJ.schedule();

		return dialogJ.getResult() == Status.OK_STATUS;
	}

	private boolean fetchRelations() {
		try {
			ArrayList<String> uris = RefersToClient.getInstance()
					.getRelationURIs(tgObject);

			if (uris == null)
				return false;

			for (String uri : uris) {
				uriMap.put(TextGridObject.getInstance(new URI(uri), false)
						.getTitle(), uri);
			}

		} catch (CrudServiceException e) {
			Activator.handleError(e);
		} catch (CoreException e) {
			Activator.handleError(e);
		} catch (URISyntaxException e) {
			Activator.handleError(e);
		}
		return (uriMap.size() > 0);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite control = new Composite(parent, SWT.NONE);
		control.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		control.setLayout(new GridLayout(1, false));

		Label intro = new Label(control, SWT.WRAP);
		intro.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		intro.setText("The selected image is related to these link-object(s).\n"
				+ "You can select a link-object from the list below!");

		createObjectsList(control);
		createButtons(control);

		return control;
	}

	private void createObjectsList(Composite control) {
		final List list = new List(control, SWT.NONE);
		list.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				selectedObjectTitle = list.getItem(list.getSelectionIndex());
			}
		});

		for (String s : uriMap.keySet()) {
			list.add(s);
		}
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
	}

	private void createButtons(Composite control) {
		Group group = new Group(control, SWT.SHADOW_ETCHED_IN);
		group.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		group.setLayout(new GridLayout(2, true));

		Button ok = new Button(group, SWT.PUSH);
		ok.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		ok.setText("Ok");
		ok.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (selectedObjectTitle != null) {
					updateLinkEditorController();
					okPressed();
				}
			}
		});

		Button cancel = new Button(group, SWT.PUSH);
		cancel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		cancel.setText("Cancel");
		cancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				controller.updateDataFromSelectLinkobjectDialog(null);
				cancelPressed();
			}
		});
	}

	private void updateLinkEditorController() {
		try {
			controller.updateDataFromSelectLinkobjectDialog(TextGridObject
					.getInstance(new URI(uriMap.get(selectedObjectTitle)),
							false));
		} catch (CrudServiceException e) {
			Activator.handleError(e);
		} catch (URISyntaxException e) {
			Activator.handleError(e);
		}
	}
}