/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.controller.dialogs;

import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectRoot;
import info.textgrid.lab.core.swtutils.DeferredListContentProvider.IDoneListener;
import info.textgrid.lab.core.swtutils.PendingLabelProvider;
import info.textgrid.lab.linkeditor.controller.LinkEditorController;
import info.textgrid.lab.linkeditor.controller.utils.UpdatingDeferredListContentProvider;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

/**
 * A dialog to select the name and location to save the new created links
 * object.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * @author leuk
 * 
 */
public class SelectNameLocationDialog extends TitleAreaDialog implements
		ISelectionChangedListener {

	private ListViewer projectViewer;
	private TextGridProject currentProject;
	private TextGridProject project = null;
	private Label projectDescLabel;
	private Text name_text = null;
	private LinkEditorController controller = null;
	private boolean saveAs = false;

	public static void openDialog(LinkEditorController controller,
			boolean saveAs) {
		SelectNameLocationDialog dialog = new SelectNameLocationDialog(
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
		dialog.controller = controller;
		dialog.saveAs = saveAs;
		dialog.open();
	}

	protected SelectNameLocationDialog(Shell parentShell) {
		super(parentShell);
	}

	private void updateLinkEditorController() {
		controller.updateDataFromSelectNameLocationDialog(project,
				name_text.getText());
	}

	private boolean isValidData() {
		boolean projectRes = (project != null);
		boolean nameRes = !name_text.getText().equals("");

		if (!projectRes && !nameRes) {
			setErrorMessage("Please select a project to save in and enter a title for the new object!");
		} else if (!projectRes) {
			setErrorMessage("Please select a project to save in!");
		} else {
			setErrorMessage("Please  enter a title for the new object!");
		}

		return projectRes && nameRes;
	}

	public TextGridProject getSelectedProject() {
		return project;
	}

	public String getEnteredTitle() {
		return name_text.getText();
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite control = new Composite(parent, SWT.NONE);
		control.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		control.setLayout(new GridLayout(1, false));

		getShell().setText("Save changes");

		setTitle("Save Text-Image-Link Object");

		Label intro = new Label(control, SWT.WRAP);
		intro.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		intro.setText("To save the changes in a new Object, please select a project to save in and enter a title for the new object.");

		createProjectGroup(control);
		createNameGroup(control);

		createButtonBar(control);
		createButtons = false;

		if (!saveAs)
			createSaveLocalGroup(control);

		// createButtons(control);

		return control;
	}

	private boolean createButtons = true;

	@Override
	protected Control createButtonBar(Composite parent) {
		if (createButtons)
			return super.createButtonBar(parent);

		return null;
	}

	@Override
	protected void okPressed() {
		if (isValidData()) {
			updateLinkEditorController();
			super.okPressed();
		}
	}

	@Override
	protected void cancelPressed() {
		project = null;
		updateLinkEditorController();
		super.cancelPressed();
	}

	private void cancelPressed2() {
		super.cancelPressed();
	}

	private void createNameGroup(Composite control) {
		Group nameGroup = new Group(control, SWT.SHADOW_ETCHED_IN);
		nameGroup.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		nameGroup.setLayout(new GridLayout(1, true));
		nameGroup.setText("Title");
		name_text = new Text(nameGroup, SWT.BORDER);
		name_text
				.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
	}

	private void createSaveLocalGroup(Composite control) {
		Group saveLocalGroup = new Group(control, SWT.SHADOW_ETCHED_IN);
		saveLocalGroup.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true,
				false));
		saveLocalGroup.setLayout(new GridLayout(1, true));

		saveLocalGroup.setText("Or save as local file:");

		Button saveLocal = new Button(saveLocalGroup, SWT.BORDER);
		saveLocal.setText("Save as local file");
		saveLocal
				.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		saveLocal.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				LinkEditorController.getInstance().saveLocalAnnotatedFile(
						saveAs);
				cancelPressed2();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});
	}

	private void createProjectGroup(Composite control) {
		Group projectGroup = new Group(control, SWT.SHADOW_ETCHED_IN);
		projectGroup
				.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		projectGroup.setLayout(new GridLayout(1, true));

		List list = new List(projectGroup, SWT.SINGLE | SWT.V_SCROLL
				| SWT.H_SCROLL | SWT.VIRTUAL);
		GridData projectTreeData = new GridData(SWT.FILL, SWT.FILL, true, true);
		projectTreeData.heightHint = 200;
		list.setLayoutData(projectTreeData);

		projectDescLabel = new Label(projectGroup, SWT.WRAP);
		GridData projectDescData = new GridData(SWT.FILL, SWT.FILL, true, false);
		projectDescData.heightHint = 48;
		projectDescLabel.setLayoutData(projectDescData);

		projectViewer = new ListViewer(list);
		projectViewer.setLabelProvider(new PendingLabelProvider());

		final UpdatingDeferredListContentProvider contentProvider = new UpdatingDeferredListContentProvider();
		projectViewer.setContentProvider(contentProvider);

		projectViewer.setInput(TextGridProjectRoot
				.getInstance(TextGridProjectRoot.LEVELS.EDITOR));
		projectViewer.addSelectionChangedListener(this);

		if (currentProject != null) {
			contentProvider.addDoneListener(new IDoneListener() {
				@Override
				public void loadDone(Viewer viewer) {
					projectViewer.setSelection(new StructuredSelection(
							currentProject), true);
					contentProvider.removeDoneListener(this);
				}
			});
		}
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		if (event.getSelectionProvider() == projectViewer) {
			Object selectedObject = null;
			if (event.getSelection() instanceof IStructuredSelection) {
				selectedObject = ((IStructuredSelection) event.getSelection())
						.getFirstElement();
			}
			if (selectedObject instanceof TextGridProject) {
				project = (TextGridProject) selectedObject;
				projectDescLabel.setText(NLS.bind("{0} ({1})",
						project.getDescription(), project.getId()));
			} else {
				project = null;
				projectDescLabel.setText("");
			}
		}
	}
}