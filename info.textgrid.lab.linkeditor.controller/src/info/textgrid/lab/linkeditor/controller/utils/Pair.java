/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.controller.utils;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 * @param <Value1>
 * @param <Value2>
 */
public class Pair<Value1, Value2> {

	private Value1 value1 = null;
	private Value2 value2 = null;

	public Pair(Value1 key, Value2 value) {
		this.value1 = key;
		this.value2 = value;
	}

	public Value1 getValue1() {
		return value1;
	}

	public Value2 getValue2() {
		return value2;
	}

	public void setValue1(Value1 value1) {
		this.value1 = value1;
	}

	public void setValue2(Value2 value2) {
		this.value2 = value2;
	}

	public static void main(String[] args) {
		Pair<String, String> p = new Pair<String, String>("a", "b");
		System.err.println(p.getValue1() + "  " + p.getValue2());
	}
}