/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.controller.utils;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.util.StringToOM;
import info.textgrid.lab.linkeditor.controller.Activator;
import info.textgrid.middleware.confclient.ConfservClientConstants;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.OMElement;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

public class RefersToClient {

	private static RefersToClient instance = null;

	public static RefersToClient getInstance() {
		if (instance == null) {
			instance = new RefersToClient();
		}
		return instance;
	}

	private RefersToClient() {
	}

	public ArrayList<String> getRelationURIs(TextGridObject object) {

		String md = "uri:" + object.getURI().toString();
		HttpMethod method = null;

		try {
			String md2 = URLEncoder.encode(md, "UTF-8");
			String sid = RBACSession.getInstance().getSID(false);
			String url = ConfClient.getInstance().getValue(
					ConfservClientConstants.TG_SEARCH)
					+ "/meta?sid=" + sid + "&md=" + md2 + "&rel=get:refersTo";

			HttpClient client = new HttpClient();
			method = new GetMethod(url);

			// Execute the method.
			int statusCode = client.executeMethod(method);

			if (statusCode != HttpStatus.SC_OK) {
				IStatus status = new Status(IStatus.INFO, Activator.PLUGIN_ID,
						"Couldn't get the refersTo-relations for " + md);
				Activator.getDefault().getLog().log(status);
				return null;
			}

			// Read the response body.
			String responseBody = method.getResponseBodyAsString();
			if (responseBody == null || responseBody.isEmpty())
				return null;

			OMElement elem = StringToOM.getOMElement(responseBody);

			Iterator<OMElement> iter = elem.getChildrenWithName(new QName(
					"relation"));

			ArrayList<String> uris = new ArrayList<String>();
			String uri = null;

			while (iter.hasNext()) {
				uri = (iter.next()).getAttributeValue(new QName("uri"));
				if (uri != null)
					uris.add(uri);
			}

			return uris;

		} catch (HttpException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					e.getMessage());
			Activator.getDefault().getLog().log(status);
		} catch (IOException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					e.getMessage());
			Activator.getDefault().getLog().log(status);
		} catch (XMLStreamException e) {
			IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					e.getMessage());
			Activator.getDefault().getLog().log(status);
		} finally {
			// Release the connection.
			method.releaseConnection();
		}

		return null;
	}
}