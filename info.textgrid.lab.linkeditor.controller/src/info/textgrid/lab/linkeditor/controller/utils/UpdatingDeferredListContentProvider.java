/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.controller.utils;

import info.textgrid.lab.core.model.IChildListChangedListener;
import info.textgrid.lab.core.model.IChildListParent;
import info.textgrid.lab.core.swtutils.DeferredListContentProvider;

import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * A DeferredListContentProvider that notifies its viewer when its input's child
 * list has changed, provided the input is a {@link IChildListParent}.
 * 
 * @author tv
 */
public class UpdatingDeferredListContentProvider extends
		DeferredListContentProvider implements IChildListChangedListener,
		IContentProvider {

	public UpdatingDeferredListContentProvider() {
		super();
	}

	/**
	 * @see DeferredListContentProvider#DeferredListContentProvider(Class)
	 */
	public UpdatingDeferredListContentProvider(Class<?> targetClass) {
		super(targetClass);
	}

	IChildListParent input = null;
	Viewer viewer = null;

	@Override
	public void dispose() {
		if (input != null)
			input.removeChildListChangedListener(this);
		super.dispose();
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		super.inputChanged(viewer, oldInput, newInput);
		this.viewer = viewer;
		if (newInput != input) {
			if (input != null)
				input.removeChildListChangedListener(this);
			if (newInput instanceof IChildListParent)
				input = (IChildListParent) newInput;
			if (input != null)
				input.addChildListChangedListener(this);
		}
	}

	@Override
	public void childListChanged(IChildListParent parent) {
		viewer.setInput(input);
	}
}