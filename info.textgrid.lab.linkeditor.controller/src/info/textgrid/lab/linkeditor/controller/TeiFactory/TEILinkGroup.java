/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.controller.TeiFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

/**
 * This class represents the tei:linkGrp element. Example:
 * 
 * <linkGrp type="text-image-links" xml:id="text-image-links-2"
 * facs="#textgrid-layer2"> <link
 * targets="#rect_2 textgrid:/dok1.xml#anchor_3 textgrid:/dok1.xml#anchor_4"/>
 * </linkGrp>
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class TEILinkGroup implements IElement {

	private String id;

	private String type = "text-image-links";
	private SVGGroup svgGrp;
	private List<TEILink> links;

	public TEILinkGroup(String id) {
		this.id = id;
		links = Collections.synchronizedList(new ArrayList<TEILink>());
	}

	public TEILinkGroup(String id, String type) {
		this(id);
		this.type = type;
	}

	public TEILinkGroup(String id, String type, SVGGroup grp) {
		this(id, type);
		this.svgGrp = grp;
	}

	public TEILinkGroup(String id, SVGGroup grp) {
		this(id);
		this.svgGrp = grp;
	}

	public String getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public SVGGroup getSvgGrp() {
		return svgGrp;
	}

	public void setSvgGrp(SVGGroup svgGrp) {
		this.svgGrp = svgGrp;
	}

	public List<TEILink> getLinks() {
		return links;
	}

	public void setLinks(List<TEILink> links) {
		this.links = links;
	}

	public boolean addLink(TEILink link) {
		return links.add(link);
	}

	public boolean removeLink(TEILink link) {
		return links.remove(link);
	}

	@Override
	public OMElement createOMElement(OMFactory factory, OMElement parent,
			OMNamespace ns) {

		OMElement linkGrp = factory.createOMElement("linkGrp", ns, parent);

		linkGrp.addAttribute("id", this.id, parent.findNamespaceURI("xml"));
		linkGrp.addAttribute("type", this.type, null);
		linkGrp.addAttribute("facs", "#" + this.svgGrp.getId(), null);

		for (TEILink l : links)
			l.createOMElement(factory, linkGrp, ns);

		return linkGrp;
	}

}