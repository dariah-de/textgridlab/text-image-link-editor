/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.controller.TeiFactory;

import info.textgrid.lab.linkeditor.model.graphics.TGShape.WRITING_MODE;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

/**
 * A class to represent the svg:text element.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class SVGText implements IElement, ISVGElement {
	private String id;
	private WRITING_MODE writing_mode = WRITING_MODE.NONE;

	public SVGText(String id, WRITING_MODE writing_mode) {
		this.id = id;
		this.writing_mode = writing_mode;
	}

	public String getId() {
		return id;
	}

	public WRITING_MODE getWriting_mode() {
		return writing_mode;
	}

	@Override
	public OMElement createOMElement(OMFactory factory, OMElement parent,
			OMNamespace ns) {

		OMElement textElem = factory.createOMElement("text", ns, parent);
		textElem.addAttribute("id", this.id, null);

		if (writing_mode != null && writing_mode != WRITING_MODE.NONE)
			textElem.addAttribute("writing-mode", this.writing_mode.toString(),
					null);

		return textElem;
	}
}