/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.controller.TeiFactory;

import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.xpath.AXIOMXPath;
import org.jaxen.JaxenException;

/**
 * This is the class which can be used as a base class for utilities to work
 * with OMItems.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class OMUtil {

	/**
	 * Searches for a certain element in an OMElement and returns it if found
	 * otherwise returns null.
	 * 
	 * @param elementName
	 *            the name of the searched element
	 * @param root
	 *            where to search
	 * @param namespace
	 *            the the namespace of the searched element
	 * @param prefix
	 *            the prefix of the namespace
	 * 
	 * @return OMElement the found element as OMElement or null.
	 * 
	 * @throws JaxenException
	 */
	public static OMElement getElementWithName(String elementName,
			OMElement root, String namespace, String prefix)
			throws JaxenException {

		AXIOMXPath xpath = new AXIOMXPath("child::" + prefix + ":"
				+ elementName);
		xpath.addNamespace(prefix, namespace);
		OMElement elem = (OMElement) xpath.selectSingleNode(root);
		return elem;
	}

	/**
	 * Searches for a certain element in an OMElement and returns a list of
	 * elements if found otherwise returns null.
	 * 
	 * @param elementName
	 *            the name of the searched element
	 * @param root
	 *            where to search
	 * @param namespace
	 *            the the namespace of the searched element
	 * @param prefix
	 *            the prefix of the namespace
	 * 
	 * @return List a list of the elements or null
	 * 
	 * @throws JaxenException
	 */
	@SuppressWarnings("unchecked")
	public static List getElementListWithName(String elementName,
			OMElement root, String namespace, String prefix)
			throws JaxenException {

		AXIOMXPath xpath = new AXIOMXPath("descendant::" + prefix + ":"
				+ elementName);
		xpath.addNamespace(prefix, namespace);
		List elems = xpath.selectNodes(root);

		return elems;
	}

}