/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.controller.TeiFactory;

import info.textgrid.lab.linkeditor.model.graphics.TGShape;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

/**
 * A class to represent the svg:rect|polygon|line element.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class SVGShape implements IElement, ISVGElement {
	private String id;
	private TGShape shape;
	private String transform;

	private int imageWidth;
	private int imageHeight;

	private static String COMMA = ",";
	private static String HASHKEY = "#";

	public SVGShape(String id, TGShape shape, int imageWidth, int imageHeight) {
		this.id = id;
		this.shape = shape;
		this.imageWidth = imageWidth;
		this.imageHeight = imageHeight;
	}

	public String getTransform() {
		return transform;
	}

	public void setTransform(String transform) {
		this.transform = transform;
	}

	public String getId() {
		return id;
	}

	public TGShape getShape() {
		return shape;
	}

	public int getImageWidth() {
		return imageWidth;
	}

	public void setImageWidth(int imageWidth) {
		this.imageWidth = imageWidth;
	}

	public int getImageHeight() {
		return imageHeight;
	}

	public void setImageHeight(int imageHeight) {
		this.imageHeight = imageHeight;
	}

	@Override
	public OMElement createOMElement(OMFactory factory, OMElement parent,
			OMNamespace ns) {
		OMElement shapeElem = this.shape.createOMElement(factory, parent, ns,
				imageWidth, imageHeight);
		shapeElem.addAttribute("id", this.id, null);
		shapeElem.addAttribute("class", setTGLayerClassAttributeString(), null);

		return shapeElem;
	}

	/**
	 * create the OMElement String for layer information
	 * 
	 * @return
	 */
	private String setTGLayerClassAttributeString() {
		return this.shape.getLayer() + COMMA + this.shape.getLayerName()
				+ COMMA + this.shape.getLayerRGB().red + HASHKEY
				+ this.shape.getLayerRGB().green + HASHKEY
				+ this.shape.getLayerRGB().blue;
	}
}