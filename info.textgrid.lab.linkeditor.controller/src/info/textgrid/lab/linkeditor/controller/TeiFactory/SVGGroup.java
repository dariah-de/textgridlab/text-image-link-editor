/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.controller.TeiFactory;

import info.textgrid.lab.linkeditor.model.graphics.TGShape.WRITING_MODE;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

/**
 * This class is a representation for the svg:g element.
 * 
 * Example:
 * 
 * <svg:g id="textgrid-layer2"> <svg:image width="1024" height="768"
 * xlink:href="..."/> <svg:rect width="10" height="10" x="" y="" id="rect_2"/>
 * </svg:g>
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class SVGGroup implements IElement {

	private String id;
	private WRITING_MODE writingMode;
	private SVGImage image;

	private List<SVGShape> shapes;
	private List<SVGText> texts;

	public SVGGroup(String id, WRITING_MODE writingMode) {
		this.id = id;
		shapes = Collections.synchronizedList(new ArrayList<SVGShape>());
		texts = Collections.synchronizedList(new ArrayList<SVGText>());
		this.writingMode = writingMode;
	}

	public String getId() {
		return id;
	}

	public SVGImage getImage() {
		return image;
	}

	public void setImage(SVGImage image) {
		this.image = image;
	}

	public List<SVGShape> getShapes() {
		return shapes;
	}

	public void setShapes(ArrayList<SVGShape> shapes) {
		this.shapes = shapes;
	}

	public boolean addShape(SVGShape shape) {
		return this.shapes.add(shape);
	}

	public boolean removeShape(SVGShape shape) {
		return this.shapes.remove(shape);
	}

	public List<SVGText> getTexts() {
		return texts;
	}

	public void setTexts(ArrayList<SVGText> texts) {
		this.texts = texts;
	}

	public boolean addText(SVGText text) {
		return this.texts.add(text);
	}

	public boolean removeText(SVGText text) {
		return this.texts.remove(text);
	}

	public WRITING_MODE getWritingMode() {
		return writingMode;
	}

	public void setWritingMode(WRITING_MODE writingMode) {
		this.writingMode = writingMode;
	}

	public SVGShape getSVGShapeById(String id) {
		for (SVGShape shape : shapes) {
			if (shape.getId().equals(id))
				return shape;
		}

		return null;
	}

	public SVGText getSVGTextById(String id) {
		for (SVGText text : texts) {
			if (text.getId().equals(id))
				return text;
		}

		return null;
	}

	@Override
	public OMElement createOMElement(OMFactory factory, OMElement parent,
			OMNamespace ns) {

		OMElement gElem = factory.createOMElement("g", ns, parent);
		gElem.addAttribute("id", this.id, null);
		if (writingMode != null && writingMode != WRITING_MODE.NONE)
			gElem.addAttribute("writing-mode", this.writingMode.toString(),
					null);

		if (image != null)
			image.createOMElement(factory, gElem, ns);

		for (SVGShape s : shapes)
			s.createOMElement(factory, gElem, ns);

		for (SVGText t : texts)
			t.createOMElement(factory, gElem, ns);

		return gElem;
	}
}