/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.controller.TeiFactory;

import info.textgrid.lab.core.model.util.StringToOM;
import info.textgrid.lab.linkeditor.controller.Activator;
import info.textgrid.lab.linkeditor.controller.LinkEditorController;
import info.textgrid.lab.linkeditor.controller.utils.XMLPrettyPrinter;
import info.textgrid.lab.linkeditor.model.graphics.TGLine;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;
import info.textgrid.lab.linkeditor.model.graphics.TGShape.WRITING_MODE;
import info.textgrid.lab.linkeditor.model.links.ILink;
import info.textgrid.lab.linkeditor.model.links.LinksContainer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.progress.UIJob;

/**
 * A class to handle the tei document for the Text-Image-Link-Editor.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class TEIDocFactory {

	private OMFactory omFactory;

	private OMNamespace teiNS;
	private OMNamespace xmlNS;
	private OMNamespace svgNS;
	private OMNamespace xlinkNS;

	private OMElement tei;
	private OMElement teiHeader;
	private OMElement svg;
	private OMElement ab;

	public static String XML_NS_URI = "http://www.w3.org/XML/1998/namespace";
	public static String TEI_NS_URI = "http://www.tei-c.org/ns/1.0";
	public static String XLINK_NS_URI = "http://www.w3.org/1999/xlink";
	public static String SVG_NS_URI = "http://www.w3.org/2000/svg";

	public static String XML_NS_Prefix = "xml";
	public static String TEI_NS_Prefix = "tei";
	public static String XLINK_NS_Prefix = "xlink";
	public static String SVG_NS_Prefix = "svg";

	private LinksContainer links = null;

	private boolean inBackground = true;

	private List<SVGGroup> svgGroups = Collections
			.synchronizedList(new ArrayList<SVGGroup>());
	private List<TEILinkGroup> teiLinkGroups = Collections
			.synchronizedList(new ArrayList<TEILinkGroup>());

	private List<TGLine> dockingLines = null;

	public TEIDocFactory(LinksContainer links, boolean inBackground) {
		this.links = links;
		this.inBackground = inBackground;
		omFactory = OMAbstractFactory.getOMFactory();
	}

	public OMElement createTeiDocument(final String parentPath,
			IProgressMonitor monitor) throws UnsupportedEncodingException,
			XMLStreamException, FactoryConfigurationError {

		if (monitor == null)
			monitor = new NullProgressMonitor();

		teiNS = omFactory.createOMNamespace(TEI_NS_URI, TEI_NS_Prefix);

		tei = omFactory.createOMElement("TEI", teiNS);

		xmlNS = omFactory.createOMNamespace(XML_NS_URI, XML_NS_Prefix);
		svgNS = omFactory.createOMNamespace(SVG_NS_URI, SVG_NS_Prefix);
		xlinkNS = omFactory.createOMNamespace(XLINK_NS_URI, XLINK_NS_Prefix);

		tei.declareNamespace(xmlNS);
		tei.declareNamespace(svgNS);
		tei.declareNamespace(xlinkNS);

		monitor.worked(10);

		// teiHeader
		String header_text = "<teiHeader><fileDesc><titleStmt><title>Text-Image-Link Object</title></titleStmt><publicationStmt><p/></publicationStmt><sourceDesc><p/></sourceDesc></fileDesc></teiHeader>";

		teiHeader = StringToOM.getOMElement(header_text);

		setNamespaceRecursive(teiHeader, teiNS);

		tei.addChild(teiHeader);

		// facsimile
		OMElement facsimile = omFactory
				.createOMElement("facsimile", teiNS, tei);
		svg = omFactory.createOMElement("svg", svgNS, facsimile);

		// text
		OMElement text = omFactory.createOMElement("text", teiNS, tei);
		OMElement body = omFactory.createOMElement("body", teiNS, text);
		ab = omFactory.createOMElement("ab", teiNS, body);

		monitor.worked(10);

		addLinksToDocument(parentPath);

		monitor.worked(10);

		return tei;
	}

	public void addDockingLines(List<TGLine> dockingLines2) {
		this.dockingLines = dockingLines2;
	}

	public void replaceUris(Map<String, String> urisMap) {
		String teiString = tei.toString();

		for (String first : urisMap.keySet()) {
			teiString = teiString.replace(first, urisMap.get(first));
		}

		try {
			tei = StringToOM.getOMElement(teiString);
		} catch (UnsupportedEncodingException e) {
			Activator.handleError(e);
		} catch (XMLStreamException e) {
			Activator.handleError(e);
		} catch (FactoryConfigurationError e) {
			Activator.handleError(e);
		}
	}

	public boolean makePersistent(IFile file, IProgressMonitor monitor) {
		if (monitor == null)
			monitor = new NullProgressMonitor();

		try {
			monitor.worked(20);

			ByteArrayOutputStream resultStream = new ByteArrayOutputStream();
			XMLPrettyPrinter.prettify(tei, resultStream);

			file.setContents(
					new ByteArrayInputStream(resultStream
							.toString("UTF-8").getBytes("UTF-8")), IResource.FORCE, monitor); //$NON-NLS-1$

			monitor.worked(30);

			return true;
		} catch (Exception e) {
			Activator.handleError(e);
		}
		return false;
	}

	public boolean makePersistent(File file, IProgressMonitor monitor) {
		if (monitor == null)
			monitor = new NullProgressMonitor();

		try {
			FileOutputStream out = null;
			try {
				out = new FileOutputStream(file);
				XMLPrettyPrinter.prettify(tei, out);
			} finally {
				if (out != null) {
					out.close();
				}
			}
			monitor.worked(50);
			return true;
		} catch (Exception e) {
			Activator.handleError(e);
		}
		return false;
	}

	private WRITING_MODE defaultWritingMode = WRITING_MODE.NONE;

	private void addLinksToDocument(final String parentPath) {

		TreeSet<String> imageUris = new TreeSet<String>();
		for (ILink l : links) {
			imageUris.add(l.getImageUri());
		}

		int svgGrpId = 1;
		int teiLinkGrpId = 1;
		int shapeId = 1;
		int textId = 1;
		int dockLineId = 1;

		for (final String uri : imageUris) {
			Point imageWH = LinkEditorController.getInstance()
					.getImageWidthHeight(uri);

			if (inBackground) {
				UIJob j1 = new UIJob("") {
					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						defaultWritingMode = LinkEditorController.getInstance()
								.getDefaultWritingMode(uri);
						return Status.OK_STATUS;
					}
				};
				j1.schedule();

				try {
					j1.join();
				} catch (InterruptedException e) {
					Activator.handleWarning(e);
				}
			} else {
				defaultWritingMode = LinkEditorController.getInstance()
						.getDefaultWritingMode(uri);
			}

			SVGGroup svgGrp = new SVGGroup("textgrid-layer-" + svgGrpId++,
					defaultWritingMode);
			svgGrp.setImage(new SVGImage(getRelativeUri(uri, parentPath),
					imageWH.x, imageWH.y));

			TEILinkGroup teiLinkGrp = new TEILinkGroup("text-image-links-"
					+ teiLinkGrpId++);

			for (ILink l : links) {
				if (l.getImageUri().equals(uri)) {
					String _shapeId = "shape-" + shapeId++;
					String _textId = "text-" + textId++;
					TGShape shape = l.getShape();
					svgGrp.addShape(new SVGShape(_shapeId, shape, imageWH.x,
							imageWH.y));
					svgGrp.addText(new SVGText(_textId, shape.getWritingMode()));
					// TODO baseLine
					teiLinkGrp.addLink(new TEILink(_shapeId, l, parentPath));
					teiLinkGrp.addLink(new TEILink(_textId, l, parentPath));
				}
			}

			if (dockingLines != null && !dockingLines.isEmpty()) {

				for (TGLine l : dockingLines) {
					if (l.getImageUri().equals(uri)) {
						svgGrp.addShape(new SVGShape(
								"dockline-" + dockLineId++, l, imageWH.x,
								imageWH.y));

					}
				}
			}

			teiLinkGrp.setSvgGrp(svgGrp);

			svgGroups.add(svgGrp);
			teiLinkGroups.add(teiLinkGrp);
		}

		for (SVGGroup g : svgGroups) {
			g.createOMElement(omFactory, svg, svgNS);
		}

		for (TEILinkGroup g : teiLinkGroups) {
			g.createOMElement(omFactory, ab, teiNS);
		}
	}

	public static String getRelativeUri(final String uri,
			final String parentPath) {

		if (parentPath == null || !uri.startsWith("file"))
			return uri;

		String relativeUri = uri;

		String parentUri = new File(parentPath).toURI().toString();

		relativeUri = uri.replace(parentUri, "");

		return relativeUri;
	}

	private void setNamespaceRecursive(OMElement elem, OMNamespace ns) {
		elem.setNamespace(ns);
		Iterator<OMElement> children = elem.getChildElements();

		while (children.hasNext()) {
			OMElement e = children.next();
			e.setNamespace(ns);
			setNamespaceRecursive(e, ns);
		}
	}

	public static void main(String[] args) {

		try {
			TEIDocFactory f = new TEIDocFactory(new LinksContainer(), false);
			OMElement tei = f.createTeiDocument("Desktop", null);

			XMLPrettyPrinter.prettify(tei, System.out);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
