/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.controller.TeiFactory;

import info.textgrid.lab.linkeditor.model.links.ILink;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.eclipse.osgi.util.NLS;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class TEILink implements IElement {
	private String refId;
	private ILink link;
	private String parentPath = null;

	public TEILink(String refId, ILink link) {
		this.refId = refId;
		this.link = link;
	}

	public TEILink(String refId, ILink link, final String parentPath) {
		this.refId = refId;
		this.link = link;
		this.parentPath = parentPath;
	}

	public ILink getLink() {
		return link;
	}

	@Override
	public OMElement createOMElement(OMFactory factory, OMElement parent,
			OMNamespace ns) {

		OMElement linkElem = factory.createOMElement("link", ns, parent);

		if (!"".equals(link.getEndAnchorId()))
			linkElem.addAttribute(
					"targets",
					NLS.bind(
							"#{0} {1}#{2} {1}#{3}",
							new Object[] {
									this.refId,
									TEIDocFactory.getRelativeUri(
											link.getTextUri(), parentPath),
									link.getStartAnchorId(),
									link.getEndAnchorId() }), null);
		else
			linkElem.addAttribute(
					"targets",
					NLS.bind(
							"#{0} {1}#{2}",
							new Object[] {
									this.refId,
									TEIDocFactory.getRelativeUri(
											link.getTextUri(), parentPath),
									link.getStartAnchorId() }), null);

		return linkElem;
	}

}