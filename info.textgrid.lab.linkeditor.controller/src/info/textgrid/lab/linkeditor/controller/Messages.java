/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.controller;

import org.eclipse.osgi.util.NLS;

/**
 * The Messages class for the graphical link editor controller.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * @author leuk
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.linkeditor.controller.messages"; //$NON-NLS-1$
	public static String LinkEditorController_Mess_AnnotatedObjectSuccessfullySaved_1;
	public static String LinkEditorController_Mess_AnnotatedObjectSuccessfullySaved_2;
	public static String LinkEditorController_Mess_AnnotatedObjectSuccessfullySaved_3;
	public static String LinkEditorController_Mess_ChangedAfterLoading_Object;
	public static String LinkEditorController_Mess_ChangedAfterLoading_Resource;
	public static String LinkEditorController_Mess_ChangesWillSaved;
	public static String LinkEditorController_Mess_CreateLinksFirst;
	public static String LinkEditorController_Mess_DeleteSelectedLink_1;
	public static String LinkEditorController_Mess_DeleteSelectedLink_2;
	public static String LinkEditorController_Mess_EditExistingLink;
	public static String LinkEditorController_Mess_LinkInterferesWithAnother;
	public static String LinkEditorController_Mess_NoMetadataFromTGO;
	public static String LinkEditorController_Mess_SaveCurrentChanges_1;
	public static String LinkEditorController_Mess_SaveCurrentChanges_2;
	public static String LinkEditorController_Mess_UnknownTypeOfText;
	public static String LinkEditorController_Mess_UseProvidedFunctionToDeleteLinks_1;
	public static String LinkEditorController_Mess_UseProvidedFunctionToDeleteLinks_2;
	public static String LinkEditorController_Title_ChangedAfterLoading_Obj;
	public static String LinkEditorController_Title_ChangedAfterLoading_Res;
	public static String LinkEditorController_Title_CreateLinksFirst;
	public static String LinkEditorController_Title_EditExistingLink;
	public static String LinkEditorController_Title_LinkInterferesWithAnother;
	public static String LinkEditorController_Title_OpenAnnotatedObject;
	public static String LinkEditorController_Title_UnknownTypeOfText;
	public static String LinkEditorController_Warning_Mess_1;
	public static String LinkEditorController_Warning_Mess_2;
	public static String LinkEditorController_Mess_NoSelectedShape;
	public static String LinkEditorController_Mess_SelectShapeText;
	public static String LinkEditorController_Mess_OldVersion;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
