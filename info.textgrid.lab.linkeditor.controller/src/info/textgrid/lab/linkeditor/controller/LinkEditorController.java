/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.controller;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.linkeditor.controller.TeiFactory.TEIDocFactory;
import info.textgrid.lab.linkeditor.controller.dialogs.SelectLinksobjectDialog;
import info.textgrid.lab.linkeditor.controller.dialogs.SelectNameLocationDialog;
import info.textgrid.lab.linkeditor.controller.utils.Pair;
import info.textgrid.lab.linkeditor.model.graphics.TGLayerManager;
import info.textgrid.lab.linkeditor.model.graphics.TGLine;
import info.textgrid.lab.linkeditor.model.graphics.TGPolygon;
import info.textgrid.lab.linkeditor.model.graphics.TGRectangle;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;
import info.textgrid.lab.linkeditor.model.graphics.TGShape.TYPE;
import info.textgrid.lab.linkeditor.model.graphics.TGShape.WRITING_MODE;
import info.textgrid.lab.linkeditor.model.links.ILink;
import info.textgrid.lab.linkeditor.model.links.LinksContainer;
import info.textgrid.lab.linkeditor.model.links.PolyLink;
import info.textgrid.lab.linkeditor.model.links.RectLink;
import info.textgrid.lab.newsearch.SearchRequest;
import info.textgrid.lab.ui.core.locking.LockingService;
import info.textgrid.lab.ui.core.menus.OpenObjectService;
import info.textgrid.namespaces.metadata.agent._2010.PersonType;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.FontDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IEditorRegistry;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IPerspectiveListener3;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.internal.Workbench;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.services.IEvaluationService;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.wst.sse.core.StructuredModelManager;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.sse.core.internal.provisional.IndexedRegion;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocument;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocumentRegion;
import org.eclipse.wst.sse.ui.StructuredTextEditor;
import org.eclipse.wst.sse.ui.internal.StructuredTextViewer;
import org.eclipse.wst.xml.core.internal.document.ElementImpl;
import org.jaxen.JaxenException;
import org.xml.sax.SAXException;

import com.google.common.collect.HashBiMap;

/**
 * The controller class for the graphical link editor.
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * @author leuk
 */
//TODO: Locking for TG-Linkeditor-Files
// Should be set if the file is opened. (LockingService.getInstance().lock(object)) -> is set on 
// Should be unset if the file is closed. (LockingService.getInstance().unlock(object))
// 

@SuppressWarnings("restriction")
public class LinkEditorController implements ITextGridObjectListener,
		IWorkbenchListener, ISelectionListener, IPartListener2,
		IPerspectiveListener3, ISelectionChangedListener, IDocumentListener,
		MouseListener {

	public static String LINK_EDITOR_TYPE = "text/linkeditorlinkedfile"; //$NON-NLS-1$

	public static String ANNOTATION_TYPE_SELECTED = "info.textgrid.lab.linkeditor.annotations.selected";
	public static String ANNOTATION_TYPE_UNSELECTED = "info.textgrid.lab.linkeditor.annotations.unselected";
	public static String XML_NS = "http://www.w3.org/XML/1998/namespace";

	private TextGridObject loaded_object = null;
	private TextGridObject selected_object = null;
	private String imageUri = ""; //$NON-NLS-1$
	private String textUri = "None"; //$NON-NLS-1$
	private TextGridProject selected_project = null;
	private String entered_title = ""; //$NON-NLS-1$
	private boolean changed_after_loading = false;

	private MessageBox msg_asking = null;
	private MessageBox msg_asking_2 = null;
	private MessageBox msg_info = null;

	private LinksContainer all_links = null;

	private List<ILinkEditorListener> listeners = null;
	private static LinkEditorController instance = null;

	private TGShape selectedShape = null;
	private List<TGShape> selectedShapes = Collections
			.synchronizedList(new ArrayList<TGShape>());

	private boolean showWarnings = true;

	private ITextEditor editor = null;
	private StyledText text = null;
	private IDocument doc = null;

	private IAnnotationModel annoModel = null;

	private String selectedText = null;
	private int selectedTextStart = -1;
	private int selectedTextLength = 0;

	public static String IMAGE_VIEW_ID = "edu.uky.mip.views.ImageView";

	private int openTabs = 1;
	private String focusedImageTabSecondaryId = "1";
	private boolean openInNewTab = false;

	private boolean openToolkit = true;

	private double lastSelectedDockingLineAngle = 0;
	private final static String defaultTGLayer = "0";
	private String currentLayer = "0";
	// Maximum number of managed Layer.
	private final static int maxTGLayerNumber = 5;

	private HashBiMap<String, TGLayerManager> allLayerManager = HashBiMap
			.create();

	private final Map<String, String> imageTabsMap = Collections
			.synchronizedMap(new HashMap<String, String>());

	private final Map<String, Point> imageInfoMap = Collections
			.synchronizedMap(new HashMap<String, Point>());

	private final Set<String> openXmlUris = Collections
			.synchronizedSet(new HashSet<String>());

	private final Set<String> unsegmentedOpenXmlUris = Collections
			.synchronizedSet(new HashSet<String>());

	private final Map<String, String> replacedRevisions = Collections
			.synchronizedMap(new HashMap<String, String>());

	/**
	 * The link editor events
	 */
	public enum Event {
		LOAD_TEXT, // load the text in the text view
		LOAD_IMAGE, // load the image in the image view
		ADD_LINK, // add a link
		ADD_LINK_LOADING, // add a link while loading
		JUMP_TO_SHAPE, // jump to the shape(rectangle, polygon) of the link
		LINKS_CHANGED, // links have been changed
		REMOVE_SHAPE, // remove a shape from the image view
		SET_TITLE_LABEL, // set the title of the loaded annotated/image
		// object
		// in the image view
		SET_TYPE_LABEL, // set the type of the loaded annotated/image object
		// in
		// the image view
		SET_COUNT, // set the Count of the unsavedRectangles object in the
		// image view
		SET_ANGLE, // set the Angle of DockingLine- object in the
		// image view
		SET_LAYER, // set the Layer of Image- object in the
		// image view
		SET_IMAGEVIEW_INVISIBLE, // a new annotated object was created
		// UPDATE_TEXT, // update the text in the controller
		UPDATE_IMAGE_IN_CONTROLLER, // update the image in the controller
		// REVERT_TEXT, // revert the changes in the text (for modifyLinks())
		CHANGES_SAVED, // changes saved in the annotated object
		FINALIZE_LOADING, // send an event to the image view to finalize the
		// loading of annotated objects.
		SET_DEFAULT_WRITING_MODE, // set the default writing_mode
		GET_DEFAULT_WRITING_MODE, // get the default writing_mode
		SET_VISIBILITY_OF_CREATELINK, // set the Visibility of the
		// addLink-Action
		ADD_DOCKING_LINE, // add a dockingLine
		UPDATE_IMAGE_VIEW_ON_CHANGES, // force updating the image view
		REMOVE_ALL_SHAPES, // remove all shapes on the image view
		UPDATE_SHAPES_LIST, // update the list of shapes
		ADD_SHAPE, // add a shape
		SELECT_SHAPE, // select a shape
		OPEN_TOOLKIT, // open the toolkit dialog
		CLOSE_TOOLKIT, // close the toolkit dialog
		SET_SELECTED_OBJECT, // set the selected object links/image in the
		// imageView (For the selectionProvider)
		SET_IMAGE_URI, // set the the imageUri in the imageView
		UPDATE_LOADED_IMAGE, // Doesn't load a new image. just update it.
		SET_LINKS_OBJECT_LABEL, // set the name of the Text-Image-Link Object in
								// the label
		// of the image view
		DESELECT_SHAPE, // the opposite of SELECT_SHAPE
		REDRAW, // just redraw the image view
		REMOVE_SELECTED_SHAPES, // remove the selected shape
	}

	/**
	 * Get a singleton instance of this controller
	 * 
	 * @return instance
	 */
	public static synchronized LinkEditorController getInstance() {
		if (instance == null) {
			instance = new LinkEditorController();
		}
		return instance;
	}

	/**
	 * Private basic constructor
	 */
	private LinkEditorController() {
		all_links = new LinksContainer();
		listeners = Collections
				.synchronizedList(new ArrayList<ILinkEditorListener>());
		msg_asking = new MessageBox(PlatformUI.getWorkbench().getDisplay()
				.getActiveShell(), SWT.ICON_QUESTION | SWT.YES | SWT.NO
				| SWT.CANCEL);
		msg_asking_2 = new MessageBox(PlatformUI.getWorkbench().getDisplay()
				.getActiveShell(), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
		msg_info = new MessageBox(PlatformUI.getWorkbench().getDisplay()
				.getActiveShell(), SWT.ICON_INFORMATION | SWT.OK);

		TextGridObject.addListener(this);

		Workbench.getInstance().getActiveWorkbenchWindow()
				.getSelectionService().addSelectionListener(this);

		Workbench.getInstance().getActiveWorkbenchWindow().getActivePage()
				.addPartListener(this);

		Workbench.getInstance().getActiveWorkbenchWindow()
				.addPerspectiveListener(this);

		PlatformUI.getWorkbench().addWorkbenchListener(this);
	}

	/**
	 * Remove the selected selection on the active image view
	 */
	public void removeSelectedSelection() {
		notifyListeners(Event.REMOVE_SELECTED_SHAPES, new Pair<String, Object>(
				focusedImageTabSecondaryId, null));
	}

	/**
	 * Add link to the links container
	 * 
	 * @param link
	 */
	void addLink(ILink link) {
		all_links.add(link);
	}

	/**
	 * Add a TG-resource to the link-Editor. To be used in the parser
	 * 
	 * @param object
	 * @throws IOException
	 */
	void addResourceToLinkEditor(final String uri_str, IProgressMonitor monitor)
			throws IOException {

		if (monitor == null)
			monitor = new NullProgressMonitor();

		try {
			final URI uri = new URI(uri_str);

			if (uri_str.startsWith("file")) {

				if (uri_str.endsWith(".xml"))
					openLocalXMLFile(getPathFromUri(uri));
				else
					openLocalImageFile(getPathFromUri(uri));

				return;
			}
			TextGridObject object = TextGridObject.getInstance(uri, false);

			if (object == null) {
				throw new IOException(
						"Couldn't fetch the object using the URI: " + uri_str);
			}

			if (!object.isAccessible()) {
				throw new IOException(
						"Access denied: The fetched object ("
								+ uri_str
								+ ") is not accessable!\n"
								+ "To open this object you should have permission to read all used "
								+ "xml- and image objects.");
			}

			// open the object in a new tab
			openInNewTab = true;

			if (imageTabsMap.isEmpty())
				openTabs = 0;

			if (object.getContentTypeID().contains("image")) {
				openToolkit = false;

				new UIJob("Opening a new image view tab...") {

					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {

						IWorkbenchPage page = Workbench.getInstance()
								.getActiveWorkbenchWindow().getActivePage();
						try {
							page.showView(IMAGE_VIEW_ID,
									String.valueOf(++openTabs),
									IWorkbenchPage.VIEW_ACTIVATE);
							return Status.OK_STATUS;
						} catch (PartInitException e) {
							Activator.handleError(e);
						}
						return Status.CANCEL_STATUS;
					}
				}.schedule();
			}

			readAddedTGObjectWaiting(object, monitor);

			if (!openInNewTab)
				selected_object = null;

			openToolkit = true;
			openInNewTab = false;

		} catch (CoreException e) {
			Activator.handleError(e);
		} catch (URISyntaxException e) {
			Activator.handleError(e);
		}
	}

	private Job readingTGOJob_addObject = null;

	/**
	 * Adds the referred object (image/xml) to the link editor
	 * 
	 * @param object
	 *            {@link TextGridObject}
	 * 
	 * @param doubleClicked
	 *            Was this object double clicked in the navigator ?
	 * @param openInTab
	 *            Should this object be opened in a new tab ?
	 */
	public void addObjectToLinkEditor(final TextGridObject object,
			final boolean doubleClicked, final boolean openInTab) {

		try {
			if (doubleClicked
					&& SelectLinksobjectDialog.openDialog(
							LinkEditorController.getInstance(), object)) {
				if (selected_object_dialog != null)
					openAnnotatedObject(selected_object_dialog);
				else
					addObjectToLinkEditor(object, false, openInTab);
			} else {
				openInNewTab = false;
				// open the object in a new tab
				if (openInTab) {
					openInNewTab = true;

					if (imageTabsMap.isEmpty())
						openTabs = 0;

					if (object.getContentTypeID().contains("image")) {

						String uri_str = object.getURI().toString();
						try {
							// just set focus, if this image
							// object is
							// open
							for (Map.Entry<String, String> entry : imageTabsMap
									.entrySet()) {
								if (entry.getValue().equals(uri_str)) {
									String key = entry.getKey();
									IWorkbenchPage page = Workbench
											.getInstance()
											.getActiveWorkbenchWindow()
											.getActivePage();
									page.showView(IMAGE_VIEW_ID, key,
											IWorkbenchPage.VIEW_ACTIVATE);
									// imageFound = true;

									openInNewTab = false;
									return;
								}
							}

							openToolkit = false;
							IWorkbenchPage page = Workbench.getInstance()
									.getActiveWorkbenchWindow().getActivePage();
							page.showView(IMAGE_VIEW_ID,
									String.valueOf(++openTabs),
									IWorkbenchPage.VIEW_ACTIVATE);
						} catch (CoreException e) {
							Activator.handleError(e);
							openInNewTab = false;
							return;
						}
					}
				}

				if (readingTGOJob_addObject != null)
					readingTGOJob_addObject.cancel();

				readingTGOJob_addObject = new Job(NLS.bind("Opening {0}.....",
						object.getTitle())) {
					@Override
					protected IStatus run(IProgressMonitor monitor) {

						if (monitor == null)
							monitor = new NullProgressMonitor();

						try {

							monitor.beginTask(
									NLS.bind("Fetching {0}.....",
											object.getTitle()), 100);

							readAddedTGObject(object, monitor);

							if (!openInNewTab)
								selected_object = null;

							openToolkit = true;
							openInNewTab = false;

							monitor.done();

							return Status.OK_STATUS;

						} catch (CoreException e) {
							Activator.handleError(e);
						}

						return Status.CANCEL_STATUS;
					}
				};

				readingTGOJob_addObject.setUser(true);
				readingTGOJob_addObject.schedule();
			}

		} catch (CoreException e1) {
			Activator.handleError(e1);
		}

	}

	/**
	 * Open the referred Text-Image-Link Object in the link editor
	 * 
	 * @param object
	 */
	public void openAnnotatedObject(TextGridObject object) {
		try {
			loaded_object = object;
			readAnnotatedObject();
			doLockTglObject();

		} catch (ParserConfigurationException e) {
			Activator.handleError(e);
		} catch (SAXException e) {
			Activator.handleError(e);
		} catch (IOException e) {
			Activator.handleError(e);
		} catch (CoreException e) {
			Activator.handleError(e);
		} catch (FactoryConfigurationError e) {
			Activator.handleError(e);
		} catch (XMLStreamException e) {
			Activator.handleError(e);
		} catch (URISyntaxException e) {
			Activator.handleError(e);
		}
	}

	/**
	 * 
	 * Read the image's {@link InputStream} from the {@link TextGridObject} and
	 * open it the the image view.
	 * 
	 * @param tgObj
	 * @param input
	 * @param uri
	 * @param title
	 * @param contentTypeId
	 */
	private synchronized void loadTGOImage(TextGridObject tgObj,
			InputStream input, String uri, String title, String contentTypeId) {
		try {
			Image img_TGO = null;
			if ("image/tiff".equals(contentTypeId)
					|| "image/tif".equals(contentTypeId)) {

				// workaround for TG-1651 - tiff images (Compression Scheme
				// Group 4) show blank in the TBLE
				File temp = null;
				OutputStream out = null;

				try {
					temp = File.createTempFile("tble_", ".tiff");

					try {
						out = new FileOutputStream(temp);
						int read = 0;
						byte[] bytes = new byte[1024];
						
						while ((read = input.read(bytes)) != -1) {
							out.write(bytes, 0, read);
						}
					} finally {
						if (out != null) {
							out.flush();
							out.close();
						}
					}

					img_TGO = new Image(PlatformUI.getWorkbench().getDisplay(),
							temp.getAbsolutePath());
				} finally {
					if (input != null)
						input.close();

					if (temp != null)
						temp.delete();
				}
			} else {
				// the normal case
				try {
					img_TGO = new Image(PlatformUI.getWorkbench().getDisplay(),
							input);
				} finally {
					if (input != null)
						input.close();
				}
			}

			int imageWidth = img_TGO.getBounds().width;
			int imageHeight = img_TGO.getBounds().height;

			this.imageUri = uri;

			imageInfoMap.put(uri, new Point(imageWidth, imageHeight));

			if (imageTabsMap.isEmpty()) {
				openTabs = 1;
				openToolkit = false;
				Workbench
						.getInstance()
						.getActiveWorkbenchWindow()
						.getActivePage()
						.showView(IMAGE_VIEW_ID, String.valueOf(openTabs),
								IWorkbenchPage.VIEW_ACTIVATE);
				openToolkit = true;
			}

			notifyListeners(Event.LOAD_IMAGE, new Pair<String, Image>(
					focusedImageTabSecondaryId, img_TGO));
			notifyListeners(Event.SET_IMAGE_URI, new Pair<String, String>(
					focusedImageTabSecondaryId, imageUri));

			notifyListeners(Event.SET_TITLE_LABEL, new Pair<String, String>(
					focusedImageTabSecondaryId, title));
			notifyListeners(Event.SET_TYPE_LABEL, new Pair<String, String>(
					focusedImageTabSecondaryId, contentTypeId));

			if (isLoadingLinksObject && loaded_object != null) {
				notifyListeners(Event.SET_LINKS_OBJECT_LABEL,
						new Pair<String, String>(focusedImageTabSecondaryId,
								"Object: "
										+ loaded_object.getProjectInstance()
												.getName() + "/"
										+ loaded_object.getTitle()));
			} else if (isLoadingLinksObject && localAnnotatedFilePath != null
					&& !"".equals(localAnnotatedFilePath)) { // is local
				notifyListeners(Event.SET_LINKS_OBJECT_LABEL,
						new Pair<String, String>(focusedImageTabSecondaryId,
								"File: " + localAnnotatedFilePath));
			}

			notifyListeners(Event.SET_SELECTED_OBJECT,
					new Pair<String, TextGridObject>(
							focusedImageTabSecondaryId, tgObj));

			imageTabsMap.put(focusedImageTabSecondaryId, imageUri);
			// all_links.clear();
		} catch (CoreException e) {
			Activator.handleError(e,
					"An Error occured while loading an image.\n\n" //$NON-NLS-1$
							+ e.toString());
		} catch (SWTException e) {
			Activator.handleError(e,
					"Couldn't create the image from the inputStream..."); //$NON-NLS-1$
		} catch (IOException e) {
			Activator.handleError(e,
					"An Error occured while loading an image.\n\n" //$NON-NLS-1$
							+ e.toString());
		}
	}

	// Just call it!
	public void testhOCR() {
		String focusedUri = imageTabsMap.get(focusedImageTabSecondaryId);
		Point widthHeight = imageInfoMap.get(focusedUri);

		System.err.println(widthHeight.x + "         " + widthHeight.y);
		// 7 393 549 408
		// 7 375 548 391

		IPath path = new Path("test.xml");
		URL url = FileLocator.find(Activator.getDefault().getBundle(), path,
				null);

		InputStream in = null;
		try {
			url = FileLocator.resolve(url);
			in = url.openStream();
			BufferedReader reader = null;

			reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));

			String s = null, text = "";

			while ((s = reader.readLine()) != null) {
				text += s + "\n";
			}

			// System.err.println(text);
			Pattern p = Pattern.compile("<span.+?>");

			Matcher m = p.matcher(text);

			String ct = "";
			String[] coords = new String[4];

			while (m.find()) {
				ct = m.group();
				ct = ct.replaceAll(".+?bbox\\s", "").replaceAll("'>", "");

				coords = ct.split(" ", 4);

				int x0 = Integer.valueOf(coords[0]), y0 = Integer
						.valueOf(coords[1]), x1 = Integer.valueOf(coords[2]), y1 = Integer
						.valueOf(coords[3]);

				TGRectangle shape = new TGRectangle(x0, Math.abs(widthHeight.y
						- y0), x1 - x0, Math.abs(y1 - y0), focusedUri);

				notifyListeners(Event.ADD_SHAPE, new Pair<String, TGShape>(
						focusedImageTabSecondaryId, shape));
			}

		} catch (Exception e) {
			Activator.handleError(e);
			return;
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					Activator.handleWarning(e);
				}
		}

		// int x0 = 7, y0 = 375, x1 = 548, y1 = 391;
		//
		// TGRectangle shape = new TGRectangle(x0, Math.abs(widthHeight.y - y0),
		// x1 - x0, Math.abs(y1 - y0), focusedUri);
		//
		// notifyListeners(Event.ADD_SHAPE, new Pair<String, TGShape>(
		// focusedImageTabSecondaryId, shape));

	}

	/**
	 * A call-back method which have to be called after closing an image-tab
	 * 
	 * @param secondaryId
	 *            The secondary id of the closed image-tab
	 */
	public void setImageTabClosed(String secondaryId) {
		imageTabsMap.remove(secondaryId);
	}

	/**
	 * Read the text's {@link InputStream} from the {@link TextGridObject} and
	 * open it in the xml-editor.
	 * 
	 * @param input
	 * @param contentType
	 * @param uri
	 * @param fileName
	 */
	private synchronized void loadTGOXML(IFileEditorInput input,
			TGContentType contentType, String uri, String fileName, TextGridObject tgo) {
			OpenObjectService.getInstance().openObject(tgo, 2, false);
//		try {
//
//			this.textUri = uri;
//			unsegmentedOpenXmlUris.add(uri);
//			openXmlUris.add(uri);
//
//			IEditorDescriptor editorDescriptor = null;
//			IEditorRegistry editorRegistry = PlatformUI.getWorkbench()
//					.getEditorRegistry();
//
//			IContentType eclipseContentType = null;
//			if (contentType != null)
//				eclipseContentType = contentType.getEclipseContentType();
//
//			editorDescriptor = editorRegistry.getDefaultEditor(fileName,
//					eclipseContentType);
//
//			try {
//				PlatformUI.getWorkbench().getActiveWorkbenchWindow()
//						.getActivePage()
//						.openEditor(input, editorDescriptor.getId());
//			} catch (NullPointerException e) {
//				Activator.handleWarning(e, "I have to solve this problem!");
//			}
//		} catch (Exception e) {
//			Activator.handleError(e,
//					"An Error occured while loading the object {0}" //$NON-NLS-1$
//					, e.toString());
//		}
	}

	/**
	 * Read the resource. The background job should wait for the ui-jobs
	 */
	private void readAddedTGObjectWaiting(final TextGridObject added_object,
			IProgressMonitor monitor) throws CoreException {

		if (added_object == null)
			return;

		final IFile file = (IFile) added_object.getAdapter(IFile.class);
		final String uri = added_object.getURI().toString();
		final String contentTypeId = added_object.getContentTypeID();
		final String title = added_object.getTitle();

		if (contentTypeId.contains("image")) { //$NON-NLS-1$
			if (!openInNewTab) {
				all_links.clear();
				selectedShape = null;
			}

			final InputStream input = file.getContents(true);

			UIJob openImgJob = new UIJob(NLS.bind(
					"Opening the image object {0}...", title)) {
				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					loadTGOImage(added_object, input, uri, title, contentTypeId);

					if (!isLoadingLinksObject)
						notifyListeners(Event.FINALIZE_LOADING,
								new Pair<String, Object>(
										focusedImageTabSecondaryId, null));
					return Status.OK_STATUS;
				}
			};
			openImgJob.setUser(true);
			openImgJob.schedule();

			try {
				openImgJob.join();
			} catch (InterruptedException e) {
				Activator.handleWarning(e, "Job interrupted!");
			}

		} else {
			if (!openInNewTab) {
				all_links.clear();
				selectedTextStart = -1;
				selectedTextLength = 0;
			}

			final TGContentType contentType = added_object
					.getContentType(false);
			final IFileEditorInput fileInput = new FileEditorInput(file);
			final String fileName = file.getName();

			UIJob openingxmlJob = new UIJob(NLS.bind(
					"Opening the xml object {0}...", title)) {

				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					loadTGOXML(fileInput, contentType, uri, fileName, added_object);

					return Status.OK_STATUS;
				}
			};
			openingxmlJob.setUser(false);
			openingxmlJob.schedule();
		}
	}

	/**
	 * Read the resource.
	 */
	private void readAddedTGObject(final TextGridObject added_object,
			IProgressMonitor monitor) throws CoreException {

		if (added_object == null)
			return;

		if (monitor == null)
			monitor = new NullProgressMonitor();

		monitor.worked(20);

		final IFile file = (IFile) added_object.getAdapter(IFile.class);
		final String uri = added_object.getURI().toString();
		final String contentTypeId = added_object.getContentTypeID();

		monitor.worked(20);

		if (contentTypeId.contains("image")) { //$NON-NLS-1$
			if (!openInNewTab) {
				all_links.clear();
				selectedShape = null;
			}

			final InputStream input = file.getContents(true);
			final String title = added_object.getTitle();

			new UIJob("Opening an image object...") {
				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					loadTGOImage(added_object, input, uri, title, contentTypeId);

					if (!isLoadingLinksObject)
						notifyListeners(Event.FINALIZE_LOADING,
								new Pair<String, Object>(
										focusedImageTabSecondaryId, null));
					return Status.OK_STATUS;
				}
			}.schedule();

		} else {
			if (!openInNewTab) {
				all_links.clear();
				selectedTextStart = -1;
				selectedTextLength = 0;
			}

			final TGContentType contentType = added_object
					.getContentType(false);
			final IFileEditorInput fileInput = new FileEditorInput(file);
			final String fileName = file.getName();

			new UIJob("Opening an xml object...") {

				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					loadTGOXML(fileInput, contentType, uri, fileName, added_object);

					return Status.OK_STATUS;
				}
			}.schedule();

		}
	}

	/**
	 * Reset the link editor to its initial state
	 * 
	 * @param ask
	 *            ask before reseting
	 */
	public void reset(boolean ask) {

		if (ask && isDirty() && showWarnings) {
			msg_asking
					.setMessage("Do you want to save the current changes before reseting the Text-Image-Link Editor?"); // displayed
			// message
			msg_asking.setText("Save current changes?"); // displayed
			// title

			int result = msg_asking.open();
			if (result == SWT.YES) {
				saveAnnotatedObject(false, false, true);
			} else if (result == SWT.NO) {
				doRevertAllXMLEditorsToSaved();
				setChangedAfterLoading_AllImageViews(false);
			} else { // cancel
				return;
			}
		} else if (ask) {
			msg_asking_2
					.setMessage("Do you really want to reset the Text-Image-Link Editor?"); // displayed
			// message
			msg_asking_2.setText("Reset Text-Image-Link Editor?"); // displayed
			// title

			int result = msg_asking_2.open();
			if (result != SWT.YES) {
				return;
			}
		}

		selected_object = null;

		notifyListeners(Event.REMOVE_ALL_SHAPES, new Pair<String, Object>(
				"all", null));

		selectedShape = null;
		selectedTextStart = -1;
		selectedTextLength = 0;
		imageUri = "";
		textUri = "None";
		annoModel = null;
		text = null;
		doc = null;
		editor = null;
		setChangedAfterLoading_AllImageViews(false);
		closeAllXMLEditors(true);
		closeAllImageViews();
		setDirty(false);
		workLocal = false;
		allLayerManager.clear(); // clear layer Container
		localAnnotatedFilePath = "";
		isReplacingText = false;
		object_save = null;
		if(newTGObject != null) 
			doUnlockTglObject();
		newTGObject = null;
		saveNewFile = false;

		all_links.clear();
		selectedShapes.clear();
		allShapes.clear();
		imageInfoMap.clear();
		imageTabsMap.clear();
		startEndMap.clear();
		openXmlUris.clear();
		unsegmentedOpenXmlUris.clear();
		replacedRevisions.clear();

		linksInfoUpdated = false;
		updateFlag = false;

		System.err.println("reseted...");
	}

	private void softReset(){
		selected_object = null;
		
		setChangedAfterLoading_AllImageViews(false);
		
		setDirty(false);
		workLocal = false;
		localAnnotatedFilePath = "";
		isReplacingText = false;
		object_save = null;
		
		newTGObject = null;
		saveNewFile = false;

		all_links.clear();
		selectedShapes.clear();
		allShapes.clear();
		imageInfoMap.clear();
		imageTabsMap.clear();
		startEndMap.clear();
		openXmlUris.clear();
		unsegmentedOpenXmlUris.clear();
		replacedRevisions.clear();

		linksInfoUpdated = false;
		updateFlag = false;

		System.out.println(" soft reseted...");
	}
	
	private boolean isLoadingLinksObject = false;
	private Job readingTGOJob_readAnnotated = null;

	private void readAnnotatedObject() throws ParserConfigurationException,
			SAXException, CoreException, XMLStreamException,
			javax.xml.stream.FactoryConfigurationError,
			UnsupportedEncodingException, IOException, URISyntaxException {

		if (loaded_object == null)
			return;

		if (isDirty() && showWarnings) {
			msg_asking
					.setMessage(Messages.LinkEditorController_Mess_ChangedAfterLoading_Object);
			// displayed
			// message
			msg_asking
					.setText(Messages.LinkEditorController_Title_ChangedAfterLoading_Obj);
			// displayed
			// title

			int result = msg_asking.open();
			if (result == SWT.YES) {
				saveReadAnnotatedObject(false, false);
				return;
			} else if (result == SWT.NO) {
				doRevertAllXMLEditorsToSaved();
				setChangedAfterLoading_AllImageViews(false);
			} else { // cancel
				return;
			}
		}

		// =========================================================

		if (readingTGOJob_readAnnotated != null)
			readingTGOJob_readAnnotated.cancel();

		readingTGOJob_readAnnotated = new Job(NLS.bind("Opening {0}.....",
				loaded_object.getTitle())) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {

				if (monitor == null)
					monitor = new NullProgressMonitor();

				try {

					monitor.beginTask(
							NLS.bind("Opening {0}.....",
									loaded_object.getTitle()), 100);

					IFile file = (IFile) loaded_object.getAdapter(IFile.class);

					monitor.worked(10);

					InputStream inputStream = file.getContents(true);

					monitor.worked(60);

					UIJob resetJob = new UIJob("Cleaning the screen...") {

						@Override
						public IStatus runInUIThread(IProgressMonitor monitor) {
							reset(false);
							return Status.OK_STATUS;
						}
					};

					resetJob.schedule();

					try {
						resetJob.join();
					} catch (InterruptedException e) {
						Activator.handleError(e);
					}

					TEIDocParser parser = new TEIDocParser();

					isLoadingLinksObject = true;
					parser.parse(inputStream, monitor);

					selected_object = loaded_object;
					loaded_object = null;
					workLocal = false;

					isLoadingLinksObject = false;
					// monitor.worked(10);

					linksInfoUpdated = false;

					new UIJob("Finalizing the loading process...") {
						@Override
						public IStatus runInUIThread(IProgressMonitor monitor) {
							if (selectedShape != null)
								jumpToText(selectedShape, true);

							notifyListeners(Event.FINALIZE_LOADING,
									new Pair<String, Object>(
											focusedImageTabSecondaryId, null));

							setChangedAfterLoading_AllImageViews(false);

							new UIJob("Updating text segments...") {

								@Override
								public IStatus runInUIThread(
										IProgressMonitor arg0) {
									updateTextSegments();
									unsegmentedOpenXmlUris.remove(textUri);
									updateFlag = true;
									return Status.OK_STATUS;
								}
							}.schedule();

							return Status.OK_STATUS;
						}
					}.schedule();

					monitor.done();

					return Status.OK_STATUS;

				} catch (JaxenException e) {
					Activator.handleError(e);
				} catch (CoreException e) {
					Activator.handleError(e);
				} catch (XMLStreamException e) {
					Activator.handleError(e);
				}

				return Status.CANCEL_STATUS;
			}
		};

		readingTGOJob_readAnnotated.setUser(false);
		readingTGOJob_readAnnotated.schedule();
	}

	/**
	 * Set an image's tab as current focused tab
	 * 
	 * @param tabSecondaryId
	 */
	public void setFocusedTab(String tabSecondaryId) {
		if (tabSecondaryId.equals(focusedImageTabSecondaryId))
			return;

		if (!isLoadingLinksObject)
			notifyListeners(Event.CLOSE_TOOLKIT, new Pair<String, Object>(
					focusedImageTabSecondaryId, null));

		focusedImageTabSecondaryId = tabSecondaryId;

		if (openToolkit && !isLoadingLinksObject)
			notifyListeners(Event.OPEN_TOOLKIT, new Pair<String, Object>(
					focusedImageTabSecondaryId, null));
	}

	/**
	 * Shows, if there are unlinked Shapes.
	 * 
	 * @param count
	 */
	public void setCountDiffLabel(int count) {
		notifyListeners(Event.SET_COUNT,
				new Pair<String, Boolean>("all", false));

		if (count > 0) {
			notifyListeners(Event.SET_COUNT, new Pair<String, Boolean>(
					focusedImageTabSecondaryId, true));
		}
	}

	/**
	 * Shows the angle of the selected Shape.
	 * 
	 * @param visible
	 * @param angle
	 */
	public void setAngleLabel(boolean visible, double angle) {
		Pair<Boolean, Double> p = new Pair<Boolean, Double>(visible, angle);
		notifyListeners(Event.SET_ANGLE,
				new Pair<String, Pair<Boolean, Double>>(
						focusedImageTabSecondaryId, p));
	}

	/**
	 * Shows the current Layer level.
	 * 
	 * @param visible
	 * @param layer
	 */
	public void setLayerLabel(boolean visible, String layer) {
		Pair<Boolean, String> p = new Pair<Boolean, String>(visible, layer);
		notifyListeners(Event.SET_LAYER,
				new Pair<String, Pair<Boolean, String>>(
						focusedImageTabSecondaryId, p));
	}

	public double getLastSelectedDockingLineAngle() {
		return lastSelectedDockingLineAngle;
	}

	public String getCurrentLayer() {
		return currentLayer;
	}

	public void setCurrentLayer(String currentLayer) {
		this.currentLayer = currentLayer;
	}

	public static String getDefaultLayer() {
		return defaultTGLayer;
	}

	public void setLastSelectedDockingLineAngle(
			double lastSelectedDockingLineAngle) {
		this.lastSelectedDockingLineAngle = lastSelectedDockingLineAngle;
	}

	public void setAddActionStatus(Object obj) {
		if (obj instanceof TGLine) {
			notifyListeners(
					Event.SET_VISIBILITY_OF_CREATELINK,
					new Pair<String, Boolean>(focusedImageTabSecondaryId, false));
		} else {
			notifyListeners(Event.SET_VISIBILITY_OF_CREATELINK,
					new Pair<String, Boolean>(focusedImageTabSecondaryId, true));
		}
	}

	public void setshowWarnings(boolean show) {
		this.showWarnings = show;
	}

	public boolean showWarnings() {
		return this.showWarnings;
	}

	private String generateId() {
		if (doc != null) {
			String text = doc.get();
			int id_nr = 1;

			while (text.matches("(?s).*id\\s*=\\s*\"a" + id_nr + "\".*")) {
				++id_nr;
			}

			return "a" + id_nr;
		}

		return null;
	}

	private String startAnchorId = "";
	private String endAnchorId = "";

	private Pair<String, String> generateAnchorIds() {
		if (doc != null) {
			String text = doc.get();
			int id_nr1 = 1;

			while (text.matches("(?s).*id\\s*=\\s*\"a" + id_nr1 + "_start"
					+ "\".*")) {
				++id_nr1;
			}

			int i1 = 1, i2 = 2;
			for (ILink l : all_links) {
				if (!textUri.equals(l.getTextUri()))
					continue;

				String s1 = l.getStartAnchorId(), s2 = l.getEndAnchorId();

				if (s1 != null && !"".equals(s1) && s1.matches("^.\\d+$"))
					i1 = Integer.parseInt(s1.substring(1));
				if (s2 != null && !"".equals(s2) && s2.matches("^.\\d+$"))
					i2 = Integer.parseInt(s2.substring(1));

				if (i1 >= id_nr1)
					id_nr1 = i1 + 1;
				if (i2 >= id_nr1)
					id_nr1 = i2 + 1;
			}

			while (text.matches("(?s).*id\\s*=\\s*\"a" + id_nr1 + "_end"
					+ "\".*")) {
				++id_nr1;
			}

			this.startAnchorId = "a" + id_nr1 + "_start";
			this.endAnchorId = "a" + id_nr1 + "_end";

			return new Pair<String, String>(this.startAnchorId,
					this.endAnchorId);
		}

		return null;
	}

	private String annotateText(String text) throws RuntimeException {
		Pair<String, String> ids = generateAnchorIds();
		if (ids == null)
			throw new RuntimeException("ids == null");

		return "<anchor xml:id=\"" + ids.getValue1() + "\" />" + text
				+ "<anchor xml:id=\"" + ids.getValue2() + "\" />";
	}

	/**
	 * Add a new link.
	 * 
	 * @return
	 * @throws RuntimeException
	 */
	public boolean addNewLink() throws RuntimeException {

		if (textFileOpen) {
			msg_info.setText("");
			msg_info.setMessage("To use this tool you should use XML-Text!");
			msg_info.open();
			return false;
		}

		if (selectedShape == null || selectedTextStart < 0
				|| (selectedTextStart < 0 && selectedTextLength <= 0)) {
			msg_info.setText("");
			msg_info.setMessage(Messages.LinkEditorController_Mess_SelectShapeText);
			msg_info.open();
			return false;
		}

		if (textUri.startsWith("textgrid-newfile")) {
			msg_info.setText("");
			msg_info.setMessage("The current active xml-object is not persistent!\nPlease save it then try again.");
			msg_info.open();
			return false;
		}

		// Is an xml-node selected ???
		ElementImpl selectedNode = getSelectedXMLNode(selectedTextStart);
		boolean isNodeSelected = false;
		boolean idAvailable = false;
		String nodeId = null;
		boolean add = true;
		String annotatedText = null;

		if (selectedNode != null && !"".equals(selectedNode.getLocalName())) {
			if (selectedNode.hasAttribute("xml:id")) {
				nodeId = selectedNode.getAttribute("xml:id");
				idAvailable = true;
			} else {
				nodeId = generateId();

				if (nodeId == null)
					throw new RuntimeException("nodeId == null");
			}
			isNodeSelected = true;
			startAnchorId = nodeId;
			endAnchorId = "";
		} else if (selectedTextLength <= 0) {
			msg_info.setText("");
			msg_info.setMessage(Messages.LinkEditorController_Mess_SelectShapeText);
			msg_info.open();
			return false;
		}

		if (!isNodeSelected)
			annotatedText = annotateText(selectedText);

		// if the shape has already been linked to a text -
		// just edit the text and do not create a new link.
		for (ILink a : all_links) {
			TGShape s = a.getShape();
			if (s != null && s.equals(this.selectedShape)) {
				if (showWarnings) {
					msg_asking_2
							.setMessage(Messages.LinkEditorController_Mess_EditExistingLink); // displayed
					// message
					msg_asking_2
							.setText(Messages.LinkEditorController_Title_EditExistingLink); // displayed
					// title
					if (msg_asking_2.open() == SWT.YES) {
						try {
							if (isNodeSelected) {
								if (!idAvailable) {
									selectedNode.setAttributeNS(XML_NS,
											"xml:id", startAnchorId);
									linksInfoUpdated = true;
								}
							} else {
								isReplacingText = true;
								doc.replace(selectedTextStart,
										selectedTextLength, annotatedText);
								linksInfoUpdated = true;
								isReplacingText = false;
							}

							// TG-1542
							if (!a.getStartAnchorId().equals(startAnchorId))
								removeSegmentFromXMLEditor(a, true);

							all_links.edit(a, startAnchorId, endAnchorId);
						} catch (BadLocationException e) {
							Activator.handleError(e);
							return false;
						}
					} else {
						return false;
					}
				} else {
					try {
						if (isNodeSelected) {
							if (!idAvailable) {
								selectedNode.setAttributeNS(XML_NS, "xml:id",
										startAnchorId);
								linksInfoUpdated = true;
							}
						} else {
							isReplacingText = true;
							doc.replace(selectedTextStart, selectedTextLength,
									annotatedText);
							linksInfoUpdated = true;
							isReplacingText = false;
						}

						removeSegmentFromXMLEditor(a, true);

						all_links.edit(a, startAnchorId, endAnchorId);
					} catch (BadLocationException e) {
						Activator.handleError(e);
						return false;
					}
					System.out.println("link successfully edited"); //$NON-NLS-1$
				}
				add = false;
				break;
			} else if (this.textUri.equals(a.getTextUri())
					&& (startAnchorId.equals(a.getStartAnchorId())
							|| startAnchorId.equals(a.getEndAnchorId())
							|| (!"".equals(endAnchorId) && endAnchorId.equals(a
									.getStartAnchorId())) || (!""
							.equals(endAnchorId) && endAnchorId.equals(a
							.getEndAnchorId())))) {
				// avoid linking an anchor elements in multiple links
				msg_info.setMessage("Selected xml-element is already linked with another shape!"); // displayed
				// message
				msg_info.setText("XML-Element in use"); // displayed
				msg_info.open();
				return false;
			}
		}

		TGShape tShape = null;

		// if the shape is not in the all_links then
		// create a new link
		if (add) {
			if (this.selectedShape != null) {
				tShape = selectedShape;
				// Add a new link to the linksContainer
				if (selectedShape instanceof TGRectangle) {
					all_links
							.add(new RectLink((TGRectangle) this.selectedShape,
									this.startAnchorId, this.endAnchorId,
									textUri, true));
				} else if (selectedShape instanceof TGPolygon) {
					all_links
							.add(new PolyLink((TGPolygon) this.selectedShape,
									this.startAnchorId, this.endAnchorId,
									textUri, true));
				}
				try {
					if (isNodeSelected) {
						if (!idAvailable)
							selectedNode.setAttributeNS(XML_NS, "xml:id",
									startAnchorId);
					} else {
						isReplacingText = true;
						doc.replace(selectedTextStart, selectedTextLength,
								annotatedText);
						isReplacingText = false;

						// Hack
						if (tShape != null)
							selectedShape = tShape;
					}
				} catch (BadLocationException e) {
					Activator.handleError(e);
					return false;
				}
			}
		}

		notifyListeners(Event.LINKS_CHANGED, new Pair<String, TGShape>(
				focusedImageTabSecondaryId, this.selectedShape));

		setChangedAfterLoading(true);

		jumpToText(this.selectedShape, false);

		linksInfoUpdated = true;

		return true;
	}

	private boolean saveReadAnnotatedObject(final boolean saveAs,
			final boolean openLocal) {

		if (all_links.isEmpty()) {
			msg_info.setMessage(Messages.LinkEditorController_Mess_CreateLinksFirst);

			msg_info.setText(Messages.LinkEditorController_Title_CreateLinksFirst);

			msg_info.open();
			return false;
		}

		// =========================================================
		Job savingOpeningTGOJob = new Job("Saving the object...") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {

				if (monitor == null)
					monitor = new NullProgressMonitor();

				try {

					monitor.beginTask("Saving the object...", 100);

					TextGridObject temp_loaded_object = loaded_object;

					if (workLocal) {
						saveLocalAnnotatedFileImpl(saveAs, true, monitor);
					} else {
						saveAnnotatedObjectImpl(saveAs, false, true, monitor);
					}

					loaded_object = temp_loaded_object;

					if (openLocal) {
						InputStream inputStream = null;
						try {
							File file = new File(filePath);
							if (file == null || !file.exists())
								return Status.CANCEL_STATUS;

							monitor.worked(10);

							inputStream = new FileInputStream(file);

							monitor.worked(60);

							UIJob resetJob = new UIJob("Cleaning the screen...") {

								@Override
								public IStatus runInUIThread(
										IProgressMonitor monitor) {
									reset(false);
									return Status.OK_STATUS;
								}
							};

							resetJob.schedule();

							try {
								resetJob.join();
							} catch (InterruptedException e) {
								Activator.handleError(e);
							}

							TEIDocParser parser = new TEIDocParser();

							isLoadingLinksObject = true;

							localAnnotatedFilePath = filePath;
							parser.parse(inputStream, monitor);
							workLocal = true;
							linksInfoUpdated = false;
						} finally {
							if (inputStream != null)
								inputStream.close();
						}
					} else {
						if (loaded_object == null)
							return Status.CANCEL_STATUS;

						IFile file = (IFile) loaded_object
								.getAdapter(IFile.class);

						monitor.worked(10);

						InputStream inputStream = file.getContents(true);

						monitor.worked(10);

						UIJob resetJob = new UIJob("Cleaning the screen...") {

							@Override
							public IStatus runInUIThread(
									IProgressMonitor monitor) {
								reset(false);
								return Status.OK_STATUS;
							}
						};

						resetJob.schedule();

						try {
							resetJob.join();
						} catch (InterruptedException e) {
							Activator.handleError(e);
						}

						TEIDocParser parser = new TEIDocParser();

						isLoadingLinksObject = true;
						parser.parse(inputStream, monitor);

						selected_object = loaded_object;
						loaded_object = null;
						workLocal = false;
						linksInfoUpdated = false;

					}

					isLoadingLinksObject = false;

					monitor.worked(10);

					new UIJob("Finalizing the loading process...") {
						@Override
						public IStatus runInUIThread(IProgressMonitor monitor) {
							if (selectedShape != null)
								jumpToText(selectedShape, true);

							notifyListeners(Event.FINALIZE_LOADING,
									new Pair<String, Object>(
											focusedImageTabSecondaryId, null));

							setChangedAfterLoading_AllImageViews(false);

							return Status.OK_STATUS;
						}
					}.schedule();

					monitor.done();

					return Status.OK_STATUS;

				} catch (XMLStreamException e) {
					Activator.handleError(e);
				} catch (UnsupportedEncodingException e) {
					Activator.handleError(e);
				} catch (FactoryConfigurationError e) {
					Activator.handleError(e);
				} catch (CrudServiceException e) {
					Activator.handleError(e);
				} catch (JaxenException e) {
					Activator.handleError(e);
				} catch (CoreException e) {
					Activator.handleError(e);
				} catch (IOException e) {
					Activator.handleError(e);
				}

				return Status.CANCEL_STATUS;
			}
		};

		savingOpeningTGOJob.setUser(false);
		savingOpeningTGOJob.schedule();

		return true;
	}

	private boolean saveLocalAnnotatedFileImpl(final boolean saveAs,
			boolean inBackground, IProgressMonitor monitor)
			throws XMLStreamException, FactoryConfigurationError, IOException {

		if (monitor == null)
			monitor = new NullProgressMonitor();

		if ("".equals(localAnnotatedFilePath) || saveAs) {

			if (inBackground) {
				UIJob selectJob = new UIJob("Selecting file to save...") {

					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						FileDialog fileDlg = new FileDialog(PlatformUI
								.getWorkbench().getActiveWorkbenchWindow()
								.getShell(), SWT.SAVE);
						fileDlg.setText("Select file to save...");
						fileDlg.setFilterExtensions(new String[] { "*.tgl" });
						fileDlg.setFilterNames(new String[] { "Text-Image-Link file (*.tgl)" });

						String filePath = fileDlg.open();

						localAnnotatedFilePath = filePath;

						if (!saveAs)
							saveNewFile = true;

						if (localAnnotatedFilePath != null
								&& !"".equals(localAnnotatedFilePath)
								&& !localAnnotatedFilePath.endsWith(".tgl")) {
							localAnnotatedFilePath += ".tgl";
						}
						return Status.OK_STATUS;
					}
				};

				selectJob.setUser(false);
				selectJob.schedule();

				try {
					selectJob.join();
				} catch (Exception e) {
				}
			} else {
				FileDialog fileDlg = new FileDialog(PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getShell(), SWT.OPEN);
				fileDlg.setText("Open File to save...");
				fileDlg.setFilterExtensions(new String[] { "*.tgl" });
				fileDlg.setFilterNames(new String[] { "Text-Image-Link file (*.tgl)" });

				String filePath = fileDlg.open();

				localAnnotatedFilePath = filePath;

				if (!saveAs)
					saveNewFile = true;

				if (localAnnotatedFilePath != null
						&& !"".equals(localAnnotatedFilePath)
						&& !localAnnotatedFilePath.endsWith(".tgl")) {
					localAnnotatedFilePath += ".tgl";
				}
			}
		}

		if (localAnnotatedFilePath == null || "".equals(localAnnotatedFilePath))
			return false;

		TEIDocFactory teiFactory = new TEIDocFactory(all_links, inBackground);

		if (!saveDockingLinesUriList.isEmpty()) {
			List<TGLine> dockingLines = Collections
					.synchronizedList(new ArrayList<TGLine>());

			for (String uri : saveDockingLinesUriList) {
				for (TGShape s : updateShapesList(uri)) {
					if (s.getType() == TYPE.DOCKING_LINE) {
						dockingLines.add((TGLine) s);
					}
				}
			}

			teiFactory.addDockingLines(dockingLines);
		}

		File file = new File(localAnnotatedFilePath);
		if (file != null && !file.exists())
			file.createNewFile(); // create a new file if it doesn't
		// exist

		if (file != null) {

			teiFactory.createTeiDocument(file.getParent(), monitor);

			save_done_successfully = true;

			UIJob saveXmlJob = new UIJob("Saving open xml files...") {
				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					if (!doSaveAllXMLEditors(monitor, false, false)) {
						save_done_successfully = false;
						throw new RuntimeException(
								"Aborted: Couldn't save open xml-editors!");
					}
					return Status.OK_STATUS;
				}
			};

			saveXmlJob.schedule();
			try {
				saveXmlJob.join();
			} catch (InterruptedException e) {
				Activator.handleWarning(e);
			}

			if (!save_done_successfully)
				return false;

			save_done_successfully = teiFactory.makePersistent(file, monitor);

			if (!save_done_successfully)
				return false;

			new UIJob("Updating the image views...") {

				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {

					if (saveNewFile || saveAs) {

						saveNewFile = false;
						workLocal = true;

						notifyListeners(Event.SET_LINKS_OBJECT_LABEL,
								new Pair<String, String>("all", "File: "
										+ localAnnotatedFilePath));
					}

					setChangedAfterLoading_AllImageViews(false);

					return Status.OK_STATUS;
				}
			}.schedule();
		}

		monitor.done();

		return true;
	}

	private boolean completeLoadNewProcess;

	public void openXmlImageObjects(final TextGridObject toSaveIn,
			final ArrayList<TextGridObject> xmlImageObjects) {

		completeLoadNewProcess = true;

		new Job("Opening selected xml/image objects...") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {

				UIJob resJ = new UIJob("") {

					@Override
					public IStatus runInUIThread(IProgressMonitor arg0) {

						if (isDirty() && showWarnings) {
							msg_asking
									.setMessage(Messages.LinkEditorController_Mess_ChangedAfterLoading_Object);
							// displayed
							// message
							msg_asking
									.setText(Messages.LinkEditorController_Title_ChangedAfterLoading_Obj);
							// displayed
							// title

							int result = msg_asking.open();
							if (result == SWT.YES) {
								saveAnnotatedObject(false, false, true);
							} else if (result == SWT.NO) {
								doRevertAllXMLEditorsToSaved();
								setChangedAfterLoading_AllImageViews(false);
							} else { // cancel
								completeLoadNewProcess = false;
								return Status.CANCEL_STATUS;
							}
						}

						reset(false);
						return Status.OK_STATUS;
					}
				};

				resJ.schedule();

				try {
					resJ.join();
				} catch (InterruptedException e1) {
					Activator.handleWarning(e1);
				}

				if (!completeLoadNewProcess)
					return Status.CANCEL_STATUS;

				setNewSaveObject(toSaveIn);

				for (final TextGridObject obj : xmlImageObjects) {
					try {
						addResourceToLinkEditor(obj.getURI().toString(), null);
					} catch (IOException e) {
						Activator.handleError(e);
					}
				}

				new UIJob("") {

					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						setChangedAfterLoading_AllImageViews(true);
						return Status.OK_STATUS;
					}
				}.schedule();

				return Status.OK_STATUS;
			}
		}.schedule();

	}

	private TextGridObject newTGObject = null;

	public void setNewSaveObject(final TextGridObject object) {
		this.newTGObject = object;
	}

	private TextGridObject object_save = null;
	private boolean save_done_successfully = true;

	private boolean saveAnnotatedObjectImpl(final boolean saveAs,
			final boolean asNewRevision, boolean inBackgroundJob,
			IProgressMonitor monitor) throws UnsupportedEncodingException,
			XMLStreamException, FactoryConfigurationError {

		boolean createdNewObjectFromNewWizard = false;

		if (workLocal)
			return saveLocalAnnotatedFile(saveAs);

		if (monitor == null)
			monitor = new NullProgressMonitor();

		if (newTGObject != null && newTGObject.isNew()) {
			selected_object = newTGObject;
			newTGObject = null;
			createdNewObjectFromNewWizard = true;
		}

		if (selected_object == null || saveAs) {
			selected_project = null;
			entered_title = null;

			if (inBackgroundJob) {
				UIJob selectJob = new UIJob("Selecting title and project...") {

					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						SelectNameLocationDialog.openDialog(instance,
								(selected_object == null) ? false : saveAs);
						return Status.OK_STATUS;
					}
				};

				selectJob.setUser(false);
				selectJob.schedule();

				try {
					selectJob.join();
				} catch (Exception e) {
					Activator.handleWarning(e);
				}
			} else {
				SelectNameLocationDialog.openDialog(instance,
						(selected_object == null) ? false : saveAs);
			}

			if (selected_project == null || entered_title == null
					|| entered_title.equals("")) { //$NON-NLS-1$
				return false;
			}

			// NEW MD-SCHEMA
			object_save = TextGridObject.getNewObjectInstance(selected_project,
					TGContentType.getContentType(LINK_EDITOR_TYPE),
					entered_title);

			PersonType rightsHolder = RBACSession.getInstance().getPerson();
			object_save.setItemMetadata(rightsHolder);
			// END NEW MD-SCHEMA
		} else if (asNewRevision) {

			if (!SearchRequest.isTgoLatestRevision(selected_object.getURI()
					.toString())) {
				new UIJob("") {

					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						MessageDialog
								.openError(
										Display.getCurrent().getActiveShell(),
										"Save as new revision",
										"The Object \""
												+ selected_object.toString()
												+ "\" could not be saved as a revision, \nbecause there exists a successive one.\nSaving process cancelled.");
						return Status.OK_STATUS;
					}
				}.schedule();

				return false;
			}

			object_save = selected_object.prepareNewRevision(monitor);
			// AdapterUtils.getAdapter(object_save, IFile.class); // force file
			// creation.
		} else {
			object_save = selected_object;
		}

		try {
			String title = object_save.getTitle();
			if (title == null || "".equals(title)
					|| TextGridObject.NO_TITLE.equals(title)) {
				object_save.setTitle("New Text-Image-Link Object");
			}
		} catch (CoreException e1) {
			Activator.handleWarning(e1);
		}

		TEIDocFactory teiFactory = new TEIDocFactory(all_links, inBackgroundJob);

		if (!saveDockingLinesUriList.isEmpty()) {
			List<TGLine> dockingLines = Collections
					.synchronizedList(new ArrayList<TGLine>());

			for (String uri : saveDockingLinesUriList) {
				for (TGShape s : updateShapesList(uri)) {
					if (s.getType() == TYPE.DOCKING_LINE) {
						dockingLines.add((TGLine) s);
					}
				}
			}

			teiFactory.addDockingLines(dockingLines);
		}

		IFile file = (IFile) object_save.getAdapter(IFile.class);

		if (file != null /* && editor != null */) {

			teiFactory.createTeiDocument(null, monitor);

			save_done_successfully = true;

			replacedRevisions.clear();

			if (inBackgroundJob) {
				UIJob saveXmlJob = new UIJob("Saving all open xml files...") {
					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						if (!doSaveAllXMLEditors(monitor, false, asNewRevision)) {
							save_done_successfully = false;
							throw new RuntimeException(
									"Aborted: Couldn't save open xml-editors!");
						}
						return Status.OK_STATUS;
					}
				};

				saveXmlJob.schedule();
				try {
					saveXmlJob.join();
				} catch (InterruptedException e) {
					Activator.handleWarning(e);
				}
			} else {
				if (!doSaveAllXMLEditors(monitor, false, asNewRevision)) {
					save_done_successfully = false;
					throw new RuntimeException(
							"Aborted: Couldn't save open xml-editors!");
				}
			}

			if (!save_done_successfully)
				return false;

			if (!replacedRevisions.isEmpty()) {
				teiFactory.replaceUris(replacedRevisions);
				replacedRevisions.clear();
			}

			save_done_successfully = teiFactory.makePersistent(file, monitor);

			if (!save_done_successfully)
				return false;

			// // stop here if lab is closing
			// if (preShutdown) {
			// return true;
			// }

			if (selected_object == null || createdNewObjectFromNewWizard
					|| saveAs) {
				// a new annotated object was created

				selected_object = loaded_object = object_save;

				new UIJob("Updating the image views...") {
					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						setChangedAfterLoading(false);
						notifyListeners(Event.CHANGES_SAVED,
								new Pair<String, Object>("all", null));

						try {
							notifyListeners(Event.SET_LINKS_OBJECT_LABEL,
									new Pair<String, String>("all", "Object: "
											+ loaded_object
													.getProjectInstance()
													.getName() + "/"
											+ loaded_object.getTitle()));

							loaded_object = null;
						} catch (CoreException e) {
							Activator.handleError(e);
							return Status.CANCEL_STATUS;
						}

						return Status.OK_STATUS;
					}
				}.schedule();

			}

			new UIJob("Updating the image view...") {

				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					setChangedAfterLoading_AllImageViews(false);
					return Status.OK_STATUS;
				}
			}.schedule();

			if (asNewRevision) {
				new UIJob("Opening the new revision...") {

					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						openAnnotatedObject(object_save);
						return Status.OK_STATUS;
					}
				}.schedule();
			}

		}

		monitor.done();

		return true;
	}

	/**
	 * Save the created links in the current used Text-Image-Link Object.
	 * <p>
	 * <strong>If there is no used Text-Image-Link Object, then a new one will
	 * be created!</strong>
	 * </p>
	 * 
	 * @param saveAs
	 * @param asNewRevision
	 * @return boolean
	 */
	public boolean saveAnnotatedObject(final boolean saveAs,
			final boolean asNewRevision, final boolean wait) {
		
		if (!saveAs && !asNewRevision && !isDirty())
			return false;

		setChangedAfterLoading(false);
		
		// save the annotated file locally
		if ("".equals(RBACSession.getInstance().getSID(false))
				|| (workLocal && !saveAs))
			return saveLocalAnnotatedFile(saveAs);

		try {
			// =========================================================
			if (preShutdown || wait) {
				return saveAnnotatedObjectImpl(saveAs, asNewRevision, false,
						null);
			} else {
				Job savingTGOJob = new Job("Saving the object...") {
					@Override
					protected IStatus run(IProgressMonitor monitor) {

						if (monitor == null)
							monitor = new NullProgressMonitor();

						monitor.beginTask("Saving the object...", 100);

						try {
							if (saveAnnotatedObjectImpl(saveAs, asNewRevision,
									true, monitor))
								return Status.OK_STATUS;
							else
								return Status.CANCEL_STATUS;
						} catch (XMLStreamException e) {
							Activator.handleError(e);
						} catch (UnsupportedEncodingException e) {
							Activator.handleError(e);
						} catch (FactoryConfigurationError e) {
							Activator.handleError(e);
						}
						softReset();
						return Status.CANCEL_STATUS;
					}
				};

				savingTGOJob.setUser(false);
				savingTGOJob.schedule();
					
				return (savingTGOJob.getResult() == Status.OK_STATUS);
			}

		} catch (XMLStreamException e) {
			Activator.handleError(e);
		} catch (UnsupportedEncodingException e) {
			Activator.handleError(e);
		} catch (FactoryConfigurationError e) {
			Activator.handleError(e);
		}

		return false;

	}

	/**
	 * Close all open image views
	 */
	private void closeAllImageViews() {
		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		if (page != null) {
			for (IViewReference ref : page.getViewReferences()) {
				if (ref.getId().equals(IMAGE_VIEW_ID)) {
					page.hideView(ref);
				}
			}
		}
	}

	/**
	 * close all open editors
	 * 
	 * @param save
	 * @return
	 */
	private boolean closeAllXMLEditors(boolean save) {
		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		if (page != null) {
			boolean closed = page.closeAllEditors(save);

			if (closed) {
				doc = null;
				annoModel = null;
				editor = null;
			}

			return closed;
		}

		return false;
	}

	/**
	 * Save all open editors
	 * 
	 * @param confirm
	 * @return
	 */
	private boolean doSaveAllXMLEditors(IProgressMonitor monitor,
			final boolean confirm, final boolean asNewRevision) {
		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		boolean res = false;
		IEditorReference[] editorRefs = page.getEditorReferences();
		for (IEditorReference eRef : editorRefs) {
			IEditorPart part = eRef.getEditor(false);
			ITextEditor e = AdapterUtils.getAdapter(part, ITextEditor.class);
			if (e != null
					&& e.isDirty()
					&& e.getEditorInput().getName().toLowerCase()
							.endsWith("xml")) {

				TextGridObject tgO = (TextGridObject) e.getEditorInput()
						.getAdapter(TextGridObject.class);

				if (tgO != null && asNewRevision) {

					if (!SearchRequest.isTgoLatestRevision(tgO.getURI()
							.toString())) {
						MessageDialog
								.openError(
										Display.getCurrent().getActiveShell(),
										"Save as new revision",
										"The Object \""
												+ tgO.toString()
												+ "\" could not be saved as a revision, \nbecause there exists a successive one.\nSaving process cancelled.");
						return false;
					}

					TextGridObject newTgO = tgO.prepareNewRevision(monitor);
					res = page.saveEditor(e, confirm);

					// System.err.println(tgO.getURI().toString() + "        "
					// + newTgO.getURI().toString());

					replacedRevisions.put(tgO.getURI().toString(), newTgO
							.getURI().toString());
				} else {
					res = page.saveEditor(e, confirm);
				}

				if (!res)
					return false;
			}
		}

		return true;
	}

	/**
	 * Reverts all open xml editors to saved
	 */
	private void doRevertAllXMLEditorsToSaved() {
		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		IEditorReference[] editorRefs = page.getEditorReferences();
		for (IEditorReference eRef : editorRefs) {
			IEditorPart part = eRef.getEditor(false);
			ITextEditor e = AdapterUtils.getAdapter(part, ITextEditor.class);
			if (e != null
					&& e.getEditorInput().getName().toLowerCase()
							.endsWith("xml")) {
				e.doRevertToSaved();
			}
		}
	}

	private void setChangedAfterLoading(boolean changed_after_loading) {
		setDirty(changed_after_loading);
		notifyListeners(Event.UPDATE_IMAGE_VIEW_ON_CHANGES,
				new Pair<String, Boolean>(focusedImageTabSecondaryId,
						this.isDirty()));
	}

	private void setChangedAfterLoading_AllImageViews(
			boolean changed_after_loading) {
		this.setDirty(changed_after_loading);
		notifyListeners(Event.UPDATE_IMAGE_VIEW_ON_CHANGES,
				new Pair<String, Boolean>("all", this.isDirty()));
	}

	public void setChangedAfterLoading(String imageUri,
			boolean changed_after_loading) {
		setDirty(changed_after_loading);
		notifyListeners(Event.UPDATE_IMAGE_VIEW_ON_CHANGES,
				new Pair<String, Boolean>(imageUri, this.isDirty()));
	}

	public boolean isDirty() {
		return changed_after_loading;
	}

	public boolean getDirty() {
		return changed_after_loading;
	}
	
	public void setDirty(boolean dirty){
		changed_after_loading = dirty;
	}
	
	/**
	 * Are there created links ?
	 * 
	 * @return boolean
	 */
	public boolean areThereLinks() {
		return (all_links.size() > 0);
	}

	/**
	 * Updates this instance with the data from the dialog.
	 * 
	 * @param selected_project
	 * @param entered_title
	 */
	public void updateDataFromSelectNameLocationDialog(
			TextGridProject selected_project, String entered_title) {
		this.selected_project = selected_project;
		this.entered_title = entered_title;
	}

	private TextGridObject selected_object_dialog = null;

	public void updateDataFromSelectLinkobjectDialog(
			TextGridObject selected_object) {
		this.selected_object_dialog = selected_object;
	}

	/**
	 * Update the text segments in the active xml editor
	 */
	public void updateTextSegments() {
		try {
			for (ILink l : all_links) {
				if (l.getTextUri().equals(this.textUri) && l.isVisible()) {
					Point p = getSartEndForLink(l);
					if (p != null) {
						if (!l.getShape().isLinked()) {
							l.getShape().setLinked(true);
							notifyListeners(Event.REDRAW,
									new Pair<String, Object>(l.getImageUri(),
											null));
						}

						if (l.getUnselectedAnnotation() == null)
							addUnselectedAnnotation(p.x, p.y, "", l);

					} else {
						l.getShape().setLinked(false);
						notifyListeners(Event.REDRAW, new Pair<String, Object>(
								l.getImageUri(), null));
					}
				}
			}
		} catch (ConcurrentModificationException e) {
			Activator.handleWarning(e);
		}
	}

	private Job updateJob = null;

	private void updateStartEndMap(boolean isBlocking) {

		if (doc == null)
			return;

		updateJob = new Job("Updating the database...") {

			@Override
			protected IStatus run(IProgressMonitor arg0) {

				startEndMap.clear();

				for (ILink l : all_links) {
					String startAnchorId = l.getStartAnchorId(), endAnchorId = l
							.getEndAnchorId();

					if (!"".equals(endAnchorId)) {

						Point p_start = getNodeById(startAnchorId);
						Point p_end = getNodeById(endAnchorId);

						if (p_start != null && p_end != null) {
							Point p = new Point(p_start.x, p_end.y);
							startEndMap.put(l.getLink(), p);
						}
					} else {
						Point p = getNodeById(startAnchorId);
						if (p != null) {
							startEndMap.put(l.getLink(), p);
						}
					}
				}
				return Status.OK_STATUS;
			}
		};

		updateJob.schedule();

		if (isBlocking) {
			try {
				updateJob.join();
			} catch (InterruptedException e) {
				Activator.handleWarning(e);
			}
		}
	}

	private final Map<String, Point> startEndMap = Collections
			.synchronizedMap(new HashMap<String, Point>());

	private boolean linksInfoUpdated = false;

	private Point getSartEndForLink(ILink l) {

		if (doc == null)
			return null;

		if (linksInfoUpdated) {
			updateStartEndMap(true);
			linksInfoUpdated = false;
		}

		Point pFound = startEndMap.get(l.getLink());
		if (pFound != null) {
			return pFound;
		}

		String startAnchorId = l.getStartAnchorId(), endAnchorId = l
				.getEndAnchorId();

		if (!"".equals(endAnchorId)) {
			Point p_start = getNodeById(startAnchorId);
			Point p_end = getNodeById(endAnchorId);

			if (p_start != null && p_end != null) {
				Point p = new Point(p_start.x, p_end.y);
				startEndMap.put(l.getLink(), p);
				return p;
			}
		} else {
			Point p = getNodeById(startAnchorId);
			if (p != null) {
				startEndMap.put(l.getLink(), p);
				return p;
			}
		}

		return null;
	}

	String percentToRect(String rectPercent, int imageWidth, int imageHeight) {
		if (imageWidth == 0 || imageHeight == 0)
			return null;

		String[] strA_Split = rectPercent.split(","); //$NON-NLS-1$
		double help = 0;

		help = (Double.valueOf(strA_Split[0].trim()).doubleValue() * imageWidth) / 100.0;
		rectPercent = help + ","; //$NON-NLS-1$
		help = (Double.valueOf(strA_Split[1].trim()).doubleValue() * imageHeight) / 100.0;
		rectPercent += help + ","; //$NON-NLS-1$
		help = (Double.valueOf(strA_Split[2].trim()).doubleValue() * imageWidth) / 100.0;
		rectPercent += (int) help + ","; //$NON-NLS-1$
		help = (Double.valueOf(strA_Split[3].trim()).doubleValue() * imageHeight) / 100.0;
		rectPercent += help;

		return rectPercent;
	}

	String percentToPoly(String str_polyPercent, int imageWidth, int imageHeight) {
		if (imageWidth == 0 || imageHeight == 0)
			return null;

		String[] strA_Split = str_polyPercent.split(","); //$NON-NLS-1$
		double help = 0;
		str_polyPercent = ""; //$NON-NLS-1$

		int i = 0;
		for (String s : strA_Split) {
			if (i % 2 == 0) {
				// x-coordinate
				help = (Double.valueOf(s.trim()).doubleValue() * imageWidth) / 100.0;
				str_polyPercent += help + ","; //$NON-NLS-1$
			} else {
				// y-coordinate
				help = (Double.valueOf(s.trim()).doubleValue() * imageHeight) / 100.0;
				str_polyPercent += help
						+ ((i == strA_Split.length - 1) ? "" : ","); //$NON-NLS-1$ //$NON-NLS-2$
			}
			i++;
		}

		return str_polyPercent;
	}

	String percentToDockingLine(String str_dockingLinePercent, int imageWidth,
			int imageHeight) {
		if (imageWidth == 0 || imageHeight == 0)
			return null;

		String[] strA_Split = str_dockingLinePercent.split(","); //$NON-NLS-1$
		double help = 0;
		str_dockingLinePercent = ""; //$NON-NLS-1$

		int i = 0;
		for (String s : strA_Split) {
			if (i % 2 == 0) {
				// x-coordinate
				help = (Double.valueOf(s.trim()).doubleValue() * imageWidth) / 100.0;
				str_dockingLinePercent += help + ","; //$NON-NLS-1$
			} else {
				// y-coordinate
				help = (Double.valueOf(s.trim()).doubleValue() * imageHeight) / 100.0;
				str_dockingLinePercent += help
						+ ((i == strA_Split.length - 1) ? "" : ","); //$NON-NLS-1$ //$NON-NLS-2$
			}
			i++;
		}

		return str_dockingLinePercent;
	}

	/**
	 * Clones the links container.
	 * 
	 * @return
	 */
	public List<ILink> cloneAllLinks() {
		List<ILink> links = Collections
				.synchronizedList(new ArrayList<ILink>());
		links.addAll(all_links.getList());

		return links;
	}

	/**
	 * Jump to the annotated text
	 * 
	 * @param selectedShape
	 * @param jumpToShape
	 * 
	 * @return true if successful, false otherwise
	 */
	public boolean jumpToText(TGShape selectedShape, boolean jumpToShape) {
		if (editor == null) {
			return false;
		}

		try {
			Iterator<ILink> iter = all_links.iterator();
			while (iter.hasNext()) {
				ILink l = iter.next();
				TGShape s = l.getShape();
				if (s != null && s.equals(selectedShape) && l.isVisible()) {
					if (!l.getTextUri().equals(this.textUri)) {
						removeSelectedAnnotation();
						if (!bringXMLEditorToTop(l.getTextUri()))
							return false;
					}

					Point p = getSartEndForLink(l);

					int start, length;
					if (p != null) {
						start = p.x;
						length = p.y - start;

						removeSelectedAnnotation();

						addUnselectedAnnotation(start, p.y, "", l);
						addSelectedAnnotation(start, p.y, "", l);

						editor.setHighlightRange(start, length, true);

						if (jumpToShape)
							notifyListeners(Event.JUMP_TO_SHAPE,
									new Pair<String, TGShape>(l.getImageUri(),
											selectedShape));

						return true;
					}

					return false;
				}
			}
		} catch (ConcurrentModificationException e) {
			Activator.handleWarning(e);
		}

		return false;
	}

	private int oldCursorPos = 0;

	/**
	 * Jump to the correct shape of the selected link
	 * 
	 * @param cursorPosition
	 * @return true if successful or it was selected before otherwise false
	 */
	public boolean jumpToShape(int cursorPosition) {

		if (cursorPosition == oldCursorPos || selectedTextLength > 0)
			return false;

		int start, end, bestMatch = -1;
		ILink matchLink = null;

		try {
			Iterator<ILink> iter = all_links.iterator();
			while (iter.hasNext()) {
				ILink l = iter.next();
				if (l.getTextUri().equals(this.textUri) && l.isVisible()) {
					Point p = getSartEndForLink(l);

					if (p != null) {
						start = p.x;
						end = p.y;

						// if the click is positioned between start and end
						if (cursorPosition >= start && cursorPosition < end) {
							if (bestMatch < 0 || bestMatch < start) {
								bestMatch = start;
								matchLink = l;
							}
						}
					}
				}
			}
		} catch (ConcurrentModificationException e) {
			Activator.handleWarning(e);
		}

		if (matchLink != null) {
			selectedShape = matchLink.getShape();
			if (selectedShape != null) {
				notifyListeners(Event.JUMP_TO_SHAPE, new Pair<String, TGShape>(
						matchLink.getImageUri(), selectedShape));
			}
			return true;
		}

		return false;
	}

	private boolean updateFlag = false;

	private boolean bringXMLEditorToTop(String xmlObjUri) {
		IEditorReference[] editorRefs = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.getEditorReferences();
		for (IEditorReference eRef : editorRefs) {
			IEditorPart part = eRef.getEditor(false);
			ITextEditor e = AdapterUtils.getAdapter(part, ITextEditor.class);
			if (e != null
					&& e.getEditorInput().getName().toLowerCase()
							.endsWith("xml")) {
				TextGridObject tgO = (TextGridObject) e.getEditorInput()
						.getAdapter(TextGridObject.class);

				if (tgO != null) {
					String uri = tgO.getURI().toString();
					if (xmlObjUri.equals(uri)) {
						try {
							PlatformUI
									.getWorkbench()
									.getActiveWorkbenchWindow()
									.getActivePage()
									.openEditor(e.getEditorInput(),
											e.getEditorSite().getId());
							return true;
						} catch (PartInitException e1) {
							Activator.handleError(e1,
									"Couldn't bring the editor to foreground!");
							return false;
						}
					}
				} else {
					// local xml-file
					IEditorInput eIn = e.getEditorInput();
					if (eIn instanceof FileStoreEditorInput) {
						FileStoreEditorInput fIn = (FileStoreEditorInput) eIn;
						String uri = fIn.getURI().toString();

						if (xmlObjUri.equals(uri)) {
							try {
								PlatformUI
										.getWorkbench()
										.getActiveWorkbenchWindow()
										.getActivePage()
										.openEditor(e.getEditorInput(),
												e.getEditorSite().getId());
								return true;
							} catch (PartInitException e1) {
								Activator
										.handleError(e1,
												"Couldn't bring the editor to foreground!");
								return false;
							}
						}
					}
				}
			}
		}

		return false;
	}

	@Deprecated
	private boolean bringImageViewToTop(String imageObjUri) {
		for (Map.Entry<String, String> entry : imageTabsMap.entrySet()) {
			if (entry.getValue().equals(imageObjUri)) {
				String key = entry.getKey();
				IWorkbenchPage page = Workbench.getInstance()
						.getActiveWorkbenchWindow().getActivePage();
				try {
					page.showView(IMAGE_VIEW_ID, key,
							IWorkbenchPage.VIEW_ACTIVATE);
					return true;
				} catch (PartInitException e) {
					Activator.handleError(e,
							"Couldn't bring the image view to top!");
					return false;
				}
			}
		}

		return false;
	}

	public void setSelectedShape(TGShape shape) {
		this.selectedShape = shape;
	}

	public TGShape getSelectedShape() {
		return this.selectedShape;
	}

	public void addSelectedShape(TGShape shape) {
		selectedShapes.add(shape);
	}

	public void clearSelectedShapes() {
		selectedShapes.clear();
	}

	public void deselectAllSelectedShapes() {
		notifyListeners(Event.DESELECT_SHAPE, new Pair<String, Object>("all",
				null));
	}

	/**
	 * Update an existing link by assigning a new shape
	 * 
	 * @param oldShape
	 * @param newShape
	 */
	public void updateLink(TGShape oldShape, TGShape newShape) {
		for (int i = 0; i < all_links.size(); ++i) {
			ILink l = all_links.get(i);
			TGShape s = l.getShape();
			if (s != null && s.equals(oldShape)) {
				if (s instanceof TGRectangle) {
					RectLink newLink = new RectLink((TGRectangle) newShape,
							l.getStartAnchorId(), l.getEndAnchorId(),
							l.getTextUri(), l.isVisible());
					newLink.getShape()
							.setWritingMode(newShape.getWritingMode());
					all_links.remove(i);
					startEndMap.remove(l.getLink());
					all_links.add(i, newLink);
				} else if (s instanceof TGPolygon) {
					PolyLink newLink = new PolyLink((TGPolygon) newShape,
							l.getStartAnchorId(), l.getEndAnchorId(),
							l.getTextUri(), l.isVisible());
					newLink.getShape()
							.setWritingMode(newShape.getWritingMode());
					all_links.remove(i);
					startEndMap.remove(l.getLink());
					all_links.add(i, newLink);
				}
				setChangedAfterLoading(true);
				break;
			}
		}
	}

	/**
	 * Update an existing link
	 * 
	 * @param shape
	 */
	public void updateLink(TGShape shape) {
		ILink linkInShape = shape.getAssocLink();
		boolean found = false;
		for (int i = 0; i < all_links.size(); ++i) {
			ILink l = all_links.get(i);

			if (l.equalsExceptShape(linkInShape)) {
				ILink newLink = null;
				if (shape instanceof TGRectangle) {
					newLink = new RectLink((TGRectangle) shape,
							l.getStartAnchorId(), l.getEndAnchorId(),
							l.getTextUri(), l.isVisible());
					newLink.getShape().setWritingMode(shape.getWritingMode());
					all_links.remove(i);
					all_links.add(i, newLink);
				} else if (shape instanceof TGPolygon) {
					newLink = new PolyLink((TGPolygon) shape,
							l.getStartAnchorId(), l.getEndAnchorId(),
							l.getTextUri(), l.isVisible());
					newLink.getShape().setWritingMode(shape.getWritingMode());
					all_links.remove(i);
					all_links.add(i, newLink);
				}
				selectedShape = shape;
				found = true;
				setChangedAfterLoading(true);

				break;
			}
		}
		shape.setLinked(found);
		linksInfoUpdated = true;
	}

	private boolean isReplacingText = false;

	private boolean removeSegmentFromXMLEditor(ILink l, boolean removeText) {
		int start, end;

		if (!this.textUri.equals(l.getTextUri())) {
			bringXMLEditorToTop(l.getTextUri());
		}

		Point p = getSartEndForLink(l);
		if (p != null) {
			start = p.x;
			end = p.y;

			try {
				if (removeText && !"".equals(l.getEndAnchorId())) {
					String t = doc.get(start, end - start);
					t = t.replaceAll("(?s)<anchor[^<>]*?\\s+xml:id\\s*=\\s*\"("
							+ l.getStartAnchorId() + "|" + l.getEndAnchorId()
							+ ")\"[^<>]*?/>", "");
					isReplacingText = true;
					doc.replace(start, end - start, t);
					isReplacingText = false;
				}
			} catch (BadLocationException e) {
				Activator.handleError(e);
				return false;
			}
			return true;
		}

		return false;
	}

	/**
	 * Remove all links, that are related to the imageUri
	 * 
	 * @param imageUri
	 */
	public void removeAllLinks(String imageUri) {
		if (all_links.isEmpty())
			return;

		for (Iterator<ILink> iter = all_links.iterator(); iter.hasNext();) {
			ILink l = iter.next();
			if (l.getImageUri().equals(imageUri)) {
				removeSegmentFromXMLEditor(l, true);
				iter.remove();
			}
		}

		removeAllAnnotations();

		linksInfoUpdated = true;

		setChangedAfterLoading(true);
		notifyListeners(Event.LINKS_CHANGED, new Pair<String, Object>(
				focusedImageTabSecondaryId, null));
	}

	public void removeLinkByShape(TGShape shape) {
		for (ILink l : all_links) {
			TGShape s = l.getShape();
			if (s != null && s.equals(shape)) {
				if (!this.textUri.equals(l.getTextUri()))
					bringXMLEditorToTop(l.getTextUri());
				removeAnnotation(l);
				removeSegmentFromXMLEditor(l, true);
				all_links.remove(l);

				removeAnnotation(l);
				linksInfoUpdated = true;

				setChangedAfterLoading(true);
				notifyListeners(Event.LINKS_CHANGED, new Pair<String, Object>(
						focusedImageTabSecondaryId, null));
				break;
			}
		}
	}

	public void removeSelectedLink() {
		if (this.selectedShape == null) {
			msg_info.setText("");
			msg_info.setMessage(Messages.LinkEditorController_Mess_NoSelectedShape);
			msg_info.open();
			return;
		}

		for (ILink l : all_links) {
			TGShape s = l.getShape();
			if (s != null && s.equals(this.selectedShape)) {
				if (!this.textUri.equals(l.getTextUri()))
					bringXMLEditorToTop(l.getTextUri());

				removeAnnotation(l);

				if (showWarnings) {
					msg_asking_2
							.setMessage(Messages.LinkEditorController_Mess_DeleteSelectedLink_1);
					msg_asking_2
							.setText(Messages.LinkEditorController_Mess_DeleteSelectedLink_2);
					if (msg_asking_2.open() == SWT.YES) {
						removeSegmentFromXMLEditor(l, true);
						all_links.remove(l);
						linksInfoUpdated = true;
						setChangedAfterLoading(true);
						notifyListeners(Event.LINKS_CHANGED,
								new Pair<String, Object>(
										focusedImageTabSecondaryId, null));
						notifyListeners(Event.REMOVE_SHAPE,
								new Pair<String, TGShape>(
										focusedImageTabSecondaryId, s));
						break;
					}
				} else {
					removeSegmentFromXMLEditor(l, true);
					all_links.remove(l);
					linksInfoUpdated = true;
					setChangedAfterLoading(true);
					notifyListeners(Event.LINKS_CHANGED,
							new Pair<String, Object>(
									focusedImageTabSecondaryId, null));
					notifyListeners(Event.REMOVE_SHAPE,
							new Pair<String, TGShape>(
									focusedImageTabSecondaryId, s));
					break;
				}
			}
		}
	}

	/**
	 * Just unlink the selected links
	 */
	public void unlinkSelectedLinks() {

		boolean asked = false;

		if (this.selectedShape == null) {
			msg_info.setText("");
			msg_info.setMessage(Messages.LinkEditorController_Mess_NoSelectedShape);
			msg_info.open();
			return;
		}

		for (Iterator<ILink> iter = all_links.iterator(); iter.hasNext();) {
			ILink l = iter.next();
			TGShape s = l.getShape();
			if (s != null) {
				if (selectedShapes.isEmpty() && s.equals(this.selectedShape)) {
					if (!this.textUri.equals(l.getTextUri()))
						bringXMLEditorToTop(l.getTextUri());
					if (showWarnings) {
						msg_asking_2.setText("Unlink selected link");
						msg_asking_2
								.setMessage("Do you really want to unlink the selected link?");
						if (msg_asking_2.open() == SWT.YES) {
							iter.remove();
							removeAnnotation(l);
							removeSegmentFromXMLEditor(l, true);
							l.getShape().setLinked(false);

							break;
						}
					} else {
						iter.remove();
						removeAnnotation(l);
						removeSegmentFromXMLEditor(l, true);
						l.getShape().setLinked(false);

						break;
					}
				} else if (!selectedShapes.isEmpty()
						&& selectedShapes.contains(s)) {

					if (!asked && showWarnings) {
						asked = true;
						msg_asking_2.setText("Unlink selected links");
						msg_asking_2
								.setMessage("Do you really want to unlink the selected links?");
						if (msg_asking_2.open() != SWT.YES) {
							return;
						}
					}

					if (!this.textUri.equals(l.getTextUri()))
						bringXMLEditorToTop(l.getTextUri());

					iter.remove();
					l.getShape().setLinked(false);
					removeAnnotation(l);
					removeSegmentFromXMLEditor(l, true);
				}
			}
		}

		linksInfoUpdated = true;
		setChangedAfterLoading(true);

		notifyListeners(Event.REDRAW, new Pair<String, Object>(
				focusedImageTabSecondaryId, null));
	}

	public void saveThenClearLinks() {
		if (isDirty() && showWarnings) {
			msg_asking_2
					.setMessage(Messages.LinkEditorController_Mess_SaveCurrentChanges_1); // displayed
			// message
			msg_asking_2
					.setText(Messages.LinkEditorController_Mess_SaveCurrentChanges_2); // displayed
			// title
			if (msg_asking_2.open() == SWT.YES) {
				saveAnnotatedObject(false, false, true);
			}
		}
		all_links.clear();
		setChangedAfterLoading_AllImageViews(false);
	}

	public void addListener(ILinkEditorListener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(ILinkEditorListener listener) {
		return listeners.remove(listener);
	}

	public void notifyListeners(Event event, Pair<String, ?> pair) {
		try {
			for (ILinkEditorListener l : listeners) {
				l.update(event, pair);
			}
		} catch (ConcurrentModificationException e) {
			Activator.handleWarning(e);
		}
	}

	public void textGridObjectChanged(
			info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener.Event event,
			TextGridObject object) {
		if (object != null && object == selected_object) {
			if (event == info.textgrid.lab.core.model.TextGridObject.ITextGridObjectListener.Event.DELETED) {
				selected_object = loaded_object = null;
				// setChangedAfterLoading(true);
				setDirty(true);
				notifyListeners(Event.UPDATE_IMAGE_VIEW_ON_CHANGES,
						new Pair<String, Boolean>("all",
								this.isDirty()));
			}
		}
	}

	public void chooseFont(StyledText text) {
		FontDialog fontdlg = new FontDialog(PlatformUI.getWorkbench()
				.getDisplay().getActiveShell());
		fontdlg.setText("FontDialog");
		FontData selectedFont = fontdlg.open();
		Font font = null;
		Color color = null;
		if (selectedFont != null) {
			font = new Font(PlatformUI.getWorkbench().getDisplay(),
					fontdlg.getFontList());
			text.setFont(font);
			// TODO it doesn't work at the moment?!
			color = new Color(PlatformUI.getWorkbench().getDisplay(),
					fontdlg.getRGB());
			text.setForeground(color);
		}
	}

	public void setWritingMode(TGShape shape, WRITING_MODE writingMode) {
		if (shape == null)
			return;

		TGShape newShape = null;

		if (shape instanceof TGRectangle)
			newShape = new TGRectangle(((TGRectangle) shape).getRectangle(),
					shape.getImageUri());
		else if (shape instanceof TGPolygon)
			newShape = new TGPolygon(((TGPolygon) shape).getPoints(),
					shape.getImageUri());

		newShape.setWritingMode(writingMode);
		newShape.setRotationValue(shape.getRotationValue());
		newShape.setLayerValues(new Integer(shape.getLayer()),
				shape.getLayerName(), shape.getLayerRGB());

		updateLink(shape, newShape);
	}

	WRITING_MODE getWritingModeFromString(String mode) {
		if (mode == null || "".equals(mode) || mode.equals("none"))
			return WRITING_MODE.NONE;

		if (mode.equals("lr"))
			return WRITING_MODE.LR;

		if (mode.equals("lr-tb"))
			return WRITING_MODE.LR_TB;

		if (mode.equals("rl"))
			return WRITING_MODE.RL;

		if (mode.equals("rl-tb"))
			return WRITING_MODE.RL_TB;

		if (mode.equals("tb"))
			return WRITING_MODE.TB;

		if (mode.equals("tb-rl"))
			return WRITING_MODE.TB_RL;

		return WRITING_MODE.NONE;
	}

	/**
	 * Check the given uri and add the revision number to it if necessary.
	 * 
	 * @param uri
	 * @param isImageUri
	 */
	public String getRevisionedUri(final String uri, final boolean isImageUri) {

		if (isImageUri) {
			for (String s : imageInfoMap.keySet()) {
				if (s.contains(".")) {
					if ((s.split("\\.")[0]).equals(uri)) {
						return s;
					}
				}
			}
		} else {
			// xml uri
			for (String s : openXmlUris) {
				if (s.contains(".")) {
					if ((s.split("\\.")[0]).equals(uri)) {
						return s;
					}
				}
			}
		}

		// if nothing found, then add the revision's number 0
		return (uri + ".0");
	}

	/**
	 * Get the width/height of an image
	 * 
	 * @param imageUri
	 */
	public Point getImageWidthHeight(String imageUri) {
		return imageInfoMap.get(imageUri);
	}

	private Set<String> saveDockingLinesUriList = Collections
			.synchronizedSet(new HashSet<String>());

	/**
	 * Save all created dockingLines on the active imageView
	 */
	public void saveAllDockingLines(String imageUri) {
		saveDockingLinesUriList.add(imageUri);
		setChangedAfterLoading(true);
		saveAnnotatedObject(false, false, false);
	}

	/**
	 * Remove all docking lines on the active imageView
	 */
	public void removeAllDockingLineTags(String imageUri) {
		saveDockingLinesUriList.remove(imageUri);
		setChangedAfterLoading(imageUri, true);
	}

	void addTosaveDockingLinesUriList(String imageUri) {
		saveDockingLinesUriList.add(imageUri);
	}

	/**
	 * Selection in xml-editor changed
	 */
	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		ITextSelection selection = (ITextSelection) event.getSelection();
		selectedText = selection.getText();

		selectedTextStart = selection.getOffset();
		selectedTextLength = (selectedText.length() > 0) ? selection
				.getLength() : 0;

		// jump in WYSIWYM mode
		jumpToShape(selectedTextStart);
		oldCursorPos = selectedTextStart;
	}

	/**
	 * Selection in xml-doc changed
	 */
	@Override
	public void documentChanged(DocumentEvent event) {
		if (!isReplacingText) {
			linksInfoUpdated = true;
		}
	}

	@Override
	public void documentAboutToBeChanged(DocumentEvent event) {
		// TODO Auto-generated method stub
	}

	// --- MouseListener for the Text-Selections

	@Override
	public void mouseUp(MouseEvent e) {
		// jump in source mode
		int cursorPos = text.getSelection().x;
		selectedTextStart = cursorPos;
		jumpToShape(cursorPos);
		oldCursorPos = cursorPos;
	}

	@Override
	public void mouseDoubleClick(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseDown(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	private boolean textFileOpen = false;

	/**
	 * Links the xml-editor with this controller
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (part != null && part instanceof IEditorPart) {
			IEditorPart ePart = (IEditorPart) part;
			if (ePart == null) {
				Activator.handleError(new Exception("No editor open!"));
			} else {
				if (!PlatformUI
						.getWorkbench()
						.getActiveWorkbenchWindow()
						.getActivePage()
						.getPerspective()
						.getId()
						.equals("info.textgrid.lab.linkeditor.rcp_linkeditor.perspective")) {
					return;
				}

				ITextEditor tempEditor = editor;
				editor = AdapterUtils.getAdapter(ePart, ITextEditor.class);
				if (editor != null) {

					if (editor.getEditorInput().getName().toLowerCase()
							.endsWith("xml")) {

						textFileOpen = false;

						TextGridObject objFromXMLEditor = (TextGridObject) editor
								.getEditorInput().getAdapter(
										TextGridObject.class);

						if (objFromXMLEditor != null) { // get the uri of
														// the
							// xml-object from the TGO
							this.textUri = objFromXMLEditor.getURI().toString();
						} else {
							// local xml-file
							IEditorInput eIn = editor.getEditorInput();
							if (eIn instanceof FileStoreEditorInput) {
								FileStoreEditorInput fIn = (FileStoreEditorInput) eIn;
								this.textUri = fIn.getURI().toString();
							}
						}

						if (forceGettingEditor || !editor.equals(tempEditor)) {
							if (annoModel != null)
								removeSelectedAnnotation();

							editor.getSelectionProvider()
									.addSelectionChangedListener(this);

							editor.getEditorSite().getPage()
									.addPartListener(this);

							IDocumentProvider dp = editor.getDocumentProvider();
							doc = dp.getDocument(editor.getEditorInput());

							doc.addDocumentListener(this);

							try {
								//Check if a structuredtexteditor is opened or
								//if another editor like oxygen is opened 
								if(editor instanceof StructuredTextEditor){
								
								StructuredTextViewer viewer = ((StructuredTextEditor) editor)
										.getTextViewer();

								annoModel = viewer.getAnnotationModel();

								text = viewer.getTextWidget();
								text.addMouseListener(this);

								if (updateFlag
										&& unsegmentedOpenXmlUris
												.contains(textUri)) {
									updateTextSegments();
									unsegmentedOpenXmlUris.remove(textUri);
								}
								}

							} catch (ClassCastException e) {
								Activator.handleError(e);
							}
						}
					} else {
						textFileOpen = true;
					}
				}
			}
		} else if (part instanceof ILinkEditorListener
				&& selection instanceof IStructuredSelection) {
			Object[] objs = ((IStructuredSelection) selection).toArray();
			if (objs.length > 1) {
				Object obj = objs[1];
				if (obj != null && obj instanceof String) {
					String sel = (String) obj;
					if (sel.contains("image")) {
						this.imageUri = sel;
					}
				}
			}
		}
	}

	private boolean forceGettingEditor = false;

	/**
	 * Handle the selected xml node in the xml editor.
	 * 
	 * @param position
	 */
	private ElementImpl getSelectedXMLNode(int position) {
		if (doc == null)
			return null;

		IStructuredModel model = null;
		try {

			model = StructuredModelManager.getModelManager().getModelForRead(
					(IStructuredDocument) doc);

			if (model == null) {
				forceGettingEditor = true;
				selectionChanged(PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage()
						.getActiveEditor(), null);
				forceGettingEditor = false;

				model = StructuredModelManager.getModelManager()
						.getModelForRead((IStructuredDocument) doc);
				if (model == null)
					return null;
			}

			IndexedRegion region = model.getIndexedRegion(position);
			if (region != null && region instanceof ElementImpl) {
				ElementImpl elem = (ElementImpl) region;
				return elem;
			}
		} finally {
			if (model != null)
				model.releaseFromRead();
		}

		return null;
	}

	private Point getNodeById(String id) {
		IStructuredModel model = null;
		try {
			model = StructuredModelManager.getModelManager().getModelForRead(
					(IStructuredDocument) doc);

			IStructuredDocumentRegion[] regions = model.getStructuredDocument()
					.getStructuredDocumentRegions();

			for (IStructuredDocumentRegion r : regions) {
				int position = r.getStartOffset();
				IndexedRegion region = model.getIndexedRegion(position);

				if (region != null && region instanceof ElementImpl) {
					ElementImpl elem = (ElementImpl) region;
					String attrId = elem.getAttribute("xml:id");

					if (attrId != null && attrId.equals(id)) {
						return new Point(elem.getStartOffset(),
								elem.getEndOffset());
					}
				}

			}

			return null;
		} finally {
			if (model != null)
				model.releaseFromRead();
		}
	}

	private List<TGShape> allShapes = Collections
			.synchronizedList(new ArrayList<TGShape>());

	private List<TGShape> updateShapesList(String imageUri) {
		allShapes.clear();
		notifyListeners(Event.UPDATE_SHAPES_LIST,
				new Pair<String, List<TGShape>>(imageUri, allShapes));
		return allShapes;
	}

	/**
	 * Update the rotation's value of the shape
	 * 
	 * @param shape
	 */
	public void updateRotationValue(TGShape shape) {
		if (shape == null)
			return;

		TGShape newShape = null;

		if (shape instanceof TGRectangle)
			newShape = new TGRectangle(((TGRectangle) shape).getRectangle(),
					shape.getImageUri());
		else if (shape instanceof TGPolygon)
			newShape = new TGPolygon(((TGPolygon) shape).getPoints(),
					shape.getImageUri());

		newShape.setRotationValue(shape.getRotationValue());
		newShape.setWritingMode(shape.getWritingMode());
		newShape.setLayerValues(new Integer(shape.getLayer()),
				shape.getLayerName(), shape.getLayerRGB());

		updateLink(shape, newShape);

	}

	/**
	 * Update the Layer values of the shape
	 * 
	 * @param shape
	 */
	public void updateLayerValues(TGShape shape) {
		if (shape == null)
			return;

		TGShape newShape = null;

		if (shape instanceof TGRectangle)
			newShape = new TGRectangle(((TGRectangle) shape).getRectangle(),
					shape.getImageUri());
		else if (shape instanceof TGPolygon)
			newShape = new TGPolygon(((TGPolygon) shape).getPoints(),
					shape.getImageUri());

		newShape.setRotationValue(shape.getRotationValue());
		newShape.setWritingMode(shape.getWritingMode());
		newShape.setLayerValues(new Integer(shape.getLayer()),
				shape.getLayerName(), shape.getLayerRGB());

		updateLink(shape, newShape);

	}

	private Annotation selectedAnnotation = null;

	private void addSelectedAnnotation(int start, int end, String text,
			ILink link) {
		if (annoModel == null)
			return;
		Position position = new Position(start, end - start);
		Annotation annotation = new Annotation(ANNOTATION_TYPE_SELECTED, false,
				"Selected Link " + text);
		synchronized (annoModel) {
			annoModel.addAnnotation(annotation, position);
			selectedAnnotation = annotation;
			link.setSelectedAnnotation(annotation);
		}
	}

	private void addUnselectedAnnotation(int start, int end, String text,
			ILink link) {

		if (annoModel == null)
			return;

		Position position = new Position(start, end - start);
		Annotation annotation = new Annotation(ANNOTATION_TYPE_UNSELECTED,
				false, "Image-Link-Editor Link " + text);

		Annotation oldUnselected = link.getUnselectedAnnotation();

		synchronized (annoModel) {
			if (oldUnselected != null)
				annoModel.removeAnnotation(oldUnselected);

			annoModel.addAnnotation(annotation, position);
			link.setUnselectedAnnotation(annotation);
		}
	}

	public void removeAnnotation(ILink link) {
		if (link == null || annoModel == null)
			return;

		Annotation annoSelected = link.getSelectedAnnotation();
		if (annoSelected != null) {
			synchronized (annoModel) {
				annoModel.removeAnnotation(annoSelected);
			}
		}

		Annotation annoUnselected = link.getUnselectedAnnotation();
		if (annoUnselected != null) {
			synchronized (annoModel) {
				annoModel.removeAnnotation(annoUnselected);
			}
		}

		link.setSelectedAnnotation(null);
		link.setUnselectedAnnotation(null);
	}

	@SuppressWarnings("unchecked")
	private void removeSelectedAnnotation() {
		if (annoModel == null)
			return;

		if (selectedAnnotation != null) {
			synchronized (annoModel) {
				annoModel.removeAnnotation(selectedAnnotation);
				selectedAnnotation = null;
			}
		} else {
			// It should not come here!
			for (Iterator<Annotation> it = annoModel.getAnnotationIterator(); it
					.hasNext();) {
				Annotation anno = (Annotation) it.next();
				String type = anno.getType();
				if (type != null && type.equals(ANNOTATION_TYPE_SELECTED)) {
					synchronized (annoModel) {
						annoModel.removeAnnotation(anno);
						selectedAnnotation = null;
					}
					break;
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Deprecated
	public void removeAllUnselectedAnnotations() {
		if (annoModel == null)
			return;

		for (Iterator<Annotation> it = annoModel.getAnnotationIterator(); it
				.hasNext();) {
			Annotation anno = (Annotation) it.next();
			String type = anno.getType();
			if (type != null && type.equals(ANNOTATION_TYPE_UNSELECTED)) {
				synchronized (annoModel) {
					annoModel.removeAnnotation(anno);
				}
				// anno.markDeleted(true);
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Deprecated
	private void removeAllAnnotations() {
		if (annoModel == null)
			return;

		for (Iterator<Annotation> it = annoModel.getAnnotationIterator(); it
				.hasNext();) {
			Annotation anno = (Annotation) it.next();
			synchronized (annoModel) {
				annoModel.removeAnnotation(anno);
			}
			// anno.markDeleted(true);
		}
		selectedAnnotation = null;
	}

	private WRITING_MODE writingMode = WRITING_MODE.NONE;

	public WRITING_MODE getDefaultWritingMode(String imageUri) {
		notifyListeners(Event.GET_DEFAULT_WRITING_MODE,
				new Pair<String, Object>(imageUri, null));

		return writingMode;
	}

	public void setDefaultWritingMode(String imageUri, WRITING_MODE writingMode) {
		notifyListeners(Event.SET_DEFAULT_WRITING_MODE,
				new Pair<String, WRITING_MODE>(imageUri, writingMode));
	}

	public void setWritingModeFromImageView(WRITING_MODE writingMode) {
		this.writingMode = writingMode;
	}

	// --- begin: The part-listener methods
	public void partActivated(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub
	}

	public void partBroughtToTop(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub
	}

	public void partClosed(IWorkbenchPartReference arg0) {

		IEvaluationService evalService = (IEvaluationService) PlatformUI
				.getWorkbench().getService(IEvaluationService.class);
		evalService
				.requestEvaluation("info.textgrid.lab.linkeditor.rcp_linkeditor.testers.ActivateOperations");
		evalService
				.requestEvaluation("info.textgrid.lab.linkeditor.rcp_linkeditor.testers.ActivateReset");

		IWorkbenchPage workbenchPage = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();

		try {
			if (workbenchPage == null
					|| !workbenchPage
							.getPerspective()
							.getId()
							.equals("info.textgrid.lab.linkeditor.rcp_linkeditor.perspective")) {
				return;
			}
		} catch (NullPointerException e) {
			return; // TG-859
		}

		if (arg0 != null && arg0 instanceof IEditorReference) {
			ITextEditor e = AdapterUtils.getAdapter(
					((IEditorReference) arg0).getEditor(false),
					ITextEditor.class);
			if (e != null && e.equals(editor)) {
				doc = null;
				annoModel = null;
				editor = null;
			}
		}

		int image_parts = 0, editor_parts = 0;

		IViewReference[] refs = workbenchPage.getViewReferences();

		for (IViewReference r : refs) {
			if (r.getId().equals(IMAGE_VIEW_ID))
				image_parts++;
		}

		IEditorReference[] eRefs = workbenchPage.getEditorReferences();

		for (IEditorReference r : eRefs) {
			try {
				if (r.getEditorInput().getName().toLowerCase().endsWith("xml"))
					editor_parts++;
			} catch (PartInitException e) {
				Activator.handleError(e);
			}
		}

		if ((image_parts + editor_parts) == 0) {
			reset(false);
		} else {
//			reset(true);
		}
	}

	public void partDeactivated(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub
	}

	public void partHidden(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stu
	}

	public void partInputChanged(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub
	}

	public void partOpened(IWorkbenchPartReference arg0) {
		IEvaluationService evalService = (IEvaluationService) PlatformUI
				.getWorkbench().getService(IEvaluationService.class);
		if (evalService != null) {
			evalService
					.requestEvaluation("info.textgrid.lab.linkeditor.rcp_linkeditor.testers.ActivateOperations");
			evalService
					.requestEvaluation("info.textgrid.lab.linkeditor.rcp_linkeditor.testers.ActivateReset");
		}
	}

	public void partVisible(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub
	}

	// --- end: The part-listener methods

	// --- begin: The perspective-listener methods
	public void perspectiveClosed(IWorkbenchPage page,
			IPerspectiveDescriptor perspective) {
		if (perspective.getId().equals(
				"info.textgrid.lab.linkeditor.rcp_linkeditor.perspective")) {
			reset(false);
		}
	}

	public void perspectiveDeactivated(IWorkbenchPage page,
			IPerspectiveDescriptor perspective) {
		// TODO Auto-generated method stub

	}

	public void perspectiveOpened(IWorkbenchPage page,
			IPerspectiveDescriptor perspective) {
		// TODO Auto-generated method stub

	}

	public void perspectiveSavedAs(IWorkbenchPage page,
			IPerspectiveDescriptor oldPerspective,
			IPerspectiveDescriptor newPerspective) {
		// TODO Auto-generated method stub

	}

	public void perspectiveChanged(IWorkbenchPage page,
			IPerspectiveDescriptor perspective,
			IWorkbenchPartReference partRef, String changeId) {
		// TODO Auto-generated method stub

	}

	public void perspectiveActivated(IWorkbenchPage page,
			IPerspectiveDescriptor perspective) {
		// TODO Auto-generated method stub

	}

	public void perspectiveChanged(IWorkbenchPage page,
			IPerspectiveDescriptor perspective, String changeId) {
		if (perspective.getId().equals(
				"info.textgrid.lab.linkeditor.rcp_linkeditor.perspective")
				&& changeId.equals("reset"))
			reset(false);

	}

	// --- end: the perspective-listener-methods

	private String localAnnotatedFilePath = "";

	private boolean saveNewFile = false;

	/**
	 * Save the Text-Image-Link Object locally
	 * 
	 * @param saveAs
	 * @return
	 */
	public boolean saveLocalAnnotatedFile(final boolean saveAs) {

		Job savingTGOJob = new Job("Saving the object...") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {

				if (monitor == null)
					monitor = new NullProgressMonitor();
				try {

					monitor.beginTask("Saving the object...", 100);

					saveLocalAnnotatedFileImpl(saveAs, true, monitor);

					return Status.OK_STATUS;
				} catch (UnsupportedEncodingException e) {
					Activator.handleError(e);
				} catch (FactoryConfigurationError e) {
					Activator.handleError(e);
				} catch (XMLStreamException e) {
					Activator.handleError(e);
				} catch (IOException e) {
					Activator.handleError(e);
				}
				return Status.CANCEL_STATUS;

			}
		};

		savingTGOJob.setUser(false);
		savingTGOJob.schedule();

		return (savingTGOJob.getResult() == Status.OK_STATUS);
	}

	private String getPathFromUri(URI uri) {
		return new File(uri).getAbsolutePath();
	}

	private volatile boolean workLocal = false;

	/**
	 * Open a local file.
	 */
	public void openLocalFile() {

		try {
			if (!workLocal) {
				// Don't ask to login!
				RBACSession.getInstance().neverAsk(true);
			}

			if (!PlatformUI
					.getWorkbench()
					.getActiveWorkbenchWindow()
					.getActivePage()
					.getPerspective()
					.getId()
					.equals("info.textgrid.lab.linkeditor.rcp_linkeditor.perspective")) {
				PlatformUI
						.getWorkbench()
						.showPerspective(
								"info.textgrid.lab.linkeditor.rcp_linkeditor.perspective",
								PlatformUI.getWorkbench()
										.getActiveWorkbenchWindow());
			}

			FileDialog fileDlg = new FileDialog(PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getShell(), SWT.OPEN);
			fileDlg.setText("Open File...");
			fileDlg.setFilterExtensions(new String[] {
					"*.xml;*.jpg;*.jpeg;*.png;*.tiff;*.tif;*.tgl", "*.xml",
					"*.jpg", "*.jpeg", "*.png", "*.tiff", "*.tif", "*.tgl" });
			fileDlg.setFilterNames(new String[] { "All proper types",
					"XML file (*.xml)", "JPG file (*.jpg)",
					"JPEG file (*.jpeg)", "PNG file (*.png)",
					"TIFF file (*.tiff)", "TIFF file (*.tif)",
					"Text-Image-Link file (*.tgl)" });

			final String filePath = fileDlg.open();

			if (filePath != null && !"".equals(filePath)) {
				System.err.println(filePath);

				File file = new File(filePath);

				if (!file.exists() || !file.isFile())
					return;
				
				if (filePath.toLowerCase().endsWith("xml")) {
					new Job("") {
						@Override
						protected IStatus run(IProgressMonitor monitor) {
							openLocalXMLFile(filePath);
							return Status.OK_STATUS;
						}
					}.schedule();
				} else if (filePath.toLowerCase().endsWith("tgl")) {
					openLocalAnnotatedFile(filePath, file.getParent());
				} else if (filePath.toLowerCase().matches(
						".+\\.(jpg|jpeg|png|tiff|tif)$")) {
					new Job("") {
						@Override
						protected IStatus run(IProgressMonitor monitor) {
							openLocalImageFile(filePath);
							return Status.OK_STATUS;
						}
					}.schedule();
				}
			}

		} catch (WorkbenchException e) {
			Activator.handleError(e);
		}
	}

	private void openLocalXMLFile(final String filePath) {

		UIJob openJob = new UIJob("Opening the xml file(s)...") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {

				IFileStore fileStore = EFS.getLocalFileSystem().getStore(
						new Path(filePath));
				textUri = fileStore.toURI().toString();
				if (!fileStore.fetchInfo().isDirectory()
						&& fileStore.fetchInfo().exists()) {
					IWorkbenchPage page = PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getActivePage();

					try {
						IDE.openEditorOnFileStore(page, fileStore);
						return Status.OK_STATUS;
					} catch (PartInitException e) {
						Activator.handleError(e);
					}
				}
				return Status.CANCEL_STATUS;
			}
		};
		openJob.setUser(false);
		openJob.schedule();
	}

	private void openLocalImageFile(final String filePath) {
		UIJob openJob = new UIJob("Opening the image file(s)...") {

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {

				InputStream input = null;
				try {
					if (imageTabsMap.isEmpty())
						openTabs = 0;

					// boolean found = false;
					File file = new File(filePath);
					String fileUri = file.toURI().toString();
					input = new FileInputStream(file);
					// just set focus, if this image
					// object is already open.
					for (Map.Entry<String, String> entry : imageTabsMap
							.entrySet()) {
						if (entry.getValue().equals(fileUri)) {
							String key = entry.getKey();
							IWorkbenchPage page = Workbench.getInstance()
									.getActiveWorkbenchWindow().getActivePage();
							page.showView(IMAGE_VIEW_ID, key,
									IWorkbenchPage.VIEW_ACTIVATE);

							return Status.OK_STATUS;
						}
					}

					openToolkit = false;
					IWorkbenchPage page = Workbench.getInstance()
							.getActiveWorkbenchWindow().getActivePage();
					page.showView(IMAGE_VIEW_ID, String.valueOf(++openTabs),
							IWorkbenchPage.VIEW_ACTIVATE);

					String name = file.getName();
					String sp[] = name.split("\\.");
					String ext = (sp.length > 0) ? "/" + sp[sp.length - 1] : "";
					loadTGOImage(null, input, fileUri, name, "image" + ext);

					if (!isLoadingLinksObject)
						notifyListeners(Event.FINALIZE_LOADING,
								new Pair<String, Object>(
										focusedImageTabSecondaryId, null));
					openToolkit = true;
					return Status.OK_STATUS;
				} catch (CoreException e) {
					Activator.handleError(e);
				} catch (FileNotFoundException e) {
					Activator.handleError(e);
				} finally {
					if (input != null)
						try {
							input.close();
						} catch (IOException e) {
							Activator.handleWarning(e,
									"Couldn't close the input stream!");
						}
				}
				return Status.CANCEL_STATUS;
			}
		};
		openJob.setUser(false);
		openJob.schedule();
		try {
			openJob.join();
		} catch (InterruptedException e) {
			Activator.handleWarning(e, "Job interrupted!");
		}

	}

	private String filePath = "";

	private void openLocalAnnotatedFile(final String filePath,
			final String parentPath) {

		if (filePath == null || "".equals(filePath))
			return;

		this.filePath = filePath;

		if (isDirty() && showWarnings) {
			msg_asking
					.setMessage(Messages.LinkEditorController_Mess_ChangedAfterLoading_Object);
			// displayed
			// message
			msg_asking
					.setText(Messages.LinkEditorController_Title_ChangedAfterLoading_Obj);
			// displayed
			// title

			int result = msg_asking.open();
			if (result == SWT.YES) {
				saveReadAnnotatedObject(false, true);
				return;
			} else if (result == SWT.NO) {
				doRevertAllXMLEditorsToSaved();
				setChangedAfterLoading_AllImageViews(false);
			} else { // cancel
				return;
			}
		}

		// =========================================================

		if (readingTGOJob_readAnnotated != null)
			readingTGOJob_readAnnotated.cancel();

		readingTGOJob_readAnnotated = new Job(NLS.bind("Opening {0}.....",
				filePath)) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {

				if (monitor == null)
					monitor = new NullProgressMonitor();

				InputStream inputStream = null;

				try {

					monitor.beginTask(NLS.bind("Opening {0}.....", filePath),
							100);

					File file = new File(filePath);
					if (file == null || !file.exists())
						return Status.CANCEL_STATUS;

					monitor.worked(10);

					inputStream = new FileInputStream(file);

					monitor.worked(60);

					UIJob resetJob = new UIJob("Cleaning the screen...") {

						@Override
						public IStatus runInUIThread(IProgressMonitor monitor) {
							reset(false);
							return Status.OK_STATUS;
						}
					};

					resetJob.schedule();

					try {
						resetJob.join();
					} catch (InterruptedException e) {
						Activator.handleError(e);
					}

					TEIDocParser parser = new TEIDocParser();

					isLoadingLinksObject = true;

					localAnnotatedFilePath = filePath;
					parser.setParentPath(parentPath);
					parser.parse(inputStream, monitor);
					workLocal = true;

					isLoadingLinksObject = false;
					// monitor.worked(10);

					linksInfoUpdated = false;

					new UIJob("Finalizing the loading process...") {
						@Override
						public IStatus runInUIThread(IProgressMonitor monitor) {
							if (selectedShape != null)
								jumpToText(selectedShape, true);

							notifyListeners(Event.FINALIZE_LOADING,
									new Pair<String, Object>(
											focusedImageTabSecondaryId, null));

							setChangedAfterLoading_AllImageViews(false);

							return Status.OK_STATUS;
						}
					}.schedule();

					monitor.done();

					return Status.OK_STATUS;

				} catch (JaxenException e) {
					Activator.handleError(e);
				} catch (XMLStreamException e) {
					Activator.handleError(e);
				} catch (FileNotFoundException e) {
					Activator.handleError(e);
				} finally {
					if (inputStream != null) {
						try {
							inputStream.close();
						} catch (IOException e) {
							Activator.handleWarning(e,
									"Couldn't close the input stream!");
						}
					}
				}

				return Status.CANCEL_STATUS;
			}
		};

		readingTGOJob_readAnnotated.setUser(false);
		readingTGOJob_readAnnotated.schedule();
	}

	public TGLayerManager getLayerManagerFromMap(String ImageUri) {
		TGLayerManager layerManag = null;
		try {
			layerManag = allLayerManager.get(ImageUri);
		} catch (Exception e) {
			Activator.handleError(e);
		}
		return layerManag;
	}

	public static int getMaxtglayernumber() {
		return maxTGLayerNumber;
	}

	public void addLayerManagerToMap(String ImageUri) {
		TGLayerManager tgLM = new TGLayerManager(ImageUri);
		try {
			allLayerManager.put(ImageUri, tgLM);
		} catch (Exception e) {
			Activator.handleError(e);
		}
	}

	public void addLayerManagerToMap(TGLayerManager tgLM) {
		try {
			allLayerManager.put(tgLM.getImageUri(), tgLM);
		} catch (Exception e) {
			Activator.handleError(e);
		}
	}

	public void setLinkVisiblityByShape(TGShape shape, final boolean visible) {
		for (ILink l : all_links) {
			TGShape s = l.getShape();
			if (s != null && s.equals(shape)) {
				if (!visible) {
					removeAnnotation(l);
				} else {
					if (l.getUnselectedAnnotation() == null
							&& l.getTextUri().equals(this.textUri)) {
						Point p = getSartEndForLink(l);
						if (p != null) {
							addUnselectedAnnotation(p.x, p.y, "", l);
						}
					}
				}

				l.setVisible(visible);
				break;
			}
		}
	}

	public void setAllLinksVisible() {
		for (ILink l : all_links)
			l.setVisible(true);

		updateTextSegments();
	}

	private boolean preShutdown = false;

	@Override
	public boolean preShutdown(IWorkbench workbench, boolean forced) {
		preShutdown = true;
		return true;
	}

	@Override
	public void postShutdown(IWorkbench workbench) {
		// TODO Auto-generated method stub

	}

	public void doSave() {
		saveAnnotatedObject(false, false, false);
	}

	public void doSaveAs() {
		saveAnnotatedObject(true, false, false);
	}

	public void doSaveNewRevision() {
		saveAnnotatedObject(false, true, false);
	}
	
	public void doLockTglObject() {
		LockingService lockService = LockingService.getInstance();
		lockService.lockObject(loaded_object, true);
//		if (lockService.lockObject(loaded_object, true)) {
//			loaded_object.setOpenAsReadOnly(true);
//			System.out.println("tgl-Object has been locked.");
//		}
	}
	
	
	
	public void doUnlockTglObject() {
		LockingService lockService = LockingService.getInstance();

		if (loaded_object == null){
			loaded_object = selected_object;
		}		
		
		if (loaded_object != null && lockService.objectIsLocked(loaded_object)) {
			lockService.unlockObject(loaded_object);
			System.out.println("tgl-Object has been unlocked.");
		}	
	}
	
	
	//
	// =============================================================================
	// Test main
	public static void main(String[] args) {

		String text = "<anchor xml:id=\"a1\" />Ludwig<anchor xml:id=\"a2\" />", text2 = "<anchor xml:id=\"a1\"> </anchor>Ludwig<anchor xml:id=\"a2\" />";
		String pattern_s = "(?s)<anchor[^<>]*?\\s+xml:id\\s*=\\s*\"" + "a1"
				+ "\"[^<>]*?/>.*?<anchor[^<>]*?\\s+xml:id\\s*=\\s*\"" + "a2"
				+ "\"[^<>]*?/>";

		System.err.println(text2.matches(pattern_s));
	}
	// --------------------------------------------------------

	
}