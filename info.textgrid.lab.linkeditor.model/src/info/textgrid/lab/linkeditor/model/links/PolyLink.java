/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.model.links;

import info.textgrid.lab.linkeditor.model.graphics.TGPolygon;

import org.eclipse.jface.text.source.Annotation;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class PolyLink implements ILink {

	private TGPolygon poly = null;

	private String textUri = null;

	private String startAnchorId;
	private String endAnchorId;

	private boolean visible = true;

	private Annotation selectedAnnotation = null;
	private Annotation unselectedAnnotation = null;

	public PolyLink(TGPolygon poly, String startAnchorId, String endAnchorId,
			String textUri, boolean visible) {
		this.poly = poly;

		this.startAnchorId = startAnchorId;
		this.endAnchorId = endAnchorId;
		this.poly.setLinked(true);
		this.textUri = textUri;
		this.visible = visible;

		poly.setAssocLink(this);
	}

	@Override
	public boolean equals(Object obj) {
		PolyLink other = null;

		if (obj instanceof PolyLink)
			other = (PolyLink) obj;
		else
			return false;

		return this.poly.equals(other.poly)
				&& (this.startAnchorId.equals(other.startAnchorId) && this.endAnchorId
						.equals(other.endAnchorId))
				&& (this.textUri.equals(other.textUri));
	}

	@Override
	public boolean equalsExceptShape(ILink link) {
		if (!(link instanceof PolyLink))
			return false;

		PolyLink other = (PolyLink) link;
		return this.poly.getImageUri().equals(other.poly.getImageUri())
				&& (this.startAnchorId.equals(other.startAnchorId) && this.endAnchorId
						.equals(other.endAnchorId))
				&& (this.textUri.equals(other.textUri));
	}

	@Override
	public String getCoords() {
		return poly.getPointsSequenceString();
	}

	@Override
	public String getLink() {
		return getCoords() + ";" + getTextUri() + ";" + getImageUri() + ";"
				+ getStartAnchorId() + ";" + getEndAnchorId();
	}

	@Override
	public TGPolygon getShape() {
		return poly;
	}

	@Override
	public String getImageUri() {
		return this.poly.getImageUri();
	}

	@Override
	public String getTextUri() {
		return textUri;
	}

	@Override
	public void setImageUri(String uri) {
		this.poly.setImageUri(uri);
	}

	@Override
	public void setTextUri(String uri) {
		textUri = uri;
	}

	@Override
	public String getEndAnchorId() {
		return this.endAnchorId;
	}

	@Override
	public String getStartAnchorId() {
		return this.startAnchorId;
	}

	@Override
	public void setEndAnchorId(String endAnchorId) {
		this.endAnchorId = endAnchorId;
	}

	@Override
	public void setStartAnchorId(String startAnchorId) {
		this.startAnchorId = startAnchorId;
	}

	@Override
	public boolean isVisible() {
		return visible;
	}

	@Override
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	@Override
	public void setSelectedAnnotation(Annotation anno) {
		this.selectedAnnotation = anno;
	}

	@Override
	public void setUnselectedAnnotation(Annotation anno) {
		this.unselectedAnnotation = anno;
	}

	@Override
	public Annotation getSelectedAnnotation() {
		return selectedAnnotation;
	}

	@Override
	public Annotation getUnselectedAnnotation() {
		return unselectedAnnotation;
	}
}