/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.model.links;

import info.textgrid.lab.linkeditor.model.graphics.TGRectangle;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class LinksContainer implements Iterable<ILink> {

	private static final long serialVersionUID = 1L;

	private final List<ILink> links = Collections
			.synchronizedList(new LinkedList<ILink>());

	public boolean add(ILink link) {
		return links.add(link);
	}

	public void add(int index, ILink link) {
		links.add(index, link);
	}

	/**
	 * Replaces the element at the specified position in this list with the
	 * specified element (optional operation).
	 * 
	 * @param index
	 * @param link
	 * @return the element previously at the specified position
	 */
	public ILink set(int index, ILink link) {
		return links.set(index, link);
	}

	public void edit(ILink link, String startAnchorId, String endAnchorId) {
		int index = links.indexOf(link);
		ILink l = links.get(index);
		l.setStartAnchorId(startAnchorId);
		l.setEndAnchorId(endAnchorId);
	}

	@Override
	public Object clone() {
		LinksContainer lc = new LinksContainer();
		for (ILink l : links) {
			lc.add(l);
		}
		return lc;
	}

	@Override
	public Iterator<ILink> iterator() {
		return links.iterator();
	}

	public void clear() {
		links.clear();
	}

	public boolean isEmpty() {
		return links.isEmpty();
	}

	public int size() {
		return links.size();
	}

	public List<ILink> getList() {
		return links;
	}

	public ILink get(int index) {
		return links.get(index);
	}

	public ILink remove(int index) {
		return links.remove(index);
	}

	public boolean remove(ILink link) {
		return links.remove(link);
	}

	// just for testing
	public static void main(String[] args) {
		TGRectangle rect = new TGRectangle("test");
		LinksContainer l = new LinksContainer();
		l.add(new RectLink(rect, "1", "2", "", true));
		l.add(new RectLink(rect, "5", "6", "", true));
		l.add(new RectLink(rect, "3", "4", "", true));

		for (ILink a : l) {
			System.err.println(a.getStartAnchorId());
		}
		System.err.println("---------------------------");

		l.edit(new RectLink(rect, "3", "4", "", true), "7", "8");
		for (ILink a : l) {
			System.err.println(a.getStartAnchorId());
		}

		TGShape s = new TGRectangle("test");

		System.err.println(s instanceof TGRectangle);

		TGShape sd = s;

		System.err.println(sd instanceof TGRectangle);
	}
}