/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.model.links;

import info.textgrid.lab.linkeditor.model.graphics.TGRectangle;

import org.eclipse.jface.text.source.Annotation;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class RectLink implements ILink {

	private TGRectangle rect = null;
	private String textUri = null;

	private String startAnchorId;
	private String endAnchorId;

	private boolean visible = true;

	private Annotation selectedAnnotation = null;
	private Annotation unselectedAnnotation = null;

	public RectLink(TGRectangle rect, String startAnchorId, String endAnchorId,
			String textUri, boolean visible) {
		this.rect = rect;
		this.startAnchorId = startAnchorId;
		this.endAnchorId = endAnchorId;
		this.rect.setLinked(true);
		this.textUri = textUri;
		this.visible = visible;

		rect.setAssocLink(this);
	}

	@Override
	public boolean equals(Object obj) {
		RectLink other = null;

		if (obj instanceof RectLink)
			other = (RectLink) obj;
		else
			return false;

		return this.rect.equals(other.rect)
				&& (this.startAnchorId.equals(other.startAnchorId) && this.endAnchorId
						.equals(other.endAnchorId))
				&& (this.textUri.equals(other.textUri));
	}

	@Override
	public boolean equalsExceptShape(ILink link) {
		if (!(link instanceof RectLink))
			return false;

		RectLink other = (RectLink) link;
		return this.rect.getImageUri().equals(other.rect.getImageUri())
				&& (this.startAnchorId.equals(other.startAnchorId) && this.endAnchorId
						.equals(other.endAnchorId))
				&& (this.textUri.equals(other.textUri));
	}

	@Override
	public String getCoords() {
		return rect.getRectangle().x + "," + rect.getRectangle().y + ","
				+ rect.getRectangle().width + "," + rect.getRectangle().height;
	}

	@Override
	public String getLink() {
		return getCoords() + ";" + getTextUri() + ";" + getImageUri() + ";"
				+ getStartAnchorId() + ";" + getEndAnchorId();
	}

	@Override
	public TGRectangle getShape() {
		return rect;
	}

	@Override
	public String getImageUri() {
		return this.rect.getImageUri();
	}

	@Override
	public String getTextUri() {
		return textUri;
	}

	@Override
	public void setImageUri(String uri) {
		this.rect.setImageUri(uri);
	}

	@Override
	public void setTextUri(String uri) {
		textUri = uri;
	}

	@Override
	public String getEndAnchorId() {
		return this.endAnchorId;
	}

	@Override
	public String getStartAnchorId() {
		return this.startAnchorId;
	}

	@Override
	public void setEndAnchorId(String endAnchorId) {
		this.endAnchorId = endAnchorId;
	}

	@Override
	public void setStartAnchorId(String startAnchorId) {
		this.startAnchorId = startAnchorId;
	}

	@Override
	public boolean isVisible() {
		return visible;
	}

	@Override
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	@Override
	public void setSelectedAnnotation(Annotation anno) {
		this.selectedAnnotation = anno;
	}

	@Override
	public void setUnselectedAnnotation(Annotation anno) {
		this.unselectedAnnotation = anno;
	}

	@Override
	public Annotation getSelectedAnnotation() {
		return selectedAnnotation;
	}

	@Override
	public Annotation getUnselectedAnnotation() {
		return unselectedAnnotation;
	}
}