/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.model.links;

import info.textgrid.lab.linkeditor.model.graphics.TGShape;

import org.eclipse.jface.text.source.Annotation;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public interface ILink {

	public String getLink();

	public String getCoords();

	public void setStartAnchorId(String startAnchorId);

	public String getStartAnchorId();

	public void setEndAnchorId(String endAnchorId);

	public String getEndAnchorId();

	public TGShape getShape();

	public void setImageUri(String uri);

	public String getImageUri();

	public void setTextUri(String uri);

	public String getTextUri();

	public boolean equalsExceptShape(ILink link);

	public boolean isVisible();

	public void setVisible(boolean visible);

	public void setSelectedAnnotation(Annotation anno);

	public void setUnselectedAnnotation(Annotation anno);

	public Annotation getSelectedAnnotation();

	public Annotation getUnselectedAnnotation();
}
