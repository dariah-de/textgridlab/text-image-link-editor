/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.model.graphics;

import java.util.Collection;
import java.util.Iterator;

import com.google.common.collect.HashBiMap;

/**
 * The TGLayerManager class handled TGLayer Objects.
 * 
 * @author leuk
 */
public class TGLayerManager {

	private String ImageUri = "";
	private HashBiMap<Integer, TGLayer> allTGLayer = HashBiMap.create();

	/**
	 * 
	 */
	public TGLayerManager(String uri) {
		this.ImageUri = uri;
	}

	/**
	 * 
	 * @param newLayer
	 * @param index
	 */
	public TGLayerManager(Integer layNum, TGLayer newLayer, String uri) {
		add(layNum, newLayer);
		this.ImageUri = uri;
	}

	/**
	 * 
	 * @return
	 */
	public String getImageUri() {
		return ImageUri;
	}

	/**
	 * 
	 * @return
	 */
	public int getNextNumber() {
		int nextNumber = 1;
		if (allTGLayer.size() == 0) {
			return nextNumber;
		}
		nextNumber = allTGLayer.size();
		nextNumber += 1;

		return nextNumber;
	}

	/**
	 * 
	 * @param imageUri
	 */
	public void setImageUri(String imageUri) {
		ImageUri = imageUri;
	}

	/**
	 * 
	 * @param newLayer
	 * @param index
	 * @return
	 */
	public TGLayer add(Integer keyNumber, TGLayer newLayer) {
		return this.allTGLayer.put(keyNumber, newLayer);
	}

	/**
	 * 
	 * @param keyNum
	 */
	public void remove(int keyNum) {
		this.allTGLayer.remove(keyNum);
	}

	/**
	 * 
	 * @return
	 */
	public boolean equals(TGLayer obj) {
		return this.equals(obj);
	}

	/**
	 * 
	 * @return
	 */
	public int getSize() {
		return this.allTGLayer.size();
	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Iterator getIterator() {
		Iterator<TGLayer> iterator = allTGLayer.values().iterator();
		return iterator;
	}

	@SuppressWarnings("rawtypes")
	public Collection keySet() {
		return allTGLayer.keySet();
	}

	@SuppressWarnings("rawtypes")
	public Collection values() {
		return allTGLayer.values();
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		StringBuffer strBuf = new StringBuffer();
		for (Iterator iterator = getIterator(); iterator.hasNext();) {
			TGLayer type = (TGLayer) iterator.next();
			strBuf.append(type.toString() + "\n");
		}
		return strBuf.toString();
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String uri = "textgrid:Maynooth+4+5+10:Lore+Ipsum+2:20100502T115658:image%2Fjpeg:1";

		TGLayer lay1 = new TGLayer("TestLayer 1", 1, true, true);
		TGLayer lay2 = new TGLayer("TestLayer 2", 2, true, false);
		TGLayer lay3 = new TGLayer("TestLayer 3", 3, true, false);

		TGLayerManager layMgr = new TGLayerManager(uri);
		System.out.println(layMgr.add(1, lay1));
		System.out.println(layMgr.add(2, lay2));
		System.out.println(layMgr.add(3, lay3));

		System.out.println(layMgr.toString());
	}
}