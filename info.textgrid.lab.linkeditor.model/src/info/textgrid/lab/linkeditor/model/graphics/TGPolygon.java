/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.model.graphics;

import info.textgrid.lab.linkeditor.model.links.PolyLink;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;

/**
 * The TGPolygon class handled Polygon Objects and extends TGShape
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * @author leuk
 */
public class TGPolygon extends TGShape {

	private Point selectedPoint = null;

	private List<Point> points = Collections
			.synchronizedList(new ArrayList<Point>());

	public TGPolygon(String imageUri) {
		this.imageUri = imageUri;
	}

	public TGPolygon(Point[] points, String imageUri) {
		addPoints(points);
		this.imageUri = imageUri;

	}

	@Override
	public TGShape clone() {
		TGPolygon p = new TGPolygon(this.getPoints(), imageUri);
		p.setWritingMode(this.getWritingMode());
		p.setLinked(this.isLinked());
		p.setLayerValues(new Integer(this.getLayer()), this.getLayerName(),
				this.getLayerRGB());
		p.assocLink = this.assocLink;
		return p;
	}

	public void addPoints(Point[] points) {
		for (int i = 0; i < points.length; ++i) {
			addPoint(points[i]);
		}
	}

	public void addPoint(int x, int y) {
		addPoint(new Point(x, y));
	}

	public void addPoint(Point point) {
		this.points.add(new Point(point.x, point.y));
	}

	public void addPointVertex(int x, int y) {
		addPointVertex(new Point(x, y));
	}

	public void addPointVertex(Point point) {
		// System.out.println("1: "+getPointsSequenceString());
		if (isPointAlmostOnSelectedRegion(point.x, point.y)) {
			final int deltaX = 10;
			Point p = new Point(selectedPoint.x, selectedPoint.y);
			Point pN = new Point(selectedPoint.x - deltaX, selectedPoint.y);
			int pos = points.indexOf(p);
			this.points.add(pos, pN);
			// System.out.println("2: "+getPointsSequenceString());
		}
	}

	public void remPoint(int x, int y) {
		remPoint(new Point(x, y));
	}

	public void remPoint(Point point) {
		if (isPointAlmostOnSelectedRegion(point.x, point.y)) {
			// String errorMessage =
			// "An Error occured while removing polygone vertex.";
			Point pN = new Point(selectedPoint.x, selectedPoint.y);
			// try {
			this.points.remove(pN);
			// } catch (ClassCastException e) {
			// Activator.handleError(e,
			//						errorMessage //$NON-NLS-1$
			// , e.toString());
			// } catch (NullPointerException e) {
			// Activator.handleError(e,
			//						errorMessage //$NON-NLS-1$
			// , e.toString());
			// } catch (UnsupportedOperationException e) {
			// Activator.handleError(e,
			//						errorMessage //$NON-NLS-1$
			// , e.toString());
			// }
		}
	}

	public Point[] getPoints() {
		Point[] p = new Point[size()];
		return this.points.toArray(p);
	}

	public int[] getPointsArray() {
		ArrayList<Integer> ia = new ArrayList<Integer>();

		for (Point p : this.points) {
			ia.add(p.x);
			ia.add(p.y);
		}

		int[] a = new int[ia.size()];

		for (int i = 0; i < ia.size(); ++i) {
			a[i] = ia.get(i);
		}

		return a;
	}

	public int[] getXPoints() {
		int ia[] = new int[points.size()];

		for (int i = 0; i < points.size(); ++i) {
			ia[i] = getPoints()[i].x;
		}

		return ia;
	}

	public int[] getYPoints() {
		int ia[] = new int[points.size()];

		for (int i = 0; i < points.size(); ++i) {
			ia[i] = getPoints()[i].y;
		}

		return ia;
	}

	public String getPointsSequenceString() {
		int[] a = getPointsArray();
		StringBuffer s = new StringBuffer();

		for (int i = 0; i < a.length; ++i) {
			s.append(a[i]);
			if (i < a.length - 1)
				s.append(",");
		}

		return s.toString();
	}

	@Override
	public boolean equals(Object obj) {
		TGPolygon other = null;

		if (obj instanceof TGPolygon)
			other = (TGPolygon) obj;
		else
			return false;

		return this.getPointsSequenceString().equals(
				other.getPointsSequenceString())
				&& this.imageUri.equals(other.imageUri);
	}

	@Override
	public String toString() {
		return super.hashCode() + " Points {x1,y1,x2,y2,...} = {"
				+ getPointsSequenceString() + "} imageUri: " + this.imageUri;
	}

	public int size() {
		return this.points.size();
	}

	public Point getLastPoint() {
		if (this.size() < 1)
			return null;

		return this.getPoints()[this.size() - 1];
	}

	public void reset() {
		points.clear();
	}

	/**
	 * @category NotCompleted
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isPointOnBoundary(int x, int y) {

		if (points.isEmpty())
			return false;

		Point p1 = points.get(0), p2 = null;
		float m, b, bb;

		for (int i = 1; i < points.size(); ++i) {
			p2 = points.get(i);

			m = (p2.y - p1.y) / (p2.x - p1.x);
			b = y - (m * x);
			bb = p1.y - (m * p1.x);

			if (Math.abs(bb - b) <= 10)
				return true;

			p1 = p2;
		}

		return false;
	}

	public boolean isPointAlmostOnSelectedRegion(int x, int y) {
		for (Point p : getPoints()) {
			if (createRectAroundThePoint(p, 5).contains(x, y)) {
				selectedPoint = p;
				return true;
			}
		}

		return false;
	}

	public static Rectangle createRectAroundThePoint(Point point, int pixel) {
		int x = point.x;
		int y = point.y;

		return new Rectangle(x - pixel, y - pixel, 2 * pixel, 2 * pixel);
	}

	@Override
	public boolean contains(int x, int y) {
		if (size() == 0)
			return false;

		TGPolygon poly = new TGPolygon(getPoints(), getImageUri());
		poly.addPoint(getPoints()[0]);

		int counter = 0;
		double xinters = 0.0;
		Point p1 = getPoints()[0];
		Point p2 = null;
		int size = poly.size();

		for (int i = 1; i <= size; i++) {
			p2 = poly.getPoints()[i % size];
			if (y > Math.min(p1.y, p2.y)) {
				if (y <= Math.max(p1.y, p2.y)) {
					if (x <= Math.max(p1.x, p2.x)) {
						if (p1.y != p2.y) {
							xinters = (y - p1.y) * (p2.x - p1.x)
									/ (p2.y - p1.y) + p1.x;
							if (p1.x == p2.x || x <= xinters)
								counter++;
						}
					}
				}
			}
			p1 = p2;
		}

		return (counter % 2 != 0);
	}

	@Override
	public void move(int dx, int dy) {
		if (this.size() == 0)
			return;

		TGPolygon tempPoly = new TGPolygon(this.imageUri);
		for (Point p : getPoints()) {
			tempPoly.addPoint(p.x + dx, p.y + dy);
		}

		this.reset();
		this.addPoints(tempPoly.getPoints());

		if (selectedPoint != null)
			selectedPoint = new Point(selectedPoint.x + dx, selectedPoint.y
					+ dy);
	}

	@Override
	public void resize(int dx, int dy, int cursor) {
		if (selectedPoint == null) {
			return;
		}

		TGPolygon temp = new TGPolygon(this.imageUri);
		for (Point p : this.getPoints()) {
			if (!p.equals(selectedPoint)) {
				temp.addPoint(p.x, p.y);
			} else {
				selectedPoint = new Point(p.x + dx, p.y + dy);
				temp.addPoint(selectedPoint);
			}
		}
		this.reset();
		this.addPoints(temp.getPoints());
	}

	public Rectangle getBoundsRect() {
		if (size() == 0)
			return new Rectangle(0, 0, 0, 0);

		int minX = 0, minY = 0, maxX = 0, maxY = 0;
		Point first = getPoints()[0];
		minX = first.x;
		minY = first.y;
		for (Point p : getPoints()) {
			if (p.x < minX)
				minX = p.x;
			if (p.y < minY)
				minY = p.y;
			if (p.x > maxX)
				maxX = p.x;
			if (p.y > maxY)
				maxY = p.y;
		}

		return new Rectangle(minX, minY, maxX - minX, maxY - minY);
	}

	@Override
	public boolean isCompletedShape() {
		return (size() > 0);
	}

	/**
	 * Copied from {@link PolyLink}
	 * 
	 * @param image_width
	 * @param image_height
	 * @return
	 */
	private String polyCoordsToPercent(int image_width, int image_height) {
		double help = 0.0;
		String polyPercent = "";

		for (int i = 0; i < this.size(); ++i) {
			Point p = this.getPoints()[i];
			// x-coordinate
			help = ((double) p.x * 100) / image_width;
			polyPercent += help + "%" + ",";
			// y-coordinate
			help = ((double) p.y * 100) / image_height;
			polyPercent += help + "%" + ((i == this.size() - 1) ? "" : " ");
		}

		return polyPercent;
	}

	@Override
	public OMElement createOMElement(OMFactory factory, OMElement parent,
			OMNamespace ns, int imageWidth, int imageHeight) {

		OMElement polyElem = factory.createOMElement("polygon", ns, parent);
		polyElem.addAttribute("points",
				polyCoordsToPercent(imageWidth, imageHeight), null);

		return polyElem;
	}

	@Override
	public TYPE getType() {
		return TYPE.POLY;
	}

	public static void main(String[] args) {
		TGPolygon p1 = new TGPolygon("hallo");
		p1.addPoint(new Point(2, 3));
		p1.addPoint(new Point(5, 6));
		p1.addPoint(new Point(7, 9));
		p1.addPoint(new Point(3, 3));
		p1.addPoint(new Point(1, 6));

		TGPolygon p2 = new TGPolygon("test");
		p2.addPoint(new Point(2, 3));
		p2.addPoint(new Point(5, 6));

		p2.addPoint(new Point(7, 9));

		p2.addPoint(new Point(3, 3));

		p2.addPoint(new Point(1, 6));

		Point[] p = p2.getPoints();
		for (Point o : p)
			System.err.println(o);

		System.err.println(p1.equals(p2));
		p2.addPoint(2, 3);
		System.err.println(p1.equals(p2));
		p1.addPoint(new Point(2, 3));
		System.err.println(p1.equals(p2));
		p1.addPoint(new Point(1, 6));
		System.err.println(p1.equals(p2));

		System.err.println(p1.getPointsSequenceString());

		System.err.println(p1.getLastPoint());

		System.err.println(p1);
		System.err.println(p2);

		p1.move(3, -2);
		System.err.println(p1);

		System.err.println(p1.isPointAlmostOnSelectedRegion(13, 6));

		p1.resize(2, 3, 0);
		System.err.println(p1);

		p1.move(1, 1);
		System.err.println(p1);

		p1.resize(2, 3, 0);
		System.err.println(p1);

		String as[] = p1.getPointsSequenceString().split(",");

		double help = 0.0;
		String polyPercent = "";

		int i = 0;
		for (String s : as) {
			if (i % 2 == 0) {
				// x-coordinate
				help = (Double.valueOf(s).doubleValue() * 100) / 2;
				polyPercent += help + ", ";
			} else {
				// y-coordinate
				help = (Double.valueOf(s).doubleValue() * 100) / 2;
				polyPercent += help + ((i == as.length - 1) ? "" : ", ");
			}
			i++;
		}

		System.err.println(polyPercent);

		System.err.println(p1.getPointsSequenceString());
		// p1.showBoundBox();
		System.err.println(p1.getBoundsRect());

		System.err.println(p1.contains(8, 4));

		System.err.println(p1.getXPoints());
		System.err.println(p1.getYPoints());
	}
}
