/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.model.graphics;

import info.textgrid.lab.linkeditor.model.links.ILink;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.eclipse.swt.graphics.RGB;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public abstract class TGShape {

	// These values are predefined for the svg-standard.
	public static enum WRITING_MODE {
		NONE {
			@Override
			public String toString() {
				return "none";
			}
		},
		LR {
			@Override
			public String toString() {
				return "lr";
			}
		},
		LR_TB {
			@Override
			public String toString() {
				return "lr-tb";
			}
		},
		RL {
			@Override
			public String toString() {
				return "rl";
			}
		},
		RL_TB {
			@Override
			public String toString() {
				return "rl-tb";
			}
		},
		TB {
			@Override
			public String toString() {
				return "tb";
			}
		},
		TB_RL {
			@Override
			public String toString() {
				return "tb-rl";
			}
		}
	}

	// the default text direction (Left->Right and Top->Down)
	protected WRITING_MODE direction = WRITING_MODE.NONE;

	// These value is used for the svg class attribute.
	protected int layer = 0;
	protected String layerName = "\u2013"; // dash
	protected RGB layerRGB = new RGB(255, 255, 255);
	protected boolean isVisible = true;

	protected double rotate = 0;

	private boolean isLinked = false;

	protected String imageUri = "";

	public static enum TYPE {
		RECT, POLY, DOCKING_LINE
	};

	protected ILink assocLink = null;

	public ILink getAssocLink() {
		return assocLink;
	}

	public void setAssocLink(ILink assocLink) {
		this.assocLink = assocLink;
	}

	public abstract boolean contains(int x, int y);

	@Override
	public abstract boolean equals(Object obj);

	public abstract boolean isCompletedShape();

	public abstract void move(int dx, int dy);

	public abstract void resize(int dx, int dy, int cursor);

	@Override
	public abstract TGShape clone();

	public void setLinked(boolean linkFlag) {
		this.isLinked = linkFlag;
	}

	public boolean isLinked() {
		return this.isLinked;
	}

	public void setWritingMode(WRITING_MODE writing_mode) {
		this.direction = writing_mode;
	}

	public WRITING_MODE getWritingMode() {
		return this.direction;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	public String getLayer() {
		return (new Integer(this.layer)).toString();
	}

	public String getLayerName() {
		return layerName;
	}

	public void setLayerName(String layerName) {
		this.layerName = layerName;
	}

	public RGB getLayerRGB() {
		return layerRGB;
	}

	public void setLayerRGB(RGB layerRGB) {
		this.layerRGB = layerRGB;
	}

	public void setLayerValues(int layNumber, String layName, RGB layRGB) {
		if (layNumber == 0) {
			layName = "\u2013";
			layRGB = new RGB(255, 255, 255);
		}
		setLayer(layNumber);
		setLayerName(layName);
		setLayerRGB(layRGB);
	}

	public boolean isVisible() {
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}

	public double getRotationValue() {
		return this.rotate;
	}

	public void setRotationValue(double angle) {
		this.rotate = angle;
	}

	public void setImageUri(String uri) {
		imageUri = uri;
	}

	public String getImageUri() {
		return imageUri;
	}

	public abstract TYPE getType();

	public abstract OMElement createOMElement(OMFactory factory,
			OMElement parent, OMNamespace ns, int imageWidth, int imageHeight);

}