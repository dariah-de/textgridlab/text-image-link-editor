/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.model.graphics;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;

/**
 * The TGRectangle class handles Rectangle Objects and extends TGShape
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * @author leuk
 */
public class TGRectangle extends TGShape {

	private Rectangle rect = null;

	public TGRectangle(String imageUri) {
		this(0, 0, 0, 0, imageUri);
	}

	public TGRectangle(int x, int y, int width, int height, String imageUri) {
		rect = new Rectangle(x, y, width, height);
		this.imageUri = imageUri;
	}

	public TGRectangle(Rectangle rect, String imageUri) {
		if (rect != null) {
			this.rect = new Rectangle(rect.x, rect.y, rect.width, rect.height);
		}
		this.imageUri = imageUri;
	}

	@Override
	public TGShape clone() {
		TGRectangle r = new TGRectangle(rect.x, rect.y, rect.width,
				rect.height, imageUri);
		r.setWritingMode(this.getWritingMode());
		r.setLinked(this.isLinked());
		r.setRotationValue(this.getRotationValue());
		r.setLayerValues(new Integer(this.getLayer()), this.getLayerName(),
				this.getLayerRGB());
		r.assocLink = this.assocLink;
		return r;
	}

	public Rectangle getRectangle() {
		return this.rect;
	}

	public Rectangle getOuterRectangle() {
		if (Math.abs(rotate) == 90.0 || Math.abs(rotate) == 270.0) {
			return this.getBoundry2();
		} else if (rotate % 180 != 0.0) {
			return this.getBoundry();
		} else {
			return this.rect;
		}
	}

	public boolean contains(Point p) {
		return rect.contains(p.x, p.y);
	}

	// public Point p;

	@Override
	public boolean contains(int x, int y) {
		return getOuterRectangle().contains(x, y);
	}

	/*
	 * 
	 */
	public Rectangle getBoundry() {
		Rectangle rectNew = null;
		int max = (int) Math.sqrt((rect.width * rect.width)
				+ (rect.height * rect.height));
		Point p = getCenter();
		int x = p.x - max / 2;
		int y = p.y - max / 2;
		rectNew = new Rectangle(x, y, max, max);
		return rectNew;
	}

	/*
	 * 
	 */
	public Rectangle getBoundry2() {
		Rectangle rectNew = null;
		Point p = getCenter();
		int x = p.x - rect.height / 2;
		int y = p.y - rect.width / 2;
		rectNew = new Rectangle(x, y, rect.height, rect.width);
		return rectNew;
	}

	/*
	 * 
	 */
	public Point getCenter() {
		Point pm = new Point(rect.x + rect.width / 2, rect.y + rect.height / 2);
		return pm;
	}

	@Override
	public String toString() {
		return super.hashCode() + rect.toString() + " imageUri: "
				+ this.imageUri;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof TGRectangle))
			return false;

		TGRectangle other = (TGRectangle) obj;

		return (other != null) ? (this.rect.equals(other.rect) && this.imageUri
				.equals(other.imageUri)) : false;
	}

	@Override
	public boolean isCompletedShape() {
		return (rect.width != 0 && rect.height != 0);
	}

	@Override
	public void move(int dx, int dy) {
		this.rect.x += dx;
		this.rect.y += dy;
	}

	@Override
	public void resize(int dx, int dy, int cursor) {
		switch (cursor) {
		case SWT.CURSOR_SIZEE:
			this.rect.width += dx;
			break;
		case SWT.CURSOR_SIZEW:
			this.rect.x += dx;
			this.rect.width -= dx;
			break;
		case SWT.CURSOR_SIZEN:
			this.rect.y += dy;
			this.rect.height -= dy;
			break;
		case SWT.CURSOR_SIZES:
			this.rect.height += dy;
			break;
		case SWT.CURSOR_SIZENE:
			this.rect.y += dy;
			this.rect.height -= dy;
			this.rect.width += dx;
			break;
		case SWT.CURSOR_SIZESE:
			this.rect.width += dx;
			this.rect.height += dy;
			break;
		case SWT.CURSOR_SIZENW:
			this.rect.y += dy;
			this.rect.height -= dy;
			this.rect.x += dx;
			this.rect.width -= dx;
			break;
		case SWT.CURSOR_SIZESW:
			this.rect.height += dy;
			this.rect.x += dx;
			this.rect.width -= dx;
			break;
		}
	}

	@Override
	public OMElement createOMElement(OMFactory factory, OMElement parent,
			OMNamespace ns, int imageWidth, int imageHeight) {

		OMElement rectElem = factory.createOMElement("rect", ns, parent);

		double value = 0.0;

		value = ((double) this.rect.x * 100) / imageWidth;
		rectElem.addAttribute("x", String.valueOf(value) + "%", null);

		value = ((double) this.rect.y * 100) / imageHeight;
		rectElem.addAttribute("y", String.valueOf(value) + "%", null);

		value = ((double) this.rect.width * 100) / imageWidth;
		rectElem.addAttribute("width", String.valueOf(value) + "%", null);

		value = ((double) this.rect.height * 100) / imageHeight;
		rectElem.addAttribute("height", String.valueOf(value) + "%", null);

		double xPos = (((double) this.rect.x * 100) / imageWidth)
				+ ((((double) this.rect.width * 100) / imageWidth) / 2);
		double yPos = (((double) this.rect.y * 100) / imageHeight)
				+ ((((double) this.rect.height * 100) / imageHeight) / 2);

		rectElem.addAttribute("transform", "rotate(" + getRotationValue()
				+ ", " + xPos + "%" + ", " + yPos + "%" + ")", null);

		return rectElem;
	}

	@Override
	public TYPE getType() {
		return TYPE.RECT;
	}

	public static void main(String[] args) {
		TGRectangle r = new TGRectangle(0, 6, 6, 6, "testuri");
		r.setRotationValue(45);
	}
}
