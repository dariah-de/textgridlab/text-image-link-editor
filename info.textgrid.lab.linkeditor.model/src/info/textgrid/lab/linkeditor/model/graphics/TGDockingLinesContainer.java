/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.model.graphics;

import java.util.ArrayList;

/**
 * The TGDockingLinesContainer class encapsulate GridLine(DockingLine) Objects
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * @author leuk
 */
public class TGDockingLinesContainer {

	private ArrayList<TGLine> lines = null;

	public TGDockingLinesContainer() {
	}

	public ArrayList<TGLine> getAllTGLines() {
		return lines;
	}

	public void addTGLine(TGLine newLine) {
		this.lines.add(newLine);
	}

	public void addTGLine(ArrayList allLines) {
		this.lines = allLines;
	}

	public void removeTGLine(TGLine newLine) {
		this.lines.remove(newLine);
	}

	public boolean removeAll() {
		this.lines.clear();
		return this.lines.isEmpty();
	}

	public boolean contains(TGLine tgLine) {
		return lines.contains(tgLine);
	}

	public ArrayList<TGLine> getAll() {
		return this.lines;
	}
}
