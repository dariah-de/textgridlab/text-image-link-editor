/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.model.graphics;

import info.textgrid.lab.linkeditor.model.Activator;

import java.awt.geom.Line2D;
import java.util.ArrayList;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;

/**
 * The TGLine class handled GridLine(DockingLine) Objects and extends TGShape
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * @author leuk
 */
public class TGLine extends TGShape {
	private static final int maxPointToLineDist = 5;

	private static final int minDeltaX = 10;
	private static final int minDeltaY = 10;

	private Point selectedPoint = null;

	private ArrayList<Point> points = new ArrayList<Point>(2);

	public TGLine(String imageUri) {
		this.imageUri = imageUri;
	}

	public TGLine(Point[] points, String imageUri) {
		addPoints(points);
		this.imageUri = imageUri;
	}

	@Override
	public TGShape clone() {
		TGLine l = new TGLine(this.getPoints(), this.imageUri);
		l.setWritingMode(this.getWritingMode());
		l.setLinked(this.isLinked());
		return l;
	}

	public void addPoints(Point[] points) {
		for (int i = 0; i < points.length; ++i) {
			addPoint(points[i]);
		}
	}

	public void addPoint(int x, int y) {
		addPoint(new Point(x, y));
	}

	public void addPoint(Point point) {
		this.points.add(new Point(point.x, point.y));
	}

	public Point[] getPoints() {
		Point[] p = new Point[size()];
		return this.points.toArray(p);
	}

	public int[] getPointsArray() {
		ArrayList<Integer> ia = new ArrayList<Integer>();

		for (Point p : this.points) {
			ia.add(p.x);
			ia.add(p.y);
		}

		int[] a = new int[ia.size()];

		for (int i = 0; i < ia.size(); ++i) {
			a[i] = ia.get(i);
		}

		return a;
	}

	public int[] getXPoints() {
		int ia[] = new int[points.size()];

		for (int i = 0; i < points.size(); ++i) {
			ia[i] = getPoints()[i].x;
		}

		return ia;
	}

	public int[] getYPoints() {
		int ia[] = new int[points.size()];

		for (int i = 0; i < points.size(); ++i) {
			ia[i] = getPoints()[i].y;
		}

		return ia;
	}

	public String getPointsSequenceString() {
		int[] a = getPointsArray();
		StringBuffer s = new StringBuffer();

		for (int i = 0; i < a.length; ++i) {
			s.append(a[i]);
			if (i < a.length - 1)
				s.append(",");
		}

		return s.toString();
	}

	@Override
	public boolean equals(Object obj) {
		TGLine other = null;

		if (obj instanceof TGLine)
			other = (TGLine) obj;
		else
			return false;

		return this.getPointsSequenceString().equals(
				other.getPointsSequenceString())
				&& this.imageUri.equals(other.imageUri);
	}

	@Override
	public String toString() {
		return super.hashCode() + " Points {x1,y1,x2,y2} = {"
				+ getPointsSequenceString() + "} imageUri: " + getImageUri();
	}

	public int size() {
		return this.points.size();
	}

	public boolean isDeltaXOK() {
		int[] xp = this.getXPoints();
		int[] yp = this.getYPoints();
		if (Math.abs(xp[0] - xp[1]) >= minDeltaX
				|| Math.abs(yp[0] - yp[1]) >= minDeltaY) {
			return true;
		}
		return false;
	}

	public Point getLastPoint() {
		if (this.size() < 1)
			return null;

		return this.getPoints()[this.size() - 1];
	}

	public void reset() {
		points.clear();
	}

	public boolean isPointAlmostOnSelectedRegion(int x, int y) {
		for (Point p : getPoints()) {
			if (createRectAroundThePoint(p, 5).contains(x, y)) {
				selectedPoint = p;
				return true;
			}
		}

		return false;
	}

	public static Rectangle createRectAroundThePoint(Point point, int pixel) {
		int x = point.x;
		int y = point.y;

		return new Rectangle(x - pixel, y - pixel, 2 * pixel, 2 * pixel);
	}

	@Override
	public boolean contains(int px, int py) {
		int x1 = this.getXPoints()[0];
		int x2 = this.getXPoints()[1];
		int y1 = this.getYPoints()[0];
		int y2 = this.getYPoints()[1];
		double dist = Line2D.ptLineDist(x1, y1, x2, y2, px, py);

		int minX = Math.min(x1, x2);
		int maxX = Math.max(x1, x2);

		int minY = Math.min(y1, y2);
		int maxY = Math.max(y1, y2);

		if (dist <= maxPointToLineDist && px >= minX && px <= maxX
				&& py >= minY && py <= maxY) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void move(int dx, int dy) {
		if (this.size() == 0)
			return;

		TGLine tempLine = new TGLine(this.imageUri);
		for (Point p : getPoints()) {
			tempLine.addPoint(p.x + dx, p.y + dy);
		}

		this.reset();
		this.addPoints(tempLine.getPoints());

		if (selectedPoint != null)
			selectedPoint = new Point(selectedPoint.x + dx, selectedPoint.y
					+ dy);
	}

	@Override
	public void resize(int dx, int dy, int cursor) {
		if (selectedPoint == null) {
			return;
		}

		TGLine temp = new TGLine(this.imageUri);
		for (Point p : this.getPoints()) {
			if (!p.equals(selectedPoint)) {
				temp.addPoint(p.x, p.y);
			} else {
				selectedPoint = new Point(p.x + dx, p.y + dy);
				temp.addPoint(selectedPoint);
			}
		}
		this.reset();
		this.addPoints(temp.getPoints());
	}

	public Rectangle getBoundsRect() {
		if (size() == 0)
			return new Rectangle(0, 0, 0, 0);

		int minX = 0, minY = 0, maxX = 0, maxY = 0;
		Point first = getPoints()[0];
		minX = first.x;
		minY = first.y;
		for (Point p : getPoints()) {
			if (p.x < minX)
				minX = p.x;
			if (p.y < minY)
				minY = p.y;
			if (p.x > maxX)
				maxX = p.x;
			if (p.y > maxY)
				maxY = p.y;
		}

		return new Rectangle(minX, minY, maxX - minX, maxY - minY);
	}

	@Override
	public boolean isCompletedShape() {
		return (size() > 0);
	}

	/**
	 * Creates the String to paste in the xml-editorlinkfile
	 * 
	 * @param image_width
	 * @param image_height
	 * @return String
	 */
	public String createAnnotatedLink(int image_width, int image_height) {
		return "<dockingLine "
				+ dockingLineToPercent(image_width, image_height)
				+ " imageUri=\"" + this.imageUri + "\"/>\n";
	}

	/**
	 * Calculate the X,Y- Values in percent based on Image width and Image
	 * height
	 * 
	 * @return angle
	 */
	private String dockingLineToPercent(int image_width, int image_height) {
		double help = 0.0;
		String linePercent = "";
		for (int i = 0; i < size(); ++i) {
			Point p = getPoints()[i];
			// x-coordinate
			help = ((double) p.x * 100) / image_width;
			linePercent += help + ",";
			// y-coordinate
			help = ((double) p.y * 100) / image_height;
			linePercent += help + ((i == size() - 1) ? "" : ",");
		}

		return "lineCoords=\"" + linePercent + "\"";
	}

	/**
	 * Calculate the angle in a range of 0�-180�
	 * 
	 * @return angle
	 */
	public double getAngleValue() {
		Point p1 = null;
		Point p2 = null;
		// TODO Calculate the angle.
		double angle = 0;

		try {
			int[] xPoints = this.getXPoints();
			int[] yPoints = this.getYPoints();
			p1 = new Point(xPoints[0], yPoints[0]);
			p2 = new Point(xPoints[1], yPoints[1]);
			double dx = p2.x - p1.x;
			double dy = p2.y - p1.y;
			angle = Math.toDegrees(Math.atan(dy / dx));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// FIXME Is it a warning when it fails or an Error ??
			Activator.handleWarning(e);
		}

		if (angle < 0) {
			angle = angle * (-1);
		} else {
			angle = 180 - angle;
		}
		if (angle == 180) {
			angle = 0;
		}
		angle = StrictMath.floor(angle);
		return angle;
	}

	/**
	 * Transform TGLine in a horizontal or vertical Line
	 * 
	 * @param art
	 * @return points[]
	 */
	public Point[] transformLine(String art) {
		Point p1 = null;
		Point p2 = null;
		try {
			int[] xPoints = getXPoints();
			int[] yPoints = getYPoints();
			p1 = new Point(xPoints[0], yPoints[0]);
			p2 = new Point(xPoints[1], yPoints[1]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Point points[] = null;

		try {
			double dx = p2.x - p1.x;
			double dy = p2.y - p1.y;

			double length = Math.sqrt((dy * dy) + (dx * dx));

			int ym = Math.abs((p1.y - p2.y) / 2);
			if (p1.y >= p2.y) {
				ym += p2.y;
			} else {
				ym += p1.y;
			}
			int xm = Math.abs((p1.x - p2.x) / 2);
			if (p1.x >= p2.x) {
				xm += p2.x;
			} else {
				xm += p1.x;
			}

			points = new Point[2];

			if (art.equals("HORIZONTAL")) {
				points[0] = new Point((int) (xm - length / 2), ym);
				points[1] = new Point((int) (xm + length / 2), ym);
			} else if (art.equals("VERTICAL")) {
				points[0] = new Point(xm, (int) (ym - length / 2));
				points[1] = new Point(xm, (int) (ym + length / 2));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return points;
	}

	/**
	 * This method is an extension of dockingLineToPercent(..)
	 * 
	 * @return angle
	 */
	@Deprecated
	private String dockingLineCoordsToPercent(int image_width, int image_height) {
		double help = 0.0;
		String linePercent = "";
		Point p1 = points.get(0);
		Point p2 = points.get(1);

		help = ((double) p1.x * 100) / image_width;
		linePercent += "x1=\"" + help + "\" ";
		help = ((double) p1.y * 100) / image_height;
		linePercent += "y1=\"" + help + "\" ";

		help = ((double) p2.x * 100) / image_width;
		linePercent += "x2=\"" + help + "\" ";
		help = ((double) p2.y * 100) / image_height;
		linePercent += "y2=\"" + help + "\"";

		return linePercent;
	}

	@Override
	public OMElement createOMElement(OMFactory factory, OMElement parent,
			OMNamespace ns, int imageWidth, int imageHeight) {

		OMElement lineElem = factory.createOMElement("line", ns, parent);

		double value = 0.0;
		Point p1 = points.get(0);
		Point p2 = points.get(1);

		value = ((double) p1.x * 100) / imageWidth;
		lineElem.addAttribute("x1", String.valueOf(value) + "%", null);

		value = ((double) p1.y * 100) / imageHeight;
		lineElem.addAttribute("y1", String.valueOf(value) + "%", null);

		value = ((double) p2.x * 100) / imageWidth;
		lineElem.addAttribute("x2", String.valueOf(value) + "%", null);

		value = ((double) p2.y * 100) / imageHeight;
		lineElem.addAttribute("y2", String.valueOf(value) + "%", null);

		return lineElem;
	}

	@Override
	public TYPE getType() {
		return TYPE.DOCKING_LINE;
	}

	public static void main(String[] args) {
		TGLine l = new TGLine("ssssss");
		l.addPoint(5, 10);
		l.addPoint(20, 30);
		System.err.println(l.getPointsSequenceString());
		System.err.println(l.getXPoints()[0]);
	}

}
