/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.model.graphics;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.ui.PlatformUI;

/**
 * The TGLayer class handled Layer Objects
 * 
 * @author Leuk
 */
public class TGLayer implements Comparable<TGLayer> {

	private String name = "";
	private int number = 0;
	private boolean isVisible = true;
	private boolean isActive = false;
	// default color
	private Color layercolor = new Color(
			PlatformUI.getWorkbench().getDisplay(), new RGB(255, 255, 255));

	/**
	 * 
	 */
	public TGLayer() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param name
	 * @param number
	 */
	public TGLayer(String name, int number) {
		this.name = name;
		this.number = number;
	}

	/**
	 * 
	 * @param name
	 * @param number
	 * @param visible
	 * @param active
	 */
	public TGLayer(String name, int number, boolean visible, boolean active) {
		this.name = name;
		this.number = number;
		this.isVisible = visible;
		this.isActive = active;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * @param number
	 *            the number to set
	 */
	public void setNumber(int number) {
		this.number = number;
	}

	/**
	 * 
	 * @param isVisible
	 */
	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isVisible() {
		return this.isVisible;
	}

	/**
	 * 
	 * @param isActive
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		return "Layer: " + this.number + ", " + this.name + ", "
				+ this.isVisible + ", " + this.isActive + ", "
				+ this.layercolor.toString();
	}

	/**
	 * 
	 * @param newLayer
	 */
	@Override
	public int compareTo(TGLayer newLayer) {
		final int BEFORE = -1;
		final int EQUAL = 0;
		final int AFTER = 1;

		if (this.getNumber() < newLayer.getNumber())
			return BEFORE;
		if (this.getNumber() > newLayer.getNumber())
			return AFTER;

		return EQUAL;
	}

	/**
	 * 
	 * @param layercolor
	 */
	public void setLayercolor(Color layercolor) {
		this.layercolor = layercolor;
	}

	/**
	 * 
	 * @return
	 */
	public Color getLayercolor() {
		return layercolor;
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		TGLayer lay1 = new TGLayer("TestLayer 1", 1, true, false);
		TGLayer lay2 = new TGLayer("TestLayer 2", 2, true, true);

		System.out.println(lay1.toString());
		System.out.println(lay2.toString());
	}

}
