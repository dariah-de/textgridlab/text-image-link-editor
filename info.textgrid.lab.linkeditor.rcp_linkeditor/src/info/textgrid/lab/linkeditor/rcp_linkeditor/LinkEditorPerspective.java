/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.rcp_linkeditor;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.services.IEvaluationService;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * @author leuk
 */
public class LinkEditorPerspective implements IPerspectiveFactory {
	// important: if you want to change the id, then you maybe have to modify
	// the OpenHandler.
	public static String ID = "info.textgrid.lab.linkeditor.rcp_linkeditor.perspective";
	private IPageLayout factory;

	public void createInitialLayout(IPageLayout layout) {
		this.factory = layout;
		addViews();
		IEvaluationService evalService = (IEvaluationService) PlatformUI
				.getWorkbench().getService(IEvaluationService.class);
		evalService
				.requestEvaluation("info.textgrid.lab.linkeditor.rcp_linkeditor.testers.ShowLinkEditorToolbar");
	}

	private void addViews() {
		IWorkbench wb = PlatformUI.getWorkbench();
		wb.getIntroManager().closeIntro(wb.getIntroManager().getIntro());

		String editorArea = factory.getEditorArea();
		factory.setEditorAreaVisible(true);

		IFolderLayout topLeft = factory.createFolder("topleft", // NON-NLS-1
				IPageLayout.LEFT, 0.2f, editorArea);
		topLeft.addView("info.textgrid.lab.navigator.view"); // NaviView

		IFolderLayout topRight = factory.createFolder("topright", // NON-NLS-1
				IPageLayout.TOP, 0.5f, editorArea);

		topRight.addPlaceholder("edu.uky.mip.views.ImageView:*");
		topRight.addPlaceholder("info.textgrid.lab.linkeditor.controller.linksObjectEditorView");

		factory.addStandaloneView("edu.uky.mip.component.views.ThumbView",
				false, IPageLayout.BOTTOM, 0.8f, "topleft");
	}
}
