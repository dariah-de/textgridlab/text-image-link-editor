package info.textgrid.lab.linkeditor.rcp_linkeditor.handlers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Iterator;

import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.linkeditor.controller.LinkEditorController;
import info.textgrid.lab.linkeditor.rcp_linkeditor.Activator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.statushandlers.StatusManager;

public class DefaultOpenHandler extends AbstractHandler implements IHandler {

	private static final String LINKEDITOR_PERSPECTIVE = "info.textgrid.lab.linkeditor.rcp_linkeditor.perspective";
	private static final String PARAMETER_URI_ID = "info.textgrid.lab.linkeditor.rcp_linkeditor.defaultOpen.parameter1";
	private static final String PARAMETER_READONLY_ID = "info.textgrid.lab.linkeditor.rcp_linkeditor.defaultOpen.parameter2";
	
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String parameterURI = null;
		String parameterReadOnly = null;
		TextGridObject object = null;
		ISelection selection = null;
		
		//get parameter
		parameterURI = event.getParameter(PARAMETER_URI_ID);
		parameterReadOnly = event.getParameter(PARAMETER_READONLY_ID);
		
		//get TextGrid object
		if (parameterURI != null) {
			try {
				object = TextGridObject.getInstance(new URI(parameterURI), false);
			} catch (CrudServiceException e) {
				StatusManager.getManager().handle(
						new Status(IStatus.ERROR, Activator.PLUGIN_ID, "Could not retrieve TextGrid object.", e));
			} catch (URISyntaxException e) {
				StatusManager.getManager().handle(
						new Status(IStatus.ERROR, Activator.PLUGIN_ID, "Could not retrieve TextGrid object.", e));
			}
		} else {
			selection = HandlerUtil.getCurrentSelection(event);
			if (selection instanceof IStructuredSelection) {
				Object obj = ((IStructuredSelection) selection).getFirstElement();
				object = AdapterUtils.getAdapter(obj, TextGridObject.class);
			}	
		}
		
		if (object != null) {
			try {
				if (TGContentType.of("text/linkeditorlinkedfile").equals(object.getContentType(false))) {
					PlatformUI.getWorkbench().showPerspective(LINKEDITOR_PERSPECTIVE,
							PlatformUI.getWorkbench().getActiveWorkbenchWindow());
					LinkEditorController.getInstance().openAnnotatedObject(object);
				} else if (object.getContentType(false).getId().contains("image")) {
					PlatformUI.getWorkbench().showPerspective(LINKEDITOR_PERSPECTIVE,
							PlatformUI.getWorkbench().getActiveWorkbenchWindow());
					LinkEditorController.getInstance().addObjectToLinkEditor(object, true, true);
				} else if (TGContentType.of("text/xml").equals(object.getContentType(false))
						&& LINKEDITOR_PERSPECTIVE.equals(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getPerspective().getId()))
					LinkEditorController.getInstance().addObjectToLinkEditor(object, true, true);
			} catch (CoreException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			}

		}

		return null;
	}

}
