/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.rcp_linkeditor.handlers;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.linkeditor.tools.ExportImageLinkEditorFileCombinationDialog;

import java.text.MessageFormat;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class ExportLinksObjectHandler extends AbstractHandler implements
		IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection sel = HandlerUtil.getCurrentSelection(event);
		if (sel instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) sel;
			TextGridObject textGridObject = (TextGridObject) selection
					.getFirstElement();

			if (textGridObject == null) {
				throw new ExecutionException(
						MessageFormat
								.format("Could not find any TextGridObjects in the current selection ({0}). "
										+ "This should have been prevented by the handler's enabled specification.",
										selection));
			}
			ExportImageLinkEditorFileCombinationDialog
					.openDialog(textGridObject);

		} else {
			throw new ExecutionException(
					MessageFormat
							.format("Cannot export from this kind of selection ({0}). "
									+ "This should have been prevented by the handler's enabled specification.",
									sel));
		}

		return null;
	}

}
