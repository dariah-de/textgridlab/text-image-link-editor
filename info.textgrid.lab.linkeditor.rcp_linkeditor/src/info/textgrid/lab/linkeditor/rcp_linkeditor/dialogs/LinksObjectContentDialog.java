/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.rcp_linkeditor.dialogs;

import static info.textgrid.lab.linkeditor.rcp_linkeditor.Activator.handleError;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;

import org.apache.commons.io.IOUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import info.textgrid.lab.core.browserfix.TextGridLabBrowser;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;

/**
 * A dialog to display the content of the selected Text-Image-Link Object
 * 
 * @author Thorsten Vitt for <a href="http://www.textgrid.de/">TextGrid</a> (tv)
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 */
public class LinksObjectContentDialog extends TitleAreaDialog {

	private TextGridObject textGridObject;
	private Browser xmlTextField;

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite dialogArea = new Composite(parent, SWT.NONE);
		dialogArea.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		dialogArea.setLayout(new GridLayout(1, false));

		setTitle(Messages.LinksObjectContentDialog_TheContentOfTheSelectedTILO);
		getShell().setText(Messages.LinksObjectContentDialog_ShowContent);

		xmlTextField = TextGridLabBrowser.createBrowser(dialogArea);
		GridData layoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
		layoutData.heightHint = 600;
		xmlTextField.setLayoutData(layoutData);

		xmlTextField
				.setText("<html><body><div style=\"height:100%;width:100%\"><textarea wrap=\"off\" readonly=\"true\" style=\"height:100%;width:100%;background:inherit\">" //$NON-NLS-1$
						+ renderContentAsHTML()
						+ "</textarea></div></body></html>"); //$NON-NLS-1$
		setMessage(textGridObject.toString());

		return dialogArea;
	}

	@Override
	protected Button createButton(Composite parent, int id, String label,
			boolean defaultButton) {
		if (id == 0) {
			return super.createButton(parent, id, "Close", defaultButton); //$NON-NLS-1$
		}

		return null;
	}

	private String renderContentAsHTML() {

		IStatus status = Status.OK_STATUS;

		try {
			IFile file = (IFile) textGridObject.getAdapter(IFile.class);
			InputStream in = file.getContents(true);
			
			
//			OMElement root = new StAXOMBuilder(in).getDocumentElement();
// 			root.serialize(baos);
//			ByteArrayOutputStream resultStream = new ByteArrayOutputStream();
			return IOUtils.toString(in, "UTF-8"); //$NON-NLS-1$
			
//			// ? This does not actually produce HTML ...  
//			XMLPrettyPrinter.prettify(root, resultStream);
//
//			return resultStream.toString("UTF-8");

		} catch (IOException e) {
			status = handleError(
					e,
					"Could not generate HTML representation of the content for {0}", //$NON-NLS-1$
					getTextGridObject());
		} catch (CrudServiceException e) {
			status = handleError(
					e,
					"Could not generate HTML representation of the content for {0}", //$NON-NLS-1$
					getTextGridObject());
		} catch (CoreException e) {
			status = handleError(
					e,
					"Could not generate HTML representation of the content for {0}", //$NON-NLS-1$
					getTextGridObject());
		} catch (Exception e) {
			status = handleError(
					e,
					"Could not generate HTML representation of the content for {0}", //$NON-NLS-1$
					getTextGridObject());
		}

		return MessageFormat.format(
				"<html><head><title>Error</title></head><body><p>{0}</p>" //$NON-NLS-1$
						+ "<p>The details have been logged.</p></body></html>", //$NON-NLS-1$
				status);
	}

	public LinksObjectContentDialog(Shell parentShell, TextGridObject object) {
		super(parentShell);
		setShellStyle(getShellStyle() | SWT.RESIZE | SWT.MAX);
		this.textGridObject = object;
	}

	/**
	 * Creates and opens a new non-blocking {@link LinksObjectContentDialog}.
	 * 
	 * @param textGridObject
	 *            The object to display
	 * @param parentShell
	 *            The parent shell for the dialog.
	 * @return The dialog.
	 */
	public static LinksObjectContentDialog show(TextGridObject textGridObject,
			Shell parentShell) {
		LinksObjectContentDialog dialog = new LinksObjectContentDialog(
				parentShell, textGridObject);
		dialog.setBlockOnOpen(false);
		dialog.open();
		return dialog;
	}

	public void setTextGridObject(TextGridObject textGridObject) {
		this.textGridObject = textGridObject;
	}

	public TextGridObject getTextGridObject() {
		return textGridObject;
	}

}