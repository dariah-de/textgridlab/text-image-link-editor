package info.textgrid.lab.linkeditor.rcp_linkeditor.dialogs;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.linkeditor.rcp_linkeditor.dialogs.messages"; //$NON-NLS-1$
	public static String LinksObjectContentDialog_ShowContent;
	public static String LinksObjectContentDialog_TheContentOfTheSelectedTILO;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
