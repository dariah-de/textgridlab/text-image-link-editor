package info.textgrid.lab.linkeditor.rcp_linkeditor.newObject;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.linkeditor.rcp_linkeditor.newObject.messages"; //$NON-NLS-1$
	public static String AddObjectsToLinkeditorPage_AddTGObjects;
	public static String AddObjectsToLinkeditorPage_AddXmlOrImageObjects;
	public static String AddObjectsToLinkeditorPage_AddXmlOrImageObjectsToNewTILO;
	public static String AddObjectsToLinkeditorPage_RemoveSelectedObject;
	public static String AddObjectsToLinkeditorPage_SelectedXmlORImageObjects;
	public static String AddObjectsToLinkeditorPage_ToCreateANewTILO;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
