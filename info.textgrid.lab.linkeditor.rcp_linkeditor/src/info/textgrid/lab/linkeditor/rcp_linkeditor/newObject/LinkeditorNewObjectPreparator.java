/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.rcp_linkeditor.newObject;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.linkeditor.controller.LinkEditorController;
import info.textgrid.lab.linkeditor.rcp_linkeditor.Activator;
import info.textgrid.lab.ui.core.dialogs.INewObjectPreparator;
import info.textgrid.lab.ui.core.dialogs.ITextGridWizard;

import java.util.ArrayList;

import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class LinkeditorNewObjectPreparator implements INewObjectPreparator {

	private ArrayList<TextGridObject> selectedObjects = new ArrayList<TextGridObject>();

	private TextGridObject object;

	public void setWizard(ITextGridWizard wizard) {

	}

	public void initializeObject(TextGridObject textGridObject) {
		this.object = textGridObject;
	}

	public void setSelectedObjectsList(ArrayList<TextGridObject> objects) {
		selectedObjects = objects;
	}

	public TextGridObject getNewTGObject() {
		return this.object;
	}

	public boolean performFinish(final TextGridObject textGridObject) {

		if (selectedObjects.isEmpty())
			return false;

		try {
			PlatformUI.getWorkbench().showPerspective(
					"info.textgrid.lab.linkeditor.rcp_linkeditor.perspective",
					PlatformUI.getWorkbench().getActiveWorkbenchWindow());

			LinkEditorController.getInstance().openXmlImageObjects(
					textGridObject, selectedObjects);

		} catch (WorkbenchException e) {
			Activator.handleError(e);
		}

		return true;
	}
}