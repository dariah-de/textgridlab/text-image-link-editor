/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.rcp_linkeditor.newObject;

import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.linkeditor.rcp_linkeditor.Activator;
import info.textgrid.lab.linkeditor.tools.FetchTGObjectsDialog;
import info.textgrid.lab.linkeditor.tools.IProjectBrowserListener;
import info.textgrid.lab.ui.core.dialogs.INewObjectPreparator;
import info.textgrid.lab.ui.core.dialogs.ITextGridWizard;
import info.textgrid.lab.ui.core.dialogs.ITextGridWizardPage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.PlatformUI;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class AddObjectsToLinkeditorPage extends WizardPage implements
		ITextGridWizardPage, IProjectBrowserListener {

	private LinkeditorNewObjectPreparator preparator = null;

	private TableViewer objectsTable = null;
	private Group buttonsGrp = null;
	private Group tableGroup = null;

	private Map<String, TextGridObject> nameUriMap = new HashMap<String, TextGridObject>();

	private AddObjectsToLinkeditorPage getInstance() {
		return this;
	}

	public AddObjectsToLinkeditorPage() {
		super(Messages.AddObjectsToLinkeditorPage_AddXmlOrImageObjects,
				Messages.AddObjectsToLinkeditorPage_AddXmlOrImageObjectsToNewTILO,
				null);
		setMessage(Messages.AddObjectsToLinkeditorPage_ToCreateANewTILO);
		setPageComplete(false);
	}

	public void createControl(Composite parent) {

		Composite control = new Composite(parent, SWT.NONE);
		GridDataFactory fillGrabData = GridDataFactory.fillDefaults();
		fillGrabData.grab(true, true);
		GridLayoutFactory.fillDefaults().applyTo(control);
		fillGrabData.applyTo(control);
		setControl(control);

		create(control);

		validatePageComplete();
	}

	private void validatePageComplete() {
		int count = objectsTable.getTable().getItemCount();
		if (count < 2) {
			setPageComplete(false);
			return;
		}

		TableItem[] items = objectsTable.getTable().getItems();

		boolean imageFound = false, xmlFound = false;
		for (int i = 0; i < count && !(imageFound && xmlFound); ++i) {
			if (items[i].getText().endsWith(".")) //$NON-NLS-1$
				imageFound = true;
			else
				xmlFound = true;
		}

		setPageComplete(imageFound && xmlFound);
	}

	public Composite create(Composite parent) {
		tableGroup = new Group(parent, SWT.SHADOW_ETCHED_IN | SWT.V_SCROLL
				| SWT.SCROLL_PAGE);
		tableGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		tableGroup.setLayout(new GridLayout(1, true));
		tableGroup.setText(""); //$NON-NLS-1$

		GridData urisTreeData2 = new GridData(SWT.FILL, SWT.FILL, true, false);
		urisTreeData2.heightHint = 300;
		urisTreeData2.widthHint = 100;

		objectsTable = new TableViewer(tableGroup, SWT.SINGLE | SWT.BORDER
				| SWT.V_SCROLL | SWT.H_SCROLL | SWT.VIRTUAL);

		objectsTable
				.addSelectionChangedListener(new ISelectionChangedListener() {
					public void selectionChanged(SelectionChangedEvent event) {
						// TODO
					}
				});

		Table table = objectsTable.getTable();
		table.setLayoutData(urisTreeData2);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		TableColumn tblclmnFirst = new TableColumn(table, SWT.None);
		tblclmnFirst.setWidth(100);
		tblclmnFirst.setText(Messages.AddObjectsToLinkeditorPage_SelectedXmlORImageObjects);

		buttonsGrp = new Group(tableGroup, SWT.FILL);
		buttonsGrp.setLayout(new GridLayout(2, true));
		buttonsGrp.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));

		final Button getTGUriButton = new Button(buttonsGrp, SWT.PUSH);
		getTGUriButton.setText(Messages.AddObjectsToLinkeditorPage_AddTGObjects);
		getTGUriButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				FetchTGObjectsDialog.openDialog(getInstance());
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		final Button removeButton = new Button(buttonsGrp, SWT.PUSH);
		removeButton.setText(Messages.AddObjectsToLinkeditorPage_RemoveSelectedObject);
		removeButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				removeSelectedObject();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		return tableGroup;
	}

	private void removeSelectedObject() {
		int index = objectsTable.getTable().getSelectionIndex();
		if (index >= 0) {
			objectsTable.getTable().remove(index);
			objectsTable.getTable().select(0);
		}

		validatePageComplete();
	}

	public void init(ITextGridWizard wizard, INewObjectPreparator preparator) {
		if (preparator instanceof LinkeditorNewObjectPreparator)
			this.preparator = (LinkeditorNewObjectPreparator) preparator;
	}

	public void loadObject(TextGridObject textGridObject) {
		// TODO Auto-generated method stub
	}

	public void finishPage() {
		TableItem[] items = objectsTable.getTable().getItems();

		final int itemsCount = items.length;

		if (itemsCount < 1)
			return;

		ArrayList<TextGridObject> tgObjects = new ArrayList<TextGridObject>();

		String s = ""; //$NON-NLS-1$
		for (int i = 0; i < itemsCount; ++i) {
			s = items[i].getText();
			if (s != null && !"".equals(s)) { //$NON-NLS-1$
				TextGridObject obj = nameUriMap.get(s);
				if (obj != null)
					tgObjects.add(obj);
			}
		}

		preparator.setSelectedObjectsList(tgObjects);

	}

	public void setSelectedTGObjectsFromDialog(TextGridObject[] tgObjs) {
		try {
			for (TextGridObject tgObj : tgObjs) {
				String contentType = tgObj.getContentTypeID();

				String suffix = ""; //$NON-NLS-1$

				if (contentType == null || "".equals(contentType)) //$NON-NLS-1$
					continue;
				else if (contentType.contains("image")) //$NON-NLS-1$
					suffix = "."; //$NON-NLS-1$
				else if ("text/xml".equals(contentType)) //$NON-NLS-1$
					suffix = ""; //$NON-NLS-1$
				else
					continue;

				String name = getInfoFromObject(tgObj, suffix);

				nameUriMap.put(name, tgObj);

				setSelectedTextInInput(name);

				validatePageComplete();
			}
		} catch (CoreException e) {
			Activator.handleError(e);
		}
	}

	private String getInfoFromObject(TextGridObject obj, String suffix) {

		try {
			obj.reloadMetadata(false);
			return obj.getNameCandidate() + " (" + obj.getURI() + ")" + suffix; //$NON-NLS-1$ //$NON-NLS-2$
		} catch (CrudServiceException e) {
			Activator.handleError(e);
		} catch (CoreException e) {
			Activator.handleError(e);
		}

		return null;
	}

	private void setSelectedTextInInput(String entry) {
		TableItem item = new TableItem(objectsTable.getTable(), SWT.NONE);
		item.setText(entry);
		if (entry.endsWith(".")) //$NON-NLS-1$
			setImageIconToItem(item);
		else
			setXMLIconToItem(item);
	}

	private void setXMLIconToItem(TableItem item) {
		item.setImage(new Image(PlatformUI.getWorkbench().getDisplay(),
				ImageDescriptor.createFromFile(this.getClass(),
						"/icons/xmldoc.gif").getImageData())); //$NON-NLS-1$
	}

	private void setImageIconToItem(TableItem item) {
		item.setImage(new Image(PlatformUI.getWorkbench().getDisplay(),
				ImageDescriptor.createFromFile(this.getClass(),
						"/icons/image_obj.gif").getImageData())); //$NON-NLS-1$
	}
}