/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.rcp_linkeditor.testers;

import info.textgrid.lab.linkeditor.controller.LinkEditorController;
import info.textgrid.lab.linkeditor.rcp_linkeditor.Activator;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class ActivateOperations extends PropertyTester {

	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		return activateOperations();
	}

	/**
	 * Ask whether the Link Editor have to activate the operations (without
	 * reset)
	 * 
	 * @return
	 */
	private boolean activateOperations() {

		try {
			if (PlatformUI.getWorkbench().getActiveWorkbenchWindow() == null)
				return false;

			IWorkbenchPage workbenchPage = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage();

			if (workbenchPage == null
					|| !"info.textgrid.lab.linkeditor.rcp_linkeditor.perspective"
							.equals(workbenchPage.getPerspective().getId())) {
				return false;
			}

			int image_parts = 0, editor_parts = 0;

			IViewReference[] refs = workbenchPage.getViewReferences();

			for (IViewReference r : refs) {
				if (r.getId().equals(LinkEditorController.IMAGE_VIEW_ID))
					image_parts++;
			}

			IEditorReference[] eRefs = workbenchPage.getEditorReferences();

			for (IEditorReference r : eRefs) {
				try {
					if (r.getEditorInput().getName().toLowerCase()
							.endsWith("xml"))
						editor_parts++;
				} catch (PartInitException e) {
					Activator.handleError(e);
				}
			}

			return (image_parts > 0) && (editor_parts > 0);

		} catch (Exception e) {
			return false;
		}
	}
}
