/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.rcp_linkeditor.testers;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.linkeditor.rcp_linkeditor.Activator;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.CoreException;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class CanBeLoadedToLinkEditor extends PropertyTester {

	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {

		if (receiver != null) {
			TextGridObject tgo = AdapterUtils.getAdapter(receiver,
					TextGridObject.class);
			if (tgo != null)
				return isOK(tgo);
		}

		return false;
	}

	private boolean isOK(TextGridObject tgo) {
		String contentTypeId = "";

		try {
			contentTypeId = tgo.getContentTypeID();

			if (contentTypeId.contains("image")) {
				return true;
			} else if (contentTypeId.equals("text/xml")) {
				return true;
			}
		} catch (CoreException e) {
			Activator
					.handleWarning(e,
							"Couldn't get the content type id of the selected textgrid object");
		}

		return false;
	}
}
