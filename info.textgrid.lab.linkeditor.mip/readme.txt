MIP: My Image-processing Plug-in 1.0.0.


To Run:
=======
1) compile.
2) Run-> run time workbench
3) Window->Open Perspective->Other->MPI:My Image Processing
4) You may using TAB to show/hide toolkit panel.


Note:
=====
I registered global Re/Undo command in 2.1 version. However, the ID: "IWorkbenchActionConstants.UNDO" has deprecated for 3.0, but it works with a compile warning. If you test it on 3.0 and want to get rid of it, using "ActionFactory.UNDO.getId()". I have commented in the source file.

For Help & JavaDoc:
===================
Help->Help Contents->MIP manual


Good luck.

-Chengdong ( cli4@uky.edu )
