/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui;

import info.textgrid.lab.linkeditor.mip.component.canvas.SWT2dUtil;
import info.textgrid.lab.linkeditor.mip.component.canvas.SWTImageCanvas;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Composite;

/**
 * A canvas for image processing. It will care only the image processing stuff,
 * nothing with java2d stuff.
 *
 * @author Chengdong Li
 */
public class ImageProcessCanvas extends SWTImageCanvas {

	// backup for self imageData
	private ImageData undoData = null;
	private ImageData redoData = null;

	@Override
	public ImageData getImageData() {
		ImageData imageData = super.getImageData();
		if (imageData != null) {
			return imageData;
		}
		if (sourceImage == null) {
			return null;
		}
		return sourceImage.getImageData();
	}

	/**
	 * Image data must be 32-bit depth.
	 *
	 * @param data
	 *            image data to be set.
	 */
	@Override
	public void setImageData(ImageData data) {
		super.setImageData(data);
	}

	@Override
	public Image reloadImage(String filename) {
		try {
			super.reloadImage(filename);
			if (sourceImage != null)
				SWT2dUtil.createDepth32Data(sourceImage
						.getImageData());
		} catch (Exception e) {
			System.out.println("Incorrect file format!");
			return null;
		}
		return sourceImage;
	}

	/**
	 * Constructor for ImageProcessCanvas
	 *
	 * @param parent
	 *            parent of this control
	 */
	public ImageProcessCanvas(final Composite parent) {
		super(parent);
	}

	/**
	 * Constructor for ImageProcessCanvas.
	 *
	 * @param parent
	 *            the parent of this control
	 * @param style
	 *            style of this control
	 */
	public ImageProcessCanvas(final Composite parent, int style) {
		super(parent, style);
	}

	/**
	 * Get the image data from redo buffer
	 *
	 * @return image data from redo buffer
	 */
	public ImageData getRedoData() {
		return redoData;
	}

	/**
	 * Get the image data from undo buffer
	 *
	 * @return image data in the undo buffer
	 */
	public ImageData getUndoData() {
		return undoData;
	}

	/**
	 * backup current image data to the undo buffer
	 */
	public void backupUndoData() {
		ImageData source = this.getImageData();
		if (source != null)
			undoData = (ImageData) source.clone();
	}

	/**
	 * Restore the image data from the undo buffer
	 */
	public void restoreFromUndo() {
		setImageData(undoData);
		undoData = null;
	}

	/**
	 * Save the image data into redo buffer
	 */
	public void backupRedoData() {
		ImageData source = this.getImageData();
		if (source != null) {
			redoData = (ImageData) source.clone();
		}
	}

	/**
	 * Restore the image data from redo buffer
	 */
	public void restoreFromRedo() {
		setImageData(redoData);
		redoData = null;
	}

	/**
	 * Reload the image data inside the selected rectangle It will also refresh
	 * the image
	 *
	 * @param data
	 *            sub-image (part of whole image) to be loaded
	 */
	public void reloadSelectImage(ImageData data) {
		if (selectedShape == null || (!selectedShape.isCompletedShape())) {
			setImageData(data);
			return;
		}

		ImageData imageData = getImageData();
		// SWT2dUtil.setSubImage(imageData,data,selectRect);
		setImageData(imageData);
	}

	/**
	 * Initialize the extensible context menu.
	 *
	 * @param menuManager
	 *            IMenuManager
	 */
	@Override
	public void initContextMenu(IMenuManager menuManager) {
		super.initContextMenu(menuManager);
	}
}