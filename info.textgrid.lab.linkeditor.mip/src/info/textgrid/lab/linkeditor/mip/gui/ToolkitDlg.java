/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui;

import info.textgrid.lab.linkeditor.mip.MIPImagePlugin;
import info.textgrid.lab.linkeditor.mip.component.canvas.session.IImageSession;
import info.textgrid.lab.linkeditor.mip.gui.toolkitdelegate.IToolkitCheckButton;

import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TaskItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.PlatformUI;

/**
 * A dialog to show the toolkit panel.
 *
 * @author Chengdong Li
 */
public class ToolkitDlg extends Dialog implements Observer {
	private ImagePanel panel;
	public Shell shell;
	private final int COL = 5;
	private HashMap<String, ToolItem> itemMap = new HashMap<String, ToolItem>();
	private ToolItem activeItem;
	private Canvas colorCanvas;
	private Image switchImage;

	private int item_width = 25;
	private int item_height = 28;

	/**
	 * Constructor.
	 *
	 * @param parent
	 *            Shell
	 */
	public ToolkitDlg(ImagePanel panel) {
		super(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
				SWT.DIALOG_TRIM);
		this.panel = panel;
		panel.getTkSetting().addObserver(this);
		switchImage = MIPImagePlugin.getSwitchImage();

	}

	/**
	 * Show the dialog.
	 */
	public Object open() {
		Shell parent = this.getParent();

		if (shell == null) {
			shell = new Shell(parent, SWT.TITLE | SWT.DIALOG_TRIM);
		}
		shell.setText(Messages.ToolkitDlg_Toolkit);
		shell.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent event) {
				if (event.keyCode == SWT.TAB) { // Tab key
					shell.setVisible(false);
					panel.imageCanvas.setFocus();
				}
			}
		});

		createDialogArea(shell);
		int rows1 = (int) Math.ceil(((double) panel.sidList.size()) / COL);
		int rows2 = (int) Math.ceil(((double) panel.getTkSetting()
				.getChkButtonList().size())
				/ COL);
		shell.setSize((item_width + 2) * COL + 4, (item_height + 2)
				* (rows1 + rows2) + (item_width + 2) * COL + 30);
		shell.setLocation(100, 100);
		shell.open();

		// Possible bug here: confilict with thumbView
		// Display display = parent.getDisplay();
		// while (!shell.isDisposed()) {
		// if (!display.readAndDispatch())
		// display.sleep();
		// }

		return this;
	}

	/**
	 * Method inherited from Dialog. This will create and initalize GUI.
	 *
	 * @param parent
	 *            Composite widget.
	 */
	public Control createDialogArea(Composite parent) {
		FormLayout layout;
		FormData formData;
		Vector v = panel.sidList;
		int rows = (int) Math.floor(((double) panel.sidList.size()) / COL);

		parent.setLayout(new FillLayout());

		Composite container = new Composite(parent, SWT.NULL);
		layout = new FormLayout();
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		container.setLayout(layout);

		Composite buttonGroup = new Composite(container, SWT.NULL);
		layout = new FormLayout();
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		buttonGroup.setLayout(layout);

		int offset = 0;
		formData = new FormData();
		formData.top = new FormAttachment(0, offset);
		formData.left = new FormAttachment(0, 0);
		formData.right = new FormAttachment(100, 0);
		buttonGroup.setLayoutData(formData);

		for (int i = 0; i < COL * rows - 1; i += COL) {
			ToolBar toolBar = new ToolBar(buttonGroup, SWT.FLAT);
			toolBar.setSize((item_width + 1) * COL, item_height);
			formData = new FormData();
			formData.top = new FormAttachment(0, (i / COL) * item_height);
			formData.left = new FormAttachment(0, 0);
			formData.width = (item_width + 1) * COL;
			formData.height = item_height;
			toolBar.setLayoutData(formData);

			for (int j = 0; j < COL; j++) {
				// create the check button toolbar
				Object key = v.elementAt(i + j);
				IImageSession session = panel.sessionMap.get(key);
				addOneSession(session, toolBar);
			}
		}
		if (rows * COL != panel.sidList.size()) {
			ToolBar toolBar = new ToolBar(buttonGroup, SWT.FLAT);
			toolBar.setSize((item_width + 1) * COL, item_height);
			formData = new FormData();
			formData.top = new FormAttachment(0, (rows) * item_height);
			formData.left = new FormAttachment(0, 0);
			formData.width = (item_width + 1) * COL;
			formData.height = item_height;
			toolBar.setLayoutData(formData);
			for (int i = COL * rows; i < v.size(); i++) {
				Object key = v.elementAt(i);
				IImageSession session = panel.sessionMap.get(key);
				addOneSession(session, toolBar);
			}
		}

		rows = (int) Math.ceil(((double) panel.sidList.size()) / COL);
		// update offset.
		offset += item_height * (rows);
		// now initiate the color area.
		Group colorGroup = new Group(container, SWT.SHADOW_IN);
		formData = new FormData();
		formData.top = new FormAttachment(0, offset);
		formData.left = new FormAttachment(0, 0);
		formData.width = item_width * COL;
		formData.height = item_width * COL;
		colorGroup.setLayoutData(formData);

		layout = new FormLayout();
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		colorGroup.setLayout(layout);

		colorCanvas = new ColorCanvas(colorGroup, SWT.NULL);
		formData = new FormData();
		formData.top = new FormAttachment(0, 0);
		formData.left = new FormAttachment(0, 0);
		formData.width = item_width * COL;
		formData.height = item_width * COL;
		colorCanvas.setLayoutData(formData);
		colorCanvas.setToolTipText(Messages.ToolkitDlg_Colorselect);
		
		//adding grid on/off button and others (clone, image resize)
		// update offset.
		offset += item_width * COL;
		// now initiate the check buttons area.
		Group chkButtonGroup = new Group(container, SWT.SHADOW_IN);
		formData = new FormData();
		formData.top = new FormAttachment(0, offset);
		formData.left = new FormAttachment(0, 0);
		formData.width = item_width * COL;
		formData.height = item_width * COL;
		chkButtonGroup.setLayoutData(formData);

		layout = new FormLayout();
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		chkButtonGroup.setLayout(layout);

		// update v.
		v = panel.getTkSetting().getChkButtonList();
		rows = (int) Math.floor(((double) v.size()) / COL);

		for (int i = 0; i < COL * rows - 1; i += COL) {
			ToolBar toolBar = new ToolBar(chkButtonGroup, SWT.FLAT);
			toolBar.setSize((item_width + 1) * COL, item_height);
			formData = new FormData();
			formData.top = new FormAttachment(0, (i / COL) * item_height);
			formData.left = new FormAttachment(0, 0);
			formData.width = (item_width + 1) * COL;
			formData.height = item_height;
			toolBar.setLayoutData(formData);

			for (int j = 0; j < COL; j++) {
				// create the check button toolbar
				Object key = v.elementAt(i + j);
				IToolkitCheckButton handler = (IToolkitCheckButton) panel
						.getTkSetting().getChkButtonMap().get(key);
				if(key.toString().equals("toolbar.cloneselection")  ||
						key.toString().equals("toolbar.imagefit") ||
						key.toString().equals("toolbar.imagevertical") ||
						key.toString().equals("toolbar.imagehorizontal") ||
						key.toString().equals("toolbar.imageoriginal")){
						addCHKButton(handler, toolBar, SWT.NORMAL);
				} else {
					addCHKButton(handler, toolBar);
				}
			}
		}
		if (rows * COL != v.size()) {
			ToolBar toolBar = new ToolBar(chkButtonGroup, SWT.FLAT);
			toolBar.setSize((item_width + 1) * COL, item_height);
			formData = new FormData();
			formData.top = new FormAttachment(0, (rows) * item_height);
			formData.left = new FormAttachment(0, 0);
			formData.width = (item_width + 1) * COL;
			formData.height = item_height;
			toolBar.setLayoutData(formData);
			for (int i = COL * rows; i < v.size(); i++) {
				Object key = v.elementAt(i);
				IToolkitCheckButton handler = (IToolkitCheckButton) panel
						.getTkSetting().getChkButtonMap().get(key);
				if(key.toString().equals("toolbar.cloneselection")  ||
					key.toString().equals("toolbar.imagefit") ||
					key.toString().equals("toolbar.imagevertical") ||
					key.toString().equals("toolbar.imagehorizontal") ||
					key.toString().equals("toolbar.imageoriginal")){
					addCHKButton(handler, toolBar, SWT.NORMAL);
				} else {
					addCHKButton(handler, toolBar);
				}
			}
		}
		return parent;
	}

	private void addOneSession(IImageSession session, ToolBar toolBar) {
		ToolItem item = new ToolItem(toolBar, SWT.CHECK);
		itemMap.put(session.getSessionID(), item);
		item.setData(session.getSessionID());
		item.setImage(session.getSessionIcon().createImage());
		item.setToolTipText(mapSessionToolTip(session.getSessionToolTip()));
		if (session.isSessionSelected())
			activeItem = item;
		item.setSelection(session.isSessionSelected());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ToolItem ti = (ToolItem) e.getSource();
				String id = (String) ti.getData();
				handleSessionAction(id);
				panel.setSessionByID(id);
			}
		});
	}

	/**
	 * Map the Session ToolTip
	 *
	 * @param s
	 *            message
	 * @param res
	 *            message to be shown
	 */
	public String mapSessionToolTip(String s) {
		String res = ""; //$NON-NLS-1$
		if (s.equals("session.default.tooltip")) { //$NON-NLS-1$
			res = Messages.ToolkitDlg_NoModeSelected;
		} else if (s.equals("toolbar.select.tooltip")) { //$NON-NLS-1$
			res = Messages.ToolkitDlg_SetRectangleMode;
		} else if (s.equals("toolbar.poly.tooltip")) { //$NON-NLS-1$
			res = Messages.ToolkitDlg_SetPolygoneMode;
		} else if (s.equals("toolbar.lense.tooltip")) { //$NON-NLS-1$
			res = Messages.ToolkitDlg_SetLenseMode;
		} else if (s.equals("toolbar.zoomin.tooltip")) { //$NON-NLS-1$
			res = Messages.ToolkitDlg_SetZoomInMode;
		} else if (s.equals("toolbar.zoomout.tooltip")) { //$NON-NLS-1$
			res = Messages.ToolkitDlg_SetZoomOutMode;
		} else if (s.equals("toolbar.move.tooltip")) { //$NON-NLS-1$
			res = Messages.ToolkitDlg_SetMoveMode;
		} else if (s.equals("toolbar.dockingLines.tooltip")) { //$NON-NLS-1$
			res = Messages.ToolkitDlg_SetDockingLineMode;
		} else if (s.equals("toolbar.rotate.tooltip")) {
			res = Messages.ToolkitDlg_RotateSelection;
		} else {
			res = Messages.ToolkitDlg_None;
		}
		return res;
	}

	private void addCHKButton(IToolkitCheckButton handler, ToolBar toolBar, int style) {
		ToolItem item = new ToolItem(toolBar, style);
		itemMap.put(handler.getId(), item);
		item.setData(handler.getId());
		item.setImage(handler.getIcon().createImage());
		item.setToolTipText(handler.getToolTip());
		
		
		if (handler.isSelected())
			activeItem = item;
		item.setSelection(handler.isSelected());
		item.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ToolItem ti = (ToolItem) e.getSource();
				String id = (String) ti.getData();
				IToolkitCheckButton handler = (IToolkitCheckButton) (panel
						.getTkSetting().getChkButtonMap().get(id));
				handler.setSelected(ti.getSelection());
				handler.run();
			}
		});
	}
	
	private void addCHKButton(IToolkitCheckButton handler, ToolBar toolBar) {
		addCHKButton(handler, toolBar, SWT.CHECK);
	}	
	
	/**
	 * Maintain a set of mutually exclusive ToolItems, and toggles the old item
	 * based on new id off if necessary.
	 *
	 * @param id
	 *            new toolitem id to be actived
	 * @return
	 */
	private void handleSessionAction(String id) {
		ToolItem item = itemMap.get(id);
		if (activeItem != null) {
			activeItem.setSelection(false);
		}
		item.setSelection(true);
		activeItem = item;
	}

	class ColorCanvas extends Canvas {
		Rectangle foreRect;
		Rectangle backRect;
		Rectangle iconRect;
		Rectangle switchRect;
		RGB defaultRGB;

		/**
		 * @param parent
		 * @param style
		 */
		public ColorCanvas(Composite parent, int style) {
			super(parent, style);
			this.addPaintListener(new PaintListener() {
				@Override
				public void paintControl(PaintEvent e) {
					paint(e.gc);
				}
			});
			this.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseDown(MouseEvent e) {
					onMouseDown(e);
				}
			});
		}

		private void paint(GC gc) {
			Rectangle bound = colorCanvas.getBounds();
			Color selected;
			Color unSelected;
			int w = bound.width;
			int h = bound.height;
			foreRect = new Rectangle(w / 16, h / 16, (9 * w) / 16, (9 * h) / 16);
			backRect = new Rectangle(6 * w / 16, 6 * h / 16, (9 * w) / 16,
					(9 * h) / 16);

			Rectangle fRect = new Rectangle(foreRect.x + 2, foreRect.y + 2,
					foreRect.width - 3, foreRect.height - 3);
			Rectangle bRect = new Rectangle(backRect.x + 2, backRect.y + 2,
					backRect.width - 3, backRect.height - 3);

			gc.drawRectangle(backRect.x, backRect.y, backRect.width,
					backRect.height);
			if (defaultRGB == null) {
				defaultRGB = gc.getBackground().getRGB();
			} else {
				gc.getBackground().dispose();
			}

			RGB foreRGB = panel.getTkSetting().getForeRGB();
			RGB backRGB = panel.getTkSetting().getBackRGB();

			unSelected = new Color(this.getDisplay(), backRGB.red,
					backRGB.green, backRGB.blue);
			selected = new Color(this.getDisplay(), foreRGB.red, foreRGB.green,
					foreRGB.blue);
			panel.imageCanvas.setUnSelectedColor(unSelected);
			panel.imageCanvas.setSelectedColor(selected);

			gc.setBackground(unSelected);
			gc.fillRectangle(bRect);
			gc.getBackground().dispose();
			gc.setBackground(selected);
			gc.fillRectangle(foreRect.x, foreRect.y, foreRect.width,
					foreRect.height);
			gc.getBackground().dispose();
			gc.setBackground(new Color(this.getDisplay(), foreRGB.red,
					foreRGB.green, foreRGB.blue));
			gc.fillRectangle(fRect);
			gc.drawRectangle(foreRect.x, foreRect.y, foreRect.width,
					foreRect.height);

			// draw icon rect
			int ww = 5 * w / 16;
			int hh = 5 * h / 16;
			int x = w / 16;
			int y = 5 * h / 8;
			iconRect = new Rectangle(x, y, ww, hh);
			Rectangle sfRect = new Rectangle(x + ww / 16, y + hh / 16,
					9 * ww / 16, 9 * hh / 16);
			Rectangle sbRect = new Rectangle(x + ww - ww / 16,
					y + hh - hh / 16, -9 * ww / 16, -9 * hh / 16);
			gc.getBackground().dispose();
			gc.setBackground(new Color(this.getDisplay(), 255, 255, 255));
			gc.fillRectangle(sbRect);
			gc.getBackground().dispose();
			gc.setBackground(new Color(this.getDisplay(), 0, 0, 0));
			gc.fillRectangle(sfRect);

			// draw image
			bound = switchImage.getBounds();
			switchRect = new Rectangle(5 * w / 8, h / 16, 4 * w / 16,
					4 * h / 16);
			gc.drawImage(switchImage, 0, 0, bound.width, bound.height,
					5 * w / 8 + 4, h / 16 + 8, 4 * w / 16 - 4, 4 * h / 16 - 4);

		}

		private void onMouseDown(MouseEvent e) {
			int x = e.x;
			int y = e.y;
			if (foreRect.contains(x, y)) { // change foreground color
				ColorDialog dialog = new ColorDialog(getShell());
				dialog.setText(Messages.ToolkitDlg_Colorselect_Foreground);
				dialog.setRGB(panel.getTkSetting().getForeRGB());
				RGB rgb = dialog.open();
				if (rgb != null)
					panel.getTkSetting().setForeRGB(rgb);
			} else if (backRect.contains(x, y)) { // change background color
				ColorDialog dialog = new ColorDialog(getShell());
				dialog.setText(Messages.ToolkitDlg_Colorselect_Background);
				dialog.setRGB(panel.getTkSetting().getBackRGB());
				RGB rgb = dialog.open();
				if (rgb != null)
					panel.getTkSetting().setBackRGB(rgb);
			} else if (iconRect.contains(x, y)) { // default color
				panel.getTkSetting().setForeRGB(new RGB(0, 0, 0));
				panel.getTkSetting().setBackRGB(new RGB(255, 255, 255));
			} else if (switchRect.contains(x, y)) { // switch color
				panel.getTkSetting().switchForeBack();
			} else {
				return;
			}

		}
	}

	/**
	 * dispose this dialog.
	 */
	public void dispose() {
		Display display = this.getParent().getDisplay();
		if (shell.isDisposed()) {
			panel.getTkSetting().deleteObserver(this);
			panel.tkDlg = null;
			return;
		}
		shell.close();
		shell.dispose();
		if (display.sleep())
			display.wake();
		panel.getTkSetting().deleteObserver(this);
		panel.tkDlg = null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable arg0, Object arg1) {
		int event = panel.getTkSetting().checkEvent();
		if ((event & ToolkitSetting.COLOR_FORE) != 0) {
			// foreground color change
			colorCanvas.redraw();
		}
		if ((event & ToolkitSetting.COLOR_BACK) != 0) {
			// background color change
			colorCanvas.redraw();
		}

	}
}