package info.textgrid.lab.linkeditor.mip.gui;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.linkeditor.mip.gui.messages"; //$NON-NLS-1$
	public static String ImagePanel_ActiveLayer;
	public static String ImagePanel_Angle;
	public static String ImagePanel_NoLayerEnabledOrActive;
	public static String ImagePanel_TheCurrentLayerIs;
	public static String ImagePanel_ThereAreStillUnlinkedObjects;
	public static String StatusBar_DefaultMode;
	public static String StatusBar_PolygonMode;
    public static String StatusBar_LenseMode;
    public static String StatusBar_ZoomInMode;
    public static String StatusBar_ZoomOutMode;
    public static String StatusBar_MoveMode;
    public static String StatusBar_None;
    public static String StatusBar_DockingLineMode;
    public static String StatusBar_RectangleMode;
    public static String StatusBar_Grid_OnOff;
	public static String ToolkitDlg_NoModeSelected;
	public static String ToolkitDlg_None;
	public static String ToolkitDlg_SetDockingLineMode;
	public static String ToolkitDlg_SetLenseMode;
	public static String ToolkitDlg_SetMoveMode;
	public static String ToolkitDlg_SetPolygoneMode;
	public static String ToolkitDlg_SetRectangleMode;
	public static String ToolkitDlg_RotateSelection;
	public static String ToolkitDlg_SetZoomInMode;
	public static String ToolkitDlg_SetZoomOutMode;
	public static String ToolkitDlg_Toolkit;
	public static String ToolkitDlg_Colorselect;
	public static String ToolkitDlg_Colorselect_Foreground;
	public static String ToolkitDlg_Colorselect_Background;
	public static String ToolkitDlg_CloneSelection;
	public static String ToolkitDlg_ImgResizeFitSelection;
	public static String ToolkitDlg_ImgResizeHorizontalSelection;
	public static String ToolkitDlg_ImgResizeVerticalSelection;
	public static String ToolkitDlg_ImgResizeOriginalSelection;
	public static String DisposeDlg_Title;
	public static String DisposeDlg_Message;
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
