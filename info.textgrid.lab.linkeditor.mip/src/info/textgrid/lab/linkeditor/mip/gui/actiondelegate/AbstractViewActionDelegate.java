/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui.actiondelegate;

import info.textgrid.lab.linkeditor.mip.views.ImageView;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IViewActionDelegate;
import org.eclipse.ui.IViewPart;

/**
 * An abstract class for all the action handlers.
 * <p>
 * If you want to add a new menu/menuitem, you must add it first to the
 * plugin.xml as an viewActions extension, then subclass this class to implement
 * the corresponding algorithm in the subclass.
 * <p>
 * 
 * @author Chengdong Li
 */
public abstract class AbstractViewActionDelegate implements IViewActionDelegate {

	protected ImageView view = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IViewActionDelegate#init(org.eclipse.ui.IViewPart)
	 */
	@Override
	public void init(IViewPart viewPart) {
		if (viewPart instanceof ImageView) {
			this.view = (ImageView) viewPart;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action
	 * .IAction, org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void selectionChanged(IAction action, ISelection selection) {
	}

}
