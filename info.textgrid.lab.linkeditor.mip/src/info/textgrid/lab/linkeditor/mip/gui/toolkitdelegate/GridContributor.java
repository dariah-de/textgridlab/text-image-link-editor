/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/

package info.textgrid.lab.linkeditor.mip.gui.toolkitdelegate;

import info.textgrid.lab.linkeditor.mip.component.canvas.IPaintContributor;
import info.textgrid.lab.linkeditor.mip.component.canvas.SWT2dUtil;
import info.textgrid.lab.linkeditor.mip.gui.ImageProcessCanvas;

import java.awt.geom.AffineTransform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;

/**
 * Grid painter contributor
 * 
 * @author Chengdong
 */
public class GridContributor implements IPaintContributor {
	private final int GRID_SIZE = 40;
	ImageProcessCanvas canvas;

	@Override
	public void prePaint(GC gc) {
	}

	@Override
	public void paint(GC gc) {
	}

	@Override
	public void postPaint(GC gc) {
		Image sourceImage = canvas.getSourceImage();
		if (sourceImage == null || sourceImage.isDisposed())
			return;
		AffineTransform transform = canvas.getTransform();
		Rectangle imageBound = sourceImage.getBounds();

		int lineStyle = gc.getLineStyle();
		int lineWidth = gc.getLineWidth();
		Color foreColor = gc.getForeground();
		Color newColor = new Color(canvas.getDisplay(), 255, 0, 0);

		gc.setLineWidth(1);
		gc.setLineStyle(SWT.LINE_DOT);
		gc.setForeground(newColor);

		// horizontal line;
		for (int i = 1; i <= imageBound.width / GRID_SIZE; i++) {
			Point p1 = SWT2dUtil.transformPoint2(transform, new Point(i
					* GRID_SIZE, 0));
			Point p2 = SWT2dUtil.transformPoint2(transform, new Point(i
					* GRID_SIZE, imageBound.height));
			gc.drawLine(p1.x, p1.y, p2.x, p2.y);
		}

		// vertical line;
		for (int i = 1; i <= imageBound.height / GRID_SIZE; i++) {
			Point p1 = SWT2dUtil.transformPoint2(transform, new Point(0, i
					* GRID_SIZE));
			Point p2 = SWT2dUtil.transformPoint2(transform, new Point(
					imageBound.width, i * GRID_SIZE));
			gc.drawLine(p1.x, p1.y, p2.x, p2.y);
		}

		gc.setLineStyle(lineStyle);
		gc.setLineWidth(lineWidth);
		gc.setForeground(foreColor);
		newColor.dispose();

	}

	/**
	 * @return
	 */
	public ImageProcessCanvas getCanvas() {
		return canvas;
	}

	/**
	 * @param canvas
	 */
	public void setCanvas(ImageProcessCanvas canvas) {
		this.canvas = canvas;
	}

}
