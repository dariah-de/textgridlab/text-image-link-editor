/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.part.EditorInputTransfer;
import org.eclipse.ui.part.ResourceTransfer;

/**
 * Drop target adapter: reload the dropped image.
 * 
 * Origianally from EditrArea's EditorAreaDropAdapter.
 * 
 * @see org.eclipse.ui.internal.EditorAreaDropAdapter
 * @see org.eclipse.ui.internal.EditorArea
 * 
 * @author Chengdong Li
 */
public class ImageDropAdapter extends DropTargetAdapter {

	ImageProcessCanvas canvas;

	/**
	 * Constructs a new DropTargetAdapter.
	 * 
	 * @param canvas
	 *            the target canvas
	 */
	public ImageDropAdapter(ImageProcessCanvas canvas) {
		this.canvas = canvas;
	}

	@Override
	public void drop(final DropTargetEvent event) {
		Display d = canvas.getShell().getDisplay();
		d.asyncExec(new Runnable() {
			@Override
			public void run() {
				asyncDrop(event);
			}
		});
	}

	private void asyncDrop(DropTargetEvent event) {
		/* Open Editor for resource */
		if (ResourceTransfer.getInstance().isSupportedType(
				event.currentDataType)) {
			Assert.isTrue(event.data instanceof IResource[]);
			IResource[] files = (IResource[]) event.data;
			for (int i = 0; i < files.length; i++) {
				if (files[i] instanceof IFile) {
					if (canvas.isFormatSupported(((IFile) files[i])
							.getFileExtension())) {
						canvas.reloadImage(((IFile) files[i]).getLocation()
								.toString());
					} else {
						MessageDialog
								.openError(canvas.getShell(), "Warnning",
										"You can only drag and drop 'jpg','bmp','gif','png'!!!!!");
					}
				}
			}
		}

		/* Open Editor for generic IEditorInput */
		else if (EditorInputTransfer.getInstance().isSupportedType(
				event.currentDataType)) {
			/*
			 * event.data is an array of EditorInputData, which contains an
			 * IEditorInput and the corresponding editorId
			 */
			Assert.isTrue(event.data instanceof EditorInputTransfer.EditorInputData[]);
			EditorInputTransfer.EditorInputData[] editorInputs = (EditorInputTransfer.EditorInputData[]) event.data;

			for (int i = 0; i < editorInputs.length; i++) {
				IEditorInput editorInput = editorInputs[i].input;
				canvas.reloadImage(editorInput.getName());
			}
		}

	}

	@Override
	public void dragEnter(DropTargetEvent event) {
		// make sure the file is never moved; always do a copy
		event.detail = DND.DROP_COPY;
	}

	@Override
	public void dragOperationChanged(DropTargetEvent event) {
		// make sure the file is never moved; always do a copy
		event.detail = DND.DROP_COPY;
	}

}
