package info.textgrid.lab.linkeditor.mip.gui.actiondelegate;

import org.eclipse.jface.action.IAction;

/**
 * Layer action delegate
 * 
 * @author M.Leuk
 * 
 */
public class ShowAllLayerActionDelegate extends AbstractViewActionDelegate {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public void run(IAction action) {
		this.view.imagePanel.imageCanvas.showAllLayersOnImage();
	}

}
