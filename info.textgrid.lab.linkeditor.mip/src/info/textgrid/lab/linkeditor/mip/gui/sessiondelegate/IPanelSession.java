/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui.sessiondelegate;

import info.textgrid.lab.linkeditor.mip.component.canvas.session.IImageSession;
import info.textgrid.lab.linkeditor.mip.gui.ImagePanel;

/**
 * Interface for image-panel based session.
 * 
 * @author Chengdong Li
 */
public interface IPanelSession extends IImageSession {
	public void setPanel(ImagePanel panel);

	public ImagePanel getPanel();
}
