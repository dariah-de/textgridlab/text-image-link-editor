/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui.actiondelegate;

import info.textgrid.lab.linkeditor.mip.MIPImagePlugin;
import info.textgrid.lab.linkeditor.mip.gui.ImageProcessCanvas;
import info.textgrid.lab.linkeditor.mip.views.ImageView;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.PartInitException;

/**
 * This will add an items to the navigator's context menu.
 * 
 * @author Chengdong Li
 */
public class PopupMenuActionDelegate implements IObjectActionDelegate {
	private IWorkbenchPart part;
	private String curSelection = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IObjectActionDelegate#setActivePart(org.eclipse.jface.
	 * action.IAction, org.eclipse.ui.IWorkbenchPart)
	 */
	@Override
	public void setActivePart(IAction arg0, IWorkbenchPart workbenchPart) {
		this.part = workbenchPart;
	}

	/**
	 * Action is called when popup menu is selected.
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public void run(IAction action) {
		IWorkbenchPartSite site = part.getSite();
		IWorkbenchPage page = site.getPage();
		// find and active the ThumbView
		IViewPart thumbView = page.findView("edu.uky.mip.views.ThumbView");
		if (thumbView == null) {
			try {
				thumbView = page.showView("edu.uky.mip.views.ThumbView");
			} catch (PartInitException e) {
				e.printStackTrace();
			}
		}

		// find and active the ImageView
		ImageView imageView = (ImageView) page
				.findView("edu.uky.mip.views.ImageView");
		if (imageView == null) {
			try {
				imageView = (ImageView) page
						.showView("edu.uky.mip.views.ImageView");
			} catch (PartInitException e) {
				e.printStackTrace();
			}
		}

		if (curSelection != null) {
			String s = MIPImagePlugin.getResourceString("PopupAction.addImage");
			if (action.getText().equals(s)) {
				ImageProcessCanvas canvas = imageView.imagePanel.imageCanvas;
				canvas.reloadImage(curSelection);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action
	 * .IAction, org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			Object first = ((IStructuredSelection) selection).getFirstElement();
			if (first instanceof IFile) {
				curSelection = ((IFile) first).getLocation().toOSString();
			}
		}

	}

}
