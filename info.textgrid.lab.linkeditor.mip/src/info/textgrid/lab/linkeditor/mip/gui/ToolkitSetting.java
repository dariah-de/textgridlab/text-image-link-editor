/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui;

import info.textgrid.lab.linkeditor.mip.gui.toolkitdelegate.IToolkitCheckButton;

import java.util.HashMap;
import java.util.Observable;
import java.util.Vector;

import org.eclipse.swt.graphics.RGB;

/**
 * 
 * Toolkit setting.
 * 
 * @author Chengdong Li
 */
public class ToolkitSetting extends Observable {
	
	//Attention! These are hex values! 
	public static final int COLOR_FORE = 0x01;
	public static final int COLOR_BACK = 0x02;
	public static final int GRID_ON = 0x04;
	public static final int CLONE_SELECTION = 0x08;
	public static final int IMG_RESIZE_ORIGINAL = 0x10;
	public static final int IMG_RESIZE_FIT = 0x20;
	public static final int IMG_RESIZE_VERTICAL = 0x40;
	public static final int IMG_RESIZE_HORIZONTAL = 0x80;
	
	private int lineSize = 1;

	private HashMap chkButtonMap = new HashMap();
	private Vector chkButtonList = new Vector();

	RGB foreRGB = new RGB(204, 0, 34); // Rot
	RGB backRGB = new RGB(85, 0, 238); // Blau

	private int event = 0x0;

	private int counter = 0;

	private boolean gridOn = false;

	/**
	 * Check event info.
	 * 
	 * @return integer of envent info.
	 */
	public int checkEvent() {
		int ret = event;
		counter++;
		if (counter == this.countObservers()) {
			event = 0x0;
			counter = 0;
		}
		return ret;
	}

	/**
	 * @return Returns the backRGB.
	 */
	public RGB getBackRGB() {
		return backRGB;
	}

	/**
	 * @param rgb
	 *            The background RGB to set.
	 */
	public void setBackRGB(RGB rgb) {
		if (rgb == null)
			return;
		this.backRGB = rgb;
		event = event | COLOR_BACK;
		fireModelChanged();
	}

	/**
	 * @return Returns the foreRGB.
	 */
	public RGB getForeRGB() {
		return foreRGB;
	}

	/**
	 * @param rgb
	 *            The foreground RGB to set.
	 */
	public void setForeRGB(RGB rgb) {
		if (rgb == null)
			return;
		this.foreRGB = rgb;
		event = event | COLOR_FORE;
		fireModelChanged();
	}

	/**
	 * Turn on/off grid.
	 * 
	 * @param gridon
	 *            grid status.
	 */
	public void setGridOn(boolean on) {
		this.gridOn = on;
		event = event | GRID_ON;
		fireModelChanged();
	}

	public void switchForeBack() {
		RGB tempf = foreRGB;
		RGB tempb = backRGB;
		foreRGB = tempb;
		backRGB = tempf;
		event = event | COLOR_BACK | COLOR_FORE;
		fireModelChanged();
	}

	/**
	 * Send model changed notification.
	 */
	public void fireModelChanged() {
		setChanged();
		notifyObservers();
	}

	/*
	 * Create the check-box button handler map.
	 */
	public void registerCHKButtonHandler(String id, IToolkitCheckButton handler) {
		chkButtonMap.put(id, handler);
		chkButtonList.add(id);
	}

	/**
	 * @return
	 */
	public Vector getChkButtonList() {
		return chkButtonList;
	}

	/**
	 * @param vector
	 */
	public void setChkButtonList(Vector vector) {
		chkButtonList = vector;
	}

	/**
	 * @return
	 */
	public HashMap getChkButtonMap() {
		return chkButtonMap;
	}

	/**
	 * @param map
	 */
	public void setChkButtonMap(HashMap map) {
		chkButtonMap = map;
	}

	/**
	 * @return
	 */
	public boolean isGridOn() {
		return gridOn;
	}

	/**
	 * Get Line size.
	 * 
	 * @return line size.
	 */
	public int getLineSize() {
		return lineSize;
	}

	/**
	 * Set line size.
	 * 
	 * @param i
	 *            line width.
	 */
	public void setLineSize(int i) {
		lineSize = i;
	}

	public void cloneSelection() {
		event = event | CLONE_SELECTION;
		fireModelChanged();
	}

	public void imgResizeOriginal() {
		event = event | IMG_RESIZE_ORIGINAL;
		fireModelChanged();
	}

	public void imgResizeFit() {
		event = event | IMG_RESIZE_FIT;
		fireModelChanged();
	}

	public void imgResizeVertical() {
		event = event | IMG_RESIZE_VERTICAL;
		fireModelChanged();
	}
	
	public void imgResizeHorizontal(){
		event = event | IMG_RESIZE_HORIZONTAL;
		fireModelChanged();
	}

}
