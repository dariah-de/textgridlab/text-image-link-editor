/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui.sessiondelegate.curve;

/**
 * @author Stephen Hunt
 * 
 *         General point class for storing points of double values
 */
public class DPoint {
	public double x;
	public double y;

	/**
	 * Constructor Point.
	 * 
	 * @param x
	 * @param y
	 */
	public DPoint(double newX, double newY) {
		x = newX;
		y = newY;
	}

}
