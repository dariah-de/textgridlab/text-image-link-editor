/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui.sessiondelegate;

import info.textgrid.lab.linkeditor.mip.component.canvas.SWT2dUtil;
import info.textgrid.lab.linkeditor.mip.component.canvas.SessionedImageCanvas;
import info.textgrid.lab.linkeditor.mip.component.canvas.session.AbstractImageSession;
import info.textgrid.lab.linkeditor.mip.gui.ImagePanel;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;

/**
 * Pencil session delegate.
 * 
 * @author Chengdong Li
 */
public class PencilDelegate extends AbstractImageSession implements
		IPanelSession {
	ImagePanel panel;
	private Image all; // save current offscreen image, this is diff from
	// AffineCanvas
	private boolean mousePressed = false; // mouse is pressed

	Vector ptList = new Vector(); // current operation point list
	Vector ptRGBs = new Vector(); // original point colors.
	RGB prevRGB; // previous background color, for undo only
	// previous line size: initiated at drawPencilPoint; used
	// when cancel current drawing in keypressed.
	int lineSize = 1;

	public PencilDelegate() {
	}

	/**
	 * Constructs a session.
	 * 
	 * @param id
	 *            session id
	 * @param panel
	 *            Image panel.
	 */
	public PencilDelegate(String id, ImagePanel panel) {
		super(id);
		this.panel = panel;
	}

	@Override
	public void mouseMove(MouseEvent evt) {
		super.mouseMove(evt);

		SessionedImageCanvas canvas = (SessionedImageCanvas) (evt.getSource());
		if (sessionCanvas.getSourceImage() == null)
			return;

		if (mousePressed) {
			Point pt = canvas.screenToImage(new Point(evt.x, evt.y));
			if (addPoint(pt)) {
				drawPencilPoint();
			}
		}
	}

	@Override
	public void mouseDown(MouseEvent evt) {
		super.mouseDown(evt);
		if (evt.button == 1) { // left button only{
			mousePressed = true;
			ptList.clear();
			ptRGBs.clear();
			refreshAllImage();

			SessionedImageCanvas canvas = (SessionedImageCanvas) evt
					.getSource();
			// if no image, we return;
			if (canvas.getSourceImage() == null)
				return;

			RGB rgb = panel.getTkSetting().getForeRGB();
			Point pt = canvas.screenToImage(new Point(evt.x, evt.y));

			prevRGB = rgb;
			if (addPoint(pt))
				drawPencilPoint();
		}
	}

	@Override
	public void mouseUp(MouseEvent evt) {
		super.mouseUp(evt);
		mousePressed = false;
	}

	private void drawPencilPoint() {
		Image imageSource;
		ImageData data;

		SessionedImageCanvas canvas = panel.imageCanvas;
		imageSource = canvas.getSourceImage();
		data = canvas.getImageData(); // original data

		if (canvas.getSourceImage() == null)
			return;
		ptRGBs.clear();
		lineSize = panel.getTkSetting().getLineSize();
		Image temp = new Image(canvas.getDisplay(), lineSize, lineSize);
		GC tempGC = new GC(temp);
		tempGC.drawImage(imageSource, 0, 0, lineSize, lineSize, 0, 0, lineSize,
				lineSize);
		GC sourceGC = new GC(imageSource);
		RGB rgb = panel.getTkSetting().getForeRGB();
		Rectangle bound = imageSource.getBounds();
		boolean drawOrigin = true;
		for (int i = 0; i < ptList.size(); i++) {
			Point pp = (Point) ptList.elementAt(i);
			int dx = SWT2dUtil.getLeft(pp.x, lineSize, bound);
			int dy = SWT2dUtil.getTop(pp.y, lineSize, bound);
			ptRGBs.add(new BlockRGB(lineSize, lineSize, dx, dy, data)); // save
			// old
			// rgb
			sourceGC.setBackground(new Color(canvas.getDisplay(), rgb));
			sourceGC.fillRectangle(0, 0, lineSize, lineSize);
			sourceGC.copyArea(0, 0, lineSize, lineSize, dx, dy);
			if (pp.x == 0 && pp.y == 0)
				drawOrigin = false;
		}
		if (drawOrigin) {
			sourceGC.drawImage(temp, 0, 0, lineSize, lineSize, 0, 0, lineSize,
					lineSize);
		}
		sourceGC.dispose();
		tempGC.dispose();
		temp.dispose();
		canvas.redraw();
	}

	public RGB getRGB(Image image, int x, int y) {
		if (image == null)
			return null;
		Rectangle bound = image.getBounds();
		if (x < bound.width && x >= 0 && y < bound.height && y >= 0) {
			Image tempImage = new Image(sessionCanvas.getDisplay(), 1, 1);
			GC tempGC = new GC(tempImage);
			tempGC.drawImage(image, x, y, 1, 1, 0, 0, 1, 1);
			tempGC.dispose();
			ImageData data = tempImage.getImageData();
			int pixel = data.getPixel(0, 0);
			RGB rgb = data.palette.getRGB(pixel);
			tempImage.dispose();
			return rgb;
		}
		return null;
	}

	private void refreshAllImage() {
		if (all != null) {
			all.dispose();
			all = null;
		}
		all = new Image(sessionCanvas.getDisplay(),
				sessionCanvas.getClientArea().width,
				sessionCanvas.getClientArea().height);
		GC canvasGC = new GC(sessionCanvas);
		canvasGC.copyArea(all, 0, 0);
		canvasGC.dispose();
	}

	/*
	 * check the point p to see if it is inside the current selectRect, if
	 * inside, then add point to ptList. @param p point in screen domain.
	 * 
	 * @return true if has been added or false not
	 */
	private boolean addPoint(Point p) {
		TGShape selectShape = sessionCanvas.getSelectedShape();
		if (selectShape == null || (!selectShape.isCompletedShape())
				|| selectShape.contains(p.x, p.y)) {
			ptList.add(p);
			return true;
		}
		return false;
	}

	/*
	 * @see KeyListener#keyPressed
	 */
	@Override
	synchronized public void keyPressed(KeyEvent e) {
		if (e.keyCode == SWT.ESC && e.stateMask == 0) { // ESC
			// undo
			if (ptList.size() == 0)
				return;
			try { // Here probably need synchronize.
				SessionedImageCanvas canvas = sessionCanvas;

				Image temp = new Image(canvas.getDisplay(), 1, 1);
				GC tempGC = new GC(temp);
				tempGC.drawImage(canvas.getSourceImage(), 0, 0, 1, 1, 0, 0, 1,
						1);
				GC sourceGC = new GC(canvas.getSourceImage());
				Rectangle bound = canvas.getSourceImage().getBounds();

				RGB rgb = panel.getTkSetting().getForeRGB();
				boolean drawOrigin = true;
				for (int i = 0; i < ptRGBs.size(); i++) {
					Point pp = (Point) ptList.elementAt(i);
					int dx = SWT2dUtil.getLeft(pp.x, lineSize, bound);
					int dy = SWT2dUtil.getTop(pp.y, lineSize, bound);
					BlockRGB block = (BlockRGB) ptRGBs.elementAt(i);
					for (int w = 0; w < block.width; w++) {
						for (int h = 0; h < block.height; h++) {
							rgb = (RGB) block.rgbs.elementAt(w * block.height
									+ h);
							sourceGC.setBackground(new Color(canvas
									.getDisplay(), rgb));
							sourceGC.fillRectangle(0, 0, 1, 1);
							sourceGC.copyArea(0, 0, 1, 1, dx + w, dy + h);
							if (dx + w == 0 && dy + h == 0)
								drawOrigin = false;
						}
					}
				}
				if (drawOrigin) {
					sourceGC.drawImage(temp, 0, 0, 1, 1, 0, 0, 1, 1);
				}
				sourceGC.dispose();
				tempGC.dispose();
				temp.dispose();
				ptRGBs.clear();
				ptList.clear();
				canvas.redraw();
			} catch (Exception ex) {
			}
		}
	}

	/**
	 * @return Returns the panel.
	 */
	@Override
	public ImagePanel getPanel() {
		return panel;
	}

	/**
	 * @param panel
	 *            The panel to set.
	 */
	@Override
	public void setPanel(ImagePanel panel) {
		this.panel = panel;
	}

	/**
	 * Customize session cursor
	 */
	@Override
	public Cursor getSessionCursor() {
		ImageData sourceData = new ImageData(32, 32, 1, paletteData);
		ImageData maskData = new ImageData(32, 32, 1, paletteData);
		sourceData.setPixels(0, 0, 1024, cursorPencil, 0);
		maskData.setPixels(0, 0, 1024, cursorMask, 0);
		return new Cursor(sessionCanvas.getDisplay(), sourceData, maskData, 0,
				31);
	}

	static int[] cursorPencil = new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
			0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0,
			0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
			0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1,
			0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
}

class BlockRGB {
	public int width;
	public int height;
	public Vector rgbs = new Vector();

	public BlockRGB(int width, int height, int x, int y, ImageData data) {
		this.width = width;
		this.height = height;
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				rgbs.add(SWT2dUtil.getRGB(data, x + i, y + j));
			}
		}
	}
}

/*
 * // This is a potential bug.......... ===================================
 * 
 * Image temp = new Image(canvas.getDisplay(), 1, 1); GC sourceGC=new
 * GC(canvas.getSourceImage()); GC tempGC = new GC(temp); RGB
 * rgb=panel.tkSetting.getForeRGB(); System.out.println("mouseup: "+rgb);
 * System.out.println("mouseup : tksettin fore "+panel.tkSetting.getForeRGB());
 * System.out.println("mouseup : tksettin back "+panel.tkSetting.getBackRGB());
 * // sourceGC.setBackground(new Color(canvas.getDisplay(),rgb)); for (int i =
 * 0; i < ptList.size(); i++) { Point p = (Point) ptList.elementAt(i); Point pp
 * = SWT2dUtil.inverseTransformPoint2(af, p); origList.add(pp);
 * ptRGBs.add(canvas.getRGB(pp.x,pp.y)); tempGC.getForeground().dispose();
 * tempGC.setForeground(new Color(canvas.getDisplay(),rgb));
 * tempGC.fillRectangle(pp.x,pp.y,1,1); sourceGC.drawImage(temp,pp.x-1,pp.y-1);
 * // tempGC.copyArea(canvas.getSourceImage(),pp.x-1,pp.y-1); }
 * sourceGC.dispose(); tempGC.dispose(); temp.dispose();
 */
