/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

/**
 * A panel which draw intensity histogram of the given image
 * 
 * @author Chengdong Li
 * 
 */
public class HistogramPanel {
	private final static String[] CHANNEL_ITEMS = { "Luminosity", "Red",
			"Green", "Blue" };
	// private ImageData imageData=null;
	private boolean hasImage = false; // has image data
	// private HistogramPanel instance;
	// private Composite parent=null;

	Composite displayArea;
	Composite upperGroup;
	Composite middleGroup;
	Composite lowerGroup;

	Combo channelCombo;
	Label levelText, countText, percentText;

	Canvas histogramCanvas;
	Canvas colorCanvas;

	Cursor histCursor = null;

	int hist[][] = new int[4][256]; // 0:all; 1:r; 2:g; 3:b
	int intensity[];

	public HistogramPanel(Composite parent) {
		// this.parent = parent;
		// instance = this;
		initGUI(parent);
	}

	/**
	 * Create histogram cavas for drawing.
	 */
	private void initGUI(Composite parent) {
		GridLayout gridLayout;
		GridData gridData;

		/*** Create principal GUI layout elements ***/
		displayArea = new Composite(parent, SWT.NONE);
		gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		displayArea.setLayout(gridLayout);
		displayArea.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent evt) {
				super.controlResized(evt);
				displayArea.layout();
			}
		});

		/****** upper panels ***********/
		upperGroup = new Composite(displayArea, SWT.NONE);
		gridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_FILL);
		upperGroup.setLayoutData(gridData);
		initUpperGroup();
		middleGroup = new Composite(displayArea, SWT.NONE);
		gridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.FILL_VERTICAL);
		middleGroup.setLayoutData(gridData);
		initMiddleGroup();
		lowerGroup = new Composite(displayArea, SWT.NONE);
		gridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_FILL);
		lowerGroup.setLayoutData(gridData);
		initLowerGroup();
	}

	private void initUpperGroup() {
		GridLayout gridLayout;
		GridData gridData;

		gridLayout = new GridLayout();
		upperGroup.setLayout(gridLayout);
		gridLayout.numColumns = 2;

		Label lChannel = new Label(upperGroup, SWT.NONE);
		lChannel.setText("Channel: ");
		gridData = new GridData(GridData.VERTICAL_ALIGN_FILL);
		lChannel.setLayoutData(gridData);

		channelCombo = new Combo(upperGroup, SWT.NONE);
		channelCombo.setItems(CHANNEL_ITEMS);
		channelCombo.select(0);

		gridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_FILL);
		channelCombo.setLayoutData(gridData);

		channelCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				redraw();
			}
		});

	}

	private void initMiddleGroup() {
		GridLayout gridLayout;
		GridData gridData;

		gridLayout = new GridLayout();
		middleGroup.setLayout(gridLayout);
		gridLayout.numColumns = 1;

		histogramCanvas = new Canvas(middleGroup, SWT.NONE);
		gridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.FILL_VERTICAL);
		histogramCanvas.setLayoutData(gridData);

		histogramCanvas.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent event) {
				histogramCanvas.redraw();
			}
		});

		histogramCanvas.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(final PaintEvent event) {
				paintHistogram(event.gc);
			}
		});

		histogramCanvas.addMouseMoveListener(new MouseMoveListener() {
			@Override
			public void mouseMove(MouseEvent e) {
				int x = e.x;
				Rectangle ca = histogramCanvas.getClientArea();
				int index = (int) (((double) (x) / (double) (ca.width)) * 256);
				showHistInfo(index);
			}
		});

		histogramCanvas.setCursor(createCursor(histogramCanvas.getDisplay()));

		colorCanvas = new Canvas(middleGroup, SWT.NONE);
		gridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_FILL);
		gridData.heightHint = 18;
		colorCanvas.setLayoutData(gridData);

		colorCanvas.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent event) {
				colorCanvas.redraw();
			}
		});

		colorCanvas.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(final PaintEvent event) {
				paintColor(event.gc);
			}
		});
	}

	private void initLowerGroup() {
		GridLayout gridLayout;
		GridData gridData;

		gridLayout = new GridLayout();
		lowerGroup.setLayout(gridLayout);
		gridLayout.numColumns = 2;

		Label label = new Label(lowerGroup, SWT.NONE);
		label.setText("Level: ");
		gridData = new GridData(GridData.VERTICAL_ALIGN_FILL);
		label.setLayoutData(gridData);

		levelText = new Label(lowerGroup, SWT.NONE);
		levelText.setText("");
		gridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_FILL);
		levelText.setLayoutData(gridData);

		label = new Label(lowerGroup, SWT.NONE);
		label.setText("Count: ");
		gridData = new GridData(GridData.VERTICAL_ALIGN_FILL);
		label.setLayoutData(gridData);

		countText = new Label(lowerGroup, SWT.NONE);
		countText.setText("");
		gridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_FILL);
		countText.setLayoutData(gridData);

		label = new Label(lowerGroup, SWT.NONE);
		label.setText("Percentile: ");
		gridData = new GridData(GridData.VERTICAL_ALIGN_FILL);
		label.setLayoutData(gridData);

		percentText = new Label(lowerGroup, SWT.NONE);
		percentText.setText("");
		gridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_FILL);
		percentText.setLayoutData(gridData);
	}

	/**
	 * Paint the image canvas
	 * 
	 * @param gc
	 *            graphics context
	 */
	public void paintHistogram(GC gc) {
		if (hasImage) {
			Rectangle ca = histogramCanvas.getClientArea(); // client area
			Color[] color = {
					new Color(histogramCanvas.getDisplay(), 128, 128, 128),
					new Color(histogramCanvas.getDisplay(), 255, 0, 0),
					new Color(histogramCanvas.getDisplay(), 0, 255, 0),
					new Color(histogramCanvas.getDisplay(), 0, 0, 255),
					new Color(histogramCanvas.getDisplay(), 255, 255, 255) };

			Color fg = gc.getForeground();

			if (ca != null && ca.width > 10 && ca.height > 10) {
				gc.setForeground(color[color.length - 1]);
				gc.drawRectangle(ca.x, ca.y, ca.width - 1, ca.height - 1);
			}
			if (hasImage) {
				int index = channelCombo.getSelectionIndex();
				intensity = getHistogram(index);
				int ymax = 0;
				for (int i = 0; i < 256; i++) {
					if (ymax < intensity[i])
						ymax = intensity[i];
				}
				gc.setForeground(color[index]);
				double dx = ((double) ca.width - 1) / 256;
				double dy = ((double) ca.height - 1) / ymax;
				for (int i = 0; i < 256; i++) {
					int x = (int) (dx * i);
					int y = (int) (dy * intensity[i]);
					gc.drawRectangle(x, ca.height, (int) dx, -y);
				}
			}
			gc.setForeground(fg);
			for (int i = 0; i < color.length; i++) {
				color[i].dispose();
			}
		}
	}

	/**
	 * Paint the color canvas
	 * 
	 * @param gc
	 */
	public void paintColor(GC gc) {
		if (hasImage) {
			Rectangle ca = colorCanvas.getClientArea(); // client area
			gc.fillRectangle(ca);

			int index = channelCombo.getSelectionIndex();
			Color fgcolor = null;
			Color bgcolor = null;
			switch (index) {
			case 0:
				fgcolor = new Color(colorCanvas.getDisplay(), 0, 0, 0);
				bgcolor = new Color(colorCanvas.getDisplay(), 255, 255, 255);
				break;
			case 1:
				fgcolor = new Color(colorCanvas.getDisplay(), 0, 0, 0);
				bgcolor = new Color(colorCanvas.getDisplay(), 255, 0, 0);
				break;
			case 2:
				fgcolor = new Color(colorCanvas.getDisplay(), 0, 0, 0);
				bgcolor = new Color(colorCanvas.getDisplay(), 0, 255, 0);
				break;
			case 3:
				fgcolor = new Color(colorCanvas.getDisplay(), 0, 0, 0);
				bgcolor = new Color(colorCanvas.getDisplay(), 0, 0, 255);
				break;
			}
			Color oldfg = gc.getForeground();
			Color oldbg = gc.getBackground();
			gc.setForeground(fgcolor);
			gc.setBackground(bgcolor);
			gc.fillGradientRectangle(ca.x, ca.y, ca.width, ca.height, false);
			gc.setForeground(oldfg);
			gc.setBackground(oldbg);
			fgcolor.dispose();
			bgcolor.dispose();
		}
	}

	/**
	 * Reload image data.
	 * 
	 * @param filename
	 */
	public void reloadImage(ImageData data) {
		if (data == null) {
			hasImage = false;
			return;
		}
		computeHistogram(data);
		hasImage = true;
		if (!displayArea.isDisposed() && displayArea.isVisible()) {
			redraw();
		}
	}

	/**
	 * Redraw image canvas and color canvas
	 */
	public void redraw() {
		histogramCanvas.redraw();
		colorCanvas.redraw();
	}

	/**
	 * Called before panel is disposed. Do your garbage collection here
	 */
	public void dispose() {
		if (histCursor != null) {
			histCursor.dispose();
		}
	}

	/**
	 * Return the intensity for a special channel
	 * 
	 * @param data
	 *            image data
	 * @param channel
	 *            color channel: 0=all;1=red;2=green;3=blue
	 * @return integer array holds intensity info
	 */
	public int[] getHistogram(int channel) {
		return hist[channel];
	}

	/* compute histogram info and save into hist[] */
	private void computeHistogram(ImageData data) {
		for (int k = 0; k < 4; k++) {
			int[] level = hist[k];
			for (int i = 0; i < 256; i++) {
				level[i] = 0;
			}
			int w = data.width;
			int h = data.height;
			for (int i = 0; i < w; i++) {
				for (int j = 0; j < h; j++) {
					int pixel = data.getPixel(i, j);
					RGB rgb = data.palette.getRGB(pixel);
					int r = rgb.red;
					int g = rgb.green;
					int b = rgb.blue;
					switch (k) {
					case 0:
						level[(r + g + b) / 3]++;
						break;
					case 1:
						level[r]++;
						break;
					case 2:
						level[g]++;
						break;
					case 3:
						level[b]++;
						break;
					}
				}
			}
		}
	}

	/*
	 * Show the histogram info for the color at index of image data.
	 * 
	 * @param index 0~255
	 */
	private void showHistInfo(int index) {
		if (hasImage) {
			int count = intensity[index];
			int total = 0;
			for (int i = 0; i < 256; i++) {
				total += intensity[i];
			}
			levelText.setText(new Integer(index).toString());
			countText.setText(new Integer(count).toString());
			percentText
					.setText(new Float(((float) 100 * (float) count / total))
							.toString());
		}
	}

	public static Cursor createCursor(Display display) {
		RGB[] rgbs = { new RGB(0, 0, 0), new RGB(255, 255, 255) };
		PaletteData paletteData = new PaletteData(rgbs);
		ImageData sourceData = new ImageData(32, 32, 1, paletteData);
		ImageData maskData = new ImageData(32, 32, 1, paletteData);
		sourceData.setPixels(0, 0, 1024, cursorHist, 0);
		maskData.setPixels(0, 0, 1024, cursorMask, 0);
		return new Cursor(display, sourceData, maskData, 14, 10);
	}

	static int[] cursorHist = new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
			0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

	static int[] cursorMask = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

}
