/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui.actiondelegate;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;

/**
 * Help button delegate
 * 
 * @author Chengdong Li
 * 
 * 
 */
public class HelpActionDelegate extends AbstractViewActionDelegate {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public void run(IAction action) {
		MessageDialog
				.openInformation(
						view.imagePanel.imageCanvas.getShell(),
						Messages.HelpActionDelegate_Help,
						"MIP(1.0.0) is My Image Processing plug-in. Please refer to Help->Help Contents->MIP manual for detail.\n\nTo use MIP, you must accept Common Public License (CPL).\nLast modified: 12/08/2003\nBy Chengdong Li"); //$NON-NLS-1$
	}

}
