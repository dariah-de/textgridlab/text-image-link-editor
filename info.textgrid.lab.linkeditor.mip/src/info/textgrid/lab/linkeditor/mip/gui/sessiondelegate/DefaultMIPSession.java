/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui.sessiondelegate;

import info.textgrid.lab.linkeditor.mip.component.canvas.session.DefaultSession;
import info.textgrid.lab.linkeditor.mip.gui.ImagePanel;

import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseEvent;

/**
 * Default session for my image plugin.
 * 
 * @author Chengdong Li
 */
public class DefaultMIPSession extends DefaultSession {
	static boolean firstRun = true;
	private ImagePanel panel;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.events.MouseMoveListener#mouseMove(org.eclipse.swt.events
	 * .MouseEvent)
	 */
	@Override
	public void mouseMove(MouseEvent e) {
		super.mouseMove(e);
		// if (firstRun) {
		// firstRun = false;
		// if (panel != null) {
		// panel.setSessionByID(panel.getSessionId());
		// panel.onTabKey();
		// }
		// } else {
		// if (panel.tkDlg == null) {
		// panel.onTabKey();
		// }
		// }
	}

	public void forceStartingToolkitDialog(boolean opeNewDialog) {
		if (firstRun || opeNewDialog) {
			firstRun = false;
			if (panel != null) {
				panel.setSessionByID(panel.getSessionId());
				panel.imageCanvas.getDisplay().timerExec(100, new Runnable() {
					@Override
					public void run() {
						panel.forceOpenToolkitDialog();
					}
				});
			}
		}
	}

	/*
	 * @see KeyListener#keyPressed
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		super.keyPressed(e);
		// if (firstRun) {
		// firstRun = false;
		// if (panel != null) {
		// panel.setSessionByID(panel.getSessionId());
		// panel.onTabKey();
		// }
		// } else {
		// if (panel.tkDlg == null) {
		// panel.onTabKey();
		// }
		// }
	}

	/**
	 * Set the image panel.
	 * 
	 * @param panel
	 *            The panel to set.
	 */
	public void setPanel(ImagePanel panel) {
		this.panel = panel;
	}

}
