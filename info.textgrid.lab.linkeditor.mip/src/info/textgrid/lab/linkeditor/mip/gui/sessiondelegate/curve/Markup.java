/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
/*
 * Created on Sep 9, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package info.textgrid.lab.linkeditor.mip.gui.sessiondelegate.curve;

import java.util.Vector;

/**
 * @author IBM
 * 
 *         This class represents a marking of a ruled document
 */
public class Markup {
	Vector pointList; // list of points in the current markup
	String textTitle;
	int color;
	String tagname = "";

	public Markup(String title) {
		pointList = new Vector();
		textTitle = title;
	}

	// get the geometric center of the markup
	public DPoint getCenter() {
		return new DPoint(2.0, 3.0); // dummy return
	}

	// add a point to this markup
	public void addPoint(DPoint newPoint) {
		pointList.add(newPoint);
	}

	public void addPoint(double x, double y) {
		addPoint(new DPoint(x, y));
	}

	public void addPoint(int x, int y) {
		addPoint((double) x, (double) y);
	}

	public void setTagname(String newName) {
		tagname = newName;
	}

	// get the string naming this tool
	public String getTitle() {
		return textTitle;
	}

	// get the color to use for this markup
	public int getColor() {
		return color;
	}

	public void generate() {
	}

	/**
	 * @return
	 */
	public Vector getPointList() {
		return pointList;
	}

	/**
	 * @param vector
	 */
	public void setPointList(Vector vector) {
		pointList = vector;
	}

}
