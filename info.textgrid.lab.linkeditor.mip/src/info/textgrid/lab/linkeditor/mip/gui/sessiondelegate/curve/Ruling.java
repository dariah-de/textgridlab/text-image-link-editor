/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
/*
 * Created on Oct 25, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package info.textgrid.lab.linkeditor.mip.gui.sessiondelegate.curve;

import java.util.Vector;

import org.eclipse.swt.SWT;

/**
 * @author Stephen Hunt
 * 
 *         To change the template for this generated type comment go to
 *         Window>Preferences>Java>Code Generation>Code and Comments
 */
public class Ruling extends Markup {
	Vector curveSet; // set of polynomials
	int width; // width or ruling lines

	public Ruling(String title) {
		super(title);
		curveSet = new Vector();
		color = SWT.COLOR_RED;
	}

	@Override
	public void addPoint(double x, double y) {
		addPoint(new DPoint(x, y));
	}

	@Override
	public void addPoint(int x, int y) {
		addPoint((double) x, (double) y);
	}

	@Override
	public void generate() {
		if (pointList.size() <= 0)
			return;
		PiecewiseCurve current = new PiecewiseCurve();
		// add all the points from the ruling markup to this curve
		DPoint currPoint;
		for (int i = 0; i < pointList.size(); ++i) {
			currPoint = (DPoint) pointList.elementAt(i);
			current.addPoint(currPoint.x, currPoint.y);
		}
		if (pointList.size() > 0) {
			current.generateCurve();
			curveSet.add(current);
		}
		sortCurves();
		pointList.removeAllElements();
	}

	// sort the points by y value
	private void sortCurves() {
		PiecewiseCurve key;
		int i;
		// insertion sort (quick to code and quicksort is inefficient here)
		for (int j = 1; j < curveSet.size(); ++j) {
			key = (PiecewiseCurve) curveSet.elementAt(j);
			i = j - 1;
			while (i >= 0
					&& ((PiecewiseCurve) curveSet.elementAt(i)).getStartY() > key
							.getStartY()) {
				curveSet.setElementAt(curveSet.elementAt(i), i + 1);
				i--;
			}
			curveSet.setElementAt(key, i + 1);
		}

	}

	public void setWidth(int newWidth) {
		width = newWidth;
	}

	public int getWidth() {
		return width;
	}

	public int findMatchingCurve(DPoint selectPoint) {
		int found = 0;
		for (int i = 0; i < curveSet.size(); ++i) {
			if (selectPoint.y > getCurveAt(i).getPointValue(selectPoint.x)) {
				found = i + 1;
			} else
				break;
		}
		return (found);
	}

	public int findMatchingCurve(int oneDCoord) {
		return oneDCoord / width;
	}

	public int getNumCurves() {
		return curveSet.size();
	}

	public PiecewiseCurve getCurveAt(int i) {
		return (PiecewiseCurve) curveSet.elementAt(i);
	}

}
