/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui;

import info.textgrid.lab.linkeditor.controller.LinkEditorController;
import info.textgrid.lab.linkeditor.mip.component.canvas.SWT2dUtil;
import info.textgrid.lab.linkeditor.mip.component.canvas.SessionedImageCanvas;
import info.textgrid.lab.linkeditor.mip.component.canvas.session.BasicSelectSession;
import info.textgrid.lab.linkeditor.mip.component.canvas.session.IImageSession;
import info.textgrid.lab.linkeditor.mip.gui.toolkitdelegate.GridContributor;

import java.awt.geom.AffineTransform;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.EditorInputTransfer;
import org.eclipse.ui.part.ResourceTransfer;

import com.swtdesigner.SWTResourceManager;

/**
 * <code>ImagePanel</code> is a panel showing image. The ImagePanel consists of
 * image canvas and a status bar for showing prompt infomation.
 *
 * @author Chengdong Li
 */

public class ImagePanel implements Observer {
	/** gui elements * */
	public ImageProcessCanvas imageCanvas;
	public ToolkitDlg tkDlg;
	/** session hash table * */
	public HashMap<String, IImageSession> sessionMap = null;
	private ToolkitSetting tkSetting;
	public Vector<String> sidList = null;

	private ImagePanel instance;

	public Composite parent;
	Composite displayArea;
	Composite panelGroup;
	private Composite statusGroup;

	/* status bar components */
	private Label promptLabel;
	private Label typeLabel;
	private Label linksObjectLabel;
	private Label countLabel;
	private Label angleLabel;
	private Label posLabel;
	private Label rgbLabel;
	private Label layerLabel;
	private ProgressBar progBar;

	private GridContributor gridPainter;

	private DropTarget dropTarget;
	private DropTargetAdapter dropTargetAdapter;

	private boolean toolkitDialogOpen = false;
	private boolean toolkitClosedByChangeCtrlT_Flag = false;

	/**
	 * Constructor.
	 */
	public ImagePanel(Composite parent) {
		this.parent = parent;
		instance = this;
		tkSetting = new ToolkitSetting();
		tkSetting.addObserver(this);
	}

	/**
	 * Called when we must grab focus.
	 */
	public void setFocus() {
		imageCanvas.setFocus();
	}

	/**
	 * Create image canvas for drawing, initialize session handler.
	 *
	 * @wbp.parser.entryPoint
	 */
	public void initGUI() {
		createLayout(parent);
		imageCanvas.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if ((e.stateMask & SWT.CTRL) != 0) { // Ctrl + t
					if (e.keyCode == 't')
						onCtrlT_Key();
				}
				// if (e.keyCode == SWT.TAB) { // Tab key
				// onTabKey();
				// }
			}
		});
		imageCanvas.addMouseMoveListener(new MouseMoveListener() {
			@Override
			public void mouseMove(MouseEvent e) {
				showRGB(e.x, e.y);
			}
		});

		gridPainter = new GridContributor();
		gridPainter.setCanvas(imageCanvas);
		
		
//		imageCanvas.addDisposeListener(new DisposeListener() {
//			
//			@Override
//			public void widgetDisposed(DisposeEvent arg0) {
//				
//				
//				
//				
//				String buttons[] = {"hellas","hellas"};
//				MessageDialog msg = new MessageDialog(imageCanvas.getShell(), "Stop", null, 
//										"hallo",
//										MessageDialog.QUESTION, buttons, 
//										0);
//				msg.setBlockOnOpen(true);
//				msg.open();
//				if(msg.getReturnCode() == MessageDialog.CANCEL){
//					System.out.println("Abbruch");
//					return;
//				} else if (msg.getReturnCode() == MessageDialog.CONFIRM){
//					System.out.println("Weitergehts");
//				}
//			}
//		});
		
		// let the user drop files/input on the view area
		addDropSupport();
	}

	public ToolkitDlg getToolkitDialog() {
		return this.tkDlg;
	}

	public boolean isToolkitDialogOpen() {
		return toolkitDialogOpen;
	}

	public void setToolkitClosedByChangeCtrlT_Flag(boolean flag) {
		toolkitClosedByChangeCtrlT_Flag = flag;
	}

	public boolean isToolkitClosedByChangeCtrlT_Flag() {
		return toolkitClosedByChangeCtrlT_Flag;
	}

	private void switchOpenCloseToolkitDialog() {
		if (tkDlg == null) {
			tkDlg = new ToolkitDlg(this);
			tkDlg.open();
			toolkitDialogOpen = true;
		} else {
			Shell shell = tkDlg.shell;
			if (shell.isDisposed()) {
				instance.tkDlg = new ToolkitDlg(this);
				instance.tkDlg.open();
				toolkitDialogOpen = true;
			} else {
				if (shell.isVisible()) {
					shell.setVisible(false);
					toolkitDialogOpen = false;
				} else {
					shell.setVisible(true);
					toolkitDialogOpen = true;
				}
			}
		}
	}

	/**
	 * Callback function when Ctrl-t key pressed
	 */
	public void onCtrlT_Key() {
		switchOpenCloseToolkitDialog();
	}

	public void forceOpenToolkitDialog() {
		if (tkDlg == null) {
			tkDlg = new ToolkitDlg(this);
			tkDlg.open();
			toolkitDialogOpen = true;
		} else {
			Shell shell = tkDlg.shell;
			if (shell.isDisposed()) {
				instance.tkDlg = new ToolkitDlg(this);
				instance.tkDlg.open();
				toolkitDialogOpen = true;
			} else if (!shell.isVisible()) {
				shell.setVisible(true);
				toolkitDialogOpen = true;
			}
		}
	}

	public void forceCloseToolkitDialog(boolean setFlag) {
		if (tkDlg != null) {
			Shell shell = tkDlg.shell;
			if (!shell.isDisposed() && shell.isVisible()) {
				shell.setVisible(false);
				toolkitDialogOpen = false;
				setToolkitClosedByChangeCtrlT_Flag(setFlag);
			}
		}
	}

	private String sessionId = ""; //$NON-NLS-1$

	public void setSessionId(String id) {
		this.sessionId = id;
	}

	public String getSessionId() {
		return sessionId;
	}

	/* create GUI */
	private void createLayout(Composite parent) {
		
		
		GridLayout gridLayout;
		GridData gridData;

		/** * Create principal GUI layout elements ** */
		displayArea = new Composite(parent, SWT.NONE);
		gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		displayArea.setLayout(gridLayout);
		displayArea.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent evt) {
				super.controlResized(evt);
				displayArea.layout();
			}
		});

		/** ****upper panels********** */
		panelGroup = new Composite(displayArea, SWT.NONE);
		gridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.FILL_VERTICAL);
		panelGroup.setLayoutData(gridData);
		panelGroup.setLayout(new FillLayout());
		imageCanvas = new ImageProcessCanvas(panelGroup);

		/** ****lower panel********** */
		/** ****status text***** */
		setupStatusPanel();
	}

	/**
	 * Called when the View is to be disposed
	 */
	public void dispose() {
		imageCanvas.dispose();
		if (tkDlg != null) {
			tkDlg.dispose();
			tkDlg = null;
		}
	}

	/*
	 * setup the status bar
	 */
	private void setupStatusPanel() {
		GridLayout gridLayout;
		GridData gridData;
		statusGroup = new Composite(displayArea, SWT.NONE);
		gridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_FILL);
		gridData.heightHint = 20;
		statusGroup.setLayoutData(gridData);

		gridLayout = new GridLayout();
		gridLayout.numColumns = 7;
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		statusGroup.setLayout(gridLayout);

		this.promptLabel = new Label(statusGroup, SWT.BORDER | SWT.SINGLE
				| SWT.READ_ONLY);
		gridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_FILL);
		// gridData.widthHint = 180;
		this.promptLabel.setLayoutData(gridData);

		// progBar = new ProgressBar(statusGroup, SWT.NONE);
		// progBar.setMaximum(100);
		// progBar.setSelection(0);
		// gridData = new GridData(GridData.VERTICAL_ALIGN_FILL);
		// gridData.widthHint = 110;
		// progBar.setLayoutData(gridData);

		typeLabel = new Label(statusGroup, SWT.BORDER | SWT.SINGLE
				| SWT.READ_ONLY);
		gridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_FILL);
		gridData.widthHint = 40;
		typeLabel.setLayoutData(gridData);

		linksObjectLabel = new Label(statusGroup, SWT.BORDER | SWT.SINGLE
				| SWT.READ_ONLY);
		gridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_FILL);
		gridData.widthHint = 220;
		linksObjectLabel.setLayoutData(gridData);
		linksObjectLabel.setToolTipText(""); //$NON-NLS-1$

		countLabel = new Label(statusGroup, SWT.SHADOW_IN | SWT.BORDER
				| SWT.CENTER);
		countLabel.setToolTipText(Messages.ImagePanel_ThereAreStillUnlinkedObjects);
		gridData = new GridData(SWT.FILL, SWT.FILL, false, false);
		gridData.widthHint = 20;
		countLabel.setLayoutData(gridData);
		this.countLabel.setVisible(false);

		angleLabel = new Label(statusGroup, SWT.SHADOW_IN | SWT.BORDER
				| SWT.CENTER);
		angleLabel.setToolTipText(Messages.ImagePanel_Angle);
		gridData = new GridData(SWT.FILL, SWT.FILL, false, false);
		gridData.widthHint = 25;
		angleLabel.setLayoutData(gridData);
		this.angleLabel.setVisible(false);
		this.angleLabel.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseDown(MouseEvent e) {
				imageCanvas.callEditRotationDialog();
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub
			}
		});

		this.rgbLabel = new Label(statusGroup, SWT.BORDER | SWT.SINGLE
				| SWT.READ_ONLY);
		gridData = new GridData(GridData.FILL_HORIZONTAL
				| GridData.VERTICAL_ALIGN_FILL);
		gridData.widthHint = 130;
		this.rgbLabel.setLayoutData(gridData);

		this.posLabel = new Label(statusGroup, SWT.BORDER | SWT.SINGLE
				| SWT.READ_ONLY);
		gridData = new GridData(GridData.VERTICAL_ALIGN_FILL);
		gridData.widthHint = 90;
		this.posLabel.setLayoutData(gridData);
		this.posLabel.setToolTipText(Messages.ImagePanel_NoLayerEnabledOrActive);
		this.posLabel.setText(Messages.ImagePanel_NoLayerEnabledOrActive);
		this.posLabel.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseDown(MouseEvent e) {
				// TODO Auto-generated method stub
				if (e.stateMask == SWT.SHIFT) {
					imageCanvas.callEditTGLayerDialog4SelectedShapes();
				} else {
					imageCanvas.callEditTGLayerDialog();
				}
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}

	/**
	 * Show current mouse postion
	 *
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 */
	public void showPosition(int x, int y) {
		// this.posLabel.setText(new String(" (" + x + "," + y + ")"));
	}

	/*
	 * Show current layer
	 *
	 *
	 * @param layer layer level
	 */
	public void showLayer(Boolean visible, String layer) {
		if (layer.equals("0")) { //$NON-NLS-1$
			this.posLabel.setToolTipText(Messages.ImagePanel_NoLayerEnabledOrActive);
		} else {
			this.posLabel.setToolTipText(Messages.ImagePanel_TheCurrentLayerIs + layer);
		}

		if (visible) {
			this.posLabel.setText(Messages.ImagePanel_ActiveLayer + layer);
			this.posLabel.setBackground(SWTResourceManager
					.getColor(SWT.COLOR_WHITE));
			this.posLabel.setForeground(SWTResourceManager
					.getColor(SWT.COLOR_DARK_BLUE));
		} else {
			this.posLabel.setText(""); //$NON-NLS-1$
			this.posLabel.setBackground(SWTResourceManager
					.getColor(SWT.COLOR_WIDGET_BACKGROUND));

		}
	}

	/**
	 * Show text on the label of the current loaded image
	 *
	 * @param text
	 *
	 */
	public void showTextOnImageLabel(String text) {
		this.typeLabel.setText(text);
	}

	/**
	 * Show text on the label of the current loaded image
	 *
	 * @param text
	 *
	 */
	public void showTextOnImageLabel2(String text) {
		text = text.replaceAll("&", "&&"); //$NON-NLS-1$ //$NON-NLS-2$
		this.linksObjectLabel.setText(text);
		this.linksObjectLabel.setToolTipText(text);
	}

	/**
	 * Show current DiffCount of Loaded Objects
	 *
	 * @param different
	 *            is the count of linked and unlinked objects different
	 */
	public void showDiffCount(boolean different) {
		// this.countLabel.setText(count);
		// System.out.println("showCount"+comp);
		if (!different) {
			this.countLabel.setVisible(false);
		} else {
			this.countLabel.setVisible(true);
			this.countLabel.setImage(PlatformUI.getWorkbench()
					.getSharedImages()
					.getImage(ISharedImages.IMG_OBJS_WARN_TSK));
		}
	}

	/**
	 * Show current Angle of shapes
	 *
	 * @param visible
	 *            visibility of the Label
	 * @param angle
	 *            Angle of the shape
	 * @wbp.parser.entryPoint
	 */
	public void showAngleInfo(boolean visible, double angle) {
		// String angleStr = "\u2014";
		String angleStr = ""; //$NON-NLS-1$
		if (angle != 999) {
			DecimalFormat format = new DecimalFormat("#.#"); //$NON-NLS-1$
			angleStr = format.format(new Double(angle)) + "\u00b0"; //$NON-NLS-1$
		}

		if (visible) {
			this.angleLabel.setVisible(true);
			this.angleLabel.setText(angleStr);
		} else {
			this.angleLabel.setVisible(false);
		}
	}

	/**
	 *
	 * @param visible
	 * @param layer
	 */
	public void showLayerInfo(boolean visible, String layer) {
		showLayer(visible, layer);
	}

	/**
	 * Show argb information for the current pixel
	 *
	 * @param alfa
	 *            alfa value
	 * @param r
	 *            red value
	 * @param g
	 *            green value
	 * @param b
	 *            blue value
	 */
	public void showRGB(int alfa, int r, int g, int b) {
		this.rgbLabel.setText(new String(" a: " + alfa + ", r: " + r + "," //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ " g: " + g + ", b: " + b)); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public RGB getRGB(int x, int y) {
		AffineTransform af = imageCanvas.getTransform();
		Point point = SWT2dUtil.inverseTransformPoint2(af, new Point(x, y));
		int xx = (point.x);
		int yy = (point.y);
		RGB rgb = imageCanvas.getRGB(xx, yy);
		return rgb;
	}

	public void showRGB(int x, int y) {

		AffineTransform af = imageCanvas.getTransform();
		Point point = SWT2dUtil.inverseTransformPoint2(af, new Point(x, y));
		int xx = (point.x);
		int yy = (point.y);

		RGB rgb = imageCanvas.getRGB(xx, yy);
		if (rgb == null)
			return;
		int a = 255;
		int r = rgb.red;
		int g = rgb.green;
		int b = rgb.blue;
		this.rgbLabel.setText(new String(" a: " + a + ", r: " + r + "," //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				+ " g: " + g + ", b: " + b)); //$NON-NLS-1$ //$NON-NLS-2$
		showPosition(xx, yy);
	}

	/**
	 * Show a message in status bar
	 *
	 * @param s
	 *            message to be shown
	 */
	public void showPromptMessage(String s) {
		this.promptLabel.setText(" " + mapPromptMessage(s)); //$NON-NLS-1$
	}

	/**
	 * Map to Mode in status bar
	 *
	 * @param s
	 *            message
	 * @param res
	 *            message to be shown
	 */
	public String mapPromptMessage(String s) {
		String res = ""; //$NON-NLS-1$
		if (s.equals("session.default.prompt")) { //$NON-NLS-1$
			res = Messages.StatusBar_DefaultMode; //$NON-NLS-1$
		} else if (s.equals("toolbar.poly.description")) { //$NON-NLS-1$
			res = Messages.StatusBar_PolygonMode; //$NON-NLS-1$
		} else if (s.equals("toolbar.lense.description")) { //$NON-NLS-1$
			res = Messages.StatusBar_LenseMode; //$NON-NLS-1$
		} else if (s.equals("toolbar.zoomin.description")) { //$NON-NLS-1$
			res = Messages.StatusBar_ZoomInMode; //$NON-NLS-1$
		} else if (s.equals("toolbar.zoomout.description")) { //$NON-NLS-1$
			res = Messages.StatusBar_ZoomOutMode; //$NON-NLS-1$
		} else if (s.equals("toolbar.move.description")) { //$NON-NLS-1$
			res = Messages.StatusBar_MoveMode; //$NON-NLS-1$
		} else if (s.equals("toolbar.dockingLines.description")) { //$NON-NLS-1$
			res = Messages.StatusBar_DockingLineMode; //$NON-NLS-1$
		} else if (s.equals("toolbar.select.description")) { //$NON-NLS-1$
			res = Messages.StatusBar_RectangleMode; //$NON-NLS-1$
		} else if (s.equals("toolbar.grid")) { //$NON-NLS-1$
			res = Messages.StatusBar_Grid_OnOff; //$NON-NLS-1$
		} else if (s.equals("toolbar.cloneselection")) { //$NON-NLS-1$
			res = Messages.ToolkitDlg_CloneSelection; //$NON-NLS-1$
		} else if (s.equals("toolbar.cloneselection")) { //$NON-NLS-1$
			res = Messages.ToolkitDlg_CloneSelection; //$NON-NLS-1$
		} else if (s.equals("toolbar.imagefit")) {
			res = Messages.ToolkitDlg_ImgResizeFitSelection;
		} else if (s.equals("toolbar.imagehorizontal")) {
			res = Messages.ToolkitDlg_ImgResizeHorizontalSelection;
		} else if (s.equals("toolbar.imagevertical")) {
			res = Messages.ToolkitDlg_ImgResizeVerticalSelection;
		} else if (s.equals("toolbar.imageoriginal")) {
			res = Messages.ToolkitDlg_ImgResizeOriginalSelection;
		} else {
			res = Messages.StatusBar_None; //$NON-NLS-1$
		}
		return res;
	}

	/**
	 * Show progress in status bar
	 *
	 * @param selection
	 *            current progress (percentage)
	 */
	public void showProgress(int selection) {
		progBar.setSelection(selection);
	}

	/**
	 * Show the processing time in status bar
	 *
	 * @param time
	 *            time to be shown
	 */
	public void showTime(long time) {
		typeLabel.setText(new String("time(ms): " + time)); //$NON-NLS-1$
	}

	/**
	 * Show the processing time in status bar
	 *
	 * @param time
	 *            time to be shown
	 */
	public void showTime(double time) {
		typeLabel.setText(new String("time(sec): " + time)); //$NON-NLS-1$
	}

	/*
	 * Create the action-handler map.
	 */
	public void registerSession(String id, IImageSession session) {
		if (sessionMap == null) {
			sessionMap = new HashMap<String, IImageSession>();
		}
		if (sidList == null) {
			sidList = new Vector<String>();
		}
		sessionMap.put(id, session);
		sidList.add(id);
	}

	/**
	 * Return the session given the identifier
	 *
	 * @param id
	 *            identifier of the session
	 * @return session
	 */
	public IImageSession getSessionByID(String id) {
		IImageSession session = sessionMap.get(id);
		return session;
	}

	/**
	 * Selects a session given its ID.
	 */
	public void setSessionByID(String id) {
		IImageSession session = sessionMap.get(id);
		setSession(session);
	}

	/**
	 * Set the session (action handler) for the tool and also change the prompt
	 * message.
	 *
	 * @param session
	 *            the session to activate; null to disable all sessions.
	 */
	public void setSession(IImageSession newSession) {
		imageCanvas.setSession(newSession);
		showPromptMessage(newSession.getSessionPrompt());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable arg0, Object arg1) {
		int event = tkSetting.checkEvent();
		if ((event & ToolkitSetting.COLOR_FORE) != 0) {
			// foreground color change
		}
		if ((event & ToolkitSetting.COLOR_BACK) != 0) {
			// background color change
		}

		//new buttons to set the clone selection and image resizing
		if( (event & ToolkitSetting.CLONE_SELECTION) != 0) {
			((BasicSelectSession)imageCanvas.getSession()).getSessionCanvas().createCloneRectangleH();
		}
		if( (event & ToolkitSetting.IMG_RESIZE_FIT) != 0) {
			((BasicSelectSession)imageCanvas.getSession()).getSessionCanvas().fitCanvas();
		}
		if( (event & ToolkitSetting.IMG_RESIZE_HORIZONTAL) != 0) {
			((BasicSelectSession)imageCanvas.getSession()).getSessionCanvas().fitHorizontally();
		}
		if( (event & ToolkitSetting.IMG_RESIZE_VERTICAL) != 0) {
			((BasicSelectSession)imageCanvas.getSession()).getSessionCanvas().fitVertically();
		}
		if( (event & ToolkitSetting.IMG_RESIZE_ORIGINAL) != 0) {
			((BasicSelectSession)imageCanvas.getSession()).getSessionCanvas().showOriginal();
		}
		
		
		
		if ((event & ToolkitSetting.GRID_ON) != 0) {
			// Grid is turned on/off
			if (this.tkSetting.isGridOn()) {
				imageCanvas.registerPaintContributor(gridPainter);
			} else {
				imageCanvas.removePaintContributor(gridPainter);
			}
			imageCanvas.redraw();
		}
	}

	/**
	 * return toolkit settings.
	 *
	 * @return ToolkitSetting object.
	 */
	public ToolkitSetting getTkSetting() {
		return tkSetting;
	}

	/**
	 * Set toolkit settings.
	 *
	 * @param setting
	 *            TookitSetting object.
	 */
	public void setTkSetting(ToolkitSetting setting) {
		tkSetting = setting;
	}

	private void addDropSupport() {
		if (dropTarget == null) {
			Transfer[] types = new Transfer[] {
					EditorInputTransfer.getInstance(),
					ResourceTransfer.getInstance() };

			dropTarget = new DropTarget(imageCanvas, DND.DROP_DEFAULT);
			dropTarget.setTransfer(types);
			dropTargetAdapter = new ImageDropAdapter(imageCanvas);
			dropTarget.addDropListener(dropTargetAdapter);
		}
	}

	// private String secondaryId;
	//
	// public void setSecondaryId(String secondaryId) {
	// this.secondaryId = secondaryId;
	// }
	//
	// public String getSecondaryId() {
	// return secondaryId;
	// }

}
