/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui.toolkitdelegate;

import info.textgrid.lab.linkeditor.mip.gui.ToolkitSetting;

import org.eclipse.jface.resource.ImageDescriptor;

/**
 * Interface for toolkit check button.
 * 
 * @author Chengdong Li
 */
public interface IToolkitCheckButton {
	/**
	 * Set ToolkitSetting object.
	 * 
	 * @param ts
	 *            ToolkitSetting Object.
	 */
	public void setToolkitSetting(ToolkitSetting ts);

	/**
	 * Get ToolkitSetting object.
	 * 
	 * @return ToolkitSetting Object.
	 */
	public ToolkitSetting getToolkitSetting();

	/**
	 * Get session ToolTip message.
	 * 
	 * @return prompt message
	 */
	public String getToolTip();

	/**
	 * Set session ToolTip message.
	 */
	public void setToolTip(String toolTip);

	/**
	 * Get button id.
	 * 
	 * @return button id.
	 */
	public String getId();

	/**
	 * Set button id.
	 */
	public void setId(String id);

	/**
	 * Set Icon.
	 */
	public void setIcon(ImageDescriptor id);

	/**
	 * Get Icon.
	 */
	public ImageDescriptor getIcon();

	/**
	 * Select button.
	 */
	public void setSelected(boolean sel);

	/**
	 * check if is selected.
	 */
	public boolean isSelected();

	public void run();
}
