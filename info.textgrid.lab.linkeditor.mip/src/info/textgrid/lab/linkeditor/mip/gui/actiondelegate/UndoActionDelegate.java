/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui.actiondelegate;

import info.textgrid.lab.linkeditor.mip.gui.ImageProcessCanvas;

import org.eclipse.jface.action.IAction;

/**
 * Undo action delegate
 * 
 * @author Chengdong
 * 
 */
public class UndoActionDelegate extends AbstractViewActionDelegate {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public void run(IAction action) {
		ImageProcessCanvas imageCanvas = this.view.imagePanel.imageCanvas;
		run(imageCanvas);
	}

	static public void run(ImageProcessCanvas imageCanvas) {
		imageCanvas.undo();
		imageCanvas.redraw();
	}
}
