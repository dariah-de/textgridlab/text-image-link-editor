package info.textgrid.lab.linkeditor.mip.gui.actiondelegate;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.linkeditor.mip.gui.actiondelegate.messages"; //$NON-NLS-1$
	public static String HelpActionDelegate_Help;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
