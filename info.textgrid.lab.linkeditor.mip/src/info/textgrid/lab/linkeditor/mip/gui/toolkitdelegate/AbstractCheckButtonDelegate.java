/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui.toolkitdelegate;

import info.textgrid.lab.linkeditor.mip.gui.ToolkitSetting;

import org.eclipse.jface.resource.ImageDescriptor;

/**
 * Abstract CheckButton delegate.
 * 
 * @author Chengdong Li
 */
public abstract class AbstractCheckButtonDelegate implements
		IToolkitCheckButton {
	ToolkitSetting tkSetting;
	String id;
	String toolTip;
	ImageDescriptor icon;
	boolean selected;

	@Override
	public void setToolkitSetting(ToolkitSetting ts) {
		tkSetting = ts;
	}

	@Override
	public ToolkitSetting getToolkitSetting() {
		return tkSetting;
	}

	@Override
	public String getToolTip() {
		return toolTip;
	}

	@Override
	public void setToolTip(String ts) {
		toolTip = ts;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public void setIcon(ImageDescriptor desc) {
		icon = desc;
	}

	@Override
	public ImageDescriptor getIcon() {
		return icon;
	}

	@Override
	public void setSelected(boolean sel) {
		selected = sel;
	}

	@Override
	public boolean isSelected() {
		return selected;
	}

	@Override
	abstract public void run();

}
