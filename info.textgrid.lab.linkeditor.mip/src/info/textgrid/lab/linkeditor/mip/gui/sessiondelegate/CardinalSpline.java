/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui.sessiondelegate;

import java.util.Vector;

import org.eclipse.swt.graphics.Point;

/**
 * Interactive 2D Cardinal splines, Evgeny Demidov 30 June 2001
 * http://www.ibiblio.org/e-notes/Splines/Cardinal.htm For a 1st order
 * continuous Bezier curve. We have following eq: B(t)=(1-t)^3*B0 +
 * 3*(1-t)^2*t*B1 + 3*(1-t)*t^2*B2 + t^3*B3 Where B0, B1 B2, B3 are control
 * points of Bezier curve.
 */

public class CardinalSpline {
	int n = 0; // points number
	int n1 = n + 1; // points number + 1
	int n2 = n + 2; // points number+2
	double[] Px; // axis X of knots
	double[] Py; // axis Y of knots

	// Bezier curve related parameters
	private final static int SEG_NUM = 26;
	static double[] B0; // coefficient for B0
	static double[] B1; // coefficient for B1
	static double[] B2; // coefficient for B2
	static double[] B3; // coefficient for B3
	static final double Al = 6; // alfa

	static { // initialize the coefficients.
		B0 = new double[SEG_NUM];
		B1 = new double[SEG_NUM];
		B2 = new double[SEG_NUM];
		B3 = new double[SEG_NUM];

		double t = 0;
		for (int i = 0; i < SEG_NUM; i++) {
			double t1 = 1 - t, t12 = t1 * t1, t2 = t * t;
			B0[i] = t1 * t12;
			B1[i] = 3 * t * t12;
			B2[i] = 3 * t2 * t1;
			B3[i] = t * t2;
			t += .04;
		}
	}

	/**
	 * Find the closest point to (x,y)
	 * 
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 * @return index of closest point.
	 */
	private int findClosestPoint(int x, int y) {
		if (n <= 0)
			return 0;
		int iMin = 1;
		double Rmin = 1e10, r2, xi, yi;
		for (int i = 1; i < n1; i++) {
			xi = (x - Px[i]);
			yi = (y - Py[i]);
			r2 = xi * xi + yi * yi;
			if (r2 < Rmin) {
				iMin = i;
				Rmin = r2;
			}
		}
		return iMin;
	}

	/**
	 * generate spline curves.
	 * 
	 * @return Vector which holds all points on generated curve.
	 */
	public Vector generateSpline() {
		Vector v = new Vector();
		if (n <= 0)
			return v;

		int X_old = -1, Y_old = -1;
		// each spline is composed of many Bezier curves.
		Px[0] = Px[1] - (Px[2] - Px[1]);
		Py[0] = Py[1] - (Py[2] - Py[1]);
		Px[n1] = Px[n] + (Px[n] - Px[n - 1]);
		Py[n1] = Py[n] + (Py[n] - Py[n - 1]);
		for (int i = 1; i < n; i++) {
			for (int k = 0; k < SEG_NUM; k++) {
				int X, Y;
				X = (int) (Px[i] * B0[k]
						+ (Px[i] + (Px[i + 1] - Px[i - 1]) / Al) * B1[k]
						+ (Px[i + 1] - (Px[i + 2] - Px[i]) / Al) * B2[k] + Px[i + 1]
						* B3[k]);
				Y = (int) (Py[i] * B0[k]
						+ (Py[i] + (Py[i + 1] - Py[i - 1]) / Al) * B1[k]
						+ (Py[i + 1] - (Py[i + 2] - Py[i]) / Al) * B2[k] + Py[i + 1]
						* B3[k]);

				if (X != X_old || Y != Y_old) { // get rid of repeated points
					Point p = new Point(X, Y);
					v.add(p);
				}
			}
		}
		return v;
	}

	/**
	 * Add a new point at (x,y).
	 * 
	 * @param x
	 *            x coordinate.
	 * @param y
	 *            y coordinate.
	 */
	public void addPoint(int x, int y) {
		int iMin = findClosestPoint(x, y) + 1;
		n++;
		n1++;
		n2++;
		double[] newPx = new double[n2], newPy = new double[n2];
		if (n - 1 <= 0) {
			Px = newPx;
			Py = newPy;
			Px[iMin] = x;
			Py[iMin] = y;
			return;
		}

		for (int i = 1; i < iMin; i++) {
			newPx[i] = Px[i];
			newPy[i] = Py[i];
		}
		for (int i = iMin; i < n1; i++) {
			newPx[i + 1] = Px[i];
			newPy[i + 1] = Py[i];
		}
		Px = newPx;
		Py = newPy;
		Px[iMin] = x;
		Py[iMin] = y;
	}

	/**
	 * Remove a point at (x,y).
	 * 
	 * @param x
	 *            x coordinate.
	 * @param y
	 *            y coordinate.
	 */
	public void removePoint(int x, int y) {
		if (n <= 0)
			return;
		int iMin = findClosestPoint(x, y);
		for (int i = iMin; i < n; i++) {
			Px[i] = Px[i + 1];
			Py[i] = Py[i + 1];
		}
		n2--;
		n1--;
		n--;
	}

	/**
	 * Move a point. This function does not work perfectly. To make work better,
	 * you need to remember the mouse down position and compute the offset each
	 * move. Currently, it just finds the closest point to move. It is roughly
	 * good now.
	 * 
	 * @param x
	 *            x coordinate of mouse position.
	 * @param y
	 *            y coordinate of mouse position.
	 */
	public void movePoint(int x, int y) {
		if (n <= 0)
			return;
		int iMin = findClosestPoint(x, y);
		Px[iMin] = x;
		Py[iMin] = y;
	}

	/**
	 * Return control points.
	 * 
	 * @return Vector holds all control points.
	 */
	public Vector getControlPoints() {
		Vector v = new Vector();
		if (n <= 0)
			return v;
		for (int i = 1; i <= n; i++) {
			v.add(new Point((int) Px[i], (int) Py[i]));
		}
		return v;
	}
}
