/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.gui.sessiondelegate;

import info.textgrid.lab.linkeditor.mip.component.canvas.IPaintContributor;
import info.textgrid.lab.linkeditor.mip.component.canvas.session.AbstractImageSession;
import info.textgrid.lab.linkeditor.mip.gui.ImagePanel;
import info.textgrid.lab.linkeditor.mip.gui.ImageProcessCanvas;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;

/**
 * Curve session delegate.
 * 
 * @author Chengdong Li
 */
public class CurveDelegate extends AbstractImageSession implements
		IPanelSession {
	ImagePanel panel;
	CardinalSpline spline;
	Vector curvePoints = new Vector();

	boolean mousePressed = false;

	CurveContributor curvePainter = new CurveContributor();

	public CurveDelegate() {
	}

	/**
	 * Constructs a session.
	 * 
	 * @param id
	 *            session id
	 * @param panel
	 *            Image panel.
	 */
	public CurveDelegate(String id, ImagePanel panel) {
		super(id);
		this.panel = panel;
	}

	@Override
	public void mouseDown(MouseEvent e) {
		super.mouseDown(e);
		if (e.button == 1) {
			Point p = panel.imageCanvas.screenToImage(new Point(e.x, e.y));
			if ((e.stateMask & SWT.CTRL) != 0) {
				spline.removePoint(p.x, p.y);
			} else if ((e.stateMask & SWT.SHIFT) != 0) {
				spline.addPoint(p.x, p.y);
			} else {
				mousePressed = true;
			}
		}
	}

	@Override
	public void mouseUp(MouseEvent e) {
		super.mouseUp(e);
		panel.imageCanvas.redraw();
		mousePressed = false;
	}

	@Override
	public void mouseMove(MouseEvent e) {
		super.mouseMove(e);
		if (!mousePressed)
			return;

		Point p = panel.imageCanvas.screenToImage(new Point(e.x, e.y));
		spline.movePoint(p.x, p.y);
		panel.imageCanvas.redraw();
	}

	@Override
	public void mouseDoubleClick(MouseEvent e) {
		super.mouseDoubleClick(e);
		panel.imageCanvas.redraw();
	}

	@Override
	public void beginSession(Object obj) {
		super.beginSession(obj);
		spline = new CardinalSpline();
		curvePainter.setCanvas(panel.imageCanvas);
		panel.imageCanvas.registerPaintContributor(curvePainter);
	}

	@Override
	public void endSession() {
		panel.imageCanvas.removePaintContributor(curvePainter);
		super.endSession();
	}

	/**
	 * @return Returns the panel.
	 */
	@Override
	public ImagePanel getPanel() {
		return panel;
	}

	/**
	 * @param panel
	 *            The panel to set.
	 */
	@Override
	public void setPanel(ImagePanel panel) {
		this.panel = panel;
	}

	/**
	 * Customize session cursor
	 */
	@Override
	public Cursor getSessionCursor() {
		ImageData sourceData = new ImageData(32, 32, 1, paletteData);
		ImageData maskData = new ImageData(32, 32, 1, paletteData);
		sourceData.setPixels(0, 0, 1024, cursorCurve, 0);
		maskData.setPixels(0, 0, 1024, cursorMask, 0);
		return new Cursor(sessionCanvas.getDisplay(), sourceData, maskData, 1,
				30);
	}

	/**
	 * Curve painter contributor
	 * 
	 * @author Chengdong
	 */
	class CurveContributor implements IPaintContributor {
		ImageProcessCanvas canvas;

		@Override
		public void prePaint(GC gc) {
		}

		@Override
		public void postPaint(GC gc) {
		}

		@Override
		public void paint(GC gc) {
			Vector points = spline.getControlPoints();
			if (points.size() <= 0)
				return;
			curvePoints = spline.generateSpline();
			if (curvePoints.size() <= 0)
				return;

			Image sourceImage = canvas.getScreenImage();
			if (sourceImage == null || sourceImage.isDisposed())
				return;
			// draw control points first.
			gc.setForeground(new Color(canvas.getDisplay(), 0, 0, 255));
			for (int i = 0; i < points.size(); i++) {
				Point p = panel.imageCanvas.imageToScreen((Point) points
						.elementAt(i));
				gc.drawRectangle(p.x - 2, p.y - 2, 4, 4);
			}

			gc.setForeground(new Color(canvas.getDisplay(), 255, 0, 0));
			gc.setLineWidth(1);
			Point p_old = panel.imageCanvas.imageToScreen((Point) curvePoints
					.elementAt(0));
			gc.drawLine(p_old.x, p_old.y, p_old.x, p_old.y);
			for (int i = 1; i < curvePoints.size(); i++) {
				Point p = panel.imageCanvas.imageToScreen((Point) curvePoints
						.elementAt(i));
				gc.drawLine(p_old.x, p_old.y, p.x, p.y);
				p_old = p;
			}
		}

		/**
		 * @return
		 */
		public ImageProcessCanvas getCanvas() {
			return canvas;
		}

		/**
		 * @param canvas
		 */
		public void setCanvas(ImageProcessCanvas canvas) {
			this.canvas = canvas;
		}
	}

	static int[] cursorCurve = new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1,
			0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0,
			1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0,
			1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1,
			1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1,
			0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

}
