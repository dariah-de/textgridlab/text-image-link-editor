/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.perspective;

import info.textgrid.lab.linkeditor.mip.component.views.ThumbView;
import info.textgrid.lab.linkeditor.mip.views.HistogramView;
import info.textgrid.lab.linkeditor.mip.views.ImageView;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * This perspective will show the HistogramView and ImageView.
 * 
 * @author Chengdong Li
 */
public class ImagePerspectiveFactory implements IPerspectiveFactory {

	/**
	 * The constructor.
	 */
	public ImagePerspectiveFactory() {
	}

	/**
	 * Initialize the layout for perspective.
	 * 
	 * @see org.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui.IPageLayout)
	 */
	@Override
	public void createInitialLayout(IPageLayout layout) {
		// Get the editor area.
		String editorArea = layout.getEditorArea();
		layout.addView(ImageView.ID, IPageLayout.RIGHT, 0.2f, editorArea);

		// Top left: Resource Navigator view and Bookmarks view placeholder
		// default horizontal width 20%
		IFolderLayout left = layout.createFolder("left", IPageLayout.LEFT,
				0.2f, editorArea);

		IFolderLayout topLeft = layout.createFolder("topLeft", IPageLayout.TOP,
				0.40f, "left");
		topLeft.addView(IPageLayout.ID_RES_NAV);

		// Bottom left: histogram view
		// default vertical height 50%
		IFolderLayout middleLeft = layout.createFolder("bottomLeLeftUP",
				IPageLayout.TOP, 0.40f, "left");
		middleLeft.addView(ThumbView.ID);

		// bottom left
		left.addView(HistogramView.ID);

		// Hide the editor area
		layout.setEditorAreaVisible(false);

	}

}
