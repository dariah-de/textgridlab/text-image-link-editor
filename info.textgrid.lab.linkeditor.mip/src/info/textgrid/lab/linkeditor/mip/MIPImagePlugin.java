/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip;

import info.textgrid.lab.linkeditor.mip.views.ImageView;

import java.net.URL;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.Bundle;

/**
 * The main plugin class to be used in the desktop.
 * 
 * @author Chengdong Li
 */
public class MIPImagePlugin extends AbstractUIPlugin {
	
	public static final String PLUGIN_ID = "info.textgrid.lab.linkeditor.mip"; //$NON-NLS-1$
	// The shared instance.
	private static MIPImagePlugin plugin;
	// Resource bundle.
	private static ResourceBundle resourceBundle; // Resource bundle

	public static ImageView view;

	public static final String TG_IMAGE_ADD = "add.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Adjust16 = "Adjust16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_blue_toolkit = "blue_toolkit.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Center16 = "Center16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Content16 = "Content16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Curve16 = "Curve16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_delete = "delete.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Eyedrop16 = "Eyedrop16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Fith16 = "Fith16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Fitv16 = "Fitv16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Fliph16 = "Fliph16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_GrafLinkEdit = "GrafLinkEdit.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_graphics_toolkits = "graphics_toolkits.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Grid16 = "Grid16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Help16 = "Help16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Hflip16 = "Hflip16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_hideLayer_16 = "hideLayer_16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_icon_holongate_16x16 = "icon-holongate-16x16.png"; //$NON-NLS-1$
	public static final String TG_IMAGE_image_obj = "image_obj.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_layer_16_2 = "layer_16_2.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_layer_16 = "layer_16.bmp"; //$NON-NLS-1$
	public static final String TG_IMAGE_Lense16 = "Lense16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_LineSize16 = "LineSize16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Mark16 = "Mark16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_MIP16 = "MIP16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Move16 = "Move16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Normal16 = "Normal16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Open16 = "Open16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_openbrwsr = "openbrwsr.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Overlay16 = "Overlay16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Pencil16 = "Pencil16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_PolySelect16_6 = "PolySelect16_6.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Redo16 = "Redo16.gif.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Remove16 = "Remove16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Reset = "ns/Reset.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Rotate16 = "Rotate16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Rotater16 = "Rotater16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_sample = "sample.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_save_edit = "save_edit.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Save16 = "Save16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_saveas16_edit = "saveas16_edit.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Select_Pol16 = "Select_Pol16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Select_Poly16 = "Select_Poly16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Select16 = "Select16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_SelectPoly16 = "SelectPoly16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_SelectPoly2_16 = "SelectPoly2_16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Show_all_16_2 = "Show_all_16_2.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_showAllLayer_16 = "showAllLayer_16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_showLayer_16 = "showLayer_16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Split16 = "Split16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_switch = "switch.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Switch12 = "Switch12.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_Undo16 = "Undo16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_ZoomIn16 = "ZoomIn16.gif"; //$NON-NLS-1$
	public static final String TG_IMAGE_ZoomOut16 = "ZoomOut16.gif"; //$NON-NLS-1$

	/**
	 * The constructor.
	 * 
	 * @param descriptor
	 *            plugin descriptor
	 */
	public MIPImagePlugin() {
		super();
		plugin = this;
		try {
			resourceBundle = getResourceBundle();
			;
		} catch (MissingResourceException x) {
			resourceBundle = null;
		}
	}

	/**
	 * Returns the shared instance.
	 */
	public static MIPImagePlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns the workspace instance.
	 */
	public static IWorkspace getWorkspace() {
		return ResourcesPlugin.getWorkspace();
	}

	/**
	 * Returns the string from the plugin's resource bundle, or 'key' if not
	 * found.
	 * 
	 * @param key
	 *            key in the .properties file
	 * @return value of the key
	 */
	public static String getResourceString(String key) {
		ResourceBundle bundle = resourceBundle;
		try {
			return bundle.getString(key);
		} catch (MissingResourceException e) {
			return key;
		} catch (NullPointerException e) {
			return key;
		}
	}

	/**
	 * Returns the plugin's resource bundle,
	 */
	public ResourceBundle getResourceBundle() {
		return resourceBundle;
	}

	/**
	 * Get the switch image for toolkit.
	 * 
	 * @return switch image
	 */
	public static Image getSwitchImage() {
		Image switchImage = null;
//		try {
//			URL url = // plugin.getDescriptor().getInstallURL();
//			Platform.asLocalURL(Platform.find(plugin.getBundle(), new Path(".")));
//			url = new URL(url, "icons/Switch12.gif");
//			switchImage = ImageDescriptor.createFromURL(url).createImage();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		URL url = FileLocator.find(MIPImagePlugin
				.getDefault().getBundle(), new Path("icons/Switch12.gif"),
				null);
		switchImage = ImageDescriptor.createFromURL(url).createImage();
		return switchImage;
	}
	

	/**
	 * set the viewer.
	 * 
	 * @param view
	 *            MIPImageView
	 */
	public static void setView(ImageView v) {
		view = v;
	}

	/**
	 * return the image shown in the current canvas.
	 * 
	 * @return Image.
	 */
	public static Image getImage() {
		if (view != null)
			return view.imagePanel.imageCanvas.getSourceImage();
		else
			return null;
	}

	public IStatus handleProblem(int severity, Throwable e) {
		return handleProblem(severity, null, e);
	}

	public IStatus handleProblem(int severity, String message, Throwable cause) {
		if (message == null || message.equals("")) {
			if (cause != null)
				message = cause.getLocalizedMessage();
		}
		IStatus status = new Status(severity, "MIP ImagePlugin", message, cause);
		StatusManager.getManager().handle(
				status,
				status.getSeverity() == IStatus.ERROR ? StatusManager.SHOW
						| StatusManager.LOG : StatusManager.LOG);
		return status;
	}

	public static IStatus handleError(Throwable cause, String message,
			Object... args) {
		return MIPImagePlugin.getDefault().handleProblem(IStatus.ERROR,
				NLS.bind(message, args), cause);
	}

	public static IStatus handleError(Throwable cause) {
		return MIPImagePlugin.getDefault().handleProblem(IStatus.ERROR, cause);
	}

	public static IStatus handleWarning(Throwable cause, String message,
			Object... args) {
		return MIPImagePlugin.getDefault().handleProblem(IStatus.WARNING,
				NLS.bind(message, args), cause);
	}

	public static IStatus handleWarning(Throwable cause) {
		return MIPImagePlugin.getDefault()
				.handleProblem(IStatus.WARNING, cause);
	}

	protected void initializeImageRegistry(ImageRegistry registry) {
		super.initializeImageRegistry(registry);
		registerImage(registry, TG_IMAGE_ADD, "icons/error.gif");
		registerImage(registry, TG_IMAGE_ADD,  "icons/add.gif"); //$NON-NLS-1$                                   
		registerImage(registry, TG_IMAGE_Adjust16,  "icons/Adjust16.gif"); //$NON-NLS-1$                         
		registerImage(registry, TG_IMAGE_blue_toolkit,  "icons/blue_toolkit.gif"); //$NON-NLS-1$                 
		registerImage(registry, TG_IMAGE_Center16,  "icons/Center16.gif"); //$NON-NLS-1$                         
		registerImage(registry, TG_IMAGE_Content16,  "icons/Content16.gif"); //$NON-NLS-1$                       
		registerImage(registry, TG_IMAGE_Curve16, "icons/Curve16.gif"); //$NON-NLS-1$                           
		registerImage(registry, TG_IMAGE_delete,  "icons/delete.gif"); //$NON-NLS-1$                             
		registerImage(registry, TG_IMAGE_Eyedrop16,  "icons/Eyedrop16.gif"); //$NON-NLS-1$                       
		registerImage(registry, TG_IMAGE_Fith16, "icons/Fith16.gif"); //$NON-NLS-1$                             
		registerImage(registry, TG_IMAGE_Fitv16, "icons/Fitv16.gif"); //$NON-NLS-1$                             
		registerImage(registry, TG_IMAGE_Fliph16, "icons/Fliph16.gif"); //$NON-NLS-1$                           
		registerImage(registry, TG_IMAGE_GrafLinkEdit, "icons/GrafLinkEdit.gif"); //$NON-NLS-1$                 
		registerImage(registry, TG_IMAGE_graphics_toolkits, "icons/graphics_toolkits.gif"); //$NON-NLS-1$       
		registerImage(registry, TG_IMAGE_Grid16, "icons/Grid16.gif"); //$NON-NLS-1$                             
		registerImage(registry, TG_IMAGE_Help16, "icons/Help16.gif"); //$NON-NLS-1$                             
		registerImage(registry, TG_IMAGE_Hflip16, "icons/Hflip16.gif"); //$NON-NLS-1$                           
		registerImage(registry, TG_IMAGE_hideLayer_16, "icons/hideLayer_16.gif"); //$NON-NLS-1$                 
		registerImage(registry, TG_IMAGE_icon_holongate_16x16, "icons/icon-holongate-16x16.png"); //$NON-NLS-1$ 
		registerImage(registry, TG_IMAGE_image_obj, "icons/image_obj.gif"); //$NON-NLS-1$                       
		registerImage(registry, TG_IMAGE_layer_16_2, "icons/layer_16_2.gif"); //$NON-NLS-1$                     
		registerImage(registry, TG_IMAGE_layer_16, "icons/layer_16.bmp"); //$NON-NLS-1$                         
		registerImage(registry, TG_IMAGE_Lense16, "icons/Lense16.gif"); //$NON-NLS-1$                           
		registerImage(registry, TG_IMAGE_LineSize16, "icons/LineSize16.gif"); //$NON-NLS-1$                     
		registerImage(registry, TG_IMAGE_Mark16, "icons/Mark16.gif"); //$NON-NLS-1$                             
		registerImage(registry, TG_IMAGE_MIP16, "icons/MIP16.gif"); //$NON-NLS-1$                               
		registerImage(registry, TG_IMAGE_Move16, "icons/Move16.gif"); //$NON-NLS-1$                             
		registerImage(registry, TG_IMAGE_Normal16, "icons/Normal16.gif"); //$NON-NLS-1$                         
		registerImage(registry, TG_IMAGE_Open16, "icons/Open16.gif"); //$NON-NLS-1$                             
		registerImage(registry, TG_IMAGE_openbrwsr, "icons/openbrwsr.gif"); //$NON-NLS-1$                       
		registerImage(registry, TG_IMAGE_Overlay16, "icons/Overlay16.gif"); //$NON-NLS-1$                       
		registerImage(registry, TG_IMAGE_Pencil16, "icons/Pencil16.gif"); //$NON-NLS-1$                         
		registerImage(registry, TG_IMAGE_PolySelect16_6, "icons/PolySelect16_6.gif"); //$NON-NLS-1$             
		registerImage(registry, TG_IMAGE_Redo16, "icons/Redo16.gif.gif"); //$NON-NLS-1$                         
		registerImage(registry, TG_IMAGE_Remove16, "icons/Remove16.gif"); //$NON-NLS-1$                         
		registerImage(registry, TG_IMAGE_Reset, "icons/Reset.gif"); //$NON-NLS-1$                               
		registerImage(registry, TG_IMAGE_Rotate16, "icons/Rotate16.gif"); //$NON-NLS-1$                         
		registerImage(registry, TG_IMAGE_Rotater16, "icons/Rotater16.gif"); //$NON-NLS-1$                       
		registerImage(registry, TG_IMAGE_sample, "icons/sample.gif"); //$NON-NLS-1$                             
		registerImage(registry, TG_IMAGE_save_edit, "icons/save_edit.gif"); //$NON-NLS-1$                       
		registerImage(registry, TG_IMAGE_Save16, "icons/Save16.gif"); //$NON-NLS-1$                             
		registerImage(registry, TG_IMAGE_saveas16_edit, "icons/saveas16_edit.gif"); //$NON-NLS-1$               
		registerImage(registry, TG_IMAGE_Select_Pol16, "icons/Select_Pol16.gif"); //$NON-NLS-1$                 
		registerImage(registry, TG_IMAGE_Select_Poly16, "icons/Select_Poly16.gif"); //$NON-NLS-1$               
		registerImage(registry, TG_IMAGE_Select16, "icons/Select16.gif"); //$NON-NLS-1$                         
		registerImage(registry, TG_IMAGE_SelectPoly16, "icons/SelectPoly16.gif"); //$NON-NLS-1$                 
		registerImage(registry, TG_IMAGE_SelectPoly2_16, "icons/SelectPoly2_16.gif"); //$NON-NLS-1$             
		registerImage(registry, TG_IMAGE_Show_all_16_2, "icons/Show_all_16_2.gif"); //$NON-NLS-1$               
		registerImage(registry, TG_IMAGE_showAllLayer_16, "icons/showAllLayer_16.gif"); //$NON-NLS-1$           
		registerImage(registry, TG_IMAGE_showLayer_16, "icons/showLayer_16.gif"); //$NON-NLS-1$                 
		registerImage(registry, TG_IMAGE_Split16, "icons/Split16.gif"); //$NON-NLS-1$                           
		registerImage(registry, TG_IMAGE_switch, "icons/switch.gif"); //$NON-NLS-1$                             
		registerImage(registry, TG_IMAGE_Switch12, "icons/Switch12.gif"); //$NON-NLS-1$                         
		registerImage(registry, TG_IMAGE_Undo16, "icons/Undo16.gif"); //$NON-NLS-1$                             
		registerImage(registry, TG_IMAGE_ZoomIn16, "icons/ZoomIn16.gif"); //$NON-NLS-1$                         
		registerImage(registry, TG_IMAGE_ZoomOut16, "icons/ZoomOut16.gif"); //$NON-NLS-1$                       
	}

	/**
	 * Registers the image in the image registry.
	 * 
	 * @param registry
	 *            the image registry to use
	 * @param key
	 *            the key by which the image will be adressable
	 * @param fullPath
	 *            the full path to the image, relative to the plugin directory.
	 */
	private void registerImage(ImageRegistry registry, String key,
			String fullPath) {
		URL imageURL = FileLocator.find(getBundle(), new Path(fullPath), null);
		imageDescriptorFromPlugin(PLUGIN_ID, fullPath);
		registry.put(key, ImageDescriptor.createFromURL(imageURL));
	}
	
	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	

//	    @Override
//	    protected void initializeImageRegistry1(ImageRegistry registry) {
//	        super.initializeImageRegistry(registry);
//	        Bundle bundle = Platform.getBundle(ID);
//
//	        ImageDescriptor myImage = ImageDescriptor.createFromURL(
//	              FileLocator.find(bundle,
//	                               new Path("icons/myImage..gif"),
//	                                        null));
//	        registry.put(MY_IMAGE_ID, myImage);
//	    }

}
