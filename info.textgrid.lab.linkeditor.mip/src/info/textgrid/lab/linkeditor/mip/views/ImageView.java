/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
/*******************************************************************************
 * All rights reserved. 
 * Contributors:
 *     Chengdong Li
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.views;

import info.textgrid.lab.linkeditor.controller.ILinkEditorListener;
import info.textgrid.lab.linkeditor.controller.LinkEditorController;
import info.textgrid.lab.linkeditor.controller.LinkEditorController.Event;
import info.textgrid.lab.linkeditor.controller.utils.Pair;
import info.textgrid.lab.linkeditor.mip.MIPImagePlugin;
import info.textgrid.lab.linkeditor.mip.component.canvas.IImageObserver;
import info.textgrid.lab.linkeditor.mip.component.canvas.session.IImageSession;
import info.textgrid.lab.linkeditor.mip.component.loader.IImageLoader;
import info.textgrid.lab.linkeditor.mip.component.loader.ImageLoaderFactory;
import info.textgrid.lab.linkeditor.mip.gui.ImagePanel;
import info.textgrid.lab.linkeditor.mip.gui.actiondelegate.RedoActionDelegate;
import info.textgrid.lab.linkeditor.mip.gui.actiondelegate.UndoActionDelegate;
import info.textgrid.lab.linkeditor.mip.gui.sessiondelegate.DefaultMIPSession;
import info.textgrid.lab.linkeditor.mip.gui.sessiondelegate.IPanelSession;
import info.textgrid.lab.linkeditor.mip.gui.toolkitdelegate.IToolkitCheckButton;
import info.textgrid.lab.linkeditor.model.graphics.TGLine;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;
import info.textgrid.lab.linkeditor.model.graphics.TGShape.WRITING_MODE;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.InvalidRegistryObjectException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.ISaveablePart;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.internal.dialogs.TreeManager.ViewerCheckStateListener;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * This ImageView class shows how to use SWT to manipulate images. The gui part
 * includes an ImagePanel which wrapped up an ImageProsessCanvas, which is a
 * subclass of SWTImageCanvas who is a subclass of Canvas.
 * <p>
 * It includes 4 toolbar buttons and several local menus. The buttons on the
 * toolbar are used to open file and re/undo
 * <p>
 * It also include a toolkit panel, which can be trigger by TAB key. The toolkit
 * panel include all toolkits which will be used to view and process image. It
 * is configurable, and each button is configed using plugin.xml, and new
 * toolkit can be added by changing plugin.xml, if only you provide a new class
 * wich implements the ISession interface.
 * <p>
 * All the image processing methods will be shown on the local menu. You can add
 * your own method by just extending the view extension.
 * <p>
 * The ImagePanel also includes a status bar to show the position and the
 * pixel's RGB value; a progress bar show the progress of processing for some
 * methods; a time window showing the totally time for processing.
 * <p>
 * For future usage, it provides following extensions:
 * <ul>
 * <li>An extensible context menu, which you can extend by extending :
 * info.textgrid.lab.linkeditor.mip.contextmenu.</li>
 * <li>An extensible image loader, which you can extend by extending :
 * info.textgrid.lab.linkeditor.mip.imageloaders.</li>
 * <li>The menu and toolbar is extensible, so that you can add your own image
 * manipulation and processing algorithm by extending these extensions.</li>
 * </ul>
 * 
 * @author Chengdong Li
 * @see info.textgrid.lab.linkeditor.mip.gui.ImagePanel
 */

public class ImageView extends ViewPart implements ILinkEditorListener,

		ISaveablePart, IPartListener2, ISelectionProvider {

	public static String ID = "edu.uky.mip.views.ImageView";

	/**
	 * image panel includes the canvas and status bar
	 */
	public ImagePanel imagePanel;

	private DefaultMIPSession ds = new DefaultMIPSession();

	private String defaultPartName = "";
	private String imageViewTitle = "";

	private String secondaryId = null;

	private boolean openNewDialog = true;

	private boolean toolkitNotOpenedYet = true;

	private List<ISelectionChangedListener> selectionChangedListeners = Collections
			.synchronizedList(new ArrayList<ISelectionChangedListener>());

	private Object selectedImageObject = new Object();

	private String imageUri = "";

	private boolean dirty = false;
	
	/**
	 * The constructor.
	 */
	public ImageView() {
		MIPImagePlugin.setView(this);
	}

	@Override
	public void init(IViewSite site) throws PartInitException {
		super.init(site);
		secondaryId = site.getSecondaryId();
		if (secondaryId == null) { // the initial view
			secondaryId = "1";
		}
		
	}
	
	public String getSecondaryId() {
		return secondaryId;
	}

	/**
	 * Create the GUI.
	 * 
	 * @param frame
	 *            The Composite handle of parent
	 */
	@Override
	public void createPartControl(Composite frame) {
		// Note: following is order dependent, do not change them!
		imagePanel = createGUI(frame);
		registerSessions();
		registerLoaders();
		registerCheckButtons();
		createContextMenu();

		HistogramView histogramView = (HistogramView) PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.findView(HistogramView.ID);
		if (histogramView != null) {
			histogramView.panel.reloadImage(imagePanel.imageCanvas
					.getImageData());
			this.registerImageObserver(histogramView);
		}

		ds.setPanel(imagePanel);
		ds.setSessionPrompt(getResourceString("session.default.prompt"));
		this.imagePanel.setSession(ds);

		getSite().setSelectionProvider(this);

		LinkEditorController.getInstance().addListener(this);

		getViewSite().getPage().addPartListener(this);

		imageViewTitle = defaultPartName = getPartName();

		PlatformUI.getWorkbench().getHelpSystem()
				.setHelp(frame, "info.textgrid.lab.linkeditor.mip.ImageView");
	}

	
	/**
	 * Called when we must grab focus.
	 * 
	 * @see org.eclipse.ui.part.ViewPart#setFocus
	 */
	@Override
	public void setFocus() {
		imagePanel.setFocus();
		LinkEditorController.getInstance().setFocusedTab(this.secondaryId);
	}

	
	/**
	 * Called when the View is to be disposed
	 */
	@Override
	public void dispose() {
		    StatusManager.getManager().handle(new Status(IStatus.INFO, MIPImagePlugin.PLUGIN_ID, "Disposing Image View"), StatusManager.LOG);
			LinkEditorController.getInstance().removeListener(this);
			getViewSite().getPage().removePartListener(this);
			LinkEditorController.getInstance().doUnlockTglObject();
			LinkEditorController.getInstance().setImageTabClosed(this.secondaryId);
			imagePanel.dispose();
			super.dispose();
		    StatusManager.getManager().handle(new Status(IStatus.INFO, MIPImagePlugin.PLUGIN_ID, "Disposed Image View"), StatusManager.LOG);
	}

	/**
	 * create image panel and add action bar
	 */
	private ImagePanel createGUI(Composite parent) {
		imagePanel = new ImagePanel(parent);

		registerRedoUndo();// Handle redo undo shortcut
		imagePanel.initGUI();
		// set adapter for image canvas.
		return imagePanel;
	}

	/**
	 * Make the context menu extensible.
	 */
	private void createContextMenu() {
		// getSite().registerContextMenu(imagePanel.imageCanvas.getContextMenuManager(),this.getSite().getSelectionProvider());
	}

	/**
	 * Register redo undo handler.
	 */
	private void registerRedoUndo() {
		/* Support global actions here */
		getViewSite().getActionBars().setGlobalActionHandler(
				ActionFactory.UNDO.getId(), new Action() {
					@Override
					public void run() {
						UndoActionDelegate.run(imagePanel.imageCanvas);
					}
				});
		getViewSite().getActionBars().setGlobalActionHandler(
				ActionFactory.REDO.getId(), new Action() {
					@Override
					public void run() {
						RedoActionDelegate.run(imagePanel.imageCanvas);
					}
				});
	}

	/**
	 * Register the image observer.
	 * 
	 * @param observer
	 *            observer to be registered
	 */
	public void registerImageObserver(IImageObserver observer) {
		this.imagePanel.imageCanvas.registerImageObserver(observer);
	}

	/**
	 * Remove the registered observer from the list .
	 * 
	 * @param observer
	 *            observer to be removed
	 */
	public void removeImageObserver(IImageObserver observer) {
		this.imagePanel.imageCanvas.removeImageObserver(observer);
	}

	/**
	 * The plugin startup code does the following job:
	 * <ol>
	 * <li>Find all the well-known org.holongate.eclipse.j2d.factory extensions</li>
	 * <li>Find which extension has a platform attribute matching the runtime
	 * descriptor</li>
	 * <li>Create the executable extension for the first matching extension and
	 * returns</li>
	 * </ol>
	 * If no suitable extension is found, a, exception is thrown and the plugin
	 * is disabled.
	 * 
	 * @throws CoreException
	 *             When there is no extension for the runtime platform.
	 */
	public void registerLoaders() {
		IExtensionRegistry pluginRegistry = Platform.getExtensionRegistry();
		IExtensionPoint point = pluginRegistry
				.getExtensionPoint("info.textgrid.lab.linkeditor.mip.imageloaders");
		IExtension extensions[] = point.getExtensions();
		// For all the defined extensions...
		for (int e = 0; e < extensions.length; e++) {
			IConfigurationElement[] elements = extensions[e]
					.getConfigurationElements();
			// For all the configuration elements...
			for (int f = 0; f < elements.length; f++) {
				try {
					IImageLoader loader = (IImageLoader) elements[f]
							.createExecutableExtension("class");
					// imagePanel.imageCanvas.addLoader(loader);
					ImageLoaderFactory.registerLoader(loader);

				} catch (CoreException e1) {
					System.err.println(elements[f].getName()
							+ " createion failed!");
					MIPImagePlugin.handleError(e1);
				}
			}
		}
	}

	public String getResourceString(String id) {
		return MIPImagePlugin.getResourceString(id);
	}

	public URL getInstallURL(String pathString) {
//					return Platform.asLocalURL(Platform.find(
//					Platform.getBundle("info.textgrid.lab.linkeditor.mip"),
//					new Path(".")));
		URL url = FileLocator.find(MIPImagePlugin
				.getDefault().getBundle(), new Path(pathString),
				null);
		try {
			URL resolved = FileLocator.resolve(url);
//			System.out.println(url + " -> "  + resolved);
			return resolved;
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	/*
	 * Create the action-handler map.
	 */
	public void registerSessions() {
		// Find all the declared extensions
		IExtensionRegistry pluginRegistry = Platform.getExtensionRegistry();
		IExtensionPoint point = pluginRegistry
				.getExtensionPoint("info.textgrid.lab.linkeditor.mip.sessiondelegates");
		IExtension extensions[] = point.getExtensions();

		// For all the defined extensions...
		for (int e = 0; e < extensions.length; e++) {
			IConfigurationElement[] elements = extensions[e]
					.getConfigurationElements();
			// For all the configuration elements...
			for (int f = 0; f < elements.length; f++) {
				try {
					IImageSession session = (IImageSession) elements[f]
							.createExecutableExtension("class");
					if (session instanceof IPanelSession)
						((IPanelSession) session).setPanel(imagePanel);

					URL url = getInstallURL(elements[f].getAttribute("icon").trim());
//					url = new URL(url, elements[f].getAttribute("icon").trim());
					session.setSessionIcon(ImageDescriptor.createFromURL(url));

					boolean selected = new Boolean(
							elements[f].getAttribute("selected"))
							.booleanValue();
					session.setSessionSelected(selected);

					String id = elements[f].getAttribute("id").trim();
					session.setSessionID(id);

					if (selected)
						imagePanel.setSessionId(id);

					session.setSessionToolTip(getResourceString(id + ".tooltip"));
					session.setSessionPrompt(getResourceString(id
							+ ".description"));
					imagePanel.registerSession(id, session);
				} catch (CoreException e1) {
					System.err.println(elements[f].getAttribute("id")
							+ " creation failed!");
					MIPImagePlugin.handleError(e1);
//				} catch (MalformedURLException e2) {
//					System.err.println(elements[f].getAttribute("id")
//							+ " creation failed!");
//					MIPImagePlugin.handleError(e2);
				} catch (InvalidRegistryObjectException e3) {
					System.err.println(elements[f].getAttribute("id")
							+ " creation failed!");
					MIPImagePlugin.handleError(e3);
				}
			}
		}
	}

	/*
	 * Create toolkit check buttons.
	 */
	public void registerCheckButtons() {
		// Find all the declared extensions
		IExtensionRegistry pluginRegistry = Platform.getExtensionRegistry();
		IExtensionPoint point = pluginRegistry
				.getExtensionPoint("info.textgrid.lab.linkeditor.mip.toolkitcheckbuttons");
		IExtension extensions[] = point.getExtensions();

		// For all the defined extensions...
		for (int e = 0; e < extensions.length; e++) {
			IConfigurationElement[] elements = extensions[e]
					.getConfigurationElements();
			// For all the configuration elements...
			for (int f = 0; f < elements.length; f++) {
				try {
					IToolkitCheckButton button = (IToolkitCheckButton) elements[f]
							.createExecutableExtension("class");

					URL url = getInstallURL(elements[f].getAttribute("icon").trim());
//					url = new URL(url, elements[f].getAttribute("icon").trim());
					button.setIcon(ImageDescriptor.createFromURL(url));

					String id = elements[f].getAttribute("id").trim();
					button.setId(id);
					button.setToolTip(getResourceString(imagePanel
							.mapPromptMessage(id)));
					button.setToolkitSetting(imagePanel.getTkSetting());
					imagePanel.getTkSetting().registerCHKButtonHandler(id,
							button);

//				} catch (MalformedURLException e1) {
//					System.err.println(elements[f].getAttribute("id")
//							+ " creation failed!");
//					MIPImagePlugin.handleError(e1);
				} catch (CoreException e2) {
					System.err.println(elements[f].getAttribute("id")
							+ " creation failed!");
					MIPImagePlugin.handleError(e2);
				}
			}
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void update(Event event, Pair<String, ?> pair) {

		if (pair == null
				|| (!this.secondaryId.equals(pair.getValue1())
						&& !this.imageUri.equals(pair.getValue1()) && !"all"
						.equals(pair.getValue1())))
			return;

		Object object = pair.getValue2();

		if (event == Event.ADD_LINK_LOADING) {
			if (object != null && object instanceof TGShape) {
				TGShape helpShape = (TGShape) object;
				if (helpShape != null) {
					helpShape.setLinked(true);
					// It will be added just if it doesn't exist
					imagePanel.imageCanvas.addShapeToArrayListShapes(helpShape);
				}
			}
		} else if (event == Event.LOAD_IMAGE) {
			Image img = (Image) object;
			try {
				if (img != null) {
					if (!imagePanel.imageCanvas.isVisible())
						imagePanel.imageCanvas.setVisible(true);

					if (!img.equals(imagePanel.imageCanvas.getImage())) {// If
						// the
						// current
						// image-object
						// hasn't
						// already been
						// loaded
						if (imagePanel.imageCanvas.getImage() == null) {// If
							// the
							// canvas
							// is
							// empty

														
							URL url = FileLocator.find(Platform.getBundle("info.textgrid.lab.linkeditor.mip.component"), new Path("icons/Center16.gif"), null);
							URL uurl = url == null? null: FileLocator.toFileURL(url); // FIXME don't rely on a file here
//							System.out.println(url + " => " + uurl);
							imagePanel.imageCanvas.reloadImage(uurl.getFile());
						}

						int imgWidth = img.getBounds().width, viewWidth = imagePanel.imageCanvas
								.getClientArea().width;

						// System.err.println(imgWidth + "  " + viewWidth);

						imagePanel.imageCanvas.reloadImage(img);

						if (imgWidth > viewWidth)
							imagePanel.imageCanvas.fitHorizontally();
						else
							imagePanel.imageCanvas.showOriginal();

						imagePanel.imageCanvas.removeAllShapes(false);
						imagePanel.imageCanvas.imageReloaded = true;
					}
				}
			} catch (IOException e) {
				MIPImagePlugin.handleError(e);
			}
		} else if (event == Event.UPDATE_LOADED_IMAGE) {
			if (!imagePanel.imageCanvas.isVisible())
				imagePanel.imageCanvas.setVisible(true);
			imagePanel.imageCanvas.removeAllShapes(false);
			imagePanel.imageCanvas.imageReloaded = true;

		} else if (event == Event.SET_IMAGE_URI) {
			if (object != null && object instanceof String) {
				this.imageUri = (String) object;
				imagePanel.imageCanvas.setImageUri(this.imageUri);
			}
		} else if (event == Event.ADD_LINK || event == Event.JUMP_TO_SHAPE) {
			if (object != null && object instanceof TGShape) {
				TGShape helpShape = (TGShape) object;
				if (helpShape != null) {

					if (event == Event.ADD_LINK) {
						helpShape.setLinked(true);
					}
					// It will be added just if it doesn't exist
					imagePanel.imageCanvas.addShapeToArrayListShapes(helpShape);

					if (imagePanel.imageCanvas.getArrayListShapes().contains(
							helpShape)) {
						if (event == Event.JUMP_TO_SHAPE) {
							try {
								if (!PlatformUI.getWorkbench()
										.getActiveWorkbenchWindow()
										.getActivePage().isPartVisible(this))
									PlatformUI
											.getWorkbench()
											.getActiveWorkbenchWindow()
											.getActivePage()
											.showView(
													ID,
													secondaryId,
													IWorkbenchPage.VIEW_ACTIVATE);
							} catch (PartInitException e) {
								MIPImagePlugin.handleError(e,
										"Couldn't bring the view to top!");
							}
						}
						imagePanel.imageCanvas.setSelectedShape(helpShape);
						imagePanel.imageCanvas.showSelectedShape();
					}
				}
			}
		} else if (event == Event.REMOVE_SHAPE) {
			if (object != null && object instanceof TGShape) {
				TGShape shape = (TGShape) object;
				if (shape != null) {
					imagePanel.imageCanvas.removeShape(shape);
				}
			}
		} else if (event == Event.SET_TITLE_LABEL) {
			String title = (String) object;
			if (title != null) {
				// imagePanel.showTitle(title);
				if (!title.equals(""))
					imageViewTitle = title;
				else
					imageViewTitle = defaultPartName;

				setPartName(imageViewTitle);
			}
		} else if (event == Event.SET_TYPE_LABEL) {
			String type = (String) object;
			if (type != null) {
				// one blank for better visibility for the first character
				imagePanel.showTextOnImageLabel(" " + type);
			}
		} else if (event == Event.SET_LINKS_OBJECT_LABEL) {
			String text = (String) object;
			if (text != null) {
				// one blank for better visibility for the first character
				imagePanel.showTextOnImageLabel2(" " + text);
			}
		} else if (event == Event.SET_COUNT) {
			Boolean diff = (Boolean) object;
			imagePanel.showDiffCount(diff);
			setDirty(diff);
			// isDirty();
		} else if (event == Event.SET_ANGLE) {
			Pair<Boolean, Double> p = (Pair<Boolean, Double>) object;
			boolean visible = p.getValue1();
			double angle = p.getValue2();
			imagePanel.showAngleInfo(visible, angle);
		} else if (event == Event.SET_LAYER) {
			Pair<Boolean, String> p = (Pair<Boolean, String>) object;
			boolean visible = p.getValue1();
			String angle = p.getValue2();
			imagePanel.showLayerInfo(visible, angle);
		} else if (event == Event.SET_IMAGEVIEW_INVISIBLE) {
			imagePanel.imageCanvas.setVisible(false);
		} else if (event == Event.LINKS_CHANGED || event == Event.CHANGES_SAVED) {
			if (event == Event.LINKS_CHANGED) {
				adaptArrayListShapes(object);
			} else if (event == Event.CHANGES_SAVED) {
				setDirty(false);
				isDirty();
			}
//			isDirty();
			imagePanel.imageCanvas.redraw();
		} else if (event == Event.FINALIZE_LOADING) {
			ds.forceStartingToolkitDialog(openNewDialog);
			openNewDialog = toolkitNotOpenedYet = false;
		} else if (event == Event.ADD_DOCKING_LINE) {
			TGShape newLine = (TGLine) object;
			imagePanel.imageCanvas.addShapeToArrayListShapes(newLine);
		} else if (event == Event.UPDATE_IMAGE_VIEW_ON_CHANGES) {
			setDirty((Boolean) object);
			isDirty();
		} else if (event == Event.REMOVE_SELECTED_SHAPES) {
			imagePanel.imageCanvas.removeSelectedShapes();
		} else if (event == Event.REMOVE_ALL_SHAPES) {
			imagePanel.imageCanvas.removeAllShapes(false);
		} else if (event == Event.UPDATE_SHAPES_LIST) {
			List<TGShape> allShapes = (List<TGShape>) object;
			if (allShapes != null) {
				allShapes.addAll(imagePanel.imageCanvas.getArrayListShapes());
			}
		} else if (event == Event.ADD_SHAPE) {
			TGShape s = (TGShape) object;
			if (s != null){
				setDirty(true);
				//isDirty();
				imagePanel.imageCanvas.addShapeToArrayListShapes(s);
			}
		} else if (event == Event.SELECT_SHAPE) {
			TGShape s = (TGShape) object;
			if (s != null) {
				imagePanel.imageCanvas.setSelectedShape(s);
				imagePanel.imageCanvas.showSelectedShape();
				imagePanel.imageCanvas.redraw();
			}
		} else if (event == Event.DESELECT_SHAPE) {
			TGShape selShape = imagePanel.imageCanvas.getSelectedShape();
			if (selShape != null && selShape.isCompletedShape())
				imagePanel.imageCanvas.resetSelectedShape();
		} else if (event == Event.CLOSE_TOOLKIT) {
			imagePanel.imageCanvas.getDisplay().timerExec(100, new Runnable() {
				@Override
				public void run() {
					imagePanel.forceCloseToolkitDialog(true);
				}
			});

		} else if (event == Event.OPEN_TOOLKIT) {
			if (!(toolkitNotOpenedYet || imagePanel
					.isToolkitClosedByChangeCtrlT_Flag()))
				return;

			if (toolkitNotOpenedYet) {
				imagePanel.setSessionByID(imagePanel.getSessionId());
			}

			imagePanel.imageCanvas.getDisplay().timerExec(100, new Runnable() {
				@Override
				public void run() {
					imagePanel.forceOpenToolkitDialog();
				}
			});
			imagePanel.setToolkitClosedByChangeCtrlT_Flag(false);
			toolkitNotOpenedYet = false;
		} else if (event == Event.SET_SELECTED_OBJECT) {
			if (object != null) {
				this.selectedImageObject = object;
			} else {
				this.selectedImageObject = new Object();
			}
		} else if (event == Event.SET_DEFAULT_WRITING_MODE) {
			if (object != null && object instanceof WRITING_MODE) {
				WRITING_MODE writingMode = (WRITING_MODE) object;
				imagePanel.imageCanvas.setDefaultWritingMode(writingMode);
			}
		} else if (event == Event.GET_DEFAULT_WRITING_MODE) {
			LinkEditorController.getInstance().setWritingModeFromImageView(
					imagePanel.imageCanvas.getDefaultWritingMode());
		} else if (event == Event.REDRAW) {
			imagePanel.imageCanvas.redraw();
		}
	}

	public void adaptArrayListShapes(Object object) {
		if (object != null && object instanceof TGShape) {
			TGShape helpShape = (TGShape) object;
			helpShape.setLinked(true);
			imagePanel.imageCanvas.setSelectedShape(helpShape);

			// Update ArrayListShapes
			int pos = imagePanel.imageCanvas.getArrayListShapes().indexOf(
					helpShape);

			if (pos >= 0) {
				imagePanel.imageCanvas.getArrayListShapes().set(pos, helpShape);
				imagePanel.imageCanvas.showSelectedShape();
			}
		}
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		LinkEditorController.getInstance().doSave();
	}

	@Override
	public void doSaveAs() {
		LinkEditorController.getInstance().doSaveAs();
	}

	public void setDirty(boolean dirty) {
		this.dirty = dirty;
		LinkEditorController.getInstance().setDirty(LinkEditorController.getInstance().getDirty() || dirty);
	}
	
	boolean firePoropertyChange = false;
	
	public boolean getDirty(){
		return dirty;
	}
	
	@Override
	public boolean isDirty() {
		
		if(!firePoropertyChange){
			firePoropertyChange = true;
			// firePropertyChange(PROP_DIRTY);
			firePoropertyChange = false;
		}
		return getDirty();
	}

	@Override
	public boolean isSaveAsAllowed() {
		return LinkEditorController.getInstance().areThereLinks();
	}

	@Override
	public boolean isSaveOnCloseNeeded() {
		return true;
	}

	@Override
	public void partActivated(IWorkbenchPartReference partRef) {
		// TODO Auto-generated method stub
	}

	@Override
	public void partBroughtToTop(IWorkbenchPartReference partRef) {
		// TODO Auto-generated method stub
	}

	@Override
	public void partClosed(IWorkbenchPartReference partRef) {
	}

	@Override
	public void partDeactivated(IWorkbenchPartReference partRef) {
		imagePanel.imageCanvas.clearWorkingPoly();
	}

	@Override
	public void partHidden(IWorkbenchPartReference partRef) {
		// TODO Auto-generated method stub
	}

	@Override
	public void partInputChanged(IWorkbenchPartReference partRef) {
		// TODO Auto-generated method stub
	}

	@Override
	public void partOpened(IWorkbenchPartReference partRef) {
		// TODO Auto-generated method stub
	}

	@Override
	public void partVisible(IWorkbenchPartReference partRef) {
		// TODO Auto-generated method stub
	}

	// --- for the selection-provider ---

	@Override
	public void addSelectionChangedListener(ISelectionChangedListener listener) {
		selectionChangedListeners.add(listener);
	}

	@Override
	public ISelection getSelection() {
		if (imagePanel.imageCanvas.isDisposed()) {
			this.selectedImageObject = new Object();
		}
		return new StructuredSelection(new Object[] { this.selectedImageObject,
				this.imageUri });
	}

	@Override
	public void removeSelectionChangedListener(
			ISelectionChangedListener listener) {
		selectionChangedListeners.remove(listener);

	}

	@Override
	public void setSelection(ISelection selection) {
		// TODO Auto-generated method stub
	}
}
