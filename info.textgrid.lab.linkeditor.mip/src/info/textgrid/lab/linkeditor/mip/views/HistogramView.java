/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.mip.views;

import info.textgrid.lab.linkeditor.mip.MIPImagePlugin;
import info.textgrid.lab.linkeditor.mip.component.canvas.AffineImageCanvas;
import info.textgrid.lab.linkeditor.mip.component.canvas.IImageObserver;
import info.textgrid.lab.linkeditor.mip.gui.HistogramPanel;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;

import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

/**
 * Instances of this class are ViewParts; they will show the intensity histogram
 * of a image shown in ImageView.
 * 
 * @author Chengdong Li
 * 
 * @see info.textgrid.lab.linkeditor.mip.gui.HistogramPanel
 * 
 */
public class HistogramView extends ViewPart implements IPropertyChangeListener,
		IImageObserver {
	public HistogramPanel panel;
	public static String ID = "edu.uky.mip.views.HistogramView";

	/**
	 * Create gui for view part
	 */
	@Override
	public void createPartControl(Composite parent) {
		panel = new HistogramPanel(parent);
		MIPImagePlugin.getDefault().getPreferenceStore()
				.addPropertyChangeListener(this);

		try {
			ImageView imageView = (ImageView) PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage()
					.findView("edu.uky.mip.views.ImageView");
			imageView.registerImageObserver(this);
			panel.reloadImage(imageView.imagePanel.imageCanvas.getImageData());
		} catch (Exception e) {
		}

	}

	public void setImage(Image img) {
		panel.reloadImage(img.getImageData());
	}

	/**
	 * Behavior when this view is set focus
	 */
	@Override
	public void setFocus() {
	}

	/**
	 * Called when the view is to be disposed
	 */
	@Override
	public void dispose() {
		super.dispose();
		try {
			ImageView imageView = (ImageView) PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage()
					.findView("edu.uky.mip.views.ImageView");
			if (imageView != null) {
				imageView.removeImageObserver(this);
			}
		} catch (Exception e) {
		}

		panel.dispose();
	}

	/**
	 * Check up if the image format is supported, this is done based on the
	 * suffix of the file.
	 * <p>
	 * <dl>
	 * Currently, we support following formats:
	 * <dl>
	 * <dt><b>.jpeg; .jpg</b></dt>
	 * <dd>Joint Photographic Experts Group image format.</dd>
	 * <dt><b>.bmp</b></dt>
	 * <dd>Bitmap image format.</dd>
	 * <dt><b>.gif</b></dt>
	 * <dd>Graphics Interchange Format.</dd>
	 * <dt><b>.png</b></dt>
	 * <dd>W3C Portable Network Graphics image format.</dd>
	 * <dt><b>.ico</b></dt>
	 * <dd>Icon file.</dd>
	 * </dl>
	 * </dl>
	 * 
	 * @param filename
	 *            image file name
	 * @return <ul>
	 *         <li><b>true</b> if format is supported; <li><b>false</b> if
	 *         format is not supported.
	 *         </ul>
	 */
	public boolean isLegalFormat(String filename) {
		String suffix = filename.substring(filename.lastIndexOf(".") + 1);
		if (suffix.equalsIgnoreCase("jpg") || suffix.equalsIgnoreCase("jpeg")
				|| suffix.equalsIgnoreCase("gif")
				|| suffix.equalsIgnoreCase("png")
				|| suffix.equalsIgnoreCase("ico")
				|| suffix.equalsIgnoreCase("bmp")) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * @see org.eclipse.jface.util.IPropertyChangeListener#propertyChange(PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (panel != null) {
			panel.redraw();
		}
	}

	/**
	 * Called when the image is changed to refresh the intensity histogram
	 */
	@Override
	public void onImageChanged(AffineImageCanvas canvas, ImageData newData) {
		panel.reloadImage(newData);
	}

	@Override
	public void onSelectionChanged(AffineImageCanvas canvas, TGShape rect) {
	}
}
