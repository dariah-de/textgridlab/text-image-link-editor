/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/

package info.textgrid.lab.linkeditor.algorithmdelegate;

import info.textgrid.lab.linkeditor.mip.views.ImageView;

import org.eclipse.jface.action.IAction;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.ui.IViewPart;

/**
 * A gaussian convolution with 15x15 kernel.
 * <p>
 * 
 * @author Chengdong Li
 */
public class GaussianDelegate extends AbstractImageDelegate {

	/**
	 * Run Gaussian convolution method.
	 * 
	 * @see org.eclipse.ui.IViewActionDelegate#init(org.eclipse.ui.IViewPart)
	 */
	@Override
	public void init(IViewPart viewPart) {
		if (viewPart instanceof ImageView) {
			this.view = (ImageView) viewPart;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public void run(IAction action) {
		super.run(action);
		this.execute();
	}

	/**
	 * execute gaussian filter
	 */
	public void execute() {
		ImageData source = getImageData();
		if (source == null)
			return;
		getCanvas().reloadSelectImage(guass7(source));
	}

	/**
	 * kenel 7x7 guassian convolution
	 * 
	 * @param src
	 *            source image data
	 * @return image data created from processing
	 */
	public ImageData guass7(ImageData src) {
		float k[][] = { { 1, 1, 2, 2, 2, 1, 1 }, { 1, 2, 2, 4, 2, 2, 1 },
				{ 2, 2, 4, 8, 4, 2, 2 }, { 2, 4, 8, 16, 8, 4, 2 },
				{ 2, 2, 4, 8, 4, 2, 2 }, { 1, 2, 2, 4, 2, 2, 1 },
				{ 1, 1, 2, 2, 2, 1, 1 } };
		float sum = 0;
		for (int i = 0; i < k.length; i++) {
			for (int j = 0; j < k[0].length; j++) {
				sum += k[i][j];
			}
		}
		for (int i = 0; i < k.length; i++) {
			for (int j = 0; j < k[0].length; j++) {
				k[i][j] /= sum;
			}
		}

		long startTime = System.currentTimeMillis();
		ImageData bi = convolve(src, k);
		long endTime = System.currentTimeMillis();
		showTime(endTime - startTime);

		totalTime = (endTime - startTime) / 1.e3;
		return bi;
	}

	public void show(String s) {
		System.out.println(s);
	}
}
