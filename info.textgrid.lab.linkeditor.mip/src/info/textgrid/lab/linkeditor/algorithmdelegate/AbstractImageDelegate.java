/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.algorithmdelegate;

import info.textgrid.lab.linkeditor.mip.component.canvas.SWT2dUtil;
import info.textgrid.lab.linkeditor.mip.gui.ImagePanel;
import info.textgrid.lab.linkeditor.mip.gui.ImageProcessCanvas;
import info.textgrid.lab.linkeditor.mip.gui.actiondelegate.AbstractViewActionDelegate;
import info.textgrid.lab.linkeditor.model.graphics.TGRectangle;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;

import org.eclipse.jface.action.IAction;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;

/**
 * An abstract class for all image processing delgates.
 * 
 * @author Chengdong Li
 */
public abstract class AbstractImageDelegate extends AbstractViewActionDelegate {
	/** time for convolution */
	public static double convolveTime = 0;
	/** time for convolution and image construction */
	public static double totalTime = 0;

	public static final PaletteData palette = new PaletteData(0x0000FF00,
			0x00FF0000, 0xFF000000);

	/**
	 * Given three color plane, convert them into imagedata
	 * 
	 * @param r
	 *            red plane
	 * @param g
	 *            green plane
	 * @param b
	 *            blue plane
	 * @return image data
	 */
	public static ImageData short2Image(short[][] r, short[][] g, short[][] b) {
		int width = r.length;
		int height = r[0].length;
		ImageData dest = new ImageData(width, height, 32, palette);

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				int pixel = (r[i][j] << 8) | (g[i][j] << 16) | (b[i][j] << 24);
				dest.setPixel(i, j, pixel);
			}
		}
		return dest;
	}

	/**
	 * Show the progress of processing
	 * 
	 * @param percent
	 *            prcentage has been finished
	 */
	public void showProgress(int percent) {
		view.imagePanel.showProgress(percent);
	}

	/**
	 * Show total processing time
	 * 
	 * @param ms
	 *            time in miliseconds
	 */
	public void showTime(long ms) {
		view.imagePanel.showTime(ms);
	}

	/**
	 * Show total processing time
	 * 
	 * @param ms
	 *            time in miliseconds
	 */
	public void showTime(double ms) {
		view.imagePanel.showTime(ms);
	}

	/**
	 * Convolve the source image with the given kernel
	 * 
	 * @param src
	 *            source image
	 * @param k
	 *            kernel
	 * @return image data after covolution.(share no data with src)
	 */
	public ImageData convolve(ImageData src, float k[][]) {
		int width = src.width;
		int height = src.height;
		short r[][] = new short[width][height];
		short g[][] = new short[width][height];
		short b[][] = new short[width][height];

		// preparing data.
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				int pixel = src.getPixel(i, j);
				RGB rgb = src.palette.getRGB(pixel);
				r[i][j] = (short) rgb.red;
				g[i][j] = (short) rgb.green;
				b[i][j] = (short) rgb.blue;
			}
		}

		// call convolve
		double beginTime = System.currentTimeMillis();
		showProgress(0);
		r = JavaCall.convolve(r, k);
		showProgress(33);
		g = JavaCall.convolve(g, k);
		showProgress(66);
		b = JavaCall.convolve(b, k);
		showProgress(100);
		double endTime = System.currentTimeMillis();
		double totalTime = (endTime - beginTime) / 1.e3;
		showTime(totalTime);
		convolveTime = totalTime;

		ImageData dest = short2Image(r, g, b);
		return dest;
	}

	/**
	 * Run the action
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public void run(IAction action) {
		view.imagePanel.imageCanvas.backupUndoData();
	}

	/**
	 * Get the image canvas
	 * 
	 * @return ImageProcessCanvas a canvas for showing image
	 */
	public ImageProcessCanvas getCanvas() {
		return view.imagePanel.imageCanvas;
	}

	/**
	 * Get the image panel
	 * 
	 * @return ImagePanel a composite who wrap up the canvas and status bar
	 */
	public ImagePanel getPanel() {
		return view.imagePanel;
	}

	/**
	 * Get the image data for processing
	 * 
	 * @return ImageData image data for processing
	 */
	public ImageData getImageData() {
		// Here we assume source image is already is 32 bit depth.
		if (getCanvas().getSourceImage() == null)
			return null;
		ImageData imageData = getCanvas().getSourceImage().getImageData();
		if (imageData == null)
			return null;

		ImageData source = null;
		TGShape shape = getCanvas().getSelectedShape();
		if (shape == null || (!shape.isCompletedShape())) {
			source = imageData;
		} else {
			if (shape instanceof TGRectangle)
				source = SWT2dUtil.getSubImage(imageData, (TGRectangle) shape);
		}
		return source;
	}
}
