/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/

package info.textgrid.lab.linkeditor.algorithmdelegate;

import org.eclipse.jface.action.IAction;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.RGB;

/**
 * Threshold method.
 * <p>
 * 
 * @author Chengdong Li
 */
public class ThresholdDelegate extends AbstractImageDelegate {
	/**
	 * Run threshold method.
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public void run(IAction action) {
		super.run(action);
		ImageData source = getImageData();
		if (source == null)
			return;

		ThresholdDlg dlg = new ThresholdDlg(this);
		dlg.open();
	}

	/**
	 * Threshold transform
	 * 
	 * @param src
	 *            source image data
	 * @param t
	 *            threshold for three channels
	 * @return imagedata after processing
	 */
	public ImageData thresh3(ImageData src, short t[]) {
		short lut[][] = new short[t.length][256];
		for (int i = 0; i < lut.length; i++) {
			for (int j = 0; j < lut[i].length; j++) {
				if (j <= t[i])
					lut[i][j] = 0;
				else
					lut[i][j] = 255;
			}
		}
		long startTime = System.currentTimeMillis();
		ImageData bi = applyLut(src, lut);
		long endTime = System.currentTimeMillis();
		showTime(endTime - startTime);
		return bi;

	}

	/**
	 * 
	 * @param src
	 *            image source
	 * @param lut
	 *            [3][256] dimention array for lookup table
	 * @return image after lookup
	 */
	private static ImageData applyLut(ImageData src, short lut[][]) {
		int width = src.width;
		int height = src.height;

		short r[][] = new short[width][height];
		short g[][] = new short[width][height];
		short b[][] = new short[width][height];

		// preparing data.
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				int pixel = src.getPixel(i, j);
				RGB rgb = src.palette.getRGB(pixel);
				r[i][j] = (short) rgb.red;
				g[i][j] = (short) rgb.green;
				b[i][j] = (short) rgb.blue;
			}
		}

		// look up table
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				r[i][j] = lut[0][r[i][j]];
				g[i][j] = lut[1][g[i][j]];
				b[i][j] = lut[2][b[i][j]];
			}
		}
		ImageData dest = short2Image(r, g, b);

		return dest;
	}
}
