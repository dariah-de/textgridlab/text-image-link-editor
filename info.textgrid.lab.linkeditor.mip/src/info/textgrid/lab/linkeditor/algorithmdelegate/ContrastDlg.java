/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.algorithmdelegate;

import info.textgrid.lab.linkeditor.mip.component.canvas.SWT2dUtil;
import info.textgrid.lab.linkeditor.model.graphics.TGRectangle;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.ui.help.WorkbenchHelp;

/**
 * A dialog for setting the image processing parameters. Algorithm is
 * linear-map.
 * 
 * @author Chengdong Li
 */
public class ContrastDlg extends Dialog {
	private static final String[] LABEL = { "Contrast:", "Brightness:" };
	private double[] dValue = { 1, 0 };
	private short[] value = { 10, 100 };
	private final short[] maxVal = { 100, 200 };
	private final short[] minVal = { 0, 0 };
	private final int[] scale = { 10, 1 };
	private Label[] label = new Label[value.length];
	private Slider[] slider = new Slider[value.length];
	private Button okButton;
	private Button cancelButton;
	private BnCDelegate delegate;

	private Shell shell;

	/**
	 * Constructor for RenameDialog.
	 * 
	 * @param parentShell
	 */
	public ContrastDlg(BnCDelegate delegate) {
		super(delegate.getCanvas().getShell(), SWT.TITLE | SWT.RESIZE
				| SWT.BORDER | SWT.MODELESS);
		this.delegate = delegate;
		delegate.getCanvas().backupUndoData();
	}

	public Object open() {
		delegate.getCanvas().backupUndoData();

		Shell parent = getParent();

		if (shell == null) {
			shell = new Shell(parent, SWT.TITLE | SWT.RESIZE | SWT.BORDER
					| SWT.MODELESS);
		}
		shell.setText("Contrast dialog");
		createDialogArea(shell);
		shell.setSize(300, 200);
		shell.open();
		Display display = parent.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		shell = null;
		return new Object();
	}

	/**
	 * Method inherited from Dialog, which is used to create GUI.
	 */
	public Control createDialogArea(Composite parent) {
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		parent.setLayout(layout);
		parent.setLayoutData(new GridData(GridData.FILL_BOTH));

		Composite container = new Composite(parent, SWT.NULL);
		layout = new GridLayout();
		layout.marginHeight = 10;
		layout.marginWidth = 10;
		layout.numColumns = 2;
		container.setLayout(layout);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		Label title = new Label(container, SWT.NULL);
		title.setText("Please select level (low -> high): ");
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 2;
		title.setLayoutData(gd);

		for (int i = 0; i < value.length; i++) {
			label[i] = new Label(container, SWT.NULL);
			label[i].setText(LABEL[i]);
			gd = new GridData(GridData.VERTICAL_ALIGN_FILL);
			label[i].setLayoutData(gd);

			slider[i] = new Slider(container, SWT.BORDER);
			gd = new GridData(GridData.FILL_HORIZONTAL);
			gd.heightHint = 18;
			slider[i].setMaximum(maxVal[i]);
			slider[i].setMinimum(minVal[i]);
			slider[i].setIncrement(1);
			slider[i].setPageIncrement(10);
			slider[i].setThumb(1);
			slider[i].setSelection(value[i]);
			slider[i].setLayoutData(gd);
			slider[i].addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					// not process until release
					if (e.detail == SWT.DRAG)
						return;
					Slider sl;
					sl = (Slider) e.getSource();
					short val = (short) sl.getSelection();
					for (int i = 0; i < slider.length; i++) {
						if (sl == slider[i]) {
							value[i] = val;
						}
					}
					setDValue();
					onPreview();
				}
			});
		}
		slider[1].setIncrement(10);

		WorkbenchHelp.setHelp(container, "org.eclipse.update.ui.RenameDialog");
		Composite buttons = new Composite(parent, SWT.NULL);
		layout = new GridLayout();
		layout.numColumns = 2;
		buttons.setLayout(layout);
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		buttons.setLayoutData(gridData);

		createButtonsForButtonBar(buttons);
		return parent;
	}

	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons by default
		okButton = new Button(parent, SWT.NULL);
		okButton.setText("Accept");
		okButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent evt) {
				onOk();
			}
		});
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		okButton.setLayoutData(gridData);

		cancelButton = new Button(parent, SWT.NULL);
		cancelButton.setText("Cancel");
		cancelButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent evt) {
				onCancel();
			}
		});
		gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		cancelButton.setLayoutData(gridData);
	}

	private void onOk() {
		shell.dispose();
		ImageData data = delegate.getCanvas().getUndoData();
		TGShape shape = delegate.getCanvas().getSelectedShape();
		ImageData source = null;
		if (shape == null || (!shape.isCompletedShape())) {
			source = data;
		} else {
			if (shape instanceof TGRectangle)
				source = SWT2dUtil.getSubImage(data, (TGRectangle) shape);
		}
		if (source == null) {
			System.out.println("souce==null!");
			return;
		}
		delegate.getCanvas().reloadSelectImage(
				delegate.brighten(source, dValue[0], dValue[1]));
		delegate.getCanvas().redraw();
	}

	private void onCancel() {
		shell.dispose();
		delegate.getCanvas().restoreFromUndo();
		delegate.getCanvas().redraw();
	}

	private void onPreview() {
		if (delegate.getCanvas().getSourceImage() == null) {
			return;
		}
		delegate.getCanvas().setFocus();

		ImageData data = delegate.getCanvas().getUndoData();
		if (data == null)
			return;
		TGShape shape = delegate.getCanvas().getSelectedShape();
		if (shape == null || (!shape.isCompletedShape())) {
			delegate.getCanvas().setImageData(
					delegate.brighten(data, dValue[0], dValue[1]));
			delegate.getCanvas().redraw();
			return;
		}
		ImageData source = null;
		if (shape instanceof TGRectangle)
			source = SWT2dUtil.getSubImage(data, (TGRectangle) shape);

		delegate.getCanvas().reloadSelectImage(
				delegate.brighten(source, dValue[0], dValue[1]));
		delegate.getCanvas().redraw();
	}

	private void setDValue() {
		dValue[0] = (((double) (value[0]) - (double) (minVal[0])) / ((scale[0])));
		dValue[1] = (((value[1]) - ((double) (maxVal[1]) - (double) (minVal[1])) / 2) / ((scale[1])));
	}

}
