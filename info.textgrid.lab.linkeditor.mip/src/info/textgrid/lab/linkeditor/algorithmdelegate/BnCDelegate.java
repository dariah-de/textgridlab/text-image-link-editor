/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/

package info.textgrid.lab.linkeditor.algorithmdelegate;

import org.eclipse.jface.action.IAction;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.RGB;

/**
 * Linear map for Brighteness and contrast method.
 * 
 * @author Chengdong Li
 */
public class BnCDelegate extends AbstractImageDelegate {
	/**
	 * Run bright and contrast method.
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public void run(IAction action) {
		super.run(action);
		ImageData source = getImageData();
		if (source == null)
			return;

		ContrastDlg dlg = new ContrastDlg(this);
		dlg.open();
	}

	/**
	 * Improve the contrast and brightness using linearmap algorithm
	 * 
	 * @param src
	 *            source imagedata
	 * @param c
	 *            contrast coefficient
	 * @param b
	 *            brightness coefficient
	 * @return imagedata after processing
	 */
	public ImageData brighten(ImageData src, double c, double b) {
		long startTime = System.currentTimeMillis();

		// linear lookup table to speed up brighten
		int lut[] = new int[256];
		for (int i = 0; i < lut.length; i++) {
			double val = c * ((i)) + b;
			if (val > 255)
				val = 255;
			if (val < 0)
				val = 0;
			lut[i] = (int) val;
		}

		int h = src.height;
		int w = src.width;
		ImageData dest = new ImageData(w, h, 32, palette);
		for (int i = 0; i < w; i++) {
			for (int j = 0; j < h; j++) {
				int old = src.getPixel(i, j);
				RGB rgb = src.palette.getRGB(old);
				int red = lut[rgb.red];
				int green = lut[rgb.green];
				int blue = lut[rgb.blue];

				int pixel = (red << 8) | (green << 16) | (blue << 24);
				dest.setPixel(i, j, pixel);
			}
		}
		long endTime = System.currentTimeMillis();
		showTime(endTime - startTime);
		return dest;
	}
}
