/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/

package info.textgrid.lab.linkeditor.algorithmdelegate;

import org.eclipse.jface.action.IAction;
import org.eclipse.swt.graphics.ImageData;

/**
 * Edge detection method.
 * 
 * @author Chengdong Li
 */
public class EdgeDetectDelegate extends AbstractImageDelegate {
	/**
	 * Run edge detection method.
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public void run(IAction action) {
		super.run(action);
		ImageData source = getImageData();
		if (source == null)
			return;
		getCanvas().reloadSelectImage(edgeDetect(source));
	}

	/**
	 * Edge detection based on covolutioin
	 * 
	 * @param src
	 *            source image data
	 * @return image data created after processing
	 */
	public ImageData edgeDetect(ImageData src) {
		float[][] k = { { -1.0f, -1.0f, -1.0f }, { -1.0f, 8.0f, -1.0f },
				{ -1.0f, -1.0f, -1.0f } };
		long startTime = System.currentTimeMillis();
		ImageData bi = convolve(src, k);
		long endTime = System.currentTimeMillis();
		showTime(endTime - startTime);
		return bi;
	}

}
