/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/

package info.textgrid.lab.linkeditor.algorithmdelegate;

import org.eclipse.jface.action.IAction;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.RGB;

/**
 * Uniform Non Adaptive Histogram Equalization.
 * <p>
 * For the basic introduction and algorithm details , please see Douglas A.
 * Lyon's book:
 * <p>
 * Image Processing In Java --Prentice Hall PTR 1999 ISBN: 0-13-974577-7
 * <p>
 * 
 * @author Chengdong Li
 */
public class EqualizeDelegate extends AbstractImageDelegate {

	/**
	 * Run equalization method.
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public void run(IAction action) {
		super.run(action);
		ImageData source = getImageData();
		if (source == null)
			return;

		getCanvas().reloadSelectImage(unahe(source));
	}

	/**
	 * Uniform Non Adaptive Histogram Equalization
	 * 
	 * @param src
	 *            imageData to be equalized
	 * @return equlized image data (not share any data with src)
	 */
	public ImageData unahe(ImageData src) {
		long startTime = System.currentTimeMillis();

		int width = src.width;
		int height = src.height;
		short r[][] = new short[width][height];
		short g[][] = new short[width][height];
		short b[][] = new short[width][height];

		// preparing data.
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				int pixel = src.getPixel(i, j);
				RGB rgb = src.palette.getRGB(pixel);
				r[i][j] = (short) rgb.red;
				g[i][j] = (short) rgb.green;
				b[i][j] = (short) rgb.blue;
			}
		}

		// linear lookup table to speed up brighten
		int lut[] = new int[256];
		double h[] = getAverageCMF(r);
		for (int i = 0; i < 256; i++) {
			lut[i] = (int) (255 * h[i]);
		}

		ImageData dest = new ImageData(width, height, 32, palette);
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				int old = src.getPixel(i, j);
				RGB rgb = src.palette.getRGB(old);
				int red = lut[rgb.red];
				int green = lut[rgb.green];
				int blue = lut[rgb.blue];

				int pixel = (red << 8) | (green << 16) | (blue << 24);
				dest.setPixel(i, j, pixel);
			}
		}

		long endTime = System.currentTimeMillis();
		showTime(endTime - startTime);
		return dest;
	}

	/**
	 * Generate the average cumulative mass function
	 * 
	 * @param r
	 *            red plane
	 * @return average cumulative mass function of the three plane. (dim=1x256)
	 */
	private static double[] getAverageCMF(short[][] r) {
		double CMFr[] = computeCMF(r);
		double CMFg[] = computeCMF(r);
		double CMFb[] = computeCMF(r);

		double avg[] = new double[CMFr.length];

		for (int i = 0; i < CMFr.length; i++) {
			avg[i] = (CMFr[i] + CMFg[i] + CMFb[i]) / 3.0;
		}
		return avg;
	}

	/**
	 * Compute the cumulative mass function
	 * 
	 * @param plane
	 *            color plane (or channel)
	 * @return cumulative function (dim=1x256)
	 */
	private static double[] computeCMF(short plane[][]) {
		final int k = 256;
		double PMF[] = new double[k];
		double CMF[] = new double[k];

		int width = plane.length;
		int height = plane[0].length;

		int total = 0;

		// compute the probability mass function
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++) {
				PMF[plane[i][j] & 0xFF]++;
				total++;
			}

		// Normalize
		for (int i = 0; i < 256; i++)
			PMF[i] = (PMF[i] / total);

		// compute the cumulative PMF
		double cumulative = 0;
		for (int i = 0; i < PMF.length; i++) {
			cumulative = cumulative + PMF[i];
			CMF[i] = cumulative;
		}
		return CMF;
	}

}
