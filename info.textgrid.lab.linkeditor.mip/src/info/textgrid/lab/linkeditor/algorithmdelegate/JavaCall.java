/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.algorithmdelegate;

/**
 * All function in this class will be exported into the native call library.
 * <p>
 * 
 * @author Chengdong Li
 */
public class JavaCall {
	// public static double convolveTime=0;

	/**
	 * Return an MxN kernel using (9.1) in lyon's book.
	 * 
	 * @param M
	 *            kernel rows
	 * @param N
	 *            kernel columns
	 * @param sigma
	 *            average value
	 * @return kernel (size=M*N)
	 */
	public static float[][] getGaussKernel(int M, int N, double sigma) {
		float k[][] = new float[M][N];
		int xc = M / 2;
		int yc = N / 2;
		for (int x = 0; x < M; x++)
			for (int y = 0; y < N; y++) {
				// k[x][y] = (float) gauss(x, y, xc, yc, sigma);
				double dx = x - xc;
				double dy = y - yc;
				double dx2 = dx * dx;
				double dy2 = dy * dy;
				double sigma2 = sigma * sigma;
				double oneOnSigma2 = 1 / sigma2;
				k[x][y] = (float) (Math.exp(-(dx2 + dy2) * oneOnSigma2 / 2)
						/ Math.PI * oneOnSigma2 / 2);
			}

		double s = 0.0;
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				s += k[i][j];

		float sf = (float) (1 / s);
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < N; j++)
				k[i][j] = k[i][j] * sf;
		}
		return k;
	}

	/**
	 * Compute the convolution
	 * 
	 * @param f
	 *            image matrix
	 * @param k
	 *            kernel matrix
	 * @return image matrix after convolved.
	 */
	public static short[][] convolve(short f[][], float k[][]) {
		int uc = k.length / 2;
		int vc = k[0].length / 2;

		int width = f.length;
		int height = f[0].length;

		// Convolution, ignoring the edges
		// short h[][] = convolveNoEdge(f, k);
		short h[][] = new short[width][height];
		double sum = 0;

		long startTime = System.currentTimeMillis();

		for (int x = uc; x < width - uc; x++) {
			for (int y = vc; y < height - vc; y++) {
				sum = 0.0;
				for (int v = -vc; v <= vc; v++) {
					for (int u = -uc; u <= uc; u++) {
						sum += f[x - u][y - v] * k[u + uc][v + vc];
					}
				}
				if (sum < 0)
					sum = 0;
				if (sum > 255)
					sum = 255;
				h[x][y] = (short) sum;
			}
		}

		sum = 0;
		// convolve bottom
		for (int x = 0; x < width - 1; x++)
			for (int y = 0; y < vc; y++) {
				sum = 0.0;
				for (int v = -vc; v <= vc; v++) {
					for (int u = -uc; u <= uc; u++) {
						sum += f[cx(x - u, width)][cy(y - v, height)]
								* k[u + uc][v + vc];
					}
				}
				if (sum < 0)
					sum = 0;
				if (sum > 255)
					sum = 255;
				h[x][y] = (short) sum;
			}
		// convolve left
		for (int x = 0; x < uc; x++)
			for (int y = vc; y < height - vc; y++) {

				sum = 0.0;
				for (int v = -vc; v <= vc; v++)
					for (int u = -uc; u <= uc; u++)
						sum += f[cx(x - u, width)][y - v] * k[u + uc][v + vc];
				if (sum < 0)
					sum = 0;
				if (sum > 255)
					sum = 255;
				h[x][y] = (short) sum;
			}
		// convolve right
		for (int x = width - uc; x < width - 1; x++)
			for (int y = vc; y < height - vc; y++) {

				sum = 0.0;
				for (int v = -vc; v <= vc; v++)
					for (int u = -uc; u <= uc; u++)
						sum += f[cx(x - u, width)][y - v] * k[u + uc][v + vc];
				if (sum < 0)
					sum = 0;
				if (sum > 255)
					sum = 255;
				h[x][y] = (short) sum;
			}

		// convolve top
		for (int x = 0; x < width - 1; x++) {
			for (int y = height - vc; y < height - 1; y++) {
				sum = 0.0;
				for (int v = -vc; v <= vc; v++)
					for (int u = -uc; u <= uc; u++)
						sum += f[cx(x - u, width)][cy(y - v, height)]
								* k[u + uc][v + vc];
				if (sum < 0)
					sum = 0;
				if (sum > 255)
					sum = 255;
				h[x][y] = (short) sum;
			}
		}

		long endTime = System.currentTimeMillis();
		// convolveTime=((double)(endTime-startTime))/1000;

		AbstractImageDelegate.convolveTime = ((double) (endTime - startTime)) / 1000;

		return h;
	}

	private static int cx(int x, int width) {
		if (x > width - 1)
			return x - width + 1;
		if (x < 0)
			return width + x;
		return x;
	}

	private static int cy(int y, int height) {
		if (y > height - 1)
			return y - height + 1;
		if (y < 0)
			return height + y;
		return y;
	}

}
