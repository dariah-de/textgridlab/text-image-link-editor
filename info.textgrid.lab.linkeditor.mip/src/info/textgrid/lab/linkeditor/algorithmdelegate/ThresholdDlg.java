/*******************************************************************************
 * Copyright (c) 2002--2005 by Kevin Kiernan.  All rights reserved.
 *
 * The Development EPT software and its source code is made available with NO
 * WARRANTY by Kevin Kiernan, Jerzy Wl. Jaromczyk, and Alexander Dekhtyar
 * through the ARCHway Project, based on editing tools developed by the
 * Electronic Boethius Project.  This software is released under the terms of
 * the GNU General Public License (GPL), version 2.0.  A copy of this license
 * may be found in the file COPYING, or at:
 *   
 *   http://beowulf.engl.uky.edu/~ept/archway/source/license.txt
 *
 *******************************************************************************/
package info.textgrid.lab.linkeditor.algorithmdelegate;

import info.textgrid.lab.linkeditor.mip.component.canvas.SWT2dUtil;
import info.textgrid.lab.linkeditor.model.graphics.TGRectangle;
import info.textgrid.lab.linkeditor.model.graphics.TGShape;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Slider;

/**
 * A dialog for setting the image processing parameters for threshold filtering.
 * 
 * @author Chengdong Li
 */
public class ThresholdDlg extends Dialog {
	private static final String[] LABEL = { "threshold R", "threshold G",
			"threshold B" };
	private short[] value = { 0, 0, 0 };
	private Label[] tLabel = new Label[value.length];
	private Slider[] slider = new Slider[value.length];
	private Button okButton;
	private Button cancelButton;
	private ThresholdDelegate delegate;

	private Shell shell;

	/**
	 * Constructor.
	 * 
	 * @param ThresholdDelegate
	 *            : parent delegate
	 */
	public ThresholdDlg(ThresholdDelegate delegate) {
		super(delegate.getCanvas().getShell(), SWT.TITLE | SWT.RESIZE
				| SWT.BORDER | SWT.MODELESS);
		this.delegate = delegate;
	}

	/**
	 * Create dialog.
	 * 
	 * @return this dialog.
	 */
	public Object open() {
		delegate.getCanvas().backupUndoData();

		Shell parent = getParent();

		if (shell == null) {
			shell = new Shell(parent, SWT.TITLE | SWT.RESIZE | SWT.BORDER
					| SWT.MODELESS);
		}
		shell.setText("Threshold dialog");
		createDialogArea(shell);
		shell.setSize(300, 200);
		shell.open();
		Display display = parent.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		shell = null;
		return new Object();
	}

	/**
	 * Method inherited from Dialog. This will create and initalize GUI.
	 * 
	 * @param parent
	 *            Composite widget.
	 */
	public Control createDialogArea(Composite parent) {
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		parent.setLayout(layout);
		parent.setLayoutData(new GridData(GridData.FILL_BOTH));

		Composite container = new Composite(parent, SWT.NULL);
		layout = new GridLayout();
		layout.marginHeight = 10;
		layout.marginWidth = 10;
		layout.numColumns = 2;
		container.setLayout(layout);
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		Label title = new Label(container, SWT.NULL);
		title.setText("Color amount to show (255~0): ");
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 2;
		title.setLayoutData(gd);

		for (int i = 0; i < value.length; i++) {
			tLabel[i] = new Label(container, SWT.NULL);
			tLabel[i].setText(LABEL[i]);
			gd = new GridData(GridData.VERTICAL_ALIGN_FILL);
			tLabel[i].setLayoutData(gd);

			slider[i] = new Slider(container, SWT.BORDER);
			slider[i].setMaximum(256);
			slider[i].setIncrement(1);
			slider[i].setPageIncrement(1);
			slider[i].setThumb(1);
			slider[i].setSelection(value[i]);
			gd = new GridData(GridData.FILL_HORIZONTAL);
			gd.heightHint = 18;
			slider[i].setLayoutData(gd);
			slider[i].addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					// not process until release
					if (e.detail == SWT.DRAG)
						return;
					Slider sl;
					sl = (Slider) e.getSource();
					short threshold = (short) sl.getSelection();
					for (int i = 0; i < slider.length; i++) {
						if (sl == slider[i]) {
							value[i] = threshold;
						}
					}
					onPreview();
				}
			});
		}

		// WorkbenchHelp.setHelp(container,
		// "org.eclipse.update.ui.RenameDialog");
		Composite buttons = new Composite(parent, SWT.NULL);
		layout = new GridLayout();
		layout.numColumns = 2;
		buttons.setLayout(layout);
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		buttons.setLayoutData(gridData);

		createButtonsForButtonBar(buttons);
		return parent;
	}

	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons by default
		okButton = new Button(parent, SWT.NULL);
		okButton.setText("Accept");
		okButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent evt) {
				onOk();
			}
		});
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		okButton.setLayoutData(gridData);

		cancelButton = new Button(parent, SWT.NULL);
		cancelButton.setText("Cancel");
		cancelButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent evt) {
				onCancel();
			}
		});
		gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		cancelButton.setLayoutData(gridData);
	}

	private void onOk() {
		shell.dispose();
		ImageData data = delegate.getCanvas().getUndoData();
		TGShape shape = delegate.getCanvas().getSelectedShape();

		ImageData source = null;
		if (shape == null || (!shape.isCompletedShape())) {
			source = data;
		} else {
			if (shape instanceof TGRectangle)
				source = SWT2dUtil.getSubImage(data, (TGRectangle) shape);
		}
		if (source == null) {
			System.out.println("souce==null!");
			return;
		}
		delegate.getCanvas().reloadSelectImage(delegate.thresh3(source, value));
	}

	private void onCancel() {
		shell.dispose();
		delegate.getCanvas().restoreFromUndo();
		delegate.getCanvas().redraw();
	}

	private void onPreview() {
		if (delegate.getCanvas().getImageData() == null) {
			return;
		}
		delegate.getCanvas().setFocus();

		ImageData data = delegate.getCanvas().getUndoData();
		if (data == null)
			return;
		TGShape shape = delegate.getCanvas().getSelectedShape();
		if (shape == null || (!shape.isCompletedShape())) {
			// delegate.getCanvas().reloadImage(delegate.thresh3(data,value));
			delegate.getCanvas().setImageData(delegate.thresh3(data, value));
			delegate.getCanvas().redraw();
			return;
		}
		ImageData subSource = null;
		if (shape instanceof TGRectangle)
			subSource = SWT2dUtil.getSubImage(data, (TGRectangle) shape);

		delegate.getCanvas().reloadSelectImage(
				delegate.thresh3(subSource, value));
		delegate.getCanvas().redraw();
	}
}
