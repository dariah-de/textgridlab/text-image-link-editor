/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.tools;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import static info.textgrid.lab.linkeditor.tools.Activator.PLUGIN_ID;
import info.textgrid.lab.core.model.TGContentType;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProject;
import info.textgrid.lab.core.model.TextGridProjectRoot.LEVELS;
import info.textgrid.lab.linkeditor.tools.ImportExportUtils.TGObjectAnalyzer;
import info.textgrid.lab.ui.core.utils.ProjectCombo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class ImportImageLinkEditorFileCombinationDialog extends TitleAreaDialog {

	private LinksObjectEditorSection linksObjectEditor = null;
	private FileDialog dialog = null;

	private String linkEditorFileToImport;
	private TextGridProject linksObjectTarget, imagesTarget, xmlTarget;

	private boolean firstModifyEnabled = false;

	private boolean importAll = false;
	private boolean modify = false;

	private static final String META_SUFFIX = ".meta"; //$NON-NLS-1$

	private final Map<String, String> pathUriMap = Collections
			.synchronizedMap(new HashMap<String, String>());

	public ImportImageLinkEditorFileCombinationDialog(Shell parentShell) {
		super(parentShell);
		dialog = new FileDialog(parentShell);
		dialog.setText("Import"); //$NON-NLS-1$
		dialog.setFilterExtensions(new String[] { "*.tgl", "*.*" }); //$NON-NLS-1$ //$NON-NLS-2$
		dialog.setFilterNames(new String[] { "LinkEditor file (*.tgl)", //$NON-NLS-1$
				"All types (*.*)" }); //$NON-NLS-1$
	}

	public static void openDialog(ISelection selection) {
		ImportImageLinkEditorFileCombinationDialog dialog = new ImportImageLinkEditorFileCombinationDialog(
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());

		if (selection instanceof IStructuredSelection) {
			IStructuredSelection sel = (IStructuredSelection) selection;
			Object obj = sel.getFirstElement();
			if (obj != null && (obj instanceof TextGridProject)) {
				TextGridProject tgProject = (TextGridProject) obj;
				if (tgProject != null) {
					dialog.linksObjectTarget = tgProject;
				}
			}
		}

		dialog.open();
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite control = new Composite(parent, SWT.NONE);
		control.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		control.setLayout(new GridLayout(1, false));

		getShell().setText(Messages.ImportImageLinkEditorFileCombinationDialog_ImportILEFCombination);

		setTitle(Messages.ImportImageLinkEditorFileCombinationDialog_ImportILEFCombination);

		Group grp0 = new Group(control, SWT.None);
		grp0.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grp0.setLayout(new GridLayout(2, false));

		Label lbl0 = new Label(grp0, SWT.WRAP);
		lbl0.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		lbl0.setText(Messages.ImportImageLinkEditorFileCombinationDialog_SelectLocalImageLinkEditorFileToImport);

		new Label(grp0, SWT.None);

		final Text txt10 = new Text(grp0, SWT.BORDER);
		txt10.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		txt10.setEditable(false);
		txt10.setBackground(txt10.getDisplay().getSystemColor(SWT.COLOR_WHITE));

		Button btn10 = new Button(grp0, SWT.PUSH);
		btn10.setText(Messages.ImportImageLinkEditorFileCombinationDialog_Browse);

		btn10.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				String result = dialog.open();
				if (result != null) {
					linkEditorFileToImport = result;
					txt10.setText(linkEditorFileToImport);
					if (linksObjectTarget != null)
						setErrorMessage(null);
					// if (imagesTarget == null)
					// imagesTarget = linkEditorFileTarget;
					// if (xmlTarget == null)
					// xmlTarget = linkEditorFileTarget;
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		new Label(grp0, SWT.NONE).setText(Messages.ImportImageLinkEditorFileCombinationDialog_TargetProject);
		new Label(grp0, SWT.None);

		ProjectCombo projectViewer0 = new ProjectCombo(grp0, LEVELS.EDITOR);
		projectViewer0.setProject(this.linksObjectTarget);
		GridDataFactory.fillDefaults().grab(true, false)
				.applyTo(projectViewer0.getControl());
		projectViewer0
				.addSelectionChangedListener(new ISelectionChangedListener() {
					public void selectionChanged(SelectionChangedEvent event) {
						linksObjectTarget = (TextGridProject) ((IStructuredSelection) event
								.getSelection()).getFirstElement();
						if (linkEditorFileToImport != null
								&& !"".equals(linkEditorFileToImport)) //$NON-NLS-1$
							setErrorMessage(null);
					}
				});
		new Label(grp0, SWT.None);

		// ---------------------

		Group grp1 = new Group(control, SWT.None);
		grp1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grp1.setLayout(new GridLayout(1, false));

		final Button yesNoBtn1 = new Button(grp1, SWT.RADIO);
		yesNoBtn1
				.setText(Messages.ImportImageLinkEditorFileCombinationDialog_IWantImportTheHholeImage);

		final Group grp11 = new Group(grp1, SWT.None);
		grp11.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grp11.setLayout(new GridLayout(2, false));

		Label lbl11 = new Label(grp11, SWT.None);
		lbl11.setText(Messages.ImportImageLinkEditorFileCombinationDialog_TargetProjectsForImages);
		new Label(grp11, SWT.None);

		final ProjectCombo projectViewer1 = new ProjectCombo(grp11,
				LEVELS.EDITOR);
		projectViewer1.setProject(this.imagesTarget);
		GridDataFactory.fillDefaults().grab(true, false)
				.applyTo(projectViewer1.getControl());
		projectViewer1
				.addSelectionChangedListener(new ISelectionChangedListener() {
					public void selectionChanged(SelectionChangedEvent event) {
						imagesTarget = (TextGridProject) ((IStructuredSelection) event
								.getSelection()).getFirstElement();
					}
				});
		new Label(grp11, SWT.None);

		final Group grp12 = new Group(grp1, SWT.None);
		grp12.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grp12.setLayout(new GridLayout(2, false));

		Label lbl12 = new Label(grp12, SWT.None);
		lbl12.setText(Messages.ImportImageLinkEditorFileCombinationDialog_TargetProjectForXmlFiles);
		new Label(grp12, SWT.None);

		final ProjectCombo projectViewer2 = new ProjectCombo(grp12,
				LEVELS.EDITOR);
		projectViewer2.setProject(this.xmlTarget);
		GridDataFactory.fillDefaults().grab(true, false)
				.applyTo(projectViewer2.getControl());
		projectViewer2
				.addSelectionChangedListener(new ISelectionChangedListener() {
					public void selectionChanged(SelectionChangedEvent event) {
						xmlTarget = (TextGridProject) ((IStructuredSelection) event
								.getSelection()).getFirstElement();
					}
				});
		new Label(grp12, SWT.None);

		// per default disabled
		setEnabledForComposite(grp11, false);
		setEnabledForComposite(grp12, false);

		// ---------------------

		Group grp2 = new Group(control, SWT.None);
		grp2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grp2.setLayout(new GridLayout(1, false));

		final Button yesNoBtn2 = new Button(grp2, SWT.RADIO);
		yesNoBtn2
				.setText(Messages.ImportImageLinkEditorFileCombinationDialog_IWantToModifyTheReferences);

		linksObjectEditor = new LinksObjectEditorSection();
		linksObjectEditor.create(grp2);

		final Group secGrp = linksObjectEditor.getSectionGroup();

		// per default disabled
		setEnabledForComposite(secGrp, false);

		yesNoBtn1.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				if (linksObjectTarget == null || linkEditorFileToImport == null
						|| "".equals(linkEditorFileToImport)) { //$NON-NLS-1$
					setErrorMessage(Messages.ImportImageLinkEditorFileCombinationDialog_FirstYouHaveToSelectALocalImage);
					yesNoBtn1.setSelection(false);
					return;
				}
				importAll = yesNoBtn1.getSelection();

				if (importAll) {
					modify = false;
					yesNoBtn2.setSelection(false);
					setEnabledForComposite(secGrp, modify);
				}

				setEnabledForComposite(grp11, importAll);
				setEnabledForComposite(grp12, importAll);

				if (importAll) {
					if (imagesTarget == null)
						imagesTarget = linksObjectTarget;
					if (xmlTarget == null)
						xmlTarget = linksObjectTarget;

					projectViewer1.setProject(imagesTarget);
					projectViewer2.setProject(xmlTarget);
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		yesNoBtn2.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				if (linksObjectTarget == null || linkEditorFileToImport == null
						|| "".equals(linkEditorFileToImport)) { //$NON-NLS-1$
					setErrorMessage(Messages.ImportImageLinkEditorFileCombinationDialog_FirstYouHaveToSelectALocalImage);
					yesNoBtn2.setSelection(false);
					return;
				}
				modify = yesNoBtn2.getSelection();

				if (modify) {
					importAll = false;
					yesNoBtn1.setSelection(false);
					setEnabledForComposite(grp11, importAll);
					setEnabledForComposite(grp12, importAll);
				}

				setEnabledForComposite(secGrp, modify);
				if (!firstModifyEnabled) {
					try {
						linksObjectEditor.setSelectedFile(new FileInputStream(
								new File(linkEditorFileToImport)));
						firstModifyEnabled = true;
					} catch (FileNotFoundException e1) {
						Activator.handleError(e1, "File not found!"); //$NON-NLS-1$
					}
				}

			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		return control;
	}

	private void setEnabledForComposite(Composite comp, boolean enabled) {
		for (Control c : comp.getChildren()) {
			c.setEnabled(enabled);
			if (c instanceof Composite)
				setEnabledForComposite((Composite) c, enabled);

		}
	}

	@Override
	protected void okPressed() {

		if (linksObjectTarget == null || linkEditorFileToImport == null
				|| "".equals(linkEditorFileToImport)) { //$NON-NLS-1$
			setErrorMessage(Messages.ImportImageLinkEditorFileCombinationDialog_FirstYouHaveToSelectALocalImage);
			return;
		}

		if (importAll
				&& (imagesTarget == null || xmlTarget == null
						|| "".equals(imagesTarget) || "".equals(xmlTarget))) { //$NON-NLS-2$
			setErrorMessage(Messages.ImportImageLinkEditorFileCombinationDialog_FirstYouHaveToSelectATargetProject);
			return;
		}

		if (importAll) {
			pathUriMap.clear();

			Job importJob = new Job(Messages.ImportImageLinkEditorFileCombinationDialog_ImportingTheFiles) {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					try {
						TGObjectAnalyzer analyzer = ImportExportUtils
								.getObjectAnalyzer(new FileInputStream(
										new File(linkEditorFileToImport)));

						UIJob xml_j = null;
						UIJob images_j = null;

						final String[] xml_uris = analyzer.getTextUris();
						if (xml_uris.length > 0) {
							xml_j = new UIJob(Messages.ImportImageLinkEditorFileCombinationDialog_ImportingXmlFiles) {
								@Override
								public IStatus runInUIThread(
										IProgressMonitor monitor) {
									importFilesByURI(xml_uris, xmlTarget);
									return Status.OK_STATUS;
								}
							};
							xml_j.schedule();
						}

						final String[] image_uris = analyzer.getImageUris();
						if (image_uris.length > 0) {
							images_j = new UIJob("Importing images...") { //$NON-NLS-1$
								@Override
								public IStatus runInUIThread(
										IProgressMonitor monitor) {
									importFilesByURI(image_uris, imagesTarget);
									return Status.OK_STATUS;
								}
							};
							images_j.schedule();

						}

						try {
							if (xml_j != null)
								xml_j.join();
						} catch (InterruptedException e) {
						}
						try {
							if (images_j != null)
								images_j.join();
						} catch (InterruptedException e) {
						}

						updateSelectedLinksObjectAndImport();

						return Status.OK_STATUS;
					} catch (FileNotFoundException e) {
						Activator.handleError(e);
					}
					return Status.CANCEL_STATUS;
				}
			};
			importJob.setUser(true);
			importJob.schedule();

		} else if (modify) {
			modifySelectedLinksObjectManuallyAndImport();
		} else {
			importFile(linkEditorFileToImport, this.linksObjectTarget);
		}

		super.okPressed();
	}

	private void updateSelectedLinksObjectAndImport() {

		Job updatingTGOJob = new Job("Updating the importing...") { //$NON-NLS-1$
			@Override
			protected IStatus run(IProgressMonitor monitor) {

				if (monitor == null)
					monitor = new NullProgressMonitor();
				try {
					monitor.beginTask("Updating the file...", 100); //$NON-NLS-1$

					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					InputStream in = new FileInputStream(new File(
							linkEditorFileToImport));
					OMElement root = new StAXOMBuilder(in).getDocumentElement();
					root.serialize(baos);

					monitor.worked(20);

					String tree = baos.toString("UTF-8"); //$NON-NLS-1$

					String replaceWith = ""; //$NON-NLS-1$
					String[] uris = ImportExportUtils.getObjectAnalyzer(
							new FileInputStream(
									new File(linkEditorFileToImport)))
							.getAllUris();

					for (String uri : uris) {
						replaceWith = pathUriMap.get(uri);
						if (replaceWith != null && !"".equals(replaceWith)) //$NON-NLS-1$
							tree = tree.replace(uri, replaceWith);
					}

					pathUriMap.clear();

					final String data = tree;

					monitor.worked(20);

					new UIJob("Importing the file(s)...") { //$NON-NLS-1$
						@Override
						public IStatus runInUIThread(IProgressMonitor monitor) {
							try {
								importFile(linkEditorFileToImport,
										linksObjectTarget,
										data.getBytes("UTF-8")); //$NON-NLS-1$
							} catch (UnsupportedEncodingException e) {
								Activator.handleError(e);
								return Status.CANCEL_STATUS;
							}
							return Status.OK_STATUS;
						}
					}.schedule();

					monitor.worked(40);

					monitor.done();

					return Status.OK_STATUS;
				} catch (UnsupportedEncodingException e) {
					Activator.handleError(e);
				} catch (Exception e) {
					Activator.handleError(e);
				}
				return Status.CANCEL_STATUS;
			}
		};
		updatingTGOJob.setUser(true);
		updatingTGOJob.schedule();
	}

	private void modifySelectedLinksObjectManuallyAndImport() {

		TableItem[] selectedObjectItems = linksObjectEditor.getFirstTable()
				.getTable().getItems();
		TableItem[] replaceItems = linksObjectEditor.getSecondTable()
				.getTable().getItems();

		final int itemsCountFirstTable = selectedObjectItems.length;
		final int itemsCountSecondTable = replaceItems.length;

		final String[] selectedObjectItemsText = new String[itemsCountFirstTable];
		final String[] replaceItemsText = new String[itemsCountSecondTable];

		for (int i = 0; i < itemsCountFirstTable; ++i)
			selectedObjectItemsText[i] = selectedObjectItems[i].getText();

		for (int i = 0; i < itemsCountSecondTable; ++i)
			replaceItemsText[i] = replaceItems[i].getText();

		Job modifyingTGOJob = new Job("Modifying and importing...") { //$NON-NLS-1$
			@Override
			protected IStatus run(IProgressMonitor monitor) {

				if (monitor == null)
					monitor = new NullProgressMonitor();
				try {
					monitor.beginTask("Modifying the file...", 100); //$NON-NLS-1$

					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					InputStream in = new FileInputStream(new File(
							linkEditorFileToImport));
					OMElement root = new StAXOMBuilder(in).getDocumentElement();
					root.serialize(baos);

					monitor.worked(20);

					String tree = baos.toString("UTF-8"); //$NON-NLS-1$

					// boolean changed = false;

					String first = "", replaceWith = ""; //$NON-NLS-1$ //$NON-NLS-2$
					for (int i = 0; i < itemsCountFirstTable
							&& i < itemsCountSecondTable; ++i) {
						first = linksObjectEditor
								.getObjectUriFromTable1(selectedObjectItemsText[i]);
						replaceWith = linksObjectEditor
								.getObjectUriFromTable2(replaceItemsText[i]);

						tree = tree.replace(first, replaceWith);
						// changed = true;
					}

					final String data = tree;

					monitor.worked(20);

					new UIJob("Importing the file(s)...") { //$NON-NLS-1$
						@Override
						public IStatus runInUIThread(IProgressMonitor monitor) {
							try {
								importFile(linkEditorFileToImport,
										linksObjectTarget,
										data.getBytes("UTF-8")); //$NON-NLS-1$
							} catch (UnsupportedEncodingException e) {
								Activator.handleError(e);
								return Status.CANCEL_STATUS;
							}
							return Status.OK_STATUS;
						}
					}.schedule();

					monitor.worked(40);

					monitor.done();

					return Status.OK_STATUS;
				} catch (UnsupportedEncodingException e) {
					Activator.handleError(e);
				} catch (Exception e) {
					Activator.handleError(e);
				}
				return Status.CANCEL_STATUS;
			}
		};
		modifyingTGOJob.setUser(true);
		modifyingTGOJob.schedule();

	}

	private TextGridObject createTGObject(File file, TextGridProject project,
			boolean useMetaFiles) {
		// Try to determine the content type by file extension
		// ...
		String fileName = file.getName();
		int i = fileName.lastIndexOf('.');
		TGContentType contentType = null;
		if (i != -1)
			contentType = TGContentType.findByExtension(fileName
					.substring(i + 1));
		if (contentType == null)
			contentType = TGContentType.findMatchingContentType(fileName
					.substring(i + 1));
		if (contentType == null)
			contentType = TGContentType
					.getContentType(TGContentType.UNKNOWN_ID);

		TextGridObject object = TextGridObject.getNewObjectInstance(project,
				contentType);

		if (useMetaFiles) {
			File metaFile = new File(file.getParent(), fileName + META_SUFFIX);
			if (!metaFile.canRead())
				metaFile = new File(file.getParent(), "." + fileName //$NON-NLS-1$
						+ META_SUFFIX);

			if (metaFile.canRead()) {
				try {
					object.loadMetadata(new FileInputStream(metaFile));
				} catch (FileNotFoundException e) {
					Activator.handleError(e);
				} catch (CoreException e) {
					Activator.handleError(e);
				}
			} else {
				// --- Add valid default meta data ---
				// NEW MD-SCHEMA
				TextGridObject.addDefaultMetaDataXML(object,
						(i > 0) ? fileName.substring(0, i) : fileName);
				// END NEW MD-SCHEMA
			}
		}

		return object;
	}

	private boolean importFilesByURI(final String[] paths,
			final TextGridProject project) {

		IRunnableWithProgress importRunnable = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor)
					throws InvocationTargetException, InterruptedException {
				SubMonitor progress = SubMonitor.convert(monitor,
						paths.length * 100);

				List<IStatus> failures = new LinkedList<IStatus>();

				for (String path : paths) {

					if (!path.startsWith("file")) //$NON-NLS-1$
						continue;

					File localFile = null;
					try {
						localFile = new File(new URI(path));
					} catch (URISyntaxException e1) {
						failures.add(new Status(
								IStatus.WARNING,
								PLUGIN_ID,
								NLS.bind(
										"Skipped {0} due to uri syntax exception.", //$NON-NLS-1$
										localFile)));
					}

					if (progress.isCanceled())
						throw new InterruptedException();

					progress.subTask(NLS.bind("Importing {0} ...", localFile)); //$NON-NLS-1$

					TextGridObject object = createTGObject(localFile, project,
							true);

					if (object == null || !object.isSubmittable()) {
						failures.add(new Status(
								IStatus.WARNING,
								PLUGIN_ID,
								NLS.bind(
										"Skipped {0} due to incomplete metadata.", //$NON-NLS-1$
										localFile)));
					} else {
						IFile gridFile = getAdapter(object, IFile.class);
						try {
							gridFile.setContents(
									new FileInputStream(localFile),
									IResource.FORCE, progress.newChild(100));

							pathUriMap.put(path, object.getURI().toString());
						} catch (FileNotFoundException e) {
							failures.add(new Status(IStatus.ERROR, PLUGIN_ID,
									NLS.bind("Local file {0} was not found.", //$NON-NLS-1$
											localFile), e));
						} catch (CoreException e) {
							failures.add(new MultiStatus(
									PLUGIN_ID,
									0,
									new IStatus[] { e.getStatus() },
									NLS.bind("Failed to import {0}: {1}", //$NON-NLS-1$
											localFile, e.getLocalizedMessage()),
									e));
						}
					}
				}

				if (!failures.isEmpty()) {
					MultiStatus result = new MultiStatus(
							PLUGIN_ID,
							0,
							failures.toArray(new IStatus[0]),
							NLS.bind(
									"{0} of {1} files could not be imported. See the details for the individual reasons.", //$NON-NLS-1$
									failures.size(), paths.length), null);
					Activator.getDefault().getLog().log(result);
					throw new InvocationTargetException(new CoreException(
							result));
				}
			}

		};

		try {
			importRunnable.run(new NullProgressMonitor());
			return true;
		} catch (InvocationTargetException e) {
			// Something failed. e will always contain a CoreException with a
			// meaningful status.
			if (e.getCause() instanceof CoreException) {
				ErrorDialog.openError(getShell(),
						"Some files could not be imported.", null, //$NON-NLS-1$
						((CoreException) e.getCause()).getStatus());
				return true;
			} else
				Activator
						.handleError(
								e.getCause(),
								"An {0} occurred while importing files to the TextGridRep: {1}.\nThis comes somewhat unexpected, please file a bug report.", //$NON-NLS-1$
								e.getCause(), e.getLocalizedMessage());
		} catch (InterruptedException e) {
			// Canceled.
		}

		return false;
	}

	private boolean importFile(final String path, final TextGridProject project) {

		IRunnableWithProgress importRunnable = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor)
					throws InvocationTargetException, InterruptedException {
				SubMonitor progress = SubMonitor.convert(monitor, 100);

				List<IStatus> failures = new LinkedList<IStatus>();

				File localFile = new File(path);

				if (progress.isCanceled())
					throw new InterruptedException();

				progress.subTask(NLS.bind("Importing {0} ...", localFile)); //$NON-NLS-1$

				TextGridObject object = createTGObject(localFile, project, true);

				if (object == null || !object.isSubmittable()) {
					failures.add(new Status(IStatus.WARNING, PLUGIN_ID, NLS
							.bind("Skipped {0} due to incomplete metadata.", //$NON-NLS-1$
									localFile)));
				} else {
					IFile gridFile = getAdapter(object, IFile.class);
					try {
						gridFile.setContents(new FileInputStream(localFile),
								IResource.FORCE, progress.newChild(100));
					} catch (FileNotFoundException e) {
						failures.add(new Status(IStatus.ERROR, PLUGIN_ID, NLS
								.bind("Local file {0} was not found.", //$NON-NLS-1$
										localFile), e));
					} catch (CoreException e) {
						failures.add(new MultiStatus(PLUGIN_ID, 0,
								new IStatus[] { e.getStatus() }, NLS.bind(
										"Failed to import {0}: {1}", localFile, //$NON-NLS-1$
										e.getLocalizedMessage()), e));
					}
				}

				if (!failures.isEmpty()) {
					MultiStatus result = new MultiStatus(
							PLUGIN_ID,
							0,
							failures.toArray(new IStatus[0]),

							"file could not be imported. See the details for the individual reasons.", //$NON-NLS-1$
							null);
					Activator.getDefault().getLog().log(result);
					throw new InvocationTargetException(new CoreException(
							result));
				}
			}

		};

		try {
			importRunnable.run(new NullProgressMonitor());
			return true;
		} catch (InvocationTargetException e) {
			// Something failed. e will always contain a CoreException with a
			// meaningful status.
			if (e.getCause() instanceof CoreException) {
				ErrorDialog.openError(getShell(),
						"Some files could not be imported.", null, //$NON-NLS-1$
						((CoreException) e.getCause()).getStatus());
				return true;
			} else
				Activator
						.handleError(
								e.getCause(),
								"An {0} occurred while importing files to the TextGridRep: {1}.\nThis comes somewhat unexpected, please file a bug report.", //$NON-NLS-1$
								e.getCause(), e.getLocalizedMessage());
		} catch (InterruptedException e) {
			// Canceled.
		}

		return false;
	}

	private boolean importFile(final String path,
			final TextGridProject project, final byte[] data) {

		IRunnableWithProgress importRunnable = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor)
					throws InvocationTargetException, InterruptedException {
				SubMonitor progress = SubMonitor.convert(monitor, 100);

				List<IStatus> failures = new LinkedList<IStatus>();

				File localFile = new File(path);

				if (progress.isCanceled())
					throw new InterruptedException();

				progress.subTask(NLS.bind("Importing {0} ...", localFile)); //$NON-NLS-1$

				TextGridObject object = createTGObject(localFile, project, true);

				if (object == null || !object.isSubmittable()) {
					failures.add(new Status(IStatus.WARNING, PLUGIN_ID, NLS
							.bind("Skipped {0} due to incomplete metadata.", //$NON-NLS-1$
									localFile)));
				} else {
					IFile gridFile = getAdapter(object, IFile.class);
					try {
						gridFile.setContents(new ByteArrayInputStream(data),
								IResource.FORCE, progress.newChild(100));
					} catch (CoreException e) {
						failures.add(new MultiStatus(PLUGIN_ID, 0,
								new IStatus[] { e.getStatus() }, NLS.bind(
										"Failed to import {0}: {1}", localFile, //$NON-NLS-1$
										e.getLocalizedMessage()), e));
					}
				}

				if (!failures.isEmpty()) {
					MultiStatus result = new MultiStatus(
							PLUGIN_ID,
							0,
							failures.toArray(new IStatus[0]),

							"file could not be imported. See the details for the individual reasons.", //$NON-NLS-1$
							null);
					Activator.getDefault().getLog().log(result);
					throw new InvocationTargetException(new CoreException(
							result));
				}
			}

		};

		try {
			importRunnable.run(new NullProgressMonitor());
			return true;
		} catch (InvocationTargetException e) {
			// Something failed. e will always contain a CoreException with a
			// meaningful status.
			if (e.getCause() instanceof CoreException) {
				ErrorDialog.openError(getShell(),
						"Some files could not be imported.", null, //$NON-NLS-1$
						((CoreException) e.getCause()).getStatus());
				return true;
			} else
				Activator
						.handleError(
								e.getCause(),
								"An {0} occurred while importing files to the TextGridRep: {1}.\nThis comes somewhat unexpected, please file a bug report.", //$NON-NLS-1$
								e.getCause(), e.getLocalizedMessage());
		} catch (InterruptedException e) {
			// Canceled.
		}

		return false;
	}
}
