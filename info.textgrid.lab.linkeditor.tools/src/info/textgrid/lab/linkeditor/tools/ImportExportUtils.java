/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.tools;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.linkeditor.controller.TeiFactory.OMUtil;
import info.textgrid.lab.linkeditor.controller.TeiFactory.TEIDocFactory;

import java.io.InputStream;
import java.util.List;
import java.util.TreeSet;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.eclipse.core.resources.IFile;
import org.eclipse.osgi.util.NLS;
import org.jaxen.JaxenException;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class ImportExportUtils {

	public static TGObjectAnalyzer getObjectAnalyzer(TextGridObject tgObject) {
		return new TGObjectAnalyzer(tgObject);
	}

	public static TGObjectAnalyzer getObjectAnalyzer(InputStream inputStream) {
		return new TGObjectAnalyzer(inputStream);
	}

	public static class TGObjectAnalyzer {
		private OMElement root = null;

		public TGObjectAnalyzer(TextGridObject tgObj) {
			try {
				IFile file = (IFile) tgObj.getAdapter(IFile.class);
				InputStream inputStream = file.getContents(true);

				root = new StAXOMBuilder(inputStream).getDocumentElement();
				if (!root.getLocalName().equals("TEI")) {
					throw new RuntimeException("Error: Old style Text-Image-Link Object!");
				}
			} catch (Exception e) {
				Activator.handleError(e);
			}
		}

		public TGObjectAnalyzer(InputStream inputStream) {
			try {
				root = new StAXOMBuilder(inputStream).getDocumentElement();
				if (!root.getLocalName().equals("TEI")) {
					throw new RuntimeException("Error: Old style Text-Image-Link Object!");
				}
			} catch (Exception e) {
				Activator.handleError(e);
			}
		}

		public String[] getTextUris() {
			TreeSet<String> resources = new TreeSet<String>();
			try {
				List<OMElement> linkElems = OMUtil.getElementListWithName(
						"link", root, TEIDocFactory.TEI_NS_URI,
						TEIDocFactory.TEI_NS_Prefix);

				for (OMElement e : linkElems) {
					String textUri = getTextUriFromElement(e);
					resources.add(textUri);
				}

			} catch (JaxenException e) {
				Activator.handleError(e);
			} catch (Exception e) {
				Activator.handleError(e);
			}

			return resources.toArray(new String[] {});
		}

		public String[] getImageUris() {
			TreeSet<String> resources = new TreeSet<String>();
			try {
				List<OMElement> imageElements = OMUtil.getElementListWithName(
						"image", root, TEIDocFactory.SVG_NS_URI,
						TEIDocFactory.SVG_NS_Prefix);

				for (OMElement e : imageElements) {
					String imageUri = e.getAttributeValue(new QName(
							TEIDocFactory.XLINK_NS_URI, "href",
							TEIDocFactory.XLINK_NS_Prefix));
					resources.add(imageUri);
				}
			} catch (JaxenException e) {
				Activator.handleError(e);
			} catch (Exception e) {
				Activator.handleError(e);
			}

			return resources.toArray(new String[] {});
		}

		public String[] getAllUris() {
			TreeSet<String> resources = new TreeSet<String>();
			try {
				List<OMElement> imageElements = OMUtil.getElementListWithName(
						"image", root, TEIDocFactory.SVG_NS_URI,
						TEIDocFactory.SVG_NS_Prefix);

				for (OMElement e : imageElements) {
					String imageUri = e.getAttributeValue(new QName(
							TEIDocFactory.XLINK_NS_URI, "href",
							TEIDocFactory.XLINK_NS_Prefix));
					resources.add(imageUri);
				}

				List<OMElement> linkElems = OMUtil.getElementListWithName(
						"link", root, TEIDocFactory.TEI_NS_URI,
						TEIDocFactory.TEI_NS_Prefix);

				for (OMElement e : linkElems) {
					String textUri = getTextUriFromElement(e);
					resources.add(textUri);
				}

			} catch (JaxenException e) {
				Activator.handleError(e);
			} catch (Exception e) {
				Activator.handleError(e);
			}

			return resources.toArray(new String[] {});
		}

		private String getTextUriFromElement(OMElement linkElem) {
			String targets = linkElem.getAttributeValue(new QName("targets"));

			String[] values = targets.split("\\s");

			if (values.length < 2)
				throw new RuntimeException(NLS.bind(
						"The target attribute in tei:link have {0} values.",
						values.length));

			return values[1].split("#")[0];
		}

	}

}
