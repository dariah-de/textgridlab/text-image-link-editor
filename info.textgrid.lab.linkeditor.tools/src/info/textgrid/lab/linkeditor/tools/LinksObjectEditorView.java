/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.tools;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.UIJob;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class LinksObjectEditorView extends ViewPart implements
		ISelectionListener {

	public static final String ID = "info.textgrid.lab.linkeditor.controller.linksObjectEditorView"; //$NON-NLS-1$
	private TextGridObject tgObj = null;
	private Label urisDescLabel = null;
	private MessageBox msgBox = new MessageBox(PlatformUI.getWorkbench()
			.getDisplay().getActiveShell(), SWT.ICON_QUESTION | SWT.YES
			| SWT.NO);

	private LinksObjectEditorSection linksObjectEditor = null;

	@Override
	public void createPartControl(Composite parent) {

		getSite().getWorkbenchWindow().getSelectionService()
				.addPostSelectionListener(this);

		msgBox.setMessage(Messages.LinksObjectEditorView_TheSelectedTILObjectWillBeModified); // displayed
		// message
		msgBox.setText(Messages.LinksObjectEditorView_ModifiedTheSelectedTILObjects); // displayed
		// title

		parent.setLayout(new GridLayout(1, true));

		urisDescLabel = new Label(parent, SWT.WRAP);
		GridData projectDescData = new GridData(SWT.FILL, SWT.FILL, true, false);
		projectDescData.heightHint = 15;
		urisDescLabel.setLayoutData(projectDescData);

		linksObjectEditor = new LinksObjectEditorSection();
		linksObjectEditor.create(parent);

		Group buttonsGrp = linksObjectEditor.getButtonsGroup();

		Group saveGrp = new Group(buttonsGrp, SWT.None);
		saveGrp.setText(Messages.LinksObjectEditorView_Apply);

		saveGrp.setLayout(new GridLayout(1, true));
		saveGrp.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));

		final Button saveBtn = new Button(saveGrp, SWT.PUSH);
		saveBtn.setText(Messages.LinksObjectEditorView_ApplyChanges);
		saveBtn.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				if (tgObj != null) {
					if (msgBox.open() == SWT.YES) {
						makeChangesPersistentAndReload();
					}
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		IViewPart naviView = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage()
				.findView("info.textgrid.lab.navigator.view"); //$NON-NLS-1$
		if (naviView != null) {
			ISelection sel = naviView.getSite().getSelectionProvider()
					.getSelection();
			if (sel != null && sel instanceof IStructuredSelection) {
				Object obj = ((IStructuredSelection) sel).getFirstElement();
				if (obj != null) {
					TextGridObject tgO = AdapterUtils.getAdapter(obj,
							TextGridObject.class);
					if (tgO == null)
						return;
					tgObj = tgO;

					try {
						linksObjectEditor.setSelectedTGObject(tgObj);
						urisDescLabel
								.setText(Messages.LinksObjectEditorView_SelectedTILObject
										+ tgObj.getTitle() + " (" //$NON-NLS-1$
										+ tgObj.getURI().toString() + ")"); //$NON-NLS-1$
					} catch (Exception e) {
						Activator.handleError(e);
					}
				}
			}
		}
	}

	private void makeChangesPersistentAndReload() {

		TableItem[] selectedObjectItems = linksObjectEditor.getFirstTable()
				.getTable().getItems();
		TableItem[] replaceItems = linksObjectEditor.getSecondTable()
				.getTable().getItems();

		final int itemsCountFirstTable = selectedObjectItems.length;
		final int itemsCountSecondTable = replaceItems.length;

		final String[] selectedObjectItemsText = new String[itemsCountFirstTable];
		final String[] replaceItemsText = new String[itemsCountSecondTable];

		for (int i = 0; i < itemsCountFirstTable; ++i)
			selectedObjectItemsText[i] = selectedObjectItems[i].getText();

		for (int i = 0; i < itemsCountSecondTable; ++i)
			replaceItemsText[i] = replaceItems[i].getText();

		Job savingTGOJob = new Job("Saving the object...") { //$NON-NLS-1$
			@Override
			protected IStatus run(IProgressMonitor monitor) {

				if (monitor == null)
					monitor = new NullProgressMonitor();
				try {
					monitor.beginTask("Saving the object...", 100); //$NON-NLS-1$

					ByteArrayOutputStream baos = new ByteArrayOutputStream();

					IFile file = (IFile) tgObj.getAdapter(IFile.class);
					InputStream in = file.getContents(true);
					OMElement root = new StAXOMBuilder(in).getDocumentElement();
					root.serialize(baos);

					monitor.worked(20);

					String tree = baos.toString("UTF-8"); //$NON-NLS-1$

					boolean changed = false;

					String item1 = "", item2 = "", first = "", replaceWith = ""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					for (int i = 0; i < itemsCountFirstTable
							&& i < itemsCountSecondTable; ++i) {
						item1 = selectedObjectItemsText[i];
						item2 = replaceItemsText[i];

						if ((item1.endsWith(".") && !item2.endsWith(".")) //$NON-NLS-1$ //$NON-NLS-2$
								|| (!item1.endsWith(".") && item2.endsWith("."))) { //$NON-NLS-1$ //$NON-NLS-2$

							Activator
									.handleError(new RuntimeException(),
											"It's not allowed to replace objects of different types!"); //$NON-NLS-1$
							return Status.CANCEL_STATUS;
						}

						first = linksObjectEditor.getObjectUriFromTable1(item1);
						replaceWith = linksObjectEditor
								.getObjectUriFromTable2(item2);

						tree = tree.replace(first, replaceWith);
						changed = true;
					}

					monitor.worked(20);

					if (changed)
						file.setContents(
								new ByteArrayInputStream(tree.getBytes("UTF-8")), //$NON-NLS-1$
								IResource.FORCE, monitor);

					monitor.done();

					new UIJob("Reloading the URIs...") { //$NON-NLS-1$
						@Override
						public IStatus runInUIThread(IProgressMonitor monitor) {

							linksObjectEditor.setSelectedTGObject(tgObj);
							linksObjectEditor.clearAllItemsInSecondTable();

							return Status.OK_STATUS;
						}
					}.schedule();

					return Status.OK_STATUS;
				} catch (CoreException e) {
					Activator.handleError(e);
				} catch (UnsupportedEncodingException e) {
					Activator.handleError(e);
				} catch (Exception e) {
					Activator.handleError(e);
				}
				return Status.CANCEL_STATUS;
			}
		};

		savingTGOJob.schedule();

	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
	}

	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			Object obj = ((IStructuredSelection) selection).getFirstElement();
			if (obj != null) {
				TextGridObject tgO = AdapterUtils.getAdapter(obj,
						TextGridObject.class);

				// if the same TGObject
				try {
					if (tgO == null
							|| (tgObj != null && tgO == tgObj)
							|| (tgO != null && !"text/linkeditorlinkedfile" //$NON-NLS-1$
									.equals(tgO.getContentTypeID())))
						return;

					tgObj = tgO;

					linksObjectEditor.setSelectedTGObject(tgObj);

					urisDescLabel.setText("Selected Text-Image-Link Object: " //$NON-NLS-1$
							+ tgObj.getTitle() + " (" //$NON-NLS-1$
							+ tgObj.getURI().toString() + ")"); //$NON-NLS-1$
				} catch (CoreException e) {
					Activator.handleError(e);
				}

			}
		}
	}

	@Override
	public void dispose() {
		getSite().getWorkbenchWindow().getSelectionService()
				.removePostSelectionListener(this);
		super.dispose();
	}
}