/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.tools;

import info.textgrid._import.RewriteMethod;
import info.textgrid.lab.core.importexport.model.IExportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.IImportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.ISpecialImportEntrySupplier;
import info.textgrid.lab.core.importexport.model.ImportEntry;
import info.textgrid.lab.core.importexport.model.RewriteSetup;
import info.textgrid.lab.core.model.TGObjectReference;
import info.textgrid.lab.linkeditor.tools.ImportExportUtils.TGObjectAnalyzer;
import info.textgrid.utils.linkrewriter.ConfigurableXMLRewriter;
import info.textgrid.utils.linkrewriter.ImportMapping;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * Configures text image link files for import and export. This automatically
 * sets an appropriate rewrite method and adds all XML and Image files.
 * 
 * @see IImportEntryConfigurator
 * @see IExportEntryConfigurator
 */
public class LinkFileConfigurator implements IImportEntryConfigurator,
		IExportEntryConfigurator {

	public class FileURISupplier implements ISpecialImportEntrySupplier {

		private final List<URI> referredURIs;
		private final File linkFile;

		public FileURISupplier(final File file, final List<URI> fileURIs) {
			linkFile = file;
			referredURIs = fileURIs;
		}

		public InputStream getInputStream() throws IOException {
			final ImportMapping mapping = new ImportMapping();
			final URI linkFileURI = linkFile.toURI();
			for (final URI uri : referredURIs)
				mapping.add(uri.toString(), linkFileURI.relativize(uri)
						.toString(), null, RewriteMethod.NONE);
			final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(
					mapping, true);
			rewriter.configure(URI.create("internal:tei#tei"));
			rewriter.setBase(linkFile.getParentFile().toURI());
			final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			try {
				final FileInputStream input = new FileInputStream(linkFile);
				rewriter.rewrite(input, outputStream);
				outputStream.close();
				input.close();
				System.out.println(outputStream.toString("UTF-8"));
				return new ByteArrayInputStream(outputStream.toByteArray());
			} catch (final XMLStreamException e) {
				throw new IOException(e);
			}
		}

		public File getFile() {
			return linkFile;
		}

	}

	private static final RewriteSetup REWRITE_SETUP = RewriteSetup.of(
			RewriteMethod.XML, "internal:tei#tei");
	private static String CONTENT_TYPE_ID = "text/linkeditorlinkedfile";

	public void configureImport(final ImportEntry entry,
			final IProgressMonitor monitor) {
		final SubMonitor progress = SubMonitor.convert(monitor, 100);
		try {
			if (isLinkFile(entry)) {
				entry.setRewriteSetup(REWRITE_SETUP);
				progress.worked(10);
				final FileInputStream inputStream = new FileInputStream(
						entry.getLocalFile());
				progress.worked(10);
				if (progress.isCanceled())
					return;
				final TGObjectAnalyzer analyzer = ImportExportUtils
						.getObjectAnalyzer(inputStream);
				progress.worked(10);
				if (progress.isCanceled())
					return;
				final String[] allUris = analyzer.getAllUris();
				progress.worked(10);
				if (progress.isCanceled())
					return;
				final String parent = entry.getLocalFile().getParent();
				progress.setWorkRemaining(100 * allUris.length);

				final List<URI> fileURIs = new ArrayList<URI>();

				for (final String uri : allUris) {
					final URI parsedURI = URI.create(uri);
					if (parsedURI.isAbsolute()) {
						if ("file".equals(parsedURI.getScheme())) {
							entry.addFile(new File(parsedURI), monitor);
							fileURIs.add(parsedURI);
						} else {
							StatusManager
									.getManager()
									.handle(new Status(
											IStatus.WARNING,
											Activator.PLUGIN_ID,
											NLS.bind(
													"Cannot handle absolute URI {0} in link file {1}.",
													uri, entry.getLocalFile())));
						}
					} else
						entry.addFile(new File(parent, uri),
								progress.newChild(100));
				}

				if (!fileURIs.isEmpty()) {
					// need to rewrite file to relative URIs.
					entry.setSupplier(new FileURISupplier(entry.getLocalFile(),
							fileURIs));
				}
			}
		} catch (final CoreException e) {
			StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
		} catch (final FileNotFoundException e) {
			StatusManager
					.getManager()
					.handle(new Status(
							IStatus.ERROR,
							Activator.PLUGIN_ID,
							NLS.bind(
									"Could not open the link file {0}. This is probably a bug.",
									e)));
		}
	}

	private boolean isLinkFile(final ImportEntry entry) throws CoreException {
		return CONTENT_TYPE_ID.equals(entry.getObject().getContentTypeID());
	}

	public void configureExport(final ImportEntry entry,
			final IProgressMonitor monitor) {
		final SubMonitor progress = SubMonitor.convert(monitor, 100);
		try {
			if (isLinkFile(entry)) {
				entry.setRewriteSetup(REWRITE_SETUP);
				final TGObjectAnalyzer analyzer = ImportExportUtils
						.getObjectAnalyzer(entry.getObject());
				progress.worked(10);
				if (progress.isCanceled())
					return;
				final String[] allUris = analyzer.getAllUris();
				progress.worked(10);
				progress.setWorkRemaining(100 * allUris.length);
				if (progress.isCanceled())
					return;
				for (final String document : allUris) {
					URI uri;
					try {
						uri = new URI(document);
						final TGObjectReference reference = new TGObjectReference(
								uri);
						entry.addObject(reference, null, progress.newChild(100));
					} catch (final URISyntaxException e) {
						StatusManager
								.getManager()
								.handle(new Status(
										IStatus.WARNING,
										Activator.PLUGIN_ID,
										MessageFormat
												.format("The link file {0} references an invalid URI {1}, which was ignored during import configuration.",
														entry.getObject(),
														document), e));

					}
				}
			}
		} catch (final CoreException e) {
			StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
		}
	}
}