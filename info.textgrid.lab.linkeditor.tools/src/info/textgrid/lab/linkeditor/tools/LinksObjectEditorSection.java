/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.tools;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.linkeditor.controller.TeiFactory.OMUtil;
import info.textgrid.lab.linkeditor.controller.TeiFactory.TEIDocFactory;

import java.io.File;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URI;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;
import org.jaxen.JaxenException;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class LinksObjectEditorSection implements IProjectBrowserListener {

	private TableViewer firstTable = null;
	private TableViewer secondTable = null;

	private TableViewer lastSelectedViewer = null;

	private Map<String, String> nameUriMap1 = new HashMap<String, String>();
	private Map<String, String> nameUriMap2 = new HashMap<String, String>();

	private Group buttonsGrp = null;
	private Group urisGroup = null;

	private LinksObjectEditorSection getInstance() {
		return this;
	}

	public Composite create(Composite parent) {
		urisGroup = new Group(parent, SWT.SHADOW_ETCHED_IN | SWT.V_SCROLL
				| SWT.SCROLL_PAGE);
		urisGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		urisGroup.setLayout(new GridLayout(3, true));
		urisGroup.setText(""); //$NON-NLS-1$

		firstTable = new TableViewer(urisGroup, SWT.SINGLE | SWT.BORDER
				| SWT.V_SCROLL | SWT.H_SCROLL | SWT.VIRTUAL);

		GridData urisTreeData1 = new GridData(SWT.FILL, SWT.FILL, true, false);
		urisTreeData1.heightHint = 80;
		urisTreeData1.widthHint = 300;

		GridData urisTreeData2 = new GridData(SWT.FILL, SWT.FILL, true, false);
		urisTreeData2.heightHint = 80;
		urisTreeData2.widthHint = 300;

		firstTable.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				lastSelectedViewer = firstTable;
			}
		});

		Table table = firstTable.getTable();
		table.setLayoutData(urisTreeData1);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		TableColumn tblclmnFirst = new TableColumn(table, SWT.None);
		tblclmnFirst.setWidth(700);
		tblclmnFirst.setText(Messages.LinksObjectEditorSection_ReferencesInTheObject);

		secondTable = new TableViewer(urisGroup, SWT.SINGLE | SWT.BORDER
				| SWT.V_SCROLL | SWT.H_SCROLL | SWT.VIRTUAL);

		secondTable
				.addSelectionChangedListener(new ISelectionChangedListener() {
					public void selectionChanged(SelectionChangedEvent event) {
						lastSelectedViewer = secondTable;
					}
				});

		// set default viewer
		lastSelectedViewer = secondTable;

		Table table2 = secondTable.getTable();
		table2.setLayoutData(urisTreeData2);
		table2.setLinesVisible(true);
		table2.setHeaderVisible(true);

		TableColumn tblclmnFirst2 = new TableColumn(table2, SWT.None);
		tblclmnFirst2.setWidth(700);
		tblclmnFirst2.setText(Messages.LinksObjectEditorSection_ReplaceWith);

		firstTable.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				ISelection sel = event.getSelection();
				if (sel != null) {
					String text = sel.toString();
					text = text.substring(1, text.length() - 1);
				}
			}
		});

		buttonsGrp = new Group(urisGroup, SWT.FILL);
		buttonsGrp.setLayout(new GridLayout(1, true));
		buttonsGrp.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));

		Group addUriGrp = new Group(buttonsGrp, SWT.FILL);
		addUriGrp.setText(Messages.LinksObjectEditorSection_AddNewObjects);

		addUriGrp.setLayout(new GridLayout(2, true));
		addUriGrp.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));

		final Button getTGUriButton = new Button(addUriGrp, SWT.PUSH);
		getTGUriButton.setText(Messages.LinksObjectEditorSection_AddTGObjects);
		getTGUriButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				FetchTGObjectsDialog.openDialog(getInstance());
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		final Button getLocalUriButton = new Button(addUriGrp, SWT.PUSH);
		getLocalUriButton.setText(Messages.LinksObjectEditorSection_AddLocalFiles);

		getLocalUriButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				FileDialog fileDlg = new FileDialog(getLocalUriButton
						.getShell(), SWT.OPEN | SWT.MULTI);
				fileDlg.setText(Messages.LinksObjectEditorSection_SelectedFiles);
				fileDlg.setFilterExtensions(new String[] {
						"*.xml;*.jpg;*.jpeg;*.png;*.tiff;*.tif", "*.xml", "*.jpg", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						"*.jpeg", "*.png", "*.tiff", "*.tif" }); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				fileDlg.setFilterNames(new String[] { "All proper types", //$NON-NLS-1$
						"XML file (*.xml)", "JPG file (*.jpg)", //$NON-NLS-1$ //$NON-NLS-2$
						"JPEG file (*.jpeg)", "PNG file (*.png)", //$NON-NLS-1$ //$NON-NLS-2$
						"TIFF file (*.tiff)", "TIFF file (*.tif)" }); //$NON-NLS-1$ //$NON-NLS-2$

				fileDlg.open();
				String[] filePaths = fileDlg.getFileNames();

				if (filePaths == null)
					return;

				for (String filePath : filePaths) {
					File file = new File(filePath);

					if (file != null) {
						String uri = file.toURI().toString();

						FileNameMap fileNameMap = URLConnection
								.getFileNameMap();

						String mimeType = fileNameMap.getContentTypeFor(file
								.getPath());

						String suffix = ""; //$NON-NLS-1$

						if (mimeType == null || "".equals(mimeType)) //$NON-NLS-1$
							continue;
						else if (mimeType.contains("image")) //$NON-NLS-1$
							suffix = "."; //$NON-NLS-1$
						else if ("text/xml".equals(mimeType) //$NON-NLS-1$
								|| "application/xml".equals(mimeType)) //$NON-NLS-1$
							suffix = ""; //$NON-NLS-1$
						else
							continue;

						String name = getInfoFromUri(suffix, uri);

						nameUriMap2.put(name, uri);

						setSelectedTextInInput(name);
					}
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		Group removeUriGrp = new Group(buttonsGrp, SWT.None);
		removeUriGrp.setText(Messages.LinksObjectEditorSection_RemoveSelectedObjects);

		removeUriGrp.setLayout(new GridLayout(2, true));
		removeUriGrp
				.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));

		final Button removeButton = new Button(removeUriGrp, SWT.PUSH);
		removeButton.setText(Messages.LinksObjectEditorSection_RemoveSelectedObjects);
		removeButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				removeSelectedObject();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		Group moveUriGrp = new Group(buttonsGrp, SWT.None);
		moveUriGrp.setText(Messages.LinksObjectEditorSection_MoveSelectedObjects);

		moveUriGrp.setLayout(new GridLayout(2, true));
		moveUriGrp.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));

		Button up = new Button(moveUriGrp, SWT.PUSH);
		up.setText(Messages.LinksObjectEditorSection_Up);
		up.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				moveUp();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		Button down = new Button(moveUriGrp, SWT.PUSH);
		down.setText(Messages.LinksObjectEditorSection_Down);
		down.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				moveDown();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		return urisGroup;
	}

	public Group getSectionGroup() {
		return urisGroup;
	}

	public Group getButtonsGroup() {
		return buttonsGrp;
	}

	private void removeSelectedObject() {
		int index = lastSelectedViewer.getTable().getSelectionIndex();
		if (index >= 0) {
			lastSelectedViewer.getTable().remove(index);
			lastSelectedViewer.getTable().select(0);
		}
	}

	private void moveUp() {
		int index = lastSelectedViewer.getTable().getSelectionIndex();
		if (index >= 1) {
			TableItem item1 = lastSelectedViewer.getTable().getItem(index);
			TableItem item2 = lastSelectedViewer.getTable().getItem(index - 1);

			String tmp = item1.getText();
			item1.setText(item2.getText());
			item2.setText(tmp);

			Image tmpImage = item1.getImage();
			item1.setImage(item2.getImage());
			item2.setImage(tmpImage);

			lastSelectedViewer.getTable().setSelection(index - 1);
		}
	}

	private void moveDown() {
		int index = lastSelectedViewer.getTable().getSelectionIndex();
		if (index >= 0
				&& index < lastSelectedViewer.getTable().getItemCount() - 1) {
			TableItem item1 = lastSelectedViewer.getTable().getItem(index);
			TableItem item2 = lastSelectedViewer.getTable().getItem(index + 1);

			String tmp = item1.getText();
			item1.setText(item2.getText());
			item2.setText(tmp);

			Image tmpImage = item1.getImage();
			item1.setImage(item2.getImage());
			item2.setImage(tmpImage);

			lastSelectedViewer.getTable().setSelection(index + 1);
		}
	}

	public void clearAllItemsInFirstTable() {
		firstTable.setItemCount(0);
	}

	public void clearAllItemsInSecondTable() {
		secondTable.setItemCount(0);
	}

	public TableViewer getFirstTable() {
		return firstTable;
	}

	public TableViewer getSecondTable() {
		return secondTable;
	}

	public String getObjectUriFromTable1(String entry) {
		return nameUriMap1.get(entry);
	}

	public String getObjectUriFromTable2(String entry) {
		return nameUriMap2.get(entry);
	}

	public void addEntriesToFirstTable(String[] entries) {

		for (String s : entries) {
			TableItem item = new TableItem(firstTable.getTable(), SWT.NONE);
			item.setText(s);
			if (s.endsWith(".")) //$NON-NLS-1$
				setImageIconToItem(item);
			else
				setXMLIconToItem(item);
		}
	}

	public void addEntriesToSecondTable(String[] entries) {

		for (String s : entries) {
			TableItem item = new TableItem(secondTable.getTable(), SWT.NONE);
			item.setText(s);
			if (s.endsWith(".")) //$NON-NLS-1$
				setImageIconToItem(item);
			else
				setXMLIconToItem(item);
		}
	}

	public void setSelectedTGObjectsFromDialog(TextGridObject[] tgObjs) {
		try {
			for (TextGridObject tgObj : tgObjs) {
				String uri = tgObj.getURI().toString();

				String contentType = tgObj.getContentTypeID();

				String suffix = ""; //$NON-NLS-1$

				if (contentType == null || "".equals(contentType)) //$NON-NLS-1$
					continue;
				else if (contentType.contains("image")) //$NON-NLS-1$
					suffix = "."; //$NON-NLS-1$
				else if ("text/xml".equals(contentType)) //$NON-NLS-1$
					suffix = ""; //$NON-NLS-1$
				else
					continue;

				String name = getInfoFromUri(suffix, uri);

				nameUriMap2.put(name, uri);

				setSelectedTextInInput(name);
			}
		} catch (CoreException e) {
			Activator.handleError(e);
		}
	}

	private void setSelectedTextInInput(String entry) {

		TableItem item = new TableItem(secondTable.getTable(), SWT.NONE);
		item.setText(entry);
		if (entry.endsWith(".")) //$NON-NLS-1$
			setImageIconToItem(item);
		else
			setXMLIconToItem(item);
	}

	public void setSelectedTGObject(TextGridObject tgObj) {
		updateUrisViewer(tgObj);
	}

	public void setSelectedFile(InputStream inputStream) {
		if (inputStream != null) {
			clearAllItemsInFirstTable();
			String[] entries = getEntries(inputStream);
			addEntriesToFirstTable(entries);
		}
	}

	private void updateUrisViewer(final TextGridObject tgObj) {
		if (tgObj != null) {
			clearAllItemsInFirstTable();

			new Job("Fetching data from Grid...") { //$NON-NLS-1$

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					IFile file = (IFile) tgObj.getAdapter(IFile.class);
					try {
						InputStream inputStream = file.getContents(true);
						final String[] entries = getEntries(inputStream);
						new UIJob("") { //$NON-NLS-1$

							@Override
							public IStatus runInUIThread(
									IProgressMonitor monitor) {
								addEntriesToFirstTable(entries);
								return Status.OK_STATUS;
							}
						}.schedule();

					} catch (CoreException e) {
						Activator.handleError(e);
						return Status.CANCEL_STATUS;
					}

					return Status.OK_STATUS;
				}
			}.schedule();
		}
	}

	private void setXMLIconToItem(TableItem item) {
		item.setImage(new Image(PlatformUI.getWorkbench().getDisplay(),
				ImageDescriptor.createFromFile(this.getClass(),
						"/icons/xmldoc.gif").getImageData())); //$NON-NLS-1$
	}

	private void setImageIconToItem(TableItem item) {
		item.setImage(new Image(PlatformUI.getWorkbench().getDisplay(),
				ImageDescriptor.createFromFile(this.getClass(),
						"/icons/image_obj.gif").getImageData())); //$NON-NLS-1$
	}

	private String[] getEntries(InputStream inputStream) {
		TreeSet<String> resources = new TreeSet<String>();
		try {

			final OMElement root = new StAXOMBuilder(inputStream)
					.getDocumentElement();

			if (!root.getLocalName().equals("TEI")) { //$NON-NLS-1$
				return new String[] { "Error: Old style Text-Image-Link Object!" }; //$NON-NLS-1$
			}

			List<OMElement> imageElements = OMUtil.getElementListWithName(
					"image", root, TEIDocFactory.SVG_NS_URI, //$NON-NLS-1$
					TEIDocFactory.SVG_NS_Prefix);

			for (OMElement e : imageElements) {
				String imageUri = e.getAttributeValue(new QName(
						TEIDocFactory.XLINK_NS_URI, "href", //$NON-NLS-1$
						TEIDocFactory.XLINK_NS_Prefix));

				String entry = getInfoFromUri(".", imageUri); //$NON-NLS-1$
				resources.add(entry);
				nameUriMap1.put(entry, imageUri);

				// urisMap.put(imageUri, "image uri");
			}

			List<OMElement> linkElems = OMUtil
					.getElementListWithName("link", root, //$NON-NLS-1$
							TEIDocFactory.TEI_NS_URI,
							TEIDocFactory.TEI_NS_Prefix);

			for (OMElement e : linkElems) {
				String textUri = getTextUriFromElement(e);

				String entry = getInfoFromUri("", textUri); //$NON-NLS-1$
				if (entry != null) {
					resources.add(entry);
					nameUriMap1.put(entry, textUri);
				}
			}

			return resources.toArray(new String[] {});
		} catch (JaxenException e) {
			Activator.handleError(e);
		} catch (Exception e) {
			Activator.handleError(e);
		}

		return null;
	}

	private String getInfoFromUri(String suffix, String uri) {
		try {
			if (uri.startsWith("textgrid")) { //$NON-NLS-1$
				TextGridObject obj = TextGridObject.getInstance(new URI(uri),
						false);
				obj.reloadMetadata(false);
				return obj.getNameCandidate() + " (" + uri + ")" + suffix; //$NON-NLS-1$ //$NON-NLS-2$
			} else if (uri.startsWith("file")) { //$NON-NLS-1$
				return (new File(new URI(uri))).getName() + " (" + uri + ")" //$NON-NLS-1$ //$NON-NLS-2$
						+ suffix;
			}
		} catch (Exception e) {
			Activator.handleError(e, "Couldn't fetch the information of {0}", //$NON-NLS-1$
					uri);
		}

		return null;
	}

	private String getTextUriFromElement(OMElement linkElem) {

		String targets = linkElem.getAttributeValue(new QName("targets")); //$NON-NLS-1$

		String[] values = targets.split("\\s"); //$NON-NLS-1$

		if (values.length < 2)
			throw new RuntimeException(NLS.bind(
					"The target attribute in tei:link have {0} values.", //$NON-NLS-1$
					values.length));

		return values[1].split("#")[0]; //$NON-NLS-1$
	}
}