/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.tools;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.linkeditor.tools.ImportExportUtils.TGObjectAnalyzer;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.IProgressService;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 */
public class ExportImageLinkEditorFileCombinationDialog extends TitleAreaDialog {

	private TextGridObject selectedLinksObject = null;
	private LinksObjectEditorSection linksObjectEditor = null;
	private DirectoryDialog dialog = null;

	private String linksObjectTarget, imagesTarget, xmlTarget;

	private boolean firstModifyEnabled = false;

	private boolean exportAll = false;
	private boolean modify = false;

	private static Map<String, String> uriPathMap = Collections
			.synchronizedMap(new HashMap<String, String>());

	public ExportImageLinkEditorFileCombinationDialog(Shell parentShell) {
		super(parentShell);
		dialog = new DirectoryDialog(parentShell);
		dialog.setText(Messages.ExportImageLinkEditorFileCombinationDialog_Export);
		dialog.setMessage(Messages.ExportImageLinkEditorFileCombinationDialog_PleaseSelectADictionaryToExport);
	}

	public static void openDialog(TextGridObject object) {
		ExportImageLinkEditorFileCombinationDialog dialog = new ExportImageLinkEditorFileCombinationDialog(
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
		dialog.selectedLinksObject = object;
		dialog.open();
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite control = new Composite(parent, SWT.NONE);
		control.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		control.setLayout(new GridLayout(1, false));

		getShell().setText(Messages.ExportImageLinkEditorFileCombinationDialog_ExportILEOCombination);

		if (this.selectedLinksObject != null)
			try {
				setMessage(Messages.ExportImageLinkEditorFileCombinationDialog_SelectedTILObject
						+ selectedLinksObject.getTitle() + " (" //$NON-NLS-1$
						+ selectedLinksObject.getURI().toString() + ")"); //$NON-NLS-1$
			} catch (CoreException e) {
				Activator.handleError(e,
						"Couldn't get the title of the selected object!"); //$NON-NLS-1$
				return control;
			}

		setTitle("Export Image Link Editor Object Combination..."); //$NON-NLS-1$

		Group grp0 = new Group(control, SWT.None);
		grp0.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grp0.setLayout(new GridLayout(2, false));

		Label lbl0 = new Label(grp0, SWT.WRAP);
		lbl0.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		lbl0.setText(Messages.ExportImageLinkEditorFileCombinationDialog_SelectLocalDirectoryToExport);

		new Label(grp0, SWT.None);

		final Text txt10 = new Text(grp0, SWT.BORDER);
		txt10.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		txt10.setEditable(false);
		txt10.setBackground(txt10.getDisplay().getSystemColor(SWT.COLOR_WHITE));

		Button btn10 = new Button(grp0, SWT.PUSH);
		btn10.setText(Messages.ExportImageLinkEditorFileCombinationDialog_Browse);

		btn10.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				String result = dialog.open();
				if (result != null) {
					linksObjectTarget = result;
					txt10.setText(linksObjectTarget);
					setErrorMessage(null);
					// if (imagesTarget == null)
					// imagesTarget = linksObjectTarget;
					// if (xmlTarget == null)
					// xmlTarget = linksObjectTarget;
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		// ---------------------

		Group grp1 = new Group(control, SWT.None);
		grp1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grp1.setLayout(new GridLayout(1, false));

		// Label ask = new Label(grp1, SWT.WRAP);
		// ask.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		// ask
		// .setText("Do you want to export the whole Image Link Editor Object Combination (with all xml and image objects)?");

		final Button yesNoBtn1 = new Button(grp1, SWT.RADIO);
		yesNoBtn1
				.setText(Messages.ExportImageLinkEditorFileCombinationDialog_IWantToExportTheWholeImage);

		final Group grp11 = new Group(grp1, SWT.None);
		grp11.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grp11.setLayout(new GridLayout(2, false));

		Label lbl11 = new Label(grp11, SWT.None);
		lbl11.setText(Messages.ExportImageLinkEditorFileCombinationDialog_TargetDirectoryForImages);
		new Label(grp11, SWT.None);

		final Text txt11 = new Text(grp11, SWT.BORDER);
		txt11.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		txt11.setEditable(false);
		txt11.setBackground(txt11.getDisplay().getSystemColor(SWT.COLOR_WHITE));

		Button btn11 = new Button(grp11, SWT.PUSH);
		btn11.setText(Messages.ExportImageLinkEditorFileCombinationDialog_Browse);

		btn11.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				String result = dialog.open();
				if (result != null) {
					imagesTarget = result;
					txt11.setText(imagesTarget);
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		final Group grp12 = new Group(grp1, SWT.None);
		grp12.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grp12.setLayout(new GridLayout(2, false));

		Label lbl12 = new Label(grp12, SWT.None);
		lbl12.setText(Messages.ExportImageLinkEditorFileCombinationDialog_TargetDirectoryForXmlObjects);
		new Label(grp12, SWT.None);

		final Text txt12 = new Text(grp12, SWT.BORDER);
		txt12.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		txt12.setEditable(false);
		txt12.setBackground(txt12.getDisplay().getSystemColor(SWT.COLOR_WHITE));

		Button btn12 = new Button(grp12, SWT.PUSH);
		btn12.setText(Messages.ExportImageLinkEditorFileCombinationDialog_Browse);

		btn12.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				String result = dialog.open();
				if (result != null) {
					xmlTarget = result;
					txt12.setText(xmlTarget);
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});
		// per default disabled
		setEnabledForComposite(grp11, false);
		setEnabledForComposite(grp12, false);

		// for (Control c : grp11.getChildren())
		// c.setEnabled(yesNoBtn1.getSelection());
		// for (Control c : grp12.getChildren())
		// c.setEnabled(yesNoBtn1.getSelection());

		// ---------------------

		Group grp2 = new Group(control, SWT.None);
		grp2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		grp2.setLayout(new GridLayout(1, false));

		final Button yesNoBtn2 = new Button(grp2, SWT.RADIO);
		yesNoBtn2
				.setText(Messages.ExportImageLinkEditorFileCombinationDialog_IWantToModifyTheReferences);

		linksObjectEditor = new LinksObjectEditorSection();
		linksObjectEditor.create(grp2);

		final Group secGrp = linksObjectEditor.getSectionGroup();

		// per default disabled
		setEnabledForComposite(secGrp, false);
		// for (Control c : secGrp.getChildren())
		// c.setEnabled(yesNoBtn2.getSelection());

		yesNoBtn1.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				if (linksObjectTarget == null || "".equals(linksObjectTarget)) { //$NON-NLS-1$
					setErrorMessage("First you have to select a target directory to export the selected Image Link Editor Object!"); //$NON-NLS-1$
					yesNoBtn1.setSelection(false);
					return;
				}
				exportAll = yesNoBtn1.getSelection();

				if (exportAll) {
					modify = false;
					yesNoBtn2.setSelection(false);
					setEnabledForComposite(secGrp, modify);
				}

				setEnabledForComposite(grp11, exportAll);
				setEnabledForComposite(grp12, exportAll);

				if (exportAll) {
					if (imagesTarget == null)
						imagesTarget = linksObjectTarget;
					if (xmlTarget == null)
						xmlTarget = linksObjectTarget;

					txt11.setText(imagesTarget);
					txt12.setText(xmlTarget);
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		yesNoBtn2.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				if (linksObjectTarget == null || "".equals(linksObjectTarget)) { //$NON-NLS-1$
					setErrorMessage("First you have to select a target directory to export the selected Image Link Editor Object!"); //$NON-NLS-1$
					yesNoBtn2.setSelection(false);
					return;
				}
				modify = yesNoBtn2.getSelection();

				if (modify) {
					exportAll = false;
					yesNoBtn1.setSelection(false);
					setEnabledForComposite(grp11, exportAll);
					setEnabledForComposite(grp12, exportAll);
				}

				setEnabledForComposite(secGrp, modify);

				if (!firstModifyEnabled) {
					linksObjectEditor.setSelectedTGObject(selectedLinksObject);
					firstModifyEnabled = true;
				}

			}

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		return control;
	}

	private void setEnabledForComposite(Composite comp, boolean enabled) {
		for (Control c : comp.getChildren()) {
			c.setEnabled(enabled);
			if (c instanceof Composite)
				setEnabledForComposite((Composite) c, enabled);

		}
	}

	@Override
	protected void okPressed() {

		if (linksObjectTarget == null || "".equals(linksObjectTarget)) { //$NON-NLS-1$
			setErrorMessage("First you have to select a target directory to export the selected Image Link Editor Object!"); //$NON-NLS-1$
			return;
		}

		if (exportAll
				&& (imagesTarget == null || xmlTarget == null
						|| "".equals(imagesTarget) || "".equals(xmlTarget))) { //$NON-NLS-1$ //$NON-NLS-2$
			setErrorMessage("First you have to select target directories for saving the image/xml objects!"); //$NON-NLS-1$
			return;
		}

		if (exportAll) {
			uriPathMap.clear();

			Job exportJob = new Job("Exporting the objects...") { //$NON-NLS-1$

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					TGObjectAnalyzer analyzer = ImportExportUtils
							.getObjectAnalyzer(selectedLinksObject);

					UIJob xml_j = null;
					UIJob images_j = null;

					String[] uris = analyzer.getTextUris();
					if (uris.length > 0) {
						final ArrayList<TextGridObject> objects = new ArrayList<TextGridObject>();

						for (String uri : uris) {
							try {
								if (uri.startsWith("textgrid")) //$NON-NLS-1$
									objects.add(TextGridObject.getInstance(
											new URI(uri), true));
							} catch (CrudServiceException e) {
								Activator.handleError(e,
										"Couldn't get the TG-Object: " + uri); //$NON-NLS-1$
							} catch (URISyntaxException e) {
								Activator.handleError(e,
										"Couldn't get the TG-Object: " + uri); //$NON-NLS-1$
							}
						}

						xml_j = new UIJob("Exporting xml objects...") { //$NON-NLS-1$
							@Override
							public IStatus runInUIThread(
									IProgressMonitor monitor) {
								exportObjects(
										xmlTarget,
										objects.toArray(new TextGridObject[] {}));
								return Status.OK_STATUS;
							}
						};
						xml_j.schedule();
					}

					uris = analyzer.getImageUris();
					if (uris.length > 0) {
						final ArrayList<TextGridObject> objects = new ArrayList<TextGridObject>();

						for (String uri : uris) {
							try {
								if (uri.startsWith("textgrid")) //$NON-NLS-1$
									objects.add(TextGridObject.getInstance(
											new URI(uri), true));
							} catch (CrudServiceException e) {
								Activator.handleError(e,
										"Couldn't get the TG-Object: " + uri); //$NON-NLS-1$
							} catch (URISyntaxException e) {
								Activator.handleError(e,
										"Couldn't get the TG-Object: " + uri); //$NON-NLS-1$
							}
						}

						images_j = new UIJob("Exporting image objects...") { //$NON-NLS-1$
							@Override
							public IStatus runInUIThread(
									IProgressMonitor monitor) {
								exportObjects(
										imagesTarget,
										objects.toArray(new TextGridObject[] {}));
								return Status.OK_STATUS;
							}
						};
						images_j.schedule();
					}

					try {
						if (xml_j != null)
							xml_j.join();
					} catch (InterruptedException e) {
					}
					try {
						if (images_j != null)
							images_j.join();
					} catch (InterruptedException e) {
					}

					updateSelectedLinksObjectAndExport();

					return Status.OK_STATUS;
				}
			};
			exportJob.setUser(true);
			exportJob.schedule();

		} else if (modify) {
			modifySelectedLinksObjectManuallyAndExport();
		} else {
			exportObjects(this.linksObjectTarget,
					new TextGridObject[] { this.selectedLinksObject });
		}

		super.okPressed();
	}

	private void updateSelectedLinksObjectAndExport() {

		Job updatingTGOJob = new Job("Updating and exporting...") { //$NON-NLS-1$
			@Override
			protected IStatus run(IProgressMonitor monitor) {

				if (monitor == null)
					monitor = new NullProgressMonitor();
				try {
					monitor.beginTask("Updating the object...", 100); //$NON-NLS-1$

					ByteArrayOutputStream baos = new ByteArrayOutputStream();

					IFile file = (IFile) selectedLinksObject
							.getAdapter(IFile.class);
					InputStream in = file.getContents(true);
					OMElement root = new StAXOMBuilder(in).getDocumentElement();
					root.serialize(baos);

					monitor.worked(20);

					String tree = baos.toString("UTF-8"); //$NON-NLS-1$

					String replaceWith = ""; //$NON-NLS-1$

					String[] uris = ImportExportUtils.getObjectAnalyzer(
							selectedLinksObject).getAllUris();

					for (String uri : uris) {
						replaceWith = uriPathMap.get(uri);
						if (replaceWith != null && !"".equals(replaceWith)) //$NON-NLS-1$
							tree = tree.replace(uri, replaceWith);
					}

					uriPathMap.clear();

					final String data = tree;

					monitor.worked(20);

					new UIJob("Exporting the object(s)...") { //$NON-NLS-1$

						@Override
						public IStatus runInUIThread(IProgressMonitor monitor) {
							try {
								exportObjects(linksObjectTarget,
										selectedLinksObject,
										data.getBytes("UTF-8")); //$NON-NLS-1$
							} catch (UnsupportedEncodingException e) {
								Activator.handleError(e);
								return Status.CANCEL_STATUS;
							}
							return Status.OK_STATUS;
						}
					}.schedule();

					monitor.worked(40);
					// System.err.println(first + ";:" + second);
					// System.err.println(tree);

					monitor.done();

					return Status.OK_STATUS;
				} catch (CoreException e) {
					Activator.handleError(e);
				} catch (UnsupportedEncodingException e) {
					Activator.handleError(e);
				} catch (Exception e) {
					Activator.handleError(e);
				}
				return Status.CANCEL_STATUS;
			}
		};
		updatingTGOJob.setUser(true);
		updatingTGOJob.schedule();
	}

	private void modifySelectedLinksObjectManuallyAndExport() {

		TableItem[] selectedObjectItems = linksObjectEditor.getFirstTable()
				.getTable().getItems();
		TableItem[] replaceItems = linksObjectEditor.getSecondTable()
				.getTable().getItems();

		final int itemsCountFirstTable = selectedObjectItems.length;
		final int itemsCountSecondTable = replaceItems.length;

		final String[] selectedObjectItemsText = new String[itemsCountFirstTable];
		final String[] replaceItemsText = new String[itemsCountSecondTable];

		for (int i = 0; i < itemsCountFirstTable; ++i)
			selectedObjectItemsText[i] = selectedObjectItems[i].getText();

		for (int i = 0; i < itemsCountSecondTable; ++i)
			replaceItemsText[i] = replaceItems[i].getText();

		Job modifyingTGOJob = new Job("Modifying and exporting...") { //$NON-NLS-1$
			@Override
			protected IStatus run(IProgressMonitor monitor) {

				if (monitor == null)
					monitor = new NullProgressMonitor();
				try {
					monitor.beginTask("Modifying the object...", 100); //$NON-NLS-1$

					ByteArrayOutputStream baos = new ByteArrayOutputStream();

					IFile file = (IFile) selectedLinksObject
							.getAdapter(IFile.class);
					InputStream in = file.getContents(true);
					OMElement root = new StAXOMBuilder(in).getDocumentElement();
					root.serialize(baos);

					monitor.worked(20);

					String tree = baos.toString("UTF-8"); //$NON-NLS-1$

					// boolean changed = false;
					String first = "", replaceWith = ""; //$NON-NLS-1$ //$NON-NLS-2$

					for (int i = 0; i < itemsCountFirstTable
							&& i < itemsCountSecondTable; ++i) {
						first = linksObjectEditor
								.getObjectUriFromTable1(selectedObjectItemsText[i]);
						replaceWith = linksObjectEditor
								.getObjectUriFromTable2(replaceItemsText[i]);

						tree = tree.replace(first, replaceWith);
						// changed = true;
					}

					final String data = tree;

					monitor.worked(20);

					new UIJob("Exporting the object(s)...") { //$NON-NLS-1$

						@Override
						public IStatus runInUIThread(IProgressMonitor monitor) {
							try {
								exportObjects(linksObjectTarget,
										selectedLinksObject,
										data.getBytes("UTF-8")); //$NON-NLS-1$
							} catch (UnsupportedEncodingException e) {
								Activator.handleError(e);
								return Status.CANCEL_STATUS;
							}
							return Status.OK_STATUS;
						}
					}.schedule();

					monitor.worked(40);
					// System.err.println(first + ";:" + second);
					// System.err.println(tree);

					monitor.done();

					return Status.OK_STATUS;
				} catch (CoreException e) {
					Activator.handleError(e);
				} catch (UnsupportedEncodingException e) {
					Activator.handleError(e);
				} catch (Exception e) {
					Activator.handleError(e);
				}
				return Status.CANCEL_STATUS;
			}
		};
		modifyingTGOJob.setUser(true);
		modifyingTGOJob.schedule();
	}

	public static void exportObjects(String targetPath,
			final TextGridObject... textGridObjects) {

		if (targetPath == null)
			return; // cancelled

		final IProgressService progressService = PlatformUI.getWorkbench()
				.getProgressService();

		final ExportRunnable exportRunnable = new ExportRunnable(targetPath,
				textGridObjects);
		try {
			progressService.run(true, true, exportRunnable);
		} catch (final InvocationTargetException e) {
			if (e.getCause() instanceof CoreException)
				StatusManager.getManager().handle(
						((CoreException) e.getCause()).getStatus(),
						StatusManager.LOG | StatusManager.SHOW);
		} catch (final InterruptedException e) {
			Activator.handleWarning(e, "Export cancelled."); //$NON-NLS-1$
		}
		if (exportRunnable.status.getSeverity() < IStatus.WARNING)
			StatusManager.getManager().handle(
					new MultiStatus(Activator.PLUGIN_ID, 0,
							exportRunnable.status.getChildren(),
							"Export finished.", null)); //$NON-NLS-1$
	}

	public static void exportObjects(String targetPath,
			final TextGridObject textGridObject, byte[] data) {

		if (targetPath == null)
			return; // cancelled

		final IProgressService progressService = PlatformUI.getWorkbench()
				.getProgressService();

		final ExportRunnable exportRunnable = new ExportRunnable(targetPath,
				textGridObject, data);
		try {
			progressService.run(true, true, exportRunnable);
		} catch (final InvocationTargetException e) {
			if (e.getCause() instanceof CoreException)
				StatusManager.getManager().handle(
						((CoreException) e.getCause()).getStatus(),
						StatusManager.LOG | StatusManager.SHOW);
		} catch (final InterruptedException e) {
			Activator.handleWarning(e, "Export cancelled."); //$NON-NLS-1$
		}
		if (exportRunnable.status.getSeverity() < IStatus.WARNING)
			StatusManager.getManager().handle(
					new MultiStatus(Activator.PLUGIN_ID, 0,
							exportRunnable.status.getChildren(),
							"Export finished.", null)); //$NON-NLS-1$
	}

	private static class ExportRunnable implements IRunnableWithProgress {

		private String targetPath;
		private TextGridObject[] textGridObjects;
		private byte[] data = null;
		private MultiStatus status = new MultiStatus(
				Activator.PLUGIN_ID,
				0,
				"One or more errors occured while exporting files from the TextGridRep.", //$NON-NLS-1$
				null);

		public ExportRunnable(String targetPath,
				TextGridObject[] textGridObjects) {
			this.targetPath = targetPath;
			this.textGridObjects = textGridObjects;
		}

		public ExportRunnable(String targetPath, TextGridObject textGridObject,
				byte[] data) {
			this.targetPath = targetPath;
			this.textGridObjects = new TextGridObject[] { textGridObject };
			this.data = data;
		}

		public void run(IProgressMonitor monitor)
				throws InvocationTargetException, InterruptedException {
			SubMonitor progress = SubMonitor.convert(monitor,
					MessageFormat.format("Exporting to {0} ...", targetPath), //$NON-NLS-1$
					10 + 100 * textGridObjects.length);
			File targetDir = new File(targetPath);
			if (!targetDir.isDirectory())
				if (!targetDir.mkdirs())
					throw new InvocationTargetException(
							new IOException(
									MessageFormat
											.format("Failed to create the target directory {0}", //$NON-NLS-1$
													targetDir)));
			progress.worked(10);

			for (TextGridObject textGridObject : textGridObjects) {
				if (progress.isCanceled())
					throw new InterruptedException("Cancelled on user request."); //$NON-NLS-1$

				progress.subTask(MessageFormat.format("Exporting {0} ...", //$NON-NLS-1$
						textGridObject));

				IFile file = getAdapter(textGridObject, IFile.class);
				progress.worked(10);

				try {
					File targetFile = new File(targetDir,
							textGridObject.getURIBasedName());

					uriPathMap.put(textGridObject.getURI().toString(),
							targetFile.toURI().toString());

					progress.subTask(MessageFormat.format("Exporting {0} ...", //$NON-NLS-1$
							textGridObject)); // we know the title by now

					if (data != null) {
						FileOutputStream fOut = null;
						try {
							fOut = new FileOutputStream(targetFile);
							fOut.write(data);
							fOut.flush();
						} finally {
							if (fOut != null)
								fOut.close();
						}
					} else {
						IFileStore remoteStore = EFS.getStore(file
								.getLocationURI());
						IFileStore localStore = EFS.getLocalFileSystem()
								.fromLocalFile(targetFile);
						progress.worked(10);
						remoteStore.copy(localStore, EFS.OVERWRITE,
								progress.newChild(70));
					}

					if (progress.isCanceled())
						throw new InterruptedException(
								"Cancelled on user request"); //$NON-NLS-1$

					File metaFile = new File(targetDir, "." //$NON-NLS-1$
							+ textGridObject.getURIBasedName().concat(".meta")); //$NON-NLS-1$
					BufferedOutputStream metaStream = new BufferedOutputStream(
							new FileOutputStream(metaFile));
					textGridObject.getMetadataXML().serialize(metaStream);
					metaStream.close();
					progress.worked(10);

					status.add(new Status(IStatus.OK, Activator.PLUGIN_ID,
							MessageFormat.format(
									"Successfully exported {0} to {1}.", //$NON-NLS-1$
									textGridObject, targetFile)));

				} catch (CoreException e) {
					status.add(exportFailed(e, textGridObject));
				} catch (FileNotFoundException e) {
					status.add(exportFailed(e, textGridObject));
				} catch (IOException e) {
					status.add(exportFailed(e, textGridObject));
				} catch (Exception e) {
					status.add(exportFailed(e, textGridObject));
				}

			} // for loop over textGridObjects

			if (status.getSeverity() >= IStatus.WARNING)
				throw new InvocationTargetException(new CoreException(status));
		}

		private IStatus exportFailed(Throwable e, TextGridObject textGridObject) {
			if (e instanceof CoreException) {
				CoreException ce = (CoreException) e;
				return new MultiStatus(Activator.PLUGIN_ID, 0,
						new IStatus[] { ce.getStatus() }, MessageFormat.format(
								"Failed to export {0}.", textGridObject), e); //$NON-NLS-1$
			}
			return new Status(IStatus.ERROR, Activator.PLUGIN_ID,
					MessageFormat
							.format("Failed to export {0}", textGridObject), e); //$NON-NLS-1$
		}
	}

}
