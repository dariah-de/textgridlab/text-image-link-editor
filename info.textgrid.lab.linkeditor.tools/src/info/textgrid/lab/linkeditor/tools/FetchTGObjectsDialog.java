/**
 * This software is copyright (c) 2009 by
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - Worms University of Applied Sciences (http://www.fh-worms.de)
 *  - Technische Universität Darmstadt, Department of Linguistics 
 *    and Literary Studies (http://www.linglit.tu-darmstadt.de/)
 * This is free software. You can redistribute it
 * and/or modify it under the terms described in
 * the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download
 * it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright Worms University of Applied Sciences (http://www.fh-worms.de)
 * @copyright Technische Universität Darmstadt, Department of Linguistics 
 *            and Literary Studies (http://www.linglit.tu-darmstadt.de)
 *            
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 */
package info.textgrid.lab.linkeditor.tools;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.TextGridProjectRoot.LEVELS;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.utils.ProjectNavigator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

/**
 * 
 * @author Yahya Al-Hajj <alhajj@fh-worms.de>
 * 
 *         A dialog to select image or xml TG-objects
 * 
 */
public class FetchTGObjectsDialog extends TitleAreaDialog implements
		ISelectionChangedListener {

	private IProjectBrowserListener listener = null;
	private ProjectNavigator viewer = null;

	private List<TextGridObject> selectedObjects = new ArrayList<TextGridObject>();

	public FetchTGObjectsDialog(Shell parentShell) {
		super(parentShell);
	}

	public static void openDialog(IProjectBrowserListener section) {
		FetchTGObjectsDialog dialog = new FetchTGObjectsDialog(PlatformUI
				.getWorkbench().getActiveWorkbenchWindow().getShell());
		dialog.listener = section;
		dialog.open();
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite control = new Composite(parent, SWT.NONE);
		control.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		control.setLayout(new GridLayout(1, false));

		// getShell().setText("Select TG-Object");

		setTitle(Messages.FetchTGObjectsDialog_SelectTGObjects);

		Label intro = new Label(control, SWT.WRAP);
		intro.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		intro.setText(Messages.FetchTGObjectsDialog_PleaseSelectXmlAndImageObjects);

		createObjectsGroup(control);

		return control;
	}

	private void createObjectsGroup(Composite control) {
		Group objectsGroup = new Group(control, SWT.SHADOW_ETCHED_IN);
		objectsGroup
				.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		objectsGroup.setLayout(new GridLayout(1, true));

		// List list = new List(objectsGroup, SWT.SINGLE | SWT.V_SCROLL
		// | SWT.H_SCROLL | SWT.VIRTUAL);
		GridData projectTreeData = new GridData(SWT.FILL, SWT.FILL, true, true);
		projectTreeData.heightHint = 200;
		objectsGroup.setLayoutData(projectTreeData);

		viewer = new ProjectNavigator(objectsGroup, LEVELS.EDITOR, SWT.MULTI);
		viewer.getControl().setLayoutData(projectTreeData);
		viewer.setXmlImageFilter();
		viewer.addSelectionChangedListener(this);
	}

	public void selectionChanged(SelectionChangedEvent event) {

		if (event.getSelectionProvider() == viewer) {

			if (event.getSelection() instanceof IStructuredSelection) {
				selectedObjects.clear();
				for (Iterator iter = ((IStructuredSelection) event
						.getSelection()).iterator(); iter.hasNext();) {
					Object object = iter.next();
					if (object != null) {
						TextGridObject tgo = AdapterUtils.getAdapter(object,
								TextGridObject.class);
						try {
							if (tgo != null
									&& (tgo.getContentTypeID().equals(
											"text/xml") || tgo //$NON-NLS-1$
											.getContentTypeID().contains(
													"image/"))) { //$NON-NLS-1$
								selectedObjects.add(tgo);
								setErrorMessage(null);

								String message = ""; //$NON-NLS-1$
								for (TextGridObject t : selectedObjects)
									message += t.getNameCandidate()
											+ ((selectedObjects
													.get(selectedObjects.size() - 1)
													.equals(t)) ? "" : ", "); //$NON-NLS-1$ //$NON-NLS-2$

								setMessage(Messages.FetchTGObjectsDialog_SelectedObjects + message);
							} else {
								selectedObjects.clear();
								setErrorMessage(Messages.FetchTGObjectsDialog_PleaseSelectOnlyXmlAndImageObjects);
								return;
							}
						} catch (CoreException e) {
							Activator.handleError(e);
						}
					}
				}
			}
		}
	}

	@Override
	protected void okPressed() {
		if (selectedObjects != null) {
			listener.setSelectedTGObjectsFromDialog(selectedObjects
					.toArray(new TextGridObject[] {}));
			super.okPressed();
		}
	}

}
